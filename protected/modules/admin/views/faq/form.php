<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php if($faq['faq_id']) echo "Редагування запису #".$faq['faq_id']; else echo "Новий запис"; ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/faq" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab">Запитання</a></li>
                <li><a href="#status" data-toggle="tab">Статус</a></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade in active" id="main">
		    <div class="dataTable_wrapper">
			<input type="hidden" name="faq_id" value="<?php echo $faq['faq_id']; ?>">
	                <table class="table table-striped table-bordered table-hover">
	                    <tr>
	                        <td class="col-lg-3">Q</td>
	                        <td>
	                            <textarea
					 class="ckeditor"
	                                name="faq_question"
	                                rows="3"
	                            required><?php echo $faq->faq_question; ?></textarea>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="col-lg-3">A</td>
	                        <td>
	                            <textarea
					 class="ckeditor"
	                                name="faq_answer"
	                                rows="3"
	                            required><?php echo $faq->faq_answer; ?></textarea>
	                        </td>
	                    </tr>
	                </table>
		    </div> <!-- data wrapper -->
		</div> <!-- main tab -->

                <div class="tab-pane fade" id="status">
		    <div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover">
			    <tr>
				<td>
				    <input type="checkbox" id="faq_status" name="faq_status" <?php if($faq->faq_status) echo "checked"; ?>>
				    <label for="faq_status"> Дійсне (показувати це запитання)</label>
				</td>
			    </tr>
	                </table>
	            </div> <!-- data wrapper -->
		</div> <!-- status tab -->

	    </div> <!-- tab-content -->

            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти">
            </p>
        </form>
    </div>
</div>
<!-- /.row -->