<?php

class BodyModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'body';
    }

    public function primaryKey()
    {
        return 'body_id';
    }
}