jQuery(document).ready(function()
{
    $('#spare-boiler-link').on('click', function(){
        var a_text = $(this).text();

        if ('Показать' == a_text)
        {
            $(this).text('Скрыть');
            $('#spare-boiler-td').show();
        }
        else
        {
            $(this).text('Показать');
            $('#spare-boiler-td').hide();
        }
    });


    $('#data-table').DataTable({
	responsive: true,
	stateSave: true,
	"language": {
	    "url": "/assets/js/vendor/datatable-ua.json"
	}
    });


  $( function() {
    $( ".column" ).sortable({
      connectWith: ".column",
      handle: ".portlet-header",
      cancel: ".portlet-toggle",
      placeholder: "portlet-placeholder",
      update: function( event, ui ) {
	    order( event, ui );
	}
    });

    $( ".portlet" )
      .addClass( "ui-widget ui-widget-content ui-helper-clearfix" )
      .find( ".portlet-header" )
        .addClass( "my-widget-header" )
	.css("background","linear-gradient(to bottom, rgba(7,119,188,1) 0%,rgba(8,105,167,1) 100%);")
        .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

    $( ".portlet-toggle" ).on( "click", function() {
      var icon = $( this );
      icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
      icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
    });
  } );

    $("#dateFrom").datepicker({dateFormat:"dd.mm.yy"});
    $("#dateTo").datepicker({dateFormat:"dd.mm.yy"});

    check4changes();

});


function order( event, ui ) {
    var i=0;
    var tmp=$(".portlet");
    var str="";

    while( tmp[i] ) {
	str = str+" "+tmp[i].id;
	i++;
    } // while( tmp[i] )

    $("#faq_order_all").val( str );
}

function clearFilter() {
    $("#dateFrom").val( "" );
    $("#dateTo").val( "" );
    $("#phase").val( 0 );
    $("#user").val( 0 );
    $("#operation").val( 999999 );
    $("#number").val( "" );

    $("#logSubmit").click();
//    document.location = window.location.href.split('?')[0];
}

function editRole() {
    var role=$("#user_role").val();
    if( role ) {
	if(role & 1) {
	    $("#role_administrator").attr("checked", "true");
	} else {
	    $("#role_administrator").removeAttr( "checked" );
	}
	if(role & 2) {
	    $("#role_superuser").attr("checked", "true");
	} else {
	    $("#role_superuser").removeAttr( "checked" );
	}

	$("#role_dialog").dialog({
	    modal:true,
	    minWidth:400,
	    minHeight:200,
	    buttons: {
		"Так": function() {
		    saveRole();
		    $(this).dialog( "close" );
		},
		"Скасувати": function() {
		    $(this).dialog( "close" );
		}
	    }
	});
    } // if( role )
}

function saveRole() {
    var role=0;
    var role_str='';
    if( $("#role_administrator").prop("checked") ) {
	role += 1;
	role_str += 'Administrator';
    }
    if( $("#role_superuser").prop("checked") ) {
	role += 2;
	if(role_str != '') {
	    role_str += ',';
	}
	role_str += 'Super user';
    }
    $("#user_role").val( role );
    $("#roles_text").val( role_str );
}

function submitCSV( mode ) {

    var fd = new FormData($("#formCSV")[0]);

    // Check file size, if browser suports it
    if ( window.FileReader && window.File && window.FileList && window.Blob ) {
	var file = document.getElementById("targetCSV").files[0];
	if(file == undefined) {
	    alert( 'Пожалуйста, выберите файл' );
	} else {
	    if (file.size > 2097152) {
		alert('Не могу загрузить: файл больше 2M');
		return;
	    }
	}
    }

    $("#spinner").removeClass( "hidden" );
    $("#spinner").find("i").addClass( "fa-spin" );

    $.ajax({url: '/admin/upload/'+mode,
	type: 'POST',
	data: fd,
	success: function(data){
	    $("#CSV-content").html( data );
	    $("#spinner").find("i").removeClass( "fa-spin" );
	    $("#spinner").addClass( "hidden" );
	},
	cache: false,
	contentType: false,
	processData: false
    });

}


function formUploadCheckAll( type ) {
    $(".all-"+type).prop("checked", $("#set_all_"+type).prop("checked"));
}


function uploadPost(mode) {
    $.post('/admin/upload/save'+mode,
	    $("#formUploadData").serialize(),
	    function(data) {
		reply = JSON.parse( data );
		if(reply['err'] == undefined) {
		    if(reply['data'] != undefined) {
			var res = reply['data'];
			for(var i in res) {
			    if(res[i] == parseInt(res[i])) {
				$("#control"+i).html( '<a href="/admin/car/edit/'+res[i]+'"'+
							' target="_blank" id="buttonOk'+i+'"'+
							' class="btn btn-default text-center"'+
							' onclick="uploadMarkVisited('+i+')">Ok</a></td>' );
				$("#control"+i).removeClass( "message-error" );
			    } else {
				$("#control"+i).text( res[i] );
				$("#control"+i).addClass( "message-error" );
			    }
			}
			check4changes();
		    }
		} else {
		    $("#CSV-content").text( data );
		}
    });

}


function uploadMarkVisited( n ) {
    $("#buttonOk"+n).addClass( "visited" );
}


function cancelSubscription(id) {
    if( confirm('Ви впевнені, що необхідно скасувати цю підписку?') ) {
	$.post('/subscription/cancel', {'id':id});
	location.reload();
    }
}

function check4changes() {
    $.get('/subscription/check', function(data) {
	if(data > 0) {
	    $("#sendmail").removeClass( "hidden" );
	    $("#sendmail").addClass( "blink" );
	    $("#countChanges").val( data );
	}
    });
}

function sendMail() {
    if( confirm('Від останньої розсилки відбулося '+$("#countChanges").val()+' змін.\nЗробити розсилку зараз?') ) {

	$("#sendmail").removeClass( "blink" );
	$("#sendmail").addClass( "hidden" );
	$("#progressbar").progressbar({ value: 0 });
	$("#progressbar").removeClass( "hidden" );

	$.ajax({
	    method: 'GET',
	    url: '/subscription/sendmail',
	    dataType: 'text',
	    success: function() {
		$("#countChanges").val( 0 );
		$("#progressbar").addClass( "hidden" );
	    },
	    error: function() { },
	    progress: function(e) {
		//make sure we can compute the length
		if(e.lengthComputable) {
		    //calculate the percentage loaded
		    var pct = (e.loaded / e.total) * 100;

		    //log percentage loaded
		    //console.log(pct);
		    $("#progressbar").progressbar({ value: pct });
		} else {
		    //this usually happens when Content-Length isn't set
		    console.warn('Content Length not reported!');
		}
	    }
	});

    } // if( confirm( ... ) )

}


function changeAll(obj, type) {
    var check=$(obj).siblings(".all-"+type);
    if($(check).length == 0 || $(check).prop("checked") == false) {
	return;
    }
    var value = $(obj).val();
    $(".all-"+type+":checked").each(function() {
	var s = $(this).siblings(".edit-"+type);
	if(s != undefined) {
	    $(s).val( value );
	}
    });
}


function deleteUser(id, name) {
    if( confirm('Ви впевнені, що хочете видалити користувача \''+name+'\' назавжди?') ) {
	$.get('/admin/users/delete/'+id, function(data) { location.reload(); });
    }
}

