<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/slider" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" enctype="multipart/form-data">

            <div class="tab-content">
                <div class="tab-pane fade in active" id="main">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">Назва</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[slider_name]"
                                        type="text"
                                        value="<?php if (isset ($model)) {
                                            print $model['slider_name'];
                                        } ?>"
                                        />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Url</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[url]"
                                        type="text"
                                        value="<?php if (isset ($model)) {
                                            print $model['url'];
                                        } ?>"
                                        />
                                </td>
                            </tr>

                            <tr>
                                <td class="col-lg-3">Зображення</td>
                                <td>
                                    <?php if (isset($model['img']['image_url'])) { ?>
                                        <div class="col-lg-3">
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php print $model['img']['image_url']; ?>"/>
                                            </a>
                                        </div>
                                        <a href="?image=<?php print $model['img_id']; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    <?php } else { ?>
                                        <input type="file" name="image"/>
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти"/>
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->