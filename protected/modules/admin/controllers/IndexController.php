<?php

class IndexController extends AController
{
    public function actionIndex()
    {
        $ask        = AskModel::model()->countByAttributes(array('ask_read' => 0));
        $salon      = SalonModel::model()->countByAttributes(array('salon_read' => 0));
        $question   = QuestionModel::model()->countByAttributes(array('question_read' => 0));

        $this->render('index', array('ask' => $ask, 'salon' => $salon, 'question' => $question));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();

        $this->redirect('/admin/auth');
    }
}