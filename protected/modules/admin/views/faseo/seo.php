<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Загальна інформація</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="tab-content">

		    <div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover">
	                    <tr>
	                        <td class="col-lg-3">SEO title</td>
	                        <td>
	                            <input
	                                class="form-control"
	                                name="seo_title"
	                                type="text"
	                                value="<?php echo $faqseo->seo_title; ?>"
	                            >
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="col-lg-3">SEO description</td>
	                        <td>
	                            <textarea
	                                class="form-control"
	                                name="seo_description"><?php echo $faqseo->seo_description; ?></textarea>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="col-lg-3">SEO keywords</td>
	                        <td>
	                            <textarea
	                                class="form-control"
	                                name="seo_keywords"><?php echo $faqseo->seo_keywords; ?></textarea>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="col-lg-3">SEO text</td>
	                        <td>
	                            <textarea
	                                class="form-control"
	                                name="seo_text"><?php echo $faqseo->seo_text; ?></textarea>
	                        </td>
	                    </tr>
	                </table>
	            </div> <!-- data wrapper -->

	    </div> <!-- tab-content -->

            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти">
            </p>
        </form>
    </div>
</div>
<!-- /.row -->