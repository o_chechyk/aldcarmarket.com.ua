<?php

class CarImageModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'carimage';
    }

    public function primaryKey()
    {
        return 'carimage_id';
    }

    public function relations()
    {
        return array
        (
            'image' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'carimage_image_id')),
        );
    }
}