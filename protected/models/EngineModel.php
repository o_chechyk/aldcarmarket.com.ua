<?php

class EngineModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'engine';
    }

    public function primaryKey()
    {
        return 'engine_id';
    }
}