<?php

class LogController extends AController
{
    public function actionPrice() {

	$filter = "";
	if(isset($_POST['dateFrom']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateFrom'])) {
	    $dateFrom = $_POST['dateFrom'];
	    $filter .= "and l.dateOperation>=str_to_date('".$dateFrom." 00:00:00','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateFrom = "";
	}
	if(isset($_POST['dateTo']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateTo'])) {
	    $dateTo = $_POST['dateTo'];
	    $filter .= "and l.dateOperation<=str_to_date('".$dateTo." 23:59:59','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateTo = "";
	}
	if(isset($_POST['number']) and $_POST['number'] != "") {
	    $number = $_POST['number'];
	    $filter .= "and l.old_number='".str_replace("'", "\'", $number)."' ";
	} else if(isset($_GET['number']) and $_GET['number'] != "") {
	    $number = $_GET['number'];
	    $filter .= "and l.old_number='".str_replace("'", "\'", $number)."' ";
	} else {
	    $number = "";
	}


	$sql = "select ".
		"u.user_login user,".
		"date_format(l.dateOperation,'%d.%m.%Y %H:%i:%s') dt,".
		"l.old_number number,".
		"p.producer_name producer,".
		"m.model_name model,".
		"l.old_price,".
		"l.new_price ".
	    "from ".
		"logCar l ".
		"left join user u on u.user_id=l.user_id ".
		"left join producer p on p.producer_id=l.old_producer_id ".
		"left join model m on m.model_id=l.old_model_id ".
	    "where ".
		"l.operation=2 ". // Update
		"and l.old_price<>l.new_price ".
		$filter.
	    "order by ".
		"l.dateOperation";

	$log = Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('price', array('log' => $log, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'number' => $number));
    } // public function actionPrice()


    public function actionAll() {

	// Process filters, if set
	$filter = array();
	if(isset($_POST['dateFrom']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateFrom'])) {
	    $dateFrom = $_POST['dateFrom'];
	} else {
	    $dt = new DateTime('first day of this month');
	    $dateFrom = $dt->format('d.m.Y');
	}
	$filter[] = "l.dateOperation>=str_to_date('".$dateFrom." 00:00:00','%d.%m.%Y %H:%i:%s')";
	if(isset($_POST['dateTo']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateTo'])) {
	    $dateTo = $_POST['dateTo'];
	} else {
	    $dt = new DateTime();
	    $dateTo = $dt->format('d.m.Y');
	}
	$filter[] = "l.dateOperation<=str_to_date('".$dateTo." 23:59:59','%d.%m.%Y %H:%i:%s')";
	if(isset($_POST['user']) and preg_match("/^\d+$/", $_POST['user']) and $_POST['user'] != 0) {
	    $user_id = $_POST['user'];
	    $filter[] = "l.user_id=".$user_id;
	} else {
	    $user_id = 0;
	}
	if(isset($_POST['operation']) and preg_match("/^\d+$/", $_POST['operation']) and $_POST['operation'] != 999999) {
	    $operation_id = $_POST['operation'];
	    $filter[] = "l.operation=".$operation_id;
	} else {
	    $operation_id = 999999;
	}
	if(isset($_POST['number']) and $_POST['number'] != "") {
	    $number = $_POST['number'];
	    $filter[] = "(l.old_number='".str_replace("'", "\'", $number).
			"' or l.new_number='".str_replace("'", "\'", $number)."')";
	} else if(isset($_GET['number']) and $_GET['number'] != "") {
	    $number = $_GET['number'];
	    $filter[] = "(l.old_number='".str_replace("'", "\'", $number).
			"' or l.new_number='".str_replace("'", "\'", $number)."')";
	} else {
	    $number = "";
	}

	// Convert filter array to string
	if(count($filter) > 0) {
	    $filter_str = "where ".implode(" and ", $filter)." ";
	} else {
	    $filter_str = "";
	}

	// SQL for the main data loading
	$sql = "select ".
		"l.car_id,".
		"u.user_login user,".
		"date_format(l.dateOperation,'%d.%m.%Y %H:%i:%s') dt,".
		"l.operation,".
		"l.old_number,".
		"l.new_number,".
		"op.producer_name old_producer,".
		"np.producer_name new_producer,".
		"om.model_name old_model,".
		"nm.model_name new_model,".
		"ob.body_name old_body,".
		"nb.body_name new_body,".
		"oc.color_name old_color,".
		"nc.color_name new_color,".
		"l.old_comfort,". // not used
		"l.new_comfort, ". // not used
		"l.old_date,".
		"l.new_date, ".
		"l.old_description,". // not used
		"l.new_description, ". // not used
		"oe.engine_name old_engine,".
		"ne.engine_name new_engine,".
		"l.old_engine_capacity,".
		"l.new_engine_capacity,".
		"og.gear_name old_gear,".
		"ng.gear_name new_gear,".
		"l.old_mileage,".
		"l.new_mileage, ".
		"l.old_name,".
		"l.new_name, ".
		"if(oph.phase_name='','На продаж',oph.phase_name) old_phase,".
		"if(nph.phase_name='','На продаж',nph.phase_name) new_phase,".
		"l.old_power,". // not used
		"l.new_power, ". // not used
		"l.old_price,".
		"l.new_price,".
		"l.old_price_old,".
		"l.new_price_old,".
		"l.old_safety,". // not used
		"l.new_safety,". // not used
		"l.old_sku,".
		"l.new_sku,".
		"l.old_slide,".
		"l.new_slide,".
		"l.old_status,".
		"l.new_status,".
		"ot.transmission_name old_transmission,".
		"nt.transmission_name new_transmission,".
		"l.old_url,".
		"l.new_url,".
		"l.old_year,".
		"l.new_year ".
	    "from ".
		"logCar l ".
		"left join user u on u.user_id=l.user_id ".
		"left join producer op on op.producer_id=l.old_producer_id ".
		"left join producer np on np.producer_id=l.new_producer_id ".
		"left join model om on om.model_id=l.old_model_id ".
		"left join model nm on nm.model_id=l.new_model_id ".
		"left join body ob on ob.body_id=l.old_body_id ".
		"left join body nb on nb.body_id=l.new_body_id ".
		"left join color oc on oc.color_id=l.old_color_id ".
		"left join color nc on nc.color_id=l.new_color_id ".
		"left join engine oe on oe.engine_id=l.old_engine_id ".
		"left join engine ne on ne.engine_id=l.new_engine_id ".
		"left join gear og on og.gear_id=l.old_gear_id ".
		"left join gear ng on ng.gear_id=l.new_gear_id ".
		"left join phase oph on oph.phase_id=l.old_phase_id ".
		"left join phase nph on nph.phase_id=l.new_phase_id ".
		"left join transmission ot on ot.transmission_id=l.old_transmission_id ".
		"left join transmission nt on nt.transmission_id=l.new_transmission_id ".
	    $filter_str.
	    "order by ".
		"l.dateOperation";


	// Load the main data
	$log = Yii::app()->db->createCommand($sql)->queryAll();

	// Load user list for filters
	$users = UserModel::model()->findAll();
	array_unshift($users , array('user_id' => 0, 'user_login' => 'Всі'));
	// Operations list for filter
	$operations = array(array('id' => 999999, 'name' => "Всі"),
			    array('id' => 0, 'name' => "delete"),
			    array('id' => 1, 'name' => "insert"),
			    array('id' => 2, 'name' => "update")
		    );


        $this->render('all',
			array('log' => $log,
			    'dateFrom' => $dateFrom,
			    'dateTo' => $dateTo,
			    'users' => $users,
			    'user_id' => $user_id,
			    'operations' => $operations,
			    'operation_id' => $operation_id,
			    'number' => $number
			));
    } // public function actionIndex()


    public function actionPhase() {

	// Form the filter, if set
	$filter = "";
	if(isset($_POST['dateFrom']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateFrom'])) {
	    $dateFrom = $_POST['dateFrom'];
	    $filter .= "and l.dateOperation>=str_to_date('".$dateFrom." 00:00:00','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateFrom = "";
	}
	if(isset($_POST['dateTo']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateTo'])) {
	    $dateTo = $_POST['dateTo'];
	    $filter .= "and l.dateOperation<=str_to_date('".$dateTo." 23:59:59','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateTo = "";
	}

	if(isset($_POST['phase']) and is_numeric($_POST['phase']) and $_POST['phase'] != 0) {
	    $phase = $_POST['phase'];
	    $filter .= "and (l.old_phase_id=$phase or l.new_phase_id=$phase) ";
	} else {
	    $phase = 0;
	}
	if(isset($_POST['number']) and $_POST['number'] != "") {
	    $number = $_POST['number'];
	    $filter .= "and l.old_number='".str_replace("'", "\'", $number)."' ";
	} else if(isset($_GET['number']) and $_GET['number'] != "") {
	    $number = $_GET['number'];
	    $filter .= "and l.old_number='".str_replace("'", "\'", $number)."' ";
	} else {
	    $number = "";
	}

	// Count days for each phase
	// First, select all phases for the cars in filter
	$sql = "select ".
		"id,".
		"car_id,".
		"unix_timestamp(dateOperation) stamp,".
		"old_phase_id,".
		"new_phase_id ".
	    "from ".
		"logCar ".
	    "where ".
		"car_id in (select car_id from logCar l where 1 ".$filter.") ".
		"and operation in (1,2) ". // Create or Update
		"and old_phase_id<>new_phase_id ".
	    "order by ".
		"car_id,".
		"dateOperation";

	$log = Yii::app()->db->createCommand($sql)->queryAll();

	// Second, count the time spent in each phase
	$car_id = 0;
	$stages = array();
	foreach($log as $record) {
	    if($car_id != $record['car_id']) { // next car history
		$car_id = $record['car_id'];
		$mode = $record['new_phase_id'];
		$stamp = $record['stamp'];
		$stages[$record['id']] = "невідомо";
	    } else {
		if($record['old_phase_id'] == $mode ){
		    $tmp = $record['stamp'] - $stamp;
		    if($d=floor($tmp/86400)) { // days
			$stages[$record['id']] = $d."&nbsp;д";
		    } else if($h=floor($tmp/3600)) {
			$stages[$record['id']] = $h."&nbsp;год";
		    } else if($m=floor($tmp/60)) {
			$stages[$record['id']] = $m."&nbsp;хв";
		    } else {
			$stages[$record['id']] = $tmp."&nbsp;с";
		    }
		    $mode = $record['new_phase_id'];
		    $stamp = $record['stamp'];
		} else {
		    continue; // skip incorrect record
		}
	    }
	} // foreach($log as $record)
	// Days counting done. $stages array contains the results


	// Load the main data
	$sql = "select ".
		"l.id,".
		"u.user_login user,".
		"date_format(l.dateOperation,'%d.%m.%Y %H:%i:%s') dt,".
		"l.old_number number,".
		"p.producer_name producer,".
		"m.model_name model,".
		"if(op.phase_name='','На продаж',op.phase_name) old_phase,".
		"if(np.phase_name='','На продаж',np.phase_name) new_phase ".
	    "from ".
		"logCar l ".
		"left join user u on u.user_id=l.user_id ".
		"left join producer p on p.producer_id=l.old_producer_id ".
		"left join model m on m.model_id=l.old_model_id ".
		"left join phase op on op.phase_id=l.old_phase_id ".
		"left join phase np on np.phase_id=l.new_phase_id ".
	    "where ".
		"l.operation=2 ". // Update
		"and l.old_phase_id<>l.new_phase_id ".
		$filter.
	    "order by ".
		"l.dateOperation";

	$log = Yii::app()->db->createCommand($sql)->queryAll();


	// load phase names
	$phases  = PhaseModel::model()->findAll();
	foreach($phases as $index => $ph) {
	    if($ph['phase_name'] == "") {
		$phases[$index]['phase_name'] = "На продаж";
	    }
	}

	$this->render('phase', array('log' => $log,
					'dateFrom' => $dateFrom,
					'dateTo' => $dateTo,
					'phases' => $phases,
					'phase' => $phase,
					'stages' => $stages,
					'number' => $number));
    } // public function actionPhase()


    public function actionNew() {

	$filter = "";
	if(isset($_POST['dateFrom']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateFrom'])) {
	    $dateFrom = $_POST['dateFrom'];
	    $filter .= "and l.dateOperation>=str_to_date('".$dateFrom." 00:00:00','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateFrom = "";
	}
	if(isset($_POST['dateTo']) and preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['dateTo'])) {
	    $dateTo = $_POST['dateTo'];
	    $filter .= "and l.dateOperation<=str_to_date('".$dateTo." 23:59:59','%d.%m.%Y %H:%i:%s') ";
	} else {
	    $dateTo = "";
	}


	$sql = "select ".
		"u.user_login user,".
		"date_format(l.dateOperation,'%d.%m.%Y %H:%i:%s') dt,".
		"l.new_number number,".
		"p.producer_name producer,".
		"m.model_name model,".
		"l.new_price ".
	    "from ".
		"logCar l ".
		"left join user u on u.user_id=l.user_id ".
		"left join producer p on p.producer_id=l.new_producer_id ".
		"left join model m on m.model_id=l.new_model_id ".
	    "where ".
		"l.operation=1 ". // Insert
		$filter.
	    "order by ".
		"l.dateOperation";

	$log = Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('new', array('log' => $log, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo));
    } // public function actionPrice()


    public function actionSubscribers() {
	$subscribers = SubscriptionModel::model()->findAll();
	$this->render('subscribers', array('subscribers' => $subscribers));
    }

}
