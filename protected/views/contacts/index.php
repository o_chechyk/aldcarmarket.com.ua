<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span>Контакти</span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading">Контакти</h1>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="contacts-top clearfix">
            <div class="contacts-block">
                <div class="contacts-block-inner">
                    <div class="contacts-block-header">
                        <strong>Адреса</strong>
                        <i class="icon-pin"><img src="/assets/img/contacts-icon-pin.png" alt=""></i>
                    </div>
                    <div class="contacts-block-body">
                        <p><?php print $this->contacts['contacts_city']; ?></p>
                        <p><?php print $this->contacts['contacts_address']; ?></p>
                    </div>
                </div>
            </div>

            <div class="contacts-block">
                <div class="contacts-block-inner">
                    <div class="contacts-block-header">
                        <strong>Телефони</strong>
                        <i class="icon-phone"><img src="/assets/img/contacts-icon-phone.png" alt=""></i>
                    </div>
                    <div class="contacts-block-body">
                        <p><?php print $this->contacts['contacts_tel']; ?></p>
                        <p><?php print $this->contacts['contacts_fax']; ?></p>
                        <p><?php print $this->contacts['contacts_mob']; ?></p>
                    </div>
                </div>
            </div>

            <div class="contacts-block">
                <div class="contacts-block-inner">
                    <div class="contacts-block-header">
                        <strong>E-mail</strong>
                        <i class="icon-mail"><img src="/assets/img/contacts-icon-mail.png" alt=""></i>
                    </div>
                    <div class="contacts-block-body">
                        <p><a href="mailto:<?php print $this->contacts['contacts_email']; ?>"><?php print $this->contacts['contacts_email']; ?></a></p>
                    </div>
                </div>
            </div>

            <div class="contacts-block">
                <div class="contacts-block-inner">
                    <div class="contacts-block-header">
                        <strong>Графік роботи</strong>
                        <i class="icon-schedule"><img src="/assets/img/contacts-icon-schedule.png" alt=""></i>
                    </div>
                    <div class="contacts-block-body">
                        <table>
                            <tbody>
                                <tr>
                                    <td>ПН – ЧТ</td>
                                    <td><?php print $this->contacts['contacts_monday']; ?></td>
                                </tr>
                                <tr>
                                    <td>ПТ</td>
                                    <td><?php print $this->contacts['contacts_friday']; ?></td>
                                </tr>
                                <tr>
                                    <td>СБ – НД</td>
                                    <td><?php print $this->contacts['contacts_sunday']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-bottom clearfix">
            <div class="contacts-left">
                <div class="contacts-map">
                    <div id="contacts-map" data-lat="<?php print $this->contacts['contacts_lat']; ?>" data-lng="<?php print $this->contacts['contacts_lng']; ?>"></div>
                </div>
            </div>

            <div class="contacts-right">
                <div class="contacts-form-wrap">
                </div>
            </div>
        </div>
    </div>
</section>