<?php

class AskForm extends CFormModel {

    public $ask_email;
    public $ask_text;
    public $verifyCode;

    public function rules() {

        return array(
            array('ask_email, ask_text', 'required'),
	    array('verifyCode', 'required'),
            array('verifyCode', 'ext.yiiReCaptcha.ReCaptchaValidator'),
        );
    }
}
