<?php

class AdminModule extends CWebModule
{
    public $params = array();

    public function __construct()
    {

    }

    private $_assetsUrl;

    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('admin.assets'));
        return $this->_assetsUrl;
    }
}
