<?php

class LogModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'logCar';
    }

    public function relations() {

        return array
        (
	    'old_producer'  => array(self::HAS_ONE, 'ProducerModel', array('producer_id' => 'old_producer_id')),
	    'new_producer'  => array(self::HAS_ONE, 'ProducerModel', array('producer_id' => 'new_producer_id')),
	    'old_model'  => array(self::HAS_ONE, 'ModelModel', array('model_id' => 'old_model_id')),
	    'new_model'  => array(self::HAS_ONE, 'ModelModel', array('model_id' => 'new_model_id')),
	    'user'  => array(self::HAS_ONE, 'UserModel', array('user_id' => 'user_id')),
        );
    }
}
