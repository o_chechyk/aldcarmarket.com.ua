<?php

class NeseoController extends AController
{
    public function actionIndex()
    {
        $o_newseo   = SeoModel::model()->findByPk(1); // 1 - News SEO
        $h1         = 'Загальна інформація';

        $this->render('index', array('o_newseo' => $o_newseo, 'h1' => $h1));
    }

    public function actionEdit()
    {
        if (isset($_POST['data']))
        {
            $o_newseo = SeoModel::model()->findByPk(1);

            foreach ($_POST['data'] as $key => $value)
            {
                $o_newseo->$key = $value;
            }

            $o_newseo->save();

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/neseo');
    }
}