<?php

class SliderModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'slider';
    }

    public function primaryKey()
    {
        return 'slider_id';
    }

    public function relations()
    {
        return array
        (
            'img' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'img_id')),

        );
    }
}