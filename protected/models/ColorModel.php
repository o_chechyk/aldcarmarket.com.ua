<?php

class ColorModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'color';
    }

    public function primaryKey()
    {
        return 'color_id';
    }
}