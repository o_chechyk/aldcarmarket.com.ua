<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Повна історія змін</h1>
    </div>
</div>

<div class="logFilters">
    <div class="caption">
        Фільтри
    </div>
    <div class="body">
	<form action="/admin/log/all" method="POST">
	    <div class="section">
		<div class="caption">
		    Дата
		</div>
		<div class="body">
		    З
		    <input name="dateFrom" id="dateFrom" value="<?=$dateFrom?>" autocomplete="off">
		    По
		    <input name="dateTo" id="dateTo" value="<?=$dateTo?>" autocomplete="off">
		</div>
	    </div>
	    <div class="section">
		<div class="caption">
		    Службові
		</div>
		<div class="body">
		    Користувач
		    <select name="user" id="user">
			<?php foreach($users as $user) {
			    if($user['user_id'] == $user_id) {
				$selected = " selected";
			    } else {
				$selected = "";
			    }
			    echo "<option value=\"".$user['user_id']."\"".$selected.">".$user['user_login']."</option>";
			}?>
		    </select>
		    Операція
		    <select name="operation" id="operation">
			<?php foreach($operations as $operation) {
			    if($operation['id'] == $operation_id) {
				$selected = " selected";
			    } else {
				$selected = "";
			    }
			    echo "<option value=\"".$operation['id']."\"".$selected.">".$operation['name']."</option>";
			}?>
		    </select>
		</div>
	    </div>
	    <div class="section">
		<div class="caption">
		    Дані
		</div>
		<div class="body">
		    Номер кузова
		    <input name="number" id="number" value="<?=$number?>">
		</div>
	    </div>
	    <div class="footer">
		<input id="logSubmit" class="btn btn-default text-center" type="submit" value="Застосувати">
		<input type="button" class="btn btn-default text-center" onclick="clearFilter()" value="Скинути фільтр">
	    </div>
	</form>
    </div>
</div>
<div class="logReport">
    <?php foreach ($log as $record) { ?>

	<div class="row">
	    <div class="caption">
		Користувач: <span><?=$record['user']?></span>
		Дата: <span><?=$record['dt'];?></span>
		Операція: <span>
		    <?php
			switch($record['operation']) {
			    case 0:
				echo "Видалення";
				break;
			    case 1:
				echo "Новий запис";
				break;
			    case 2:
				echo "Редагування";
				break;
			}
		    ?>
		</span>
	    </div>

	    <div class="body">

		<?php
		    if($record['old_number'] != $record['new_number']) {
			$number_class = "changed";
		    } else {
			$number_class = "normal";
		    }
		    if($record['old_producer'] != $record['new_producer']) {
			$producer_class = "changed";
		    } else {
			$producer_class = "normal";
		    }
		    if($record['old_model'] != $record['new_model']) {
			$model_class = "changed";
		    } else {
			$model_class = "normal";
		    }
		    if($record['old_body'] != $record['new_body']) {
			$body_class = "changed";
		    } else {
			$body_class = "normal";
		    }
		    if($record['old_color'] != $record['new_color']) {
			$color_class = "changed";
		    } else {
			$color_class = "normal";
		    }
		    if($record['old_year'] != $record['new_year']) {
			$year_class = "changed";
		    } else {
			$year_class = "normal";
		    }
		    if($record['old_engine'] != $record['new_engine']) {
			$engine_class = "changed";
		    } else {
			$engine_class = "normal";
		    }
		    if($record['old_engine_capacity'] != $record['new_engine_capacity']) {
			$engine_capacity_class = "changed";
		    } else {
			$engine_capacity_class = "normal";
		    }
		    if($record['old_transmission'] != $record['new_transmission']) {
			$transmission_class = "changed";
		    } else {
			$transmission_class = "normal";
		    }
		    if($record['old_gear'] != $record['new_gear']) {
			$gear_class = "changed";
		    } else {
			$gear_class = "normal";
		    }
		    if($record['old_mileage'] != $record['new_mileage']) {
			$mileage_class = "changed";
		    } else {
			$mileage_class = "normal";
		    }
		    if($record['old_name'] != $record['new_name']) {
			$name_class = "changed";
		    } else {
			$name_class = "normal";
		    }
		    if($record['old_phase'] != $record['new_phase']) {
			$phase_class = "changed";
		    } else {
			$phase_class = "normal";
		    }
		    if($record['old_price'] != $record['new_price']) {
			$price_class = "changed";
		    } else {
			$price_class = "normal";
		    }
		    if($record['old_price_old'] != $record['new_price_old']) {
			$price_old_class = "changed";
		    } else {
			$price_old_class = "normal";
		    }
		    if($record['old_date'] != $record['new_date']) {
			$date_class = "changed";
		    } else {
			$date_class = "normal";
		    }
		    if($record['old_sku'] != $record['new_sku']) {
			$sku_class = "changed";
		    } else {
			$sku_class = "normal";
		    }
		    if($record['old_slide'] != $record['new_slide']) {
			$slide_class = "changed";
		    } else {
			$slide_class = "normal";
		    }
		    if($record['old_status'] != $record['new_status']) {
			$status_class = "changed";
		    } else {
			$status_class = "normal";
		    }
		    if($record['old_url'] != $record['new_url']) {
			$url_class = "changed";
		    } else {
			$url_class = "normal";
		    }
		    if($record['old_description'] != $record['new_description']) {
			$description_class = "changed";
		    } else {
			$description_class = "normal";
		    }
		    if($record['old_comfort'] != $record['new_comfort']) {
			$comfort_class = "changed";
		    } else {
			$comfort_class = "normal";
		    }
		    if($record['old_power'] != $record['new_power']) {
			$power_class = "changed";
		    } else {
			$power_class = "normal";
		    }
		    if($record['old_safety'] != $record['new_safety']) {
			$safety_class = "changed";
		    } else {
			$safety_class = "normal";
		    }
		?>

		<table class="table table-striped table-bordered">
		    <tbody>
			<tr>
				<td>VIN</td>
				<td>Марка</td>
				<td>Модель</td>
				<td>Кузов</td>
				<td>Колір</td>
				<td>Рік</td>
				<td>Двигун</td>
				<td>Об`єм</td>
				<td>КП</td>
				<td>Привід</td>
				<td>Пробіг</td>
				<td>Назва</td>
				<td>Стан</td>
				<td>Ціна</td>
				<td>Без акції</td>
				<td>Дата</td>
				<td>СКЮ</td>
				<td>Слайдер</td>
				<td>Дійсне</td>
				<td>URL</td>
				<td>Опис</td>
				<td>Комфорт</td>
				<td>Потужність</td>
				<td>Безпека</td>
			</tr>
			<tr>
			    <td class="<?=$number_class?>" nowrap>
				<?php echo "<a href=\"?number=".urlencode($record['old_number'])."\">".$record['old_number']."</a>"; ?>
			    </td>
			    <td class="<?=$producer_class?>" nowrap><?php echo $record['old_producer']; ?></td>
			    <td class="<?=$model_class?>" nowrap><?php echo $record['old_model']; ?></td>
			    <td class="<?=$body_class?>" nowrap><?php echo $record['old_body']; ?></td>
			    <td class="<?=$color_class?>" nowrap><?php echo $record['old_color']; ?></td>
			    <td class="<?=$year_class?>" nowrap><?php echo $record['old_year']; ?></td>
			    <td class="<?=$engine_class?>" nowrap><?php echo $record['old_engine']; ?></td>
			    <td class="<?=$engine_capacity_class?>" nowrap><?php echo $record['old_engine_capacity']; ?>&nbsp;cm&sup3;</td>
			    <td class="<?=$transmission_class?>" nowrap><?php echo $record['old_transmission']; ?></td>
			    <td class="<?=$gear_class?>" nowrap><?php echo $record['old_gear']; ?></td>
			    <td class="<?=$mileage_class?>" nowrap><?php echo $record['old_mileage']; ?>&nbsp;тис&nbsp;км</td>
			    <td class="<?=$name_class?>" nowrap><?php echo $record['old_name']; ?></td>
			    <td class="<?=$phase_class?>" nowrap><?php echo $record['old_phase']; ?></td>
			    <td class="<?=$price_class?>" nowrap><?php echo $record['old_price']; ?></td>
			    <td class="<?=$price_old_class?>" nowrap><?php echo $record['old_price_old']; ?></td>
			    <td class="<?=$date_class?>" nowrap><?php print date('d.m.Y',$record['old_date']); ?></td>
			    <td class="<?=$sku_class?>" nowrap><?php echo $record['old_sku']; ?></td>
			    <td class="<?=$slide_class?>" nowrap><?php echo $record['old_slide']; ?></td>
			    <td class="<?=$status_class?>" nowrap><?php echo $record['old_status']; ?></td>
			    <td class="<?=$url_class?>" nowrap><?php echo $record['old_url']; ?></td>
			    <td class="<?=$description_class?>"><?php echo $record['old_description']; ?></td>
			    <td class="<?=$comfort_class?>" nowrap><?php echo $record['old_comfort']; ?></td>
			    <td class="<?=$power_class?>" nowrap><?php echo $record['old_power']; ?></td>
			    <td class="<?=$safety_class?>" nowrap><?php echo $record['old_safety']; ?></td>
			</tr>
			<tr>
			    <td class="<?=$number_class?>" nowrap>
				<?php echo "<a href=\"?number=".urlencode($record['new_number'])."\">".$record['new_number']."</a>"; ?>
			    </td>
			    <td class="<?=$producer_class?>" nowrap><?php echo $record['new_producer']; ?></td>
			    <td class="<?=$model_class?>" nowrap><?php echo $record['new_model']; ?></td>
			    <td class="<?=$body_class?>" nowrap><?php echo $record['new_body']; ?></td>
			    <td class="<?=$color_class?>" nowrap><?php echo $record['new_color']; ?></td>
			    <td class="<?=$year_class?>" nowrap><?php echo $record['new_year']; ?></td>
			    <td class="<?=$engine_class?>" nowrap><?php echo $record['new_engine']; ?></td>
			    <td class="<?=$engine_capacity_class?>" nowrap><?php echo $record['new_engine_capacity']; ?>&nbsp;cm&sup3;</td>
			    <td class="<?=$transmission_class?>" nowrap><?php echo $record['new_transmission']; ?></td>
			    <td class="<?=$gear_class?>" nowrap><?php echo $record['new_gear']; ?></td>
			    <td class="<?=$mileage_class?>" nowrap><?php echo $record['new_mileage']; ?>&nbsp;тис&nbsp;км</td>
			    <td class="<?=$name_class?>" nowrap><?php echo $record['new_name']; ?></td>
			    <td class="<?=$phase_class?>" nowrap><?php echo $record['new_phase']; ?></td>
			    <td class="<?=$price_class?>" nowrap><?php echo $record['new_price']; ?></td>
			    <td class="<?=$price_old_class?>" nowrap><?php echo $record['new_price_old']; ?></td>
			    <td class="<?=$date_class?>" nowrap><?php print date('d.m.Y',$record['new_date']); ?></td>
			    <td class="<?=$sku_class?>" nowrap><?php echo $record['new_sku']; ?></td>
			    <td class="<?=$slide_class?>" nowrap><?php echo $record['new_slide']; ?></td>
			    <td class="<?=$status_class?>" nowrap><?php echo $record['new_status']; ?></td>
			    <td class="<?=$url_class?>" nowrap><?php echo $record['new_url']; ?></td>
			    <td class="<?=$description_class?>"><?php echo $record['new_description']; ?></td>
			    <td class="<?=$comfort_class?>" nowrap><?php echo $record['new_comfort']; ?></td>
			    <td class="<?=$power_class?>" nowrap><?php echo $record['new_power']; ?></td>
			    <td class="<?=$safety_class?>" nowrap><?php echo $record['new_safety']; ?></td>
			</tr>
		    </tbody>
		</table>

	    </div> <!-- body -->
	</div> <!-- row -->

    <?php }?>
</div> <!-- logReport -->