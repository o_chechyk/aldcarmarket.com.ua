<?php

class AskController extends AController
{
    public function actionIndex()
    {
        $a_ask  = AskModel::model()->findAll(array('order' => 'ask_read'));
        $h1     = 'Питання';

        $this->render('index', array('a_ask' => $a_ask, 'h1' => $h1));
    }

    public function actionAnswer($id)
    {
        $id     = (int) $id;
        $o_ask  = AskModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            $answer     = $_POST['data']['answer'];
            $mail_to    = $o_ask['ask_email'];
            $subject    = 'Відповідь на Ваше питання на сайті ' . $_SERVER['HTTP_HOST'];
            $message    = $_POST['data']['answer'];
            $header     = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
            $header    .= "Content-type:text/html; charset=utf-8\r\n";

            mail($mail_to, $subject, $message, $header);

//            Yii::app()->user->setFlash('success', $this->success_message);

	    // Save the reply text
	    $o_ask['ask_reply'] = mb_ereg_replace("\&nbsp\;", " ", $answer);
	    $o_ask->save();

	    $this->actionRead( $id );
        }

        $h1 = 'Перегляд питання';

        $this->render('answer', array('o_ask' => $o_ask, 'h1' => $h1));
    }

    public function actionRead($id)
    {
        $id = (int) $id;
        AskModel::model()->updateByPk($id, array('ask_read' => time(), 'ask_manager' => Yii::app()->user->id));

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/ask');
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        AskModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/ask');
    }
}