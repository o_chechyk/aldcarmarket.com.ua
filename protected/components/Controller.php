<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public $a_breadchamb;
    public $a_menu;
    public $breadchamb;
    public $contacts;
    public $info_time;
    public $layout = '//layouts/column1';
    public $seo_canonical;
    public $seo_description;
    public $seo_h1;
    public $seo_keywords;
    public $seo_title;
    public $seo_text;
    public $social_facebook;
    public $social_instagram;
    public $view_id;

    public function init()
    {
        $o_info                 = InfoModel::model()->findByPk(1);
        $this->info_time        = explode(',', $o_info['info_time']);
        $this->social_facebook  = $o_info['info_facebook'];
        $this->social_instagram = $o_info['info_instagram'];
        $this->a_menu           = MenuModel::model()->findAllByAttributes(array('menu_status' => 1), array('order' => 'menu_order ASC'));
        $this->contacts         = ContactsModel::model()->findByPk(1);

        if (!empty($o_info['seo_text']))
        {
            $this->seo_text = $o_info['seo_text'];
        }

	if(! isset(Yii::app()->session['code']) ) {
	    if( isset(Yii::app()->request->cookies['tcid']) ) { // Known visitor?
		Yii::app()->session['code'] = Yii::app()->request->cookies['tcid']->value;
	    }
	}

    }
}