<section class="content">
    <div class="wrap clearfix">
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="sidebar-filters">
            <div class="sidebar-mobile clearfix only-mobile">
                <a href="javascript:;" class="sidebar-mobile-left"><span>Фільтрувати</span></a>

		<?php if( $show_fav ) { ?>
		    <a class="sidebar-mobile-center"><span onclick="document.location='/'">Всі</span></a>
		<?php } else { ?>
		    <?php if( $fav_exists ) { ?>
			<a class="sidebar-mobile-center">
			    <span class="fav-mobile-container no-underline">
				<img src="/assets/img/blink-fav-mobile.png" class="blink-fav-mobile" onclick="document.location='/?favorites'">
				<?php if( $fav_changed ) { ?>
				    <span class="fav-alarm no-underline">!</span>
				<?php } ?>
			    </span>
			</a>
		    <?php } else { ?>
			<a class="sidebar-mobile-center">
			</a>
		    <?php } ?>
		<?php } ?>

                <a href="javascript:;" class="sidebar-mobile-right"><span>Сортувати</span></a>
            </div>
            <div class="sidebar-sorting-form only-mobile">
                <form method="GET">
                    <?php foreach ($filter as $key => $value) { ?>
                        <?php if ('check' == $key) { ?>
                            <?php foreach ($value as $check_key => $check_value) { ?>
                                <input type="hidden" name="filter[check][<?php print $check_key; ?>][]" value="<?php print $check_value[0]; ?>">
                            <?php } ?>
                        <?php } else if ('smart' == $key) { ?>
                            <?php foreach ($value as $smart_key => $smart_value) { ?>
                                <input type="hidden" name="filter[smart][<?php print $smart_key; ?>][]" value="<?php print $smart_value[0]; ?>">
                            <?php } ?>
                        <?php } else { ?>
                            <input type="hidden" name="filter[<?php print $key; ?>]" value="<?php print $value; ?>">
                        <?php } ?>
                    <?php } ?>
                    <div class="sidebar-sorting-heading">Сортувати за:</div>
                    <?php foreach ($mobile_order_array as $key => $value) { ?>
                        <div class="sidebar-filters-select sidebar-arrow">
                            <div>
                                <label for="mobile_price"><?php print $key; ?>:</label>
                            </div>
                            <div>
                                <select name="order[]">
                                    <?php foreach ($value as $item) { ?>
                                        <option value="<?php print $item['value']; ?>"
                                            <?php if (in_array($item['value'], $order_m)) { ?>
                                                selected
                                            <?php } ?>
                                        ><?php print $item['text']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="sidebar-filter-bottom">
                        <input type="submit" class="sidebar-sorting-submit" value="Сортувати авто">
                    </div>
                </form>
            </div>
            <form method="GET" class="sidebar-filters-form">
                <div class="filters-result">
                    <span class="filters-result-corner"><em></em></span>
                    <a href="javascript:;" class="sidebar-floating-submit">Підібрати авто</a>
                    <a href="javascript:;" class="sidebar-floating-close">x</a>
                </div>
                <div class="sidebar-filters-heading">Підібрати за параметрами:</div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="brand">Марка:</label>
                    </div>
                    <div>
                        <select id="producer-select" name="filter[check][producer_id][]">
                            <option value="0">Не вибрано</option>
                            <?php foreach ($a_producer as $item) { ?>
                                <option
                                    value="<?php print $item['producer_id']; ?>"
                                    <?php if (isset($filter['check']['producer_id']) && in_array($item['producer_id'], $filter['check']['producer_id'])) { ?>
                                        selected
                                    <?php } ?>
                                >
                                    <?php print $item['producer_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="model">Модель:</label>
                    </div>
                    <div>
                        <select id="model-select" name="filter[check][model_id][]">
                            <option value="0" data-producer="0">Не вибрано</option>
                            <?php foreach ($a_model as $item) { ?>
                                <option
                                    value="<?php print $item['model_id']; ?>"
                                    <?php if (isset($filter['check']['model_id']) && in_array($item['model_id'], $filter['check']['model_id'])) { ?>
                                        selected
                                    <?php } ?>
                                    data-producer="<?php print $item['model_producer_id']; ?>"
                                >
                                    <?php print $item['model_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="engine">Тип двигуна:</label>
                    </div>
                    <div>
                        <select id="" name="filter[check][engine_id][]">
                            <option value="0">Не вибрано</option>
                            <?php foreach ($a_engine as $item) { ?>
                                <option
                                    value="<?php print $item['engine_id']; ?>"
                                    <?php if (isset($filter['check']['engine_id']) && in_array($item['engine_id'], $filter['check']['engine_id'])) { ?>
                                        selected
                                    <?php } ?>
                                >
                                    <?php print $item['engine_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="engine">Тип приводу:</label>
                    </div>
                    <div>
                        <select id="" name="filter[check][gear_id][]">
                            <option value="0">Не вибрано</option>
                            <?php foreach ($gear_filter as $item) { ?>
                                <option
                                    value="<?php print $item['gear_id']; ?>"
                                    <?php if (isset($filter['check']['gear_id']) && in_array($item['gear_id'], $filter['check']['gear_id'])) { ?>
                                        selected
                                    <?php } ?>
                                    >
                                    <?php print $item['gear_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="transmission">Тип КПП:</label>
                    </div>
                    <div>
                        <select id="" name="filter[check][transmission_id][]">
                            <option value="0">Не вибрано</option>
                            <?php foreach ($a_transmission as $item) { ?>
                                <option
                                    value="<?php print $item['transmission_id']; ?>"
                                    <?php if (isset($filter['check']['transmission_id']) && in_array($item['transmission_id'], $filter['check']['transmission_id'])) { ?>
                                        selected
                                    <?php } ?>
                                >
                                    <?php print $item['transmission_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-accordion active">
                    <div class="sidebar-filters-accordion-head sidebar-arrow">
                        <label for="">Рiк випуску:</label>
                    </div>
                    <div class="sidebar-filters-accordion-body">
                        <div class="jq-slider" id="sidebar-year-slider" data-min="<?php print $a_year[0]['car_year']; ?>" data-max="<?php $end = end($a_year); print $end['car_year']; ?>" data-mincurrent="<?php if (isset($filter['year_min'])) { print $filter['year_min']; } else { print $a_year[0]['car_year']; } ?>" data-maxcurrent="<?php if (isset($filter['year_max'])) { print $filter['year_max']; } else { print $end['car_year']; } ?>"></div>
                        <div class="sidebar-filter-price clearfix">
                            <div>
                                <label for="">Вiд:</label>
                                <input type="text" name="filter[year_min]" id="sidebar-year-from" data-min="<?php print $a_year[0]['car_year']; ?>" data-max="<?php print $end['car_year']; ?>" value="<?php if (isset($filter['year_min'])) { print $filter['year_min']; } else { print $a_year[0]['car_year']; } ?>">
                            </div>
                            <div>
                                <label for="">До:</label>
                                <input type="text" name="filter[year_max]" id="sidebar-year-to" data-min="<?php print $a_year[0]['car_year']; ?>" data-max="<?php print $end['car_year']; ?>" value="<?php if (isset($filter['year_max'])) { print $filter['year_max']; } else { print $end['car_year']; } ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-filters-select sidebar-arrow">
                    <div>
                        <label for="carcase">Тип кузова:</label>
                    </div>
                    <div>
                        <select name="filter[check][body_id][]" id="">
                            <option value="0">Не вибрано</option>
                            <?php foreach ($a_body as $item) { ?>
                                <option
                                    value="<?php print $item['body_id']; ?>"
                                    <?php if (isset($filter['check']['body_id']) && in_array($item['body_id'], $filter['check']['body_id'])) { ?>
                                        selected
                                    <?php } ?>
                                >
                                    <?php print $item['body_name']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sidebar-filters-accordion active">
                    <div class="sidebar-filters-accordion-head sidebar-arrow">
                        <label for="">Цiна:</label>
                    </div>
                    <div class="sidebar-filters-accordion-body">
                        <div class="jq-slider" id="sidebar-price-slider" data-min="<?php print $a_price[0]['car_price']; ?>" data-max="<?php $end = end($a_price); print $end['car_price']; ?>" data-mincurrent="<?php if (isset($filter['price_min'])) { print str_replace(" ","",$filter['price_min']); } else { print $a_price[0]['car_price']; } ?>" data-maxcurrent="<?php if (isset($filter['price_max'])) { print str_replace(" ","",$filter['price_max']); } else { print $end['car_price']; } ?>"></div>
                        <div class="sidebar-filter-price clearfix">
                            <div>
                                <label for="">Вiд: грн.</label>
                                <input type="text" name="filter[price_min]" id="sidebar-price-from" data-min="<?php print $a_price[0]['car_price']; ?>" data-max="<?php print $end['car_price']; ?>" value="<?php if (isset($filter['price_min'])) { print $filter['price_min']; } else { print $a_price[0]['car_price']; } ?>" />
                            </div>
                            <div>
                                <label for="">До: грн.</label>
                                <input type="text" name="filter[price_max]" id="sidebar-price-to" data-min="<?php print $a_price[0]['car_price']; ?>" data-max="<?php print $end['car_price']; ?>" value="<?php if (isset($filter['price_max'])) { print $filter['price_max']; } else { print $end['car_price']; } ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-filters-accordion active">
                    <div class="sidebar-filters-accordion-head sidebar-arrow">
                        <label for="">Об`єм двигуна (куб. см.):</label>
                    </div>
                    <div class="sidebar-filters-accordion-body">
                        <div class="jq-slider" id="sidebar-capacity-slider" data-min="0" data-max="6.0" data-mincurrent="<?php if (isset($filter['engine_min'])) { print $filter['engine_min']; } else { print '0'; } ?>" data-maxcurrent="<?php if (isset($filter['engine_max'])) { print $filter['engine_max']; } else { print '6.0'; } ?>"></div>
                        <div class="sidebar-filter-capacity clearfix">
                            <div>
                                <label for="">Вiд:</label>
                                <input type="text" id="sidebar-capacity-from"  name="filter[engine_min]" value="<?php if (isset($filter['engine_min'])) { print $filter['engine_min']; } else { print '0'; } ?>" />
                            </div>
                            <div>
                                <label for="">До:</label>
                                <input type="text" id="sidebar-capacity-to" name="filter[engine_max]" value="<?php if (isset($filter['engine_max'])) { print $filter['engine_max']; } else { print '6.0'; } ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-filters-accordion active">
                    <div class="sidebar-filters-accordion-head sidebar-arrow">
                        <label for="">Авто за класом:</label>
                    </div>
                    <div class="sidebar-filters-accordion-body">
                        <?php foreach ($a_smart as $item) { ?>
                            <div class="sidebar-filter-checkbox">
                                <input
                                    type="checkbox"
                                    name="filter[smart][]"
                                    id="smart-<?php print $item['smart_id']; ?>"
                                    value="<?php print $item['smart_id']; ?>"
                                    class="sidebar-smartfilter-checkbox"
                                    <?php if (isset($filter['smart']) && in_array($item['smart_id'], $filter['smart'])) { ?>
                                        checked
                                    <?php } ?>
                                >
                                <label for="smart-<?php print $item['smart_id']; ?>"><?php print $item['smart_name']; ?></label>
                                <i class="smartfilter-delete">X</i>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="sidebar-filter-checkboxes">
                   <div class="sidebar-filter-custom-checkbox">
                        <input
                            type="checkbox"
                            id="only-promo"
                            name="filter[special]"
                            class="sidebar-custom-checkbox"
                            value="1"
                            <?php if (isset($filter['special']) && 1 == $filter['special']) { ?>
                                checked
                            <?php } ?>
                        >
                        <label for="only-promo">Тільки акційні пропозиції</label>
                   </div>
<!--
                   <div class="sidebar-filter-custom-checkbox">
			<input
			    type="checkbox"
			    id="not-sold"
			    name="filter[not_sold]"
			    class="sidebar-custom-checkbox"
			    value="1"
			    <?php if (isset($filter['not_sold']) && 1 == $filter['not_sold']) { ?>
			        checked
			    <?php } else { ?>
			    <?php } ?>
			>
			<label for="not-sold">Сховати продані</label>
                   </div>
-->
                </div>
                <div class="sidebar-filter-bottom">
                    <a href="javascript:;" class="sidebar-filters-reset" id="filter-empty"><span>Очистити фільтри</span></a>
                    <input type="submit" class="sidebar-filters-submit" value="Підібрати авто">
                </div>
            </form>
        </div>
        <div class="main">
            <div class="main-content">
                <div class="main-slider-wrap bx-unstyled clearfix">
                    <div class="main-slider">
                        <?php foreach ($a_slide as $item) { ?>
                            <a href="/automobile/<?php print $item['car_sku']; ?>/<?php print $item['car_url']; ?>#lang=uk" class="main-slider-item">
                                <?php if (0 != $item['car_price_old']) { ?>
                                    <i class="main-slider-special"><span>Акція</span></i>
                                <?php } ?>
                                <span class="main-slider-image">
                                    <?php if (isset($item['image'][0]['image']['image_id'])) { ?>
                                        <img src="<?php ImageIgosja::resize($item['image'][0]['image']['image_id'], 395, 296); ?>" alt="">
                                    <?php } else { ?>
                                        <img src="/assets/img/product-grid-placeholder.jpg">
                                    <?php } ?>
                                </span>
                                <span class="main-slider-info">
                                    <span class="main-slider-info-header">
                                        <span>
                                            <strong title="<?php print $item['car_name']; ?>"><?php print $item['car_name']; ?></strong>
                                            <span>ID - <?php print $item['car_sku']; ?></span>
                                        </span>
                                    </span>
                                    <span class="main-slider-info-price">
                                        <span class="main-slider-price-label">Ціна:</span>
                                        <span class="main-slider-prices-wrap">
                                            <?php if (0 != $item['car_price_old']) { ?>
                                                <span class="main-slider-price-old">
                                                    <?php print number_format($item['car_price_old'], 0, ',', ' '); ?> грн
                                                </span>
                                            <?php } ?>
                                            <span class="main-slider-price-new"><?php print number_format($item['car_price'], 0, ',', ' '); ?> грн</span>
                                        </span>
                                    </span>
                                    <span class="main-slider-info-bottom">
                                        <span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-color"><?php print $item['color']['color_name']; ?></span>
                                            </span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-transmission"><?php print $item['transmission']['transmission_name']; ?></span>
                                            </span>
                                        </span>
                                        <span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-year"><?php print $item['car_year']; ?> р.</span>
                                            </span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-mileage"><?php print $item['car_mileage']; ?> тис. км.</span>
                                            </span>
                                        </span>
                                        <span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-engine"><?php print $item['engine']['engine_name'] . ', ' . round($item['car_engine_capacity'] / 1000, 1); ?> л</span>
                                            </span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-body"><?php print $item['body']['body_name']; ?></span>
                                            </span>
                                        </span>
                                        <span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-engine-power"><?php  ($item['car_power'] == '0') ? print 'Не вказано' : print $item['car_power'] . ' к/с'  ?></span>
                                            </span>
                                            <span>
                                                <span class="main-slider-bottom-icon main-slider-drive-type"><?php echo $item['gear']['gear_name']; ?></span>
                                            </span>
                                        </span>
                                        <span>

                                        </span>
                                    </span>
                                </span>
                            </a>
                        <?php } ?>
                        <?php foreach($slider as $item) { ?>
                            <a href="<?php print $item['url'] ?>" class="main-slider-item">
                                    <?php if (isset($item['img']['image_id'])) { ?>
                                        <img src="<?php ImageIgosja::resize($item['img']['image_id'], 820, 296); ?>" alt="">
                                     <?php } else { ?>
                                        <img src="/assets/img/product-grid-placeholder.jpg">
                                    <?php } ?>
                            </a>

                        <?php } ?>
                    </div>
                </div>
                <div class="grid-sorting-block clearfix">
		    <?php if( $show_fav ) { ?>
			<div class="grid-sorting-block-left" style="position:relative;">
			    <div class="grid-sorting-block-element">
				<span class="label-fav" onclick="document.location='/'">Всі</span>
			    </div>
			</div>
		    <?php } else { ?>
			<?php if( $fav_exists ) { ?>
			    <div class="grid-sorting-block-left" style="position:relative;">
				<div class="grid-sorting-block-element">
				    <span class="label-fav" onclick="document.location='/?favorites'">Обране</span>
				    <?php if( $fav_changed ) { ?>
					<span class="fav-alarm">!</span>
				    <?php } ?>
				</div>
			    </div>
			<?php } ?>
		    <?php } ?>
                    <div class="grid-sorting-block-left">
                        <label for="">СОРТУВАТИ ЗА:</label>
                        <?php foreach ($order_array as $key => $value) { ?>
                            <div class="grid-sorting-block-element">
                                <?php $arrow_class = explode(' ', $value);
				    if( $show_fav ) {
					$buf = array('favorites' => 1, 'on_page' => $limit);
				    } else {
					$buf = array('filter' => $filter, 'on_page' => $limit);
				    }
				?>
                                <a href="/?<?php print urldecode(http_build_query($buf)); ?>&order[]=<?php print $value; ?>" class="<?php if (!isset($arrow_class[1])) { ?>arrowDown<?php } else { ?>arrowUp<?php } ?> <?php if ($limit == $value) { ?>active<?php } ?>"><span><?php print $key; ?></span></a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="grid-sorting-block-right">
                        <label for="">ПОКАЗАТИ:</label>
                        <?php foreach ($on_page_array as $key => $value) { ?>
                            <div class="grid-sorting-block-amount">
				<?php
				    if( $show_fav ) {
					$buf = array('favorites' => 1, 'order' => $order);
				    } else {
					$buf = array('filter' => $filter, 'order' => $order);
				    }
				?>
                                <a href="/?<?php print urldecode(http_build_query($buf)); ?>&on_page=<?php print $value; ?>" <?php if ($limit == $value) { ?>class="active"<?php } ?>>
                                    <span><?php print $key; ?></span>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="product-grid-wrap">
                    <div class="products-grid clearfix">
                        <?php foreach ($a_car as $item) { ?>
                            <div class="products-grid-item">
                                <?php if(isset($item['phase']) && $item['phase']['phase_name'] == 'Проданий')
                                    echo '<div class="products-grid-product sold">';
                                elseif (isset($item['phase']) && $item['phase']['phase_name'] == 'В резерві')
                                    echo '<div class="products-grid-product reserved">';
                                else
                                    echo '<div class="products-grid-product">';
                                ?>
                                    <a href="/automobile/<?php print $item['car_sku']; ?>/<?php print $item['car_url']; ?>#lang=uk" class="pg-product-images clearfix">
                                        <div class="pg-product-bigimage">
                                            <?php if(isset($item['phase']) && $item['phase']['phase_name'] == 'Проданий') {?>
                                                <div class="soldlabel">ПРОДАНИЙ</div>
                                            <?php }?>
                                            <?php if (isset($item['phase']) && $item['phase']['phase_name'] == 'В резерві') { ?>
                                                <div class="reservedlabel">В РЕЗЕРВІ</div>
                                            <?php } ?>
                                            <?php if (0 != $item['car_price_old']) { ?>
                                                <i><span>Акція</span></i>
                                            <?php } ?>
					    <?php if( isset($spin[$item['car_sku']]) ) {
						    $class = "";
						} else {
						    $class = " hidden";
						}
					    ?>
					    <div class="spinShow<?=$class?>">
						<img src="/assets/img/360_4.png">
					    </div>
                                            <?php if (isset($item['image'][0]['image']['image_id'])) { ?>
                                                <img src="<?php ImageIgosja::resize($item['image'][0]['image']['image_id'], 375, 211); ?>" alt="">
                                            <?php } else if( isset($spin[$item['car_sku']]) ){ ?>
                                                <img src="<?=$spin[$item['car_sku']]?>" class="imgIndex" alt="">
                                            <?php } else { ?>
                                                <img src="/assets/img/news-block-placeholder1.png">
                                            <?php } ?>
                                        </div>
                                    </a>

                                    <a href="/automobile/<?php print $item['car_sku']; ?>/<?php print $item['car_url']; ?>#lang=uk" class="pg-product-labels">
                                        <span class="pg-product-labels-wrap">
                                            <div class="pg-product-name"><?php print $item['car_name']; ?></div>
                                            <div class="pg-product-code">Код авто: <?php print $item['car_sku']; ?></div>
                                        </span>
                                        <div class="pg-product-priceblock">
                                            <div class="pg-product-pricelabel">Ціна:</div>
                                            <?php if (0 != $item['car_price_old']) { ?>
                                                <div class="pg-product-priceold">
                                                    <?php print number_format($item['car_price_old'], 0, ',', ' '); ?> грн
                                                </div>
                                            <?php } ?>
                                            <div class="pg-product-pricenew"><?php print number_format($item['car_price'], 0, ',', ' '); ?> грн</div>
                                        </div>
                                    </a>
                                    <a href="/automobile/<?php print $item['car_sku']; ?>/<?php print $item['car_url']; ?>#lang=uk" class="pg-product-specs">
                                        <div>
                                            <div>
                                                <span class="main-catalog-color"><?php if($item['color']) {print $item['color']['color_name'];} else {print "-";} ?></span>
                                            </div>
                                            <div>
                                                <span class="main-catalog-transmission"><?php print $item['transmission']['transmission_name']; ?></span>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <span class="main-catalog-year"><?php print $item['car_year']; ?> p.</span>
                                            </div>
                                            <div>
                                                <span class="main-catalog-mileage"><?php print $item['car_mileage']; ?> тис. км.</span>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <span class="main-catalog-engine"><?php print $item['engine']['engine_name'] . ', ' . round($item['car_engine_capacity'] / 1000, 1); ?> л</span>
                                            </div>
                                            <div>
                                                <span class="main-catalog-body"><?php if( $item['body'] ) { print $item['body']['body_name'];} else {print "-";} ?></span>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <span class="main-catalog-engine-power"><?php ($item['car_power'] == '0') ? print 'Не вказано' : print $item['car_power'] . ' к/с'  ?></span>
                                            </div>
                                            <div>
                                                <span class="main-catalog-drive-type"><?php echo $item['gear']['gear_name']; ?></span>
                                            </div>
                                        </div>
                                    </a>

                                    <!--<div class="pg-product-bottom">
                                        <div>
                                            <span class="pg-product-bottom-btn pg-product-date">
                                                <?php print date('d.m.Y', $item['car_date']); ?>
                                            </span>
                                        </div>
                                        <?php if (1 == 0) { ?>
                                            <div>
                                                <a href="javascript:;" class="pg-product-bottom-btn pg-product-addfav">
                                                    <span>До обраного</span>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="javascript::" class="pg-product-bottom-btn pg-product-print">
                                                    <span>Роздрукувати</span>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>-->
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="products-pagination">
                    <div class="products-pagination-info">
                        Відображено авто з <?php print $limit * $offset + 1; ?> по <?php print $limit * $offset + count($a_car); ?> із <?php print $count_car; ?>
                    </div>
                    <?php if(count($a_pagination) > 1):?>
                    <a href="<?php print $prev; ?>" class="products-grid-pagination-prev">Попередня</a>
                    <span class="products-pagination-pages">
                        <?php foreach ($a_pagination as $item) { ?>
                            <?php if ($offset + 1 == $item['page']) { ?><b><?php } ?>
                                <a href="<?php print $item['url']; ?>" <?php if ($offset + 1 == $item['page']) { ?>class="active"<?php } ?>>
                                    <span><?php print $item['page']; ?></span>
                                </a>
                        <?php } ?>
                    </span>
                    <a href="<?php print $next; ?>" class="products-grid-pagination-next">Наступна</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

	<?php if( count($v_car) ) { ?>
	    <div class="review-container">
		<div class="review-caption">Ви також дивились:</div>
		<div class="review-slick">
		<div class="slick review">
		    <?php foreach($v_car as $item) { ?>
			<div>
			    <a href="/automobile/show/<?php echo $item['car_id']; ?>" title="<?php echo $item['car_name']; ?>">
			    <?php if (isset($item['image'][0]['image']['image_id'])) { ?>
				<img src="<?php ImageIgosja::resize($item['image'][0]['image']['image_id'], 121, 68); ?>" alt="">
			    <?php } else if( isset($spin[$item['car_sku']]) ) { ?>
				<img src="<?=$spin[$item['car_sku']]?>" class="imgAlsoSeen">
			    <?php } else { ?>
				<img src="/assets/img/product-grid-placeholder2.jpg">
			    <?php } ?>
			    </a>
			</div>
		    <?php } ?>
		</div>
		</div>
		</div>
	<?php } ?>

    </div>

</section>
<!--
<script type="text/javascript" src="https://integrator.swipetospin.com"></script>
-->
