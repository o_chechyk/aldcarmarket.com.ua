<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Правила користування</h1>
    </div>
</div>

<div class="row" style="margin-bottom:2em;">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">

	    <table class="table table-bordered">
	    <tr>
		<td>Ключ</td>
		<td>Текст</td>
		<td></td>
	    </tr>
		<?php foreach ($a_rules as $rule) { ?>

		    <tr>
			<td class="col-lg-3">
			    <?php echo $rule->topic; ?>
			</td>
			<td class="anchor">
			    <?php
				$arr = explode("\n", $rule->rules);
				array_splice($arr, 5);
				 echo implode("\n",$arr);
			    ?>
			    <div class="fadeout"></div>
			</td>
			<td>
			    <a href="/admin/rules/edit/<?php echo $rule->topic; ?>" title="Редагувати">
				<button type="button" class="btn btn-default">
				    <i class="fa fa-pencil"></i>
				</button>
			    </a>
			</td>
		    </tr>

		<?php }?>
	    </table>

	    <a href="/admin/rules/edit" title="Додати нові Правила">
		<button type="button" class="btn btn-default">
		    <i class="fa fa-plus"></i>
		</button>
	    </a>

        </div> <!-- dataTable_wrapper -->
    </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->
