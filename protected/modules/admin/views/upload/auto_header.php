<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Завантаження автомобілів</h1>
    </div>
</div>
<!--
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/users/edit/0" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
</div>
-->

<div class="row">
    <div class="col-lg-12">

	<div class="logFilters">
	    <div class="caption">
		Файл для завантаження
	    </div>
	    <div class="body">
		<div class="section">
		<form id="formCSV" type="POST" enctype="multipart/form-data">
		    <div class="caption">
			Оберіть файл
		    </div>
		    <div class="body">
			    <input type="file" name="autoCSV" id="targetCSV">
		    </div>
		</form>
		</div>
		<div class="footer">
		    <button class="btn btn-default text-center" onclick="submitCSV('auto')">Завантажити</buton>
		    <span id="spinner" class="hidden"><i class="fa fa-spinner"></i></span>
		</div>
	    </div>
	</div>
        <div class="dataTable_wrapper" id="CSV-content">
        </div>    <!-- /.dataTable_wrapper -->

    </div>    <!-- /.col-lg-12 -->
</div>    <!-- /.row -->