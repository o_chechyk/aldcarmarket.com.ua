<?php

class SeoModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'seo';
    }

    public function primaryKey()
    {
        return 'seo_id';
    }
}