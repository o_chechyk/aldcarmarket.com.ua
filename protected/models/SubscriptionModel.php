<?php

class SubscriptionModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'subscription';
    }

    public function primaryKey()
    {
        return 'subscription_code';
    }
}
