<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/news" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" enctype="multipart/form-data">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab">Загальна інформація</a></li>
                <li><a href="#seo" data-toggle="tab">SEO</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="main">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">Назва</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[news_name]"
                                        type="text"
                                        value="<?php if (isset ($o_news)) { print $o_news['news_name']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">ЧПУ</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[news_url]"
                                        type="text"
                                        value="<?php if (isset ($o_news)) { print $o_news['news_url']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Текст</td>
                                <td>
                                    <textarea
                                        class="ckeditor"
                                        name="data[news_text]"
                                        rows="3"><?php if (isset ($o_news)) { print $o_news['news_text']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Зображення</td>
                                <td>
                                    <?php if (isset($o_news['image']['image_url'])) { ?>
                                        <div class="col-lg-3">
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php print $o_news['image']['image_url']; ?>" />
                                            </a>
                                        </div>
                                        <a href="?image=<?php print $o_news['news_image_id']; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    <?php } else { ?>
                                        <input type="file" name="image" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="seo">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">SEO title</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[seo_title]"
                                        type="text"
                                        value="<?php if (isset ($o_news)) { print $o_news['seo_title']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO description</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_description]"><?php if (isset ($o_news)) { print $o_news['seo_description']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO keywords</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_keywords]"><?php if (isset ($o_news)) { print $o_news['seo_keywords']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO text</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_text]"><?php if (isset ($o_news)) { print $o_news['seo_text']; } ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->