<?php foreach ($cars as $model) { ?>
    <table border="1" style="margin: 0 auto">
        <thead>
        <tr>
            <th colspan="2" style="text-align: center"><?= $model['model']['car_name'] ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>Назва</th>
            <td><?= $model['model']['car_name'] ?></td>
        </tr>
        <tr>
            <th>Код</th>
            <td><?= $model['model']['car_sku'] ?></td>
        </tr>
        <tr>
            <th>Ціна, грн</th>
            <td><?php print number_format($model['model']['car_price'], 0, ',', ' '); ?> грн</td>
        </tr>
        <tr>
            <th>Номер кузова</th>
            <td><?= $model['model']['car_number'] ?></td>
        </tr>
        <tr>
            <th>Колір</th>
            <td><?= $model['model']['color']['color_name'] ?></td>
        </tr>
        <tr>
            <th>Рік випуску</th>
            <td><?php print $model['model']['car_year']; ?> р.</td>
        </tr>
        <tr>
            <th>Двигун</th>
            <td><?php print $model['model']['engine']['engine_name'] . ', ' . round($model['model']['car_engine_capacity'] / 1000, 1); ?>
                л
            </td>
        </tr>
        <tr>
            <th>Об`єм двигуна, куб. см</th>
            <td><?php print $model['model']['car_engine_capacity']; ?></td>
        </tr>
        <tr>
            <th>Потужність, к/с</th>
            <td><?php ($model['model']['car_power'] == '0') ? print 'Не вказано' : print $model['model']['car_power'] . ' к/с' ?></td>
        </tr>
        <tr>
            <th>Трансмісія</th>
            <td><?php print $model['model']['transmission']['transmission_name']; ?></td>
        </tr>

        <tr>
            <th>Привід</th>
            <td><?php print $model['model']['gear']['gear_name']; ?></td>
        </tr>
        <tr>
            <th>Пробіг, тис.км</th>
            <td><?php print $model['model']['car_mileage']; ?> тис.км</td>
        </tr>
        <tr>
            <th>Тип кузова</th>
            <td><?php print $model['model']['body']['body_name']; ?></td>
        </tr>
        <?php if(1 == 0){ ?>
        <tr>
            <th>Додаткові характеристики</th>
            <td>
                <?php foreach ($model['characteristic'] as $item) {
                    echo $item['char']['characteristic_name'] . '<br>';
                } ?>
            </td>
        </tr>
        <?php } ?>
        <?php if ($showImg) { ?>
            <tr>
                <th>Фото</th>
                <td>
                    <?php if( is_array($model['model']['image']) ) { ?>
			<?php foreach (array_slice($model['model']['image'], 0) as $item) { ?>
			    <img src="<?php ImageIgosja::resize($item['image']['image_id'], 176, 132); ?>"/>
                	<?php } ?>
		    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <th>Адреса на сайті</th>
            <td>
                <?php
                echo CHtml::link(
                    '<span>Адреса</span>',
                    Yii::app()->createUrl('automobile/index', array('id' => $model['model']['car_sku'], 'alias' => $model['model']['car_url'])))
                ?>
            </td>
        </tr>
        </tbody>
    </table>
    <br><?php } ?>