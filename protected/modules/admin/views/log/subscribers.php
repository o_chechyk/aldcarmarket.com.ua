<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Підписники</h1>
    </div>
</div>

<!--
<div class="logFilters">
    <div class="caption">
        Фільтри
    </div>
    <div class="body">
	<form action="/admin/log/price" method="POST" autocomplete="off">
	    <div class="section">
		<div class="caption">
		    Дата
		</div>
		<div class="body">
		    З
		    <input name="dateFrom" id="dateFrom" value="" autocomplete="off">
		    По
		    <input name="dateTo" id="dateTo" value="" autocomplete="off">
		</div>
	    </div>
	    <div class="footer">
		<input id="logSubmit" class="btn btn-default text-center" type="submit" value="Застосувати">
		<input type="button" class="btn btn-default text-center" onclick="clearFilter()" value="Скинути фільтр">
	    </div>
	</form>
    </div>
</div>
-->

<div class="dataTable_wrapper">
    <table id="data-table" class="table table-striped table-bordered dataTable no-footer">
	<thead>
	    <tr>
		<th>E-mail</th>
		<th>Остання розсилка</th>
		<th>Статус</th>
		<th></th>
	    </tr>
	</thead>

	<?php foreach ($subscribers as $record) { ?>

	    <tr>
		<td><?=$record['subscription_email']?></td>
		<td><?=$record['subscription_last_seen']?></td>
		<td>
		    <?php
			if( $record['subscription_status'] ) { echo "Активний"; }
			else { echo "Не активовано"; }
		    ?>
		</td>
		<td>
		    <button title="Скасувати підписку" class="btn btn-danger" onclick="cancelSubscription(<?=$record['subscription_id']?>)">
			<i class="fa fa-times"></i>
		    </button>
		</td>
	    </tr>

	<?php }?>

    </table>

</div>