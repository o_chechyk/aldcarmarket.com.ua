drop event if exists garbage_collector;

DELIMITER ;;

CREATE EVENT garbage_collector ON SCHEDULE EVERY 1 DAY DO BEGIN
  delete from cookie WHERE cookie_created < date_sub(curdate(), INTERVAL 6 MONTH);
  delete from subscription WHERE subscription_status = 0 and subscription_last_seen < date_sub(curdate(), INTERVAL 6 MONTH);
END;;

DELIMITER ;