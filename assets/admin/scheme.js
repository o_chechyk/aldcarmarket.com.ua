var pinId = 0;
jQuery(document).ready(function($) {
    $('body').on('click', '.scheme-add-pin-btn', function(e) {

        var container = $('.scheme-editor-window'),
            pin = '<div class="scheme-editor-pin scheme-editor-pin' + ++pinId + '" data-id="scheme-editor-pin' + pinId + '">' + pinId + '</div>';
        container.append(pin);
        $('.scheme-editor-pin' + pinId).draggable({
            containment: 'parent',
            cursor: 'crosshair',
            opacity: 0.8,
            drag: function(event, ui) {
                var uiRef = ui,
                    pinFieldSelector = $('#' + $(this).data('id'));
                 pinFieldSelector
                    .find('.scheme-editor-ycoord').val(uiRef.position.top)
                    .end()
                    .find('.scheme-editor-xcoord').val(uiRef.position.left);
            }
        });

        $('.scheme-editor-field-clone > .scheme-editor-field')
            .clone()
            .attr('id', 'scheme-editor-pin' + pinId)
            .appendTo('.scheme-editor-fields').find('.scheme-editor-number').val(pinId);
    });

    $('body').on('keyup', '.scheme-editor-number', function() {
        var self = $(this),
            pinClass = self.parents('.scheme-editor-field').attr('id');
        $('.' + pinClass).text(self.val());
    });

    $('body').on('click', '.scheme-editor-deletepin', function() {
        var self = $(this),
            selfParent = self.parents('.scheme-editor-field'),
            pinClass = selfParent.attr('id');
        $('.' + pinClass).remove();
        selfParent.remove();
    });

    $('body').on('click', '.scheme-submit', function() {
        $('.scheme-editor-fields').submit();
    });
    
    $('.scheme-editor-pin').draggable({
        containment: 'parent',
        cursor: 'crosshair',
        opacity: 0.8,
        drag: function(event, ui) {
            var uiRef = ui,
                pinFieldSelector = $('#' + $(this).data('id'));
             pinFieldSelector
                .find('.scheme-editor-ycoord').val(uiRef.position.top)
                .end()
                .find('.scheme-editor-xcoord').val(uiRef.position.left);
        }
    });

});

$(window).resize(function() {});
$(window).load(function() {});