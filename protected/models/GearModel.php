<?php

class GearModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'gear';
    }

    public function primaryKey()
    {
        return 'gear_id';
    }
}