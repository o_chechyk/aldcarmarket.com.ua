<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/news/" title="">Новини</a>
                </span>
                <span><?php print $o_news['news_name']; ?></span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading"><?php print $o_news['news_name']; ?></h1>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="content-left">
            <article class="content-article clearfix">
                <img src="<?php if (isset($o_news['image']['image_id'])) { ImageIgosja::resize($o_news['image']['image_id'], 280, 210); } else { print '/assets/img/news-block-placeholder.jpg'; } ?>" alt="" style="float: left; margin: 0 20px 20px 0;">
                <?php print $o_news['news_text']; ?>
            </article>
            <div class="products-pagination">
                <?php if (isset($o_prev['news_url'])) { ?>
                    <a href="/news/view/<?php print $o_prev['news_url']; ?>" class="products-grid-pagination-prev">Попередня</a>
                <?php } ?>
                <?php if (isset($o_next['news_url'])) { ?>
                    <a href="/news/view/<?php print $o_next['news_url']; ?>" class="products-grid-pagination-next">Наступна</a>
                <?php } ?>
            </div>
        </div>
        <!-- <div class="content-right">
            <form class="content-right-form" method="POST" action="/form/salon">
                <div class="content-right-form-header">
                    <strong>Щоб завітати до нашого салону заповніть, <br>будь ласка, форму нижче.</strong>
                </div>
                <div class="content-right-form-body">
                    <div class="content-right-form-phone">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-phone.png" alt=""></span>
                            <span><input name="salon[salon_phone]" type="text" required></span>
                        </div>
                    </div>
                    <div class="content-right-form-date">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-date.png" alt=""></span>
                            <span><input name="salon[salon_date]" type="text" required></span>
                        </div>
                    </div>
                    <div class="content-right-form-time">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-time.png" alt=""></span>
                            <span><input name="salon[salon_time]" type="text" required></span>
                        </div>
                    </div>
                    <input type="submit" class="blue-submit" value="Відправити дані">
                </div>
            </form>
        </div> -->
    </div>
</section>
