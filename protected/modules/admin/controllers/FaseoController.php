<?php

class FaseoController extends AController
{
    public function actionIndex() {

        $a_faqseo = SeoModel::model()->findByPk(2); // 2 - FAQ SEO

	if(! $a_faqseo) {
	    $a_faqseo = new SeoModel;
	}

	if( isset($_POST['seo_title']) ) {
	    $a_faqseo['seo_title'] = $_POST['seo_title'];
	    $a_faqseo['seo_description'] = $_POST['seo_description'];
	    $a_faqseo['seo_keywords'] = $_POST['seo_keywords'];
	    $a_faqseo['seo_text'] = $_POST['seo_text'];
	    $a_faqseo->save();
	}

	$this->render('seo', array('faqseo' => $a_faqseo));

    }

}
