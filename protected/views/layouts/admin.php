<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Административный раздел</title>
    <link href="/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="/assets/admin/dist/css/timeline.css" rel="stylesheet">
    <link href="/assets/admin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="/assets/admin/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="/assets/admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--    <link href="/assets/admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet"> -->
    <link href="/assets/admin/admin.css" rel="stylesheet">
    <script src="/assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/assets/js/vendor/jquery-ui.min.js"></script>
    <link href="/assets/css/jquery-ui.min.css" rel="stylesheet">
    <script src="/assets/js/vendor/datepicker-uk.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
    $user = UserModel::model()->findByPk( Yii::app()->user->id );
?>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin/">ALD</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
		<li class="progress">
		    <div id="progressbar" class="hidden"></div>
		</li>
		<li>
		    <img src="/assets/img/sendmail.png" class="hidden" id="sendmail" onclick="sendMail()">
		    <input type="hidden" id="countChanges">
		</li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="/admin/index/logout"><i class="fa fa-sign-out fa-fw"></i> Выйти</a></li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-info-circle fa-fw"></i> Загальна інформація<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/info">Загальна інформація</a>
                                </li>
                                <li>
                                    <a href="/admin/contacts">Контакти</a>
                                </li>
                                <li>
                                    <a href="/admin/menu">Меню</a>
                                </li>
                                <li>
                                    <a href="/admin/slider">Слайдер</a>
                                </li>
                                <li>
                                    <a href="/admin/rules">Правила</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cab fa-fw"></i> Автомобілі<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/car">Автомобілі</a>
                                </li>
                                <li>
                                    <a href="/admin/producer">Марки</a>
                                </li>
                                <li>
                                    <a href="/admin/model">Моделі</a>
                                </li>
                                <li>
                                    <a href="/admin/engine">Двигуни</a>
                                </li>
                                <li>
                                    <a href="/admin/transmission">КПП</a>
                                </li>
                                <li>
                                    <a href="/admin/body">Кузова</a>
                                </li>
                                <li>
                                    <a href="/admin/gear">Привід</a>
                                </li>
                                <li>
                                    <a href="/admin/color">Кольори</a>
                                </li>
                                <li>
                                    <a href="/admin/smart">Розумні фільтри</a>
                                </li>
                                <li>
                                    <a href="/admin/phase">Статуси</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i> Характеристики авто<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/characteristicgroup">Групи характеристик</a>
                                </li>
                                <li>
                                    <a href="/admin/characteristic">Характеристики</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-clock-o fa-fw"></i> Новини<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/neseo">Загальна інформація</a>
                                </li>
                                <li>
                                    <a href="/admin/news">Новини</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/admin/page"><i class="fa fa-file-text-o fa-fw"></i> Текстові сторінки<span class="fa arrow"></span></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-question fa-fw"></i> Запитання і заявки<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/question">Зворотній зв`язок</a>
                                </li>
                                <li>
                                    <a href="/admin/ask">Запитання</a>
                                </li>
<!--
                                <li>
                                    <a href="/admin/salon">Заявки</a>
                                </li>
-->
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-question-circle fa-fw"></i> FAQ<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/faseo">Загальна інформація</a>
                                </li>
                                <li>
                                    <a href="/admin/faq">Редагування FAQ</a>
                                </li>
                            </ul>
                        </li>
			<?php if($user['user_role']&1) { ?>
			    <li>
				<a href="/admin/users"><i class="fa fa-user fa-fw"></i> Користувачі<span class="fa arrow"></span></a>
			    </li>
			<?php } ?>
                        <li>
                            <a href="#"><i class="fa fa-list fa-fw"></i> Звіти<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/log/all">Всі зміни</a>
                                </li>
                                <li>
                                    <a href="/admin/log/new">Історія завантажень</a>
                                </li>
                                <li>
                                    <a href="/admin/log/phase">Історія змін стану</a>
                                </li>
                                <li>
                                    <a href="/admin/log/price">Історія змін цін</a>
                                </li>
                                <li>
                                    <a href="/admin/log/subscribers">Підписники</a>
                                </li>
                            </ul>
                        </li>
			<?php if($user['user_role']&2) { ?>
                        <li>
                            <a href="#"><i class="fa fa-upload fa-fw"></i> Завантаження<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/upload/auto_header">Автомобілі</a>
                                </li>
                                <li>
                                    <a href="/admin/upload/price_header">Ціни</a>
                                </li>
                                <li>
                                    <a href="/admin/upload/comment_header">Технічний стан</a>
                                </li>
                            </ul>
                        </li>
			<?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper">
            <?php if(Yii::app()->user->hasFlash('success')) { ?>
                <div class="row">
                    <div class="alert alert-success alert-dismissable text-center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php print Yii::app()->user->getFlash('success'); ?>
                    </div>
                </div>
            <?php } ?>

            <?php print $content; ?>

        </div>
    </div>

    <script src="/assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/assets/admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="/assets/admin/bower_components/raphael/raphael-min.js"></script>
    <script src="/assets/admin/dist/js/sb-admin-2.js"></script>
    <script src="/assets/admin/admin.js"></script>
    <script src="/assets/admin/ckeditor/ckeditor.js"></script>
    <script src="/assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="/assets/admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="/assets/admin/js/jquery.ajax-progress.js"></script>

</body>

</html>