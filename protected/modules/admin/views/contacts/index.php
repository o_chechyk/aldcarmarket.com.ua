<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="/admin/contacts/edit">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Міський телефон</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_tel]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_tel']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Факс</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_fax]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_fax']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Мобільний</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_mob]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_mob']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_email]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_email']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Email менеджера</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_email_manager]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_email_manager']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Місто</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_city]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_city']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Адреса</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_address]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_address']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Графік роботи ПН-ЧТ</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_monday]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_monday']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Графік роботи ПТ</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_friday]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_friday']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Графік роботи СБ-НД</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_sunday]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_sunday']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Перша координата Гугл-карти</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_lat]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_lat']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Друга координата Гугл-карти</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[contacts_lng]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['contacts_lng']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">SEO title</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[seo_title]"
                                type="text"
                                value="<?php if (isset ($o_contacts)) { print $o_contacts['seo_title']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>SEO description</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_description]"
                            ><?php if (isset ($o_contacts)) { print $o_contacts['seo_description']; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>SEO keywords</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_keywords]"
                            ><?php if (isset ($o_contacts)) { print $o_contacts['seo_keywords']; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>SEO text</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_text]"
                            ><?php if (isset ($o_contacts)) { print $o_contacts['seo_text']; } ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->