<?php

class RulesController extends AController
{
    public function actionIndex() {

        $a_rules  = RulesModel::model()->findAll(array('order' => 'topic'));

        $this->render('index', array('a_rules' => $a_rules));
    } // public function actionIndex()


    public function actionEdit($id='') {

	if($id != '') {
	    $rules = RulesModel::model()->findByPk( $id );
	    if($rules == null) {
		Yii::app()->user->setFlash('error', "Правил з темою '$id' не існує!");
		$rules = new RulesModel;
		$rules->topic = $id;
		$rules->rules = "";
		$Ok = false;
	    }
	} else {
	    $rules = new RulesModel;
	    $rules->topic = '';
	    $rules->rules = "";
	}

	if( isset($_POST['topic']) ) {
	    $Ok = true;

	    if( ctype_alpha($_POST['topic']) ) {
		$topic = strtolower( $_POST['topic'] );
	    } else {
		Yii::app()->user->setFlash('error', "Тема може містити лише англійські літери!");
		$Ok = false;
	    }

	    if( $Ok ) {
		if($id == '') {
		    $rules->topic = $topic;
		}
		$rules->rules = $_POST['rules'];
		$rules->save();
		$this->redirect('/admin/rules');
	    }
	} // if( isset($_POST['faq_id']) )

	$this->render('form', array('rules' => $rules));

    } // public function actionEdit($id=0)

}
