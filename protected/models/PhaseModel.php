<?php

class PhaseModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'phase';
    }

    public function primaryKey()
    {
        return 'phase_id';
    }
}