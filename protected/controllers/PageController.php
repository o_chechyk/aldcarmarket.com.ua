<?php

class PageController extends Controller
{
    public function actionIndex()
    {
        $this->redirect('/');
    }

    public function actionView($id)
    {
        $o_page = PageModel::model()->findByAttributes(array('page_url' => $id));

        if (!isset($o_page['page_id']))
        {
            $this->redirect('/');
            exit;
        }

        if (!empty($o_page['seo_title']))
        {
            $this->seo_title = $o_page['seo_title'];
        }
        else
        {
            $this->seo_title = $o_page['page_name'];
        }

        if (!empty($o_page['seo_keywords']))
        {
            $this->seo_keywords = $o_page['seo_keywords'];
        }
        else
        {
            $this->seo_keywords = $o_page['page_name'];
        }

        if (!empty($o_page['seo_description']))
        {
            $this->seo_description = $o_page['seo_description'];
        }
        else
        {
            $this->seo_description = $o_page['page_name'];
        }

        if (!empty($o_car['seo_text']))
        {
            $this->seo_text = $o_page['seo_text'];
        }

        $this->render('view', array('o_page' => $o_page));
    }
}