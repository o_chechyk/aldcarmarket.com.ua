<?php

class NewsController extends Controller
{
    public function actionIndex()
    {
        $o_newsseo = SeoModel::model()->findByPk(1); // 1 - News SEO

        $this->seo_title        = $o_newsseo['seo_title'];
        $this->seo_description  = $o_newsseo['seo_description'];
        $this->seo_keywords     = $o_newsseo['seo_keywords'];

        if (!empty($o_newsseo['seo_text']))
        {
            $this->seo_text = $o_newsseo['seo_text'];
        }

        $limit  = 4;
        $offset = 0;

        if (isset($_GET['page']))
        {
            $offset = (int) $_GET['page'] - 1;
        }

        $a_news     = NewsModel::model()->findAllByAttributes(array('news_status' => 1), array('order' => 'news_date DESC', 'offset' => ($offset*$limit), 'limit' => $limit));
        $count_news = NewsModel::model()->countByAttributes(array('news_status' => 1));

        if (0 != $limit)
        {
            $count_page = ceil($count_news / $limit);
        }
        else
        {
            $count_page = 1;
        }

        if (0 == $offset)
        {
            $prev = 1;
        }
        else
        {
            $prev = $offset;
        }

        if ($count_page == $offset + 1)
        {
            $next = $count_page;
        }
        else
        {
            $next = $offset + 2;
        }

        $a_pagination = array();

        for ($i=1; $i<=$count_page; $i++)
        {
            $a_pagination[] = array(
                'url'  => '?page=' . $i,
                'page' => $i,
            );
        }

        $prev = '?' . urldecode(http_build_query(array('page' => $prev)));
        $next = '?' . urldecode(http_build_query(array('page' => $next)));

        $this->render('index', array('a_news' => $a_news, 'count_news' => $count_news, 'a_pagination' => $a_pagination, 'prev' => $prev, 'next' => $next, 'limit' => $limit, 'offset' => $offset));
    }

    public function actionView($id)
    {
        $o_news = NewsModel::model()->findByAttributes(array('news_url' => $id));

        if (!isset($o_news['news_id']))
        {
            $this->redirect('/');
            exit;
        }

        if (!empty($o_news['seo_title']))
        {
            $this->seo_title = $o_news['seo_title'];
        }
        else
        {
            $this->seo_title = $o_news['news_name'];
        }

        if (!empty($o_news['seo_keywords']))
        {
            $this->seo_keywords = $o_news['seo_keywords'];
        }
        else
        {
            $this->seo_keywords = $o_news['news_name'];
        }

        if (!empty($o_news['seo_description']))
        {
            $this->seo_description = $o_news['seo_description'];
        }
        else
        {
            $this->seo_description = $o_news['news_name'];
        }

        if (!empty($o_news['seo_text']))
        {
            $this->seo_text = $o_news['seo_text'];
        }

        $o_prev = NewsModel::model()->find(array('condition' => 'news_id<' . $o_news['news_id'] . ' and news_status=1', 'order' => 'news_id DESC', 'limit' => 1));

        if (!isset($o_prev['news_id']))
        {
            $o_prev = '';
        }

        $o_next = NewsModel::model()->find(array('condition' => 'news_id>' . $o_news['news_id'] . ' and news_status=1', 'order' => 'news_id', 'limit' => 1));

        if (!isset($o_next['news_id']))
        {
            $o_next = '';
        }


/*
echo "<pre>";
var_dump($o_prev);
exit;
*/




        $this->render('view', array('o_news' => $o_news, 'o_next' => $o_next, 'o_prev' => $o_prev));
    }
}