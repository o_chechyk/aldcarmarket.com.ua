<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AuController extends AController
{
    public $layout = '//layouts/login';

    public function beforeAction($action)
    {
        return true;
    }
}