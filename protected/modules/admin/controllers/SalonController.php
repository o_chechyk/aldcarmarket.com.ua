<?php

class SalonController extends AController
{
    public function actionIndex()
    {
        $a_salon    = SalonModel::model()->findAll(array('order' => 'salon_read'));
        $h1         = 'Заявки на відвідування салону';

        $this->render('index', array('a_salon' => $a_salon, 'h1' => $h1));
    }

    public function actionRead($id)
    {
        $id = (int) $id;
        SalonModel::model()->updateByPk($id, array('salon_read' => 1));

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/salon');
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        SalonModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/salon');
    }
}