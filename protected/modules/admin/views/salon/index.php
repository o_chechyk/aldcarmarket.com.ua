<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Авто</th>
                        <th>Телефон</th>
                        <th>Час і дата</th>
                        <th>Ім'я</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($a_salon as $item) { ?>
                        <tr>
                            <td><a href="/admin/car/edit/<?php print $item['car']['car_id']; ?>" target="_blank"><?php print $item['car']['car_name']; ?></a></td>
                            <td><?php print $item['salon_phone']; ?></td>
                            <td><?php print $item['salon_time'] . ' ' . $item['salon_date']; ?></td>
                            <td><?php print $item['salon_name']; ?></td>

                            <td>
                                <a href="/admin/salon/read/<?php print $item['salon_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['salon_read']) { ?>default<?php } else { ?>success<?php } ?>">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </a>
                                <a href="/admin/salon/delete/<?php print $item['salon_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->