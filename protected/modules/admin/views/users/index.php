<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Користувачі</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/users/edit/0" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover" id="data-table">
                <thead>
                    <tr>
                        <th>Ім'я</th>
                        <th>Роль</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) { ?>
			<?php
			    if($user['user_status'] == 0) { // disabled user
				$class = "user-disabled";
			    } else {
				$class = "";
			    }
			?>
                        <tr class="<?=$class?>">
                            <td><?php print $user['user_login']; ?></td>
                            <td>
				<?php
				    $role = array();
				    if($user['user_role'] & 1) {
					$role[] = "Administrator";
				    }
				    if($user['user_role'] & 2) {
					$role[] = "Super user";
				    }
				    echo implode(",",$role);
				?>
			    </td>
                            <td>
                                <a href="/admin/users/edit/<?=$user['user_id']?>">
                                    <button type="button" class="btn btn-info" title="Редагувати">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
				<button class="btn btn-danger" type="button" title="Видалити"
				    onclick="deleteUser(<?=$user['user_id']?>,'<?=$user['user_login']?>')">
				    <i class="fa fa-times"></i>
				</button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->