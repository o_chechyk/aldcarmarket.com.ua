<?php

class RulesModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'rules';
    }

    public function primaryKey()
    {
        return 'topic';
    }
}
