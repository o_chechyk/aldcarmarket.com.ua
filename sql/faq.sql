drop table if exists faq;
create table faq (
    faq_id int(11) auto_increment,
    faq_question text not null,
    faq_answer text not null,
    faq_order int(5) not null default 0,
    faq_status tinyint(1) not null default 1,
    primary key (faq_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;