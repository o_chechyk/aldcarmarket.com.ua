<?php

class SmartController extends AController
{
    public function actionIndex()
    {
        $a_smart = SmartModel::model()->findAll();
        $h1      = 'Розумні фільтри';

        $this->render('index', array('a_smart' => $a_smart, 'h1' => $h1));
    }

    public function actionOrder()
    {
        if (isset($_POST['data']))
        {
            foreach($_POST['data'] as $key => $value)
            {
                $id     = (int) $key;
                $o_smart = SmartModel::model()->findByPk($id);
                $o_smart->smart_order = $value;
                $o_smart->save();
            }

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/smart');
    }

    public function actionStatus($id)
    {
        $id      = (int) $id;
        $o_smart = SmartModel::model()->findByPk($id);
        $status  = $o_smart->smart_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_smart->smart_status = $new_status;
        $o_smart->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/smart');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_smart = new SmartModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_smart->$key = $value;
            }

            $c_smart->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/smart');
            exit;
        }

        $h1 = 'Редагування розумних фільтрів';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id      = (int) $id;
        $o_smart = SmartModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_smart->$key = $value;
            }

            $o_smart->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/smart');
            exit;
        }

        $h1 = 'Редагування розумних фільтрів';

        $this->render('form', array('o_smart' => $o_smart, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        SmartModel::model()->deleteByPk($id);
        CarSmartModel::model()->deleteAllByAttributes(array('carsmart_smart_id' => $id));

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/smart');
    }
}