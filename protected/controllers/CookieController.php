<?php

class CookieController extends Controller
{
    public function actionSet($mode) {

	if(! isset(Yii::app()->session['code']) ) {
	    $code = md5(time().rand(10000000, 99999999));
	    Yii::app()->session['code'] = $code;
	}

	if($mode == "yes") {

	    if(! isset(Yii::app()->request->cookies['tcid'])) { // New visitor?
		// Set cookie here
		$cookie = new CHttpCookie('tcid', $code);
		$cookie->expire = time()+60*60*24*180; // 6 months
		Yii::app()->request->cookies['tcid'] = $cookie;

		$cookie_db = new CookieModel;
		$cookie_db->cookie_code = $code;
		$cookie_db->save();
	    }

	} // if($mode == "yes")

    }

    public function actionRules( $topic ) {
	$rules  = RulesModel::model()->findByPk( $topic );
	if( $rules ) {
	    echo $rules->rules;
	} else {
	    echo "";
	}
    }

}
