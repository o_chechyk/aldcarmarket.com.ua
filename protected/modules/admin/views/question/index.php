<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Ім`я</th>
                        <th>Контакт</th>
                        <th>Надійшло</th>
                        <th>Відреагували</th>
                        <th>Відповідь</th>
                        <th>Відповів</th>
                        <th class="col-lg-3"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($a_question as $item) { ?>
                        <tr>
                            <td><?php print $item['question_name']; ?></a></td>
                            <td><?php print $item['question_email']; ?></td>
                            <td><?php print date('H:i d.m.Y', $item['question_date']); ?></td>
                            <td><?php if($item['question_read']) print date('H:i d.m.Y', $item['question_read']); ?></td>
                            <td><?php
					if($item['question_reply']) {
						if( $pos=mb_strpos($item['question_reply'], "повідомляємо:") ) {
							echo mb_substr($item['question_reply'], ($pos+13));
						} else {
							echo $item['question_reply'];
						}
					}
				?></td>
                            <td><?php if( $item->q_manager ) echo $item->q_manager->user_login; ?></td>
                            <td>
                                <a href="/admin/question/read/<?php print $item['question_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['question_read']) { ?>default<?php } else { ?>success<?php } ?>">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </a>
                                <a href="/admin/question/answer/<?php print $item['question_id']; ?>">
                                    <button type="button" class="btn btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="/admin/question/delete/<?php print $item['question_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->