<?php

class FormController extends Controller
{
    public function actionSalon()
    {
        if (isset($_POST['salon']))
        {
		if( ! $this->checkTimeout() ) {
			Yii::app()->user->setFlash('error', 'Неможливо відправити. Спробуйте за хвилину.');
			$this->redirect($_SERVER['HTTP_REFERER']);
		}

            $salon = $_POST['salon'];

            if (!empty($salon['question_email']) &&
                $salon['question_email'] != '(___) ___ __ __' &&
                !empty($salon['question_name']) )
            {
		$c_salon = new QuestionModel();

                foreach ($salon as $key => $value)
                {
                    $c_salon->$key = $value;
                }

		$c_salon->question_date = time();
                $c_salon->save();

		$mail_to = $this->contacts['contacts_email'];

                $subject = "Замовлено зворотній дзвінок на сайті " . $_SERVER['HTTP_HOST'];
                $message = "Замовлено зворотній дзвінок на сайті " . $_SERVER['HTTP_HOST'] . "<br>" .
                            "Ім'я - " . $salon['question_name'] . "<br>" .
                            "Телефон - " . $salon['question_email'] . "<br>";

                $header  = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
                $header .= "Content-type:text/html; charset=utf-8\r\n";

                mail($mail_to, $subject, $message, $header);

                Yii::app()->user->setFlash('success', 'Дякуємо, наш менеджер зателефонує найближчим часом');
            }
            else
            {
                Yii::app()->user->setFlash('error', 'Заповнені не всі дані');
            }
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }


    public function actionAsk()
    {
        if (isset($_POST['ask']))
        {
		if( ! $this->checkTimeout() ) {
			Yii::app()->user->setFlash('error', 'Неможливо відправити. Спробуйте за хвилину.');
			$this->redirect($_SERVER['HTTP_REFERER']);
		}

	    $f_ask = new AskForm();
	    $f_ask->attributes = $_POST['ask'];
	    $f_ask->verifyCode = $_POST['g-recaptcha-response'];

	    if( $f_ask->validate() ) {

	            $ask = $_POST['ask'];

	            if (!empty($ask['ask_email']) &&
	                !empty($ask['ask_text']))
	            {
	                $c_ask = new AskModel();

	                foreach ($ask as $key => $value)
	                {
	                    $c_ask->$key = $value;
	                }

	                $c_ask->ask_date = time();
	                $c_ask->save();

	                $o_car = CarModel::model()->findByPk($ask['ask_car_id']);

	                if (isset($o_car['car_name']))
	                {
	                    $car_name = $o_car['car_name'];
	                }
	                else
	                {
	                    $car_name = '';
	                }

			$mail_to = $this->contacts['contacts_email'];

	                $subject = 'Надійшло нове питання по авто на сайті ' . $_SERVER['HTTP_HOST'];
	                $message = 'Надійшло нове питання по авто на сайті ' . $_SERVER['HTTP_HOST'] . '<br/>
	                            Авто - ' . $car_name . '<br>
	                            Email - ' . $ask['ask_email'] . '<br>
	                            Питання - ' . $ask['ask_text']. '<br>'.
	                            "Посилання на авто - <a href=\"https://".Yii::app()->request->getServerName().
				    "/automobile/show/".$ask['ask_car_id']."\">https://".Yii::app()->request->getServerName().
				    "/automobile/show/".$ask['ask_car_id']."</a>";


	                $header  = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
	                $header .= "Content-type:text/html; charset=utf-8\r\n";

	                mail($mail_to, $subject, $message, $header);

	                Yii::app()->user->setFlash('success', 'Дякуємо за запитання, наш менеджер зв’яжеться з Вами найближчим часом');
	            }
	            else
	            {
	                Yii::app()->user->setFlash('error', 'Заповнені не всі дані');
	            }

	    } // if( $f_ask->validate() )


        } // if (isset($_POST['ask']))

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionQuestion() {

        if ( isset($_POST['question']) ) {
		if( ! $this->checkTimeout() ) {
			Yii::app()->user->setFlash('error', 'Неможливо відправити. Спробуйте за хвилину.');
			$this->redirect($_SERVER['HTTP_REFERER']);
		}

	    $f_question = new QuestionForm();
	    $f_question->attributes = $_POST['question'];
	    $f_question->verifyCode = $_POST['g-recaptcha-response'];

	    if( $f_question->validate() ) {

		$question = $_POST['question'];

                $c_question = new QuestionModel();

                foreach ($question as $key => $value)
                {
                    $c_question->$key = $value;
                }

                $c_question->question_date = time();
                $c_question->save();

		$mail_to = $this->contacts['contacts_email'];

                $subject = 'Надійшло нове повідомлення через форму зворотнього звязку на сайті ' . $_SERVER['HTTP_HOST'];
                $message = 'Надійшло нове повідомлення через форму зворотнього звязку на сайті ' . $_SERVER['HTTP_HOST'] . '<br/>
                            Ім`я - ' . $question['question_name'] . '<br>
                            Email - ' . $question['question_email'] . '<br>
                            Питання - ' . $question['question_text'];

                $header  = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
                $header .= "Content-type:text/html; charset=utf-8\r\n";

                mail($mail_to, $subject, $message, $header);

                Yii::app()->user->setFlash('success', "Дякуємо за зворотній зв'язок, наш менеджер зв’яжеться з Вами найближчим часом");
	    } else {
		Yii::app()->user->setFlash('error', 'Заповнені не всі дані');
	    }

	} // if ( isset($_POST['question']) )

        $this->redirect($_SERVER['HTTP_REFERER']);
    }


	public function checkTimeout() {
		// Check session ID to sort out bots
		$sid = session_id();
		if($sid == "") {
			return false;
		}

		$redis = new Redis();
		if( ! $redis->connect('localhost', 6379) ) {
		    // Connect fail
		    echo "Redis connection fail";
		    exit;
		}
		if( ! $redis->auth('Y308KXDC58EW0bR4wt2OvL9d1xdg0pQsaFgP9IiW+HhYvZ+iIH14WTAgY8rI7SzsDPBCjuCJu+91tRPb') ) {
		    //Auth fail
		    $redis->close();
		    echo "Redis auth fail";
		    exit;
		}

		if( $redis->get( $sid ) ) {
			$res = false;
		} else {
			$res = true;
		}

		$redis->setEx($sid, 10, 'Here');

		return $res;
	}

}
