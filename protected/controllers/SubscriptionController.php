<?php

class SubscriptionController extends Controller
{
    public function actionSubscribe() {

	if(isset($_POST['email']) and filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

	    $email = $_POST['email'];

	    $subscription = SubscriptionModel::model()->findByAttributes(array('subscription_email' => $email));
	    if( $subscription ) {
		echo "Вибачте, адреса '".$email."' вже підписана на нашу розсилку";
		return;
	    }

	} else {
	    echo "Задано невірну адресу електронної пошти: '".$_POST['email']."'";
	    return;
	}

	$code = md5(time().rand(10000000, 99999999));

	try {
	    $new_subscription = new SubscriptionModel;
	    $new_subscription->subscription_code = $code;
	    $new_subscription->subscription_email = $email;
	    $new_subscription->save();
	} catch(Exception $e) {
	    echo "Помилка збереження даних:\n".$e->getMessage();
	    return;
	}

	$mail_to = $email;

	$subject = 'Вітаємо з ALD Automotive';
	$message = "Вітаємо!<br>".
		    "<p>Щойно Ви підписалися на розсилку оновлень сайту ".$_SERVER['HTTP_HOST'].".</p>".
		    "<p>Тепер Ви будете дізнаватися про всі зміни наших цін та акційних пропозицій, щойно вони відбудуться!</p>".
		    "<p>Для підтвердження підписки, будь-ласка, перейдіть за посиланням <a href=\"https://".
		    $_SERVER['HTTP_HOST']."/subscription/confirm?code=".$code."\">https://".$_SERVER['HTTP_HOST'].
		    "/subscription/confirm?code=".$code."</a></p>".
		    "<p style=\"font-size:75%;color:grey;font-style:italic\">Ви отримали цього листа, ".
		    "тому що хтось заповнив форму підписки на сайті ".$_SERVER['HTTP_HOST']."<br>".
		    "Якщо це помилка, просто проігноруйте цей лист.</p>";

	$header  = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
	$header .= "Content-type:text/html; charset=utf-8\r\n";

	mail($mail_to, $subject, $message, $header);

	echo "Ok";

    }

    public function actionConfirm() {
	if(isset($_GET['code']) and $_GET['code'] != "") {
	    $code = $_GET['code'];
	}

	$subscription = SubscriptionModel::model()->findByAttributes(array('subscription_code' => $code));

	if( $subscription ) {

	    try {
		$subscription->subscription_status = 1;
		$subscription->save();
//		$this->render( 'success' );
		Yii::app()->user->setFlash('success', "Підписку підтверджено<br>Дякуємо");
	    } catch(Exception $e) {
		Yii::app()->user->setFlash('error', "Не вдалося підтвердити підписку. ".
						    "Помилка збереження даних:<br>".$e->getMessage());
//		$this->render('error', array('msg' => "Помилка збереження даних:\n".$e->getMessage()));
	    }

	} else {
	    Yii::app()->user->setFlash('error', "Не вдалося підтвердити підписку.<br>Нажаль, Ваш код підписки не знайдено в базі");
//	    $this->render('error', array('msg' => "Нажаль, Ваш код підписки не знайдено в базі"));
	}

	$this->render('confirm');
    }

    public function actionCancel() {
	if(isset($_POST['id']) and preg_match("/^\d+$/",$_POST['id'])) {
	    $subscription = SubscriptionModel::model()->findByPk( $_POST['id'] );
	} else if(isset($_GET['code']) and $_GET['code'] != "") {
	    $subscription = SubscriptionModel::model()->findByAttributes( array('subscription_code' => $_GET['code']) );
	    $email = $subscription['subscription_email'];
	}

	if( $subscription ) {
	    $subscription->delete();
	}

	if( isset($_GET['code']) ) {
	    $this->render('cancel', array('email' => $email));
	}
    }


    public function actionCheck() {
	$sql = "select ".
		    "count(*) cnt ".
		"from ".
		    "logCar ".
		"where ".
		    "dateOperation>(select min(subscription_last_seen) from subscription where subscription_status=1) ".
		    "and ((operation=1 and new_status=1) ".
			"or (operation=2 and old_price!=new_price and new_phase_id=3 and new_status=1) ".
//			"or (operation=2 and old_phase_id!=new_phase_id and (old_phase_id=3 or new_phase_id=3) and new_status=1) ".
			")";
	$res = Yii::app()->db->createCommand($sql)->queryRow();
	echo $res['cnt'];
    }


    public function actionSendmail() {

	if( preg_match("/test\./", $_SERVER['HTTP_HOST'])  !== 0 ) {
		return; // Do not send updates from test site
	}

	$subscriptions = SubscriptionModel::model()->findAllByAttributes( array('subscription_status' => 1) );
	$length = count( $subscriptions );
	if( $length ) {
	    $length++; // one step for each of subscribers, plus one for loading all the data
	} else {
	    return;
	}

	// Set headers for user's progress bar to work
	header('Content-type: text/plain; charset=utf8');
	header("Content-length: ".$length);
	header("Content-range: 0");
	header('Content-Disposition: inline');

	// Fix the current timestamp
	$timestamp = time();

	// New automobiles
	$sql = "select ".
		    "dateOperation,".
		    "car_id,".
		    "new_name name,".
		    "new_year year,".
		    "e.engine_name engine,".
//		    "c.color_name color,".
		    "new_mileage mileage,".
		    "new_price price ".
		"from ".
		    "logCar l ".
//		    "left join color c on c.color_id=l.new_color_id ".
		    "left join engine e on e.engine_id=l.new_engine_id ".
		"where ".
		    "dateOperation>(select min(subscription_last_seen) from subscription) ".
		    "and operation=1 ".
		    "and new_status=1";
	$new_ones = Yii::app()->db->createCommand($sql)->queryAll();

/*
	// Cache car ID's for image loading
	$car_images = array();
	foreach($new_ones as $car) {
	    if( ! isset($car_images[$car['car_id']]) ) {
		$car_images[$car['car_id']] = '';
	    }
	}
*/

	// Price changed
	$sql = "select ".
		    "car_id,".
		    "dateOperation,".
		    "new_name name,".
		    "new_year year,".
		    "e.engine_name engine,".
//		    "c.color_name color,".
		    "old_price,".
		    "new_price,".
		    "new_mileage mileage ".
		"from ".
		    "logCar l ".
//		    "left join color c on c.color_id=l.new_color_id ".
		    "left join engine e on e.engine_id=l.new_engine_id ".
		"where ".
		    "dateOperation>(select min(subscription_last_seen) from subscription) ".
		    "and operation=2 ".
		    "and old_price!=new_price ".
		    "and new_phase_id=3 ".
		    "and new_status=1";
	$price_changed = Yii::app()->db->createCommand($sql)->queryAll();

/*
	foreach($price_changed as $car) {
	    if( ! isset($car_images[$car['car_id']]) ) {
		$car_images[$car['car_id']] = '';
	    }
	}
*/
/*
	// State changed
	$sql = "select ".
		    "car_id,".
		    "dateOperation,".
		    "new_name name,".
		    "new_year year,".
		    "e.engine_name engine,".
//		    "c.color_name color,".
		    "new_phase_id state_id,".
		    "np.phase_name state,".
		    "new_price price,".
		    "new_mileage mileage ".
		"from ".
		    "logCar l ".
//		    "left join color c on c.color_id=l.new_color_id ".
		    "left join engine e on e.engine_id=l.new_engine_id ".
		    "left join phase op on op.phase_id=l.old_phase_id ".
		    "left join phase np on np.phase_id=l.new_phase_id ".
		"where ".
		    "dateOperation>(select min(subscription_last_seen) from subscription) ".
		    "and operation=2 ".
		    "and old_phase_id!=new_phase_id ".
		    "and (old_phase_id=3 or new_phase_id=3) ".
		    "and new_status=1";
	$state_changed = Yii::app()->db->createCommand($sql)->queryAll();
*/
/*
	foreach($state_changed as $car) {
	    if( ! isset($car_images[$car['car_id']]) ) {
		$car_images[$car['car_id']] = '';
	    }
	}
*/
/*
	// Load image URL's for each cached car
	foreach($car_images as $car_id => $url) {
	    $sql = "select carimage_image_id,image_url ".
		    "from carimage c left join image i on i.image_id=c.carimage_image_id ".
		    "where carimage_car_id=".$car_id." order by carimage_order limit 1";
	    $res = Yii::app()->db->createCommand($sql)->queryRow();
	    if($res['image_url'] != '') {
		$car_images[$car_id] = ImageIgosja::resize($res['carimage_image_id'], 90, 68,1,1);
	    } else {
		$car_images[$car_id] = '/assets/img/product-grid-placeholder.jpg';
	    }
	}
*/

	echo "1"; // First step done (loading all data), send signal to progress bar
	ob_flush();
	flush();

	foreach($subscriptions as $subscription) {
	    $body = $new_section = $price_section = $state_section = ""; // parts of our message

	    foreach($new_ones as $car) {
		if($car['dateOperation'] > $subscription['subscription_last_seen']) {
		    $new_section .= "<tr>\n".
//				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\"><img src=\"https://".$_SERVER['HTTP_HOST'].$car_images[$car['car_id']]."\"></a></td>\n".
				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\">".htmlspecialchars($car['name'])."</a></td>\n".
				    "<td>".$car['year']."</td>\n".
				    "<td>".number_format(($car['mileage']*1000),0,'.',' ')."</td>\n".
//				    "<td>".$car['color']."</td>\n".
				    "<td>".$car['engine']."</td>\n".
				    "<td>".number_format($car['price'],0,'.',' ')."</td>\n".
				    "</tr>\n";
		} else {
		    continue;
		}
	    } // foreach($new_ones as $car)

	    foreach($price_changed as $car) {
		if($car['dateOperation'] > $subscription['subscription_last_seen']) {
		    $price_section .= "<tr>\n".
//				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\"><img src=\"https://".$_SERVER['HTTP_HOST'].$car_images[$car['car_id']]."\"></a></td>\n".
				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\">".htmlspecialchars($car['name'])."</a></td>\n".
				    "<td>".$car['year']."</td>\n".
				    "<td>".number_format(($car['mileage']*1000),0,'.',' ')."</td>\n".
//				    "<td>".$car['color']."</td>\n".
				    "<td>".$car['engine']."</td>\n".
				    "<td><span class=\"old\">".number_format($car['old_price'],0,'.',' ')."</span> ".number_format($car['new_price'],0,'.',' ')."</td>\n".
				    "</tr>\n";
		} else {
		    continue;
		}
	    } // foreach($new_ones as $car)

/*
	    foreach($state_changed as $car) {
		if($car['dateOperation'] > $subscription['subscription_last_seen']) {
		    switch( $car['state_id'] ) {
			case 1:
			    $state = "Проданий";
			    break;
			case 2:
			    $state = "переведений в резерв";
			    break;
			case 3:
			    $state = "повернутий у продаж";
			    break;
			default:
			    $state = $car['state'];
		    }
		    $state_section .= "<tr>\n".
//				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\"><img src=\"https://".$_SERVER['HTTP_HOST'].$car_images[$car['car_id']]."\"></a></td>\n".
				    "<td><a href=\"https://".$_SERVER['HTTP_HOST']."/automobile/show/".$car['car_id']."\">".htmlspecialchars($car['name'])."</a></td>\n".
				    "<td>".$car['year']."</td>\n".
				    "<td>".number_format(($car['mileage']*1000),0,'.',' ')."</td>\n".
//				    "<td>".$car['color']."</td>\n".
				    "<td>".$car['engine']."</td>\n".
				    "<td>".number_format($car['price'],0,'.',' ')."</td>\n".
				    "<td>".htmlspecialchars($state)."</a></td>\n".
				    "</tr>\n";
		} else {
		    continue;
		}
	    } // foreach($new_ones as $car)
*/

	    if($new_section != "") {
		$new_section = "<div class=\"section\">\n".
				"<h2>Ми оновили асортимент авто на сайті aldcarmarket.com.ua! Поспішайте оглянути новинки!</h2>\n".
				"<table><thead>\n".
				"<tr>\n".
//				    "<td>Фото</td>\n".
				    "<td>Назва</td>\n".
				    "<td>Рік випуску</td>\n".
				    "<td>Пробіг</td>\n".
//				    "<td>Колір</td>\n".
				    "<td>Двигун</td>\n".
				    "<td>Ціна</td>\n".
				"</tr></thead><tbody>\n".
				$new_section.
				"</tbody></table>\n".
				"</div>\n";
	    }

	    if($price_section != "") {
		$price_section = "<div class=\"section\">\n".
				"<h2>Ми оновили ціни на авто на сайті aldcarmarket.com.ua! Поспішайте оглянути гарячі пропозиції!</h2>\n".
				"<table><thead>\n".
				"<tr>\n".
//				    "<td>Фото</td>\n".
				    "<td>Назва</td>\n".
				    "<td>Рік випуску</td>\n".
				    "<td>Пробіг</td>\n".
//				    "<td>Колір</td>\n".
				    "<td>Двигун</td>\n".
				    "<td>Ціна</td>\n".
				"</tr></thead><tbody>\n".
				$price_section.
				"</tbody></table>\n".
				"</div>\n";
	    }

/*
	    if($state_section != "") {
		$state_section = "<div class=\"section\">\n".
				"<h2>Змінено статус автомобілів</h2>\n".
				"<table><thead>\n".
				"<tr>\n".
//				    "<td>Фото</td>\n".
				    "<td>Назва</td>\n".
				    "<td>Рік випуску</td>\n".
				    "<td>Пробіг</td>\n".
//				    "<td>Колір</td>\n".
				    "<td>Двигун</td>\n".
				    "<td>Ціна</td>\n".
				    "<td>Статус</td>\n".
				"</tr></thead><tbody>\n".
				$state_section.
				"</tbody></table>\n".
				"</div>\n";
	    }
*/

	    $body = $new_section.$price_section.$state_section;

	    if($body != "") {

		$body = "<html>\n".
			"<style>\n".
			"body{font: 16px/26px Helvetica,Helvetica Neue,Arial,sans-serif;color: #222;font-size: 14px;}\n".
			".header{font-size:120%;text-align:center;display:table-cell;width:60%;}\n".
			".logo{display:table-cell;width:40%;}\n".
			".section{margin-top:40px;background-color:#f9ffff;padding:20px;}\n".
			"h2{margin:0 0 20px 0;}\n".
			"table{border:1px solid #ddd;width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;}\n".
			"thead tr td{text-align:center;font-size:90%;font-weight:bold;}\n".
			"tbody tr:nth-of-type(2n+1){background-color: #f9f9f9;}\n".
			"td{border:1px solid #ddd;padding:8px;vertical-align:top;}\n".
			".old{color:grey;text-decoration:line-through;}\n".
			".footer{font-size:80%;color:grey;font-style:italic;padding:10px 20px;text-align:center;}\n".
			".foot-welcome{font-size:120%;font-weight:bold;margin:20px 0 10px 0;text-align:center;}\n".
			"</style><body>\n".
			"<div class=\"logo\" width=\"40%\"><img src=\"https://".$_SERVER['HTTP_HOST']."/assets/img/logo.png\" width=\"100%\" alt=\"ALD Automotive\"></div>\n".
			"<div class=\"header\"><h1>Доброго дня!</h1>Маємо для Вас новини про оновлення на сайті ".$_SERVER['HTTP_HOST']."</div>\n".
			$body.
			"<div class=\"foot-welcome\">Чекаємо Вас на aldcarmarket.com.ua!</div>".
			"<div class=\"footer\">Якщо Ви не бажаєте надалі отримувати цю розсилку, \n".
			"Ви можете скасувати її, просто відвідавши сторінку за \n".
			"<a href=\"https://".$_SERVER['HTTP_HOST']."/subscription/cancel?code=".$subscription['subscription_code']."\">цим посиланням</a></div>\n".
			"</body></html>";

		$subject = "Оновлення сайту " . $_SERVER['HTTP_HOST'];
		$header  = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
		$header .= "Content-type:text/html; charset=utf-8\r\n";
		if( mail($subscription['subscription_email'], $subject, $body, $header) ) {
		    // Update timestamp
		    $sql = "update subscription set subscription_last_seen=from_unixtime(".$timestamp.") where subscription_id=".$subscription['subscription_id'];
		    Yii::app()->db->createCommand($sql)->execute();
		}

	    } // if($body != "")

	    echo "1"; // One more step done, send signal to progress bar
	    ob_flush();
	    flush();

	} // foreach($subscriptions as $subscription)

    }

}
