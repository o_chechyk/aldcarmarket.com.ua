<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/car" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $user = UserModel::model()->findByPk( Yii::app()->user->id ); ?>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" enctype="multipart/form-data">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab">Загальна інформація</a></li>
		<?php /* if($user['user_role'] & 2) { */ ?>
<!--		<li><a href="#photo" data-toggle="tab">Фото</a></li> -->
		<?php /* } */ ?>
                <li><a href="#seo" data-toggle="tab">SEO</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="main">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">Назва</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_name]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_name']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">ЧПУ</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_url]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_url']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Статус</td>
                                <td>
                                    <select class="form-control" name="data[car_phase_id]">
                                        <?php foreach ($a_phase as $item) { ?>
                                            <option
                                                value="<?php print $item['phase_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_phase_id'] == $item['phase_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['phase_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Відображати в слайдері</td>
                                <td>
                                    <select name="data[car_slide]" class="form-control">
                                        <option value="0">Ні</option>
                                        <option
                                            value="1"
                                            <?php if (isset ($o_car) && $o_car['car_slide'] == 1) { ?>
                                                selected
                                            <?php } ?>
                                        >Так</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Марка</td>
                                <td>
                                    <select class="form-control" name="data[car_producer_id]" id="producer-select">
                                        <option value="0">-</option>
                                        <?php foreach ($a_producer as $item) { ?>
                                            <option
                                                value="<?php print $item['producer_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_producer_id'] == $item['producer_id']) { $producer = $item['producer_id']; ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['producer_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Модель</td>
                                <td>
                                    <select class="form-control" name="data[car_model_id]" id="model-select">
                                        <option data-producer="0">-</option>
                                        <?php foreach ($a_model as $item) { ?>
                                            <option
                                                value="<?php print $item['model_id']; ?>"
                                                data-producer="<?php print $item['model_producer_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_model_id'] == $item['model_id']) { ?>
                                                    selected
                                                <?php } ?>
                                                <?php if (!isset($producer) || $producer != $item['model_producer_id']) { ?>
                                                    style="display:none;"
                                                <?php } ?>
                                            >
                                                <?php print $item['model_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Код</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_sku]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_sku']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Ціна, грн</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_price]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_price']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Стара ціна, грн<br />(0 якщо акції немає)</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_price_old]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_price_old']; } else { print 0; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Номер кузова</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_number]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_number']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Колір</td>
                                <td>
                                    <select class="form-control" name="data[car_color_id]">
                                        <?php foreach ($a_color as $item) { ?>
                                            <option
                                                value="<?php print $item['color_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_color_id'] == $item['color_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['color_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Рік випуску</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_year]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_year']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Двигун</td>
                                <td>
                                    <select class="form-control" name="data[car_engine_id]">
                                        <?php foreach ($a_engine as $item) { ?>
                                            <option
                                                value="<?php print $item['engine_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_engine_id'] == $item['engine_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['engine_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Об`єм двигуна, куб. см</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_engine_capacity]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_engine_capacity']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Потужність, к/с</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_power]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_power']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Трансмісія</td>
                                <td>
                                    <select class="form-control" name="data[car_transmission_id]">
                                        <?php foreach ($a_transmission as $item) { ?>
                                            <option
                                                value="<?php print $item['transmission_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_transmission_id'] == $item['transmission_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['transmission_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Привід</td>
                                <td>
                                    <select class="form-control" name="data[car_gear_id]">
                                        <?php foreach ($a_gear as $item) { ?>
                                            <option
                                                value="<?php print $item['gear_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_gear_id'] == $item['gear_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['gear_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Пробіг, тыс.км</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[car_mileage]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['car_mileage']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Тип кузова</td>
                                <td>
                                    <select class="form-control" name="data[car_body_id]">
                                        <?php foreach ($a_body as $item) { ?>
                                            <option
                                                value="<?php print $item['body_id']; ?>"
                                                <?php if (isset ($o_car) && $o_car['car_body_id'] == $item['body_id']) { ?>
                                                    selected
                                                <?php } ?>
                                            >
                                                <?php print $item['body_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Опис</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[car_description]"
                                        rows="3"><?php if (isset ($o_car)) { print $o_car['car_description']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Розумні фільтри</td>
                                <td>
                                    <?php foreach ($a_smart as $item) { ?>
                                        <input
                                            type="checkbox"
                                            name="smart[]"
                                            value="<?php print $item['smart_id']; ?>"
                                            <?php if (isset($o_car['smart'])) { ?>
                                                <?php foreach ($o_car['smart'] as $smart) { ?>
                                                    <?php if ($smart['carsmart_smart_id'] == $item['smart_id'] && 1 == $smart['carsmart_status']) { ?>
                                                        checked
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        >
                                        <?php print $item['smart_name']; ?><br />
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Додаткові характеристики</td>
                                <td>
                                    <?php foreach ($a_characteristic as $item) { ?>
                                        <input
                                            type="checkbox"
                                            name="value[]"
                                            value="<?php print $item['characteristic_id']; ?>"
                                            <?php if (isset($a_carchar)) { ?>
                                                <?php foreach ($a_carchar as $char) { ?>
                                                    <?php if ($char['carcharacteristic_characteristic_id'] == $item['characteristic_id']) { ?>
                                                        checked
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        >
                                        <?php print $item['characteristic_name']; ?>
                                        <br />
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Технічний стан</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[car_condition]"
                                        rows="3"><?php if (isset ($o_car) and isset($o_car['car_condition'])) { print $o_car['car_condition']; } else {print "";}?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="photo">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td colspan="3">
                                    <input type="file" name="image[]" multiple="multiple" />
                                </td>
                            </tr>
                            <tr>
                                <th class="col-lg-6">Фото</th>
                                <th class="col-lg-5">Порядок</th>
                                <th class="col-lg-1"></th>
                            </tr>
                            <?php if (isset($o_car)) { ?>
                                <?php foreach ($o_car['image'] as $item) { ?>
                                    <tr>
                                        <td>
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php ImageIgosja::resize($item['image']['image_id'], 180, 135, 0); ?>" />
                                            </a>
                                        </td>
                                        <td>
                                            <input
                                                class="form-control"
                                                name="image[<?php print $item['carimage_id']; ?>]"
                                                size="1"
                                                type="text"
                                                value="<?php print $item['carimage_order']; ?>"
                                            />
                                        </td>
                                        <td><a href="?image=<?php print $item['carimage_id']; ?>"><i class="fa fa-times"></i></a></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="seo">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">SEO title</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[seo_title]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['seo_title']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO description</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_description]"><?php if (isset ($o_car)) { print $o_car['seo_description']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO keywords</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_keywords]"><?php if (isset ($o_car)) { print $o_car['seo_keywords']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO h1</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[seo_h1]"
                                        type="text"
                                        value="<?php if (isset ($o_car)) { print $o_car['seo_h1']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO text</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_text]"><?php if (isset ($o_car)) { print $o_car['seo_text']; } ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $('#producer-select').on('change', function()
    {
        var producer = $(this).val();
        $('#model-select').val(0);
        var option_list = $('#model-select option');

        option_list.hide();

        for (var i=0; i<option_list.length; i++)
        {
            var data_producer = $(option_list[i]).data('producer');

            if (data_producer == producer || 0 == data_producer)
            {
                $(option_list[i]).show();
            }
        }
    });
</script>