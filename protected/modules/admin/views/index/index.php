<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Адміністративний розділ</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
</div>
<!-- /.row -->
<div class="row">
    <?php if (0 != $salon) { ?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-car fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php print $salon; ?></div>
                            <div>Заявки на візит!</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/salon">
                    <div class="panel-footer">
                        <span class="pull-left">Детальніше</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <?php if (0 != $ask) { ?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php print $ask; ?></div>
                            <div>Нові питання!</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/ask">
                    <div class="panel-footer">
                        <span class="pull-left">Детальніше</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <?php if (0 != $question) { ?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-question fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php print $question; ?></div>
                            <div>Зворотній звязок!</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/question">
                    <div class="panel-footer">
                        <span class="pull-left">Детальніше</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
</div>