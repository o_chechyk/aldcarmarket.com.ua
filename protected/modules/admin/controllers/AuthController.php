<?php

class AuthController extends AuController
{
    public function beforeAction($action)
    {
        if (!Yii::app()->user->isGuest)
        {
            $this->redirect('/admin/index');
        }
        else
        {
            return true;
        }
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionLogin()
    {
        if (isset($_POST['data']))
        {
            $login      = $_POST['data']['login'];
            $password   = $_POST['data']['password'];

            $identity   = new UserIdentity($login, $password);

            if ($identity->authenticate())
            {
//		$connection = Yii::app()->db->createCommand("SET @user_id=".$identity->getId())->execute();
                Yii::app()->user->login($identity);
            }
            else
            {
                Yii::app()->user->setFlash('error', 'Неправильная комбинация логин/пароль');
            }
        }

        $this->redirect('/admin/auth');
    }

    public function actionSignUp()
    {
//        $this->render('signup');
    }

    public function actionUserAdd()
    {

	$this->redirect('/admin/auth');
	return;

/*
        if (isset($_POST['data']))
        {
            $login      = $_POST['data']['login'];
            $password   = $_POST['data']['password'];

            if ('' != $login &&
                '' != $password)
            {
                $c_user         = new UserModel();
                $check          = $c_user->findAllByAttributes(array('user_login' => $login));
                $count_check    = count($check);

                if (0 == $count_check)
                {
                    $c_user->user_login         = $login;
                    $c_user->user_password      = $c_user->hashPassword($password);
                    $c_user->user_role		= 1;
                    $c_user->user_created       = time();
                    $c_user->save();

                    $identity = new UserIdentity($login, $password);

                    if ($identity->authenticate())
                    {
                        Yii::app()->user->login($identity);
                    }
                    else
                    {
                        Yii::app()->user->setFlash('error', 'Произошла ошибка. Попробуйте войти еще раз.');
                    }
                }
                else
                {
                    Yii::app()->user->setFlash('error', 'Такой пользователь уже зарегистрирован.');
                }
            }
            else
            {
                Yii::app()->user->setFlash('error', 'Поля заполнены неправильно.');
            }
        }

        $this->redirect('/admin/auth/signup');
*/
    }
}