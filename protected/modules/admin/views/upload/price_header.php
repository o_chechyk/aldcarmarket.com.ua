<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Завантаження даних з переоцінки</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

	<div class="logFilters">
	    <div class="caption">
		Файл для завантаження
	    </div>
	    <div class="body">
		<div class="section">
		<form id="formCSV" type="POST" enctype="multipart/form-data">
		    <div class="caption">
			Оберіть файл
		    </div>
		    <div class="body">
			    <input type="file" name="priceCSV" id="targetCSV">
		    </div>
		</form>
		</div>
		<div class="footer">
		    <button class="btn btn-default text-center" onclick="submitCSV('price')">Завантажити</buton>
		    <span id="spinner" class="hidden"><i class="fa fa-spinner"></i></span>
		</div>
	    </div>
	</div>

	<div class="dataTable_wrapper" id="CSV-content">
	</div>    <!-- /.dataTable_wrapper -->

    </div>    <!-- /.col-lg-12 -->
</div>    <!-- /.row -->