<?php

class QuestionModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'question';
    }

    public function primaryKey()
    {
        return 'question_id';
    }

    public function relations()
    {
        return array
        (
            'q_manager' => array(self::HAS_ONE, 'UserModel', array('user_id' => 'question_manager'))
        );
    }
}
