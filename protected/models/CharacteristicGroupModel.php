<?php

class CharacteristicGroupModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'characteristicgroup';
    }

    public function primaryKey()
    {
        return 'characteristicgroup_id';
    }
}