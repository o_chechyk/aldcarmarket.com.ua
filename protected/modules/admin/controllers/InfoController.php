<?php

class InfoController extends AController
{
    public function actionIndex()
    {
        $o_info = InfoModel::model()->findByPk(1);
        $h1     = 'Загальна інформація';

        $this->render('index', array('o_info' => $o_info, 'h1' => $h1));
    }

    public function actionEdit()
    {
        if (isset($_POST['data']))
        {
            $o_info = InfoModel::model()->findByPk(1);

            foreach ($_POST['data'] as $key => $value)
            {
                $o_info->$key = $value;
            }

            $o_info->save();

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/info');
    }
}