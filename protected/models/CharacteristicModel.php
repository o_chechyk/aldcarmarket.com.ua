<?php

class CharacteristicModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'characteristic';
    }

    public function primaryKey()
    {
        return 'characteristic_id';
    }

    public function relations()
    {
        return array
        (
            'group' => array(self::HAS_ONE, 'CharacteristicGroupModel', array('characteristicgroup_id' => 'characteristic_characteristicgroup_id')),
        );
    }
}