<?php

class ProducerController extends AController
{
    public function actionIndex()
    {
        $a_producer = ProducerModel::model()->findAll();
        $h1         = 'Марки';

        $this->render('index', array('a_producer' => $a_producer, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id         = (int) $id;
        $o_producer = ProducerModel::model()->findByPk($id);
        $status     = $o_producer->producer_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_producer->producer_status = $new_status;
        $o_producer->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/producer');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_producer = new ProducerModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_producer->$key = $value;
            }

            $c_producer->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/producer');
            exit;
        }

        $h1 = 'Редагування марки';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_producer = ProducerModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_producer->$key = $value;
            }

            $o_producer->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/producer');
            exit;
        }

        $h1 = 'Редагування марки';

        $this->render('form', array('o_producer' => $o_producer, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        ProducerModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/producer');
    }
}