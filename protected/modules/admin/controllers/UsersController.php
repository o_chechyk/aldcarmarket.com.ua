<?php

class UsersController extends AController
{
    public function actionIndex()
    {
	$user = UserModel::model()->findByPk( Yii::app()->user->id );
	if($user['user_role'] & 1) {
	    $users = UserModel::model()->findAll();
	} else {
	    $users = array();
	}

        $this->render('index', array('users' => $users));
    }

    public function actionEdit($id)
    {
	$user = UserModel::model()->findByPk( Yii::app()->user->id );
	if(($user['user_role'] & 1) == 0) {
	    Yii::app()->user->setFlash('error', "Недостатній рівень доступу! Зверніться до адміністратора системи.");
	    $this->render('form', array('user' => null));
	    exit;
	}

	if($id == 0) { // New user
	    $user = new UserModel;
	} else {
	    $user = UserModel::model()->findByPk( $id );
	}

	if( count($_POST) ) {

	    $errors = array();
	    if(isset($_POST['login']) and $_POST['login'] != $user['user_login']) {
		$tmp_user = UserModel::model()->findByAttributes( array('user_login' => $_POST['login']) );
		if( $tmp_user ) {
		    $errors[] = "Користувач '".$_POST['login']."' вже існує в системі!";
		} else {
		    $user['user_login'] = $_POST['login'];
		}
	    }

	    $pass1 = $pass2 = "No password here"; // default value on form
	    if( isset($_POST['pass1']) ) {
		$pass1 = $_POST['pass1'];
	    }
	    if( isset($_POST['pass2']) ) {
		$pass2 = $_POST['pass2'];
	    }
	    if($pass1 == $pass2) {
		if($pass1 != "No password here" and $pass2 != "") {
		    $user['user_password'] = md5($pass1);
		}
	    } else {
		$errors[] = "Введено різні паролі";
	    }

	    if(isset($_POST['user_role']) and preg_match("/^\d+$/",$_POST['user_role'])) {
		$user['user_role'] = $_POST['user_role'];
	    }

	    if(isset($_POST['user_status']) and preg_match("/^\d$/",$_POST['user_status'])) {
		$user['user_status'] = $_POST['user_status'];
	    }

	    if( count($errors) ) {
		Yii::app()->user->setFlash('error', implode("<br>",$errors));
	    } else {
		$user->save();
		$this->redirect('/admin/users');
		exit;
	    }

	} // if( count($_POST) )

        $this->render('form', array('user' => $user));
    }


    public function actionDelete($id)
    {
	// check if user have enougth rights to do it
	$user = UserModel::model()->findByPk( Yii::app()->user->id );
	if(($user['user_role'] & 1) == 0) {
	    exit;
	}
	unset( $user );

	// actually delete
	$user = UserModel::model()->findByPk( $id );
	$user->delete();
    }

}