<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span>FAQ</span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading">Часті питання (FAQ)</h1>
        </div>



	<div id="accordion">
	    <?php foreach($o_faq as $faq) { ?>
		<h3><?php echo $faq->faq_question; ?></h3>
		<div>
		    <p><?php echo $faq->faq_answer; ?></p>
		</div>
	    <?php } ?>
	</div> <!-- accordion -->




<!--
	<div class="content-left">
	    <?php /*foreach($o_faq as $faq) { ?>
		<span class="faq-container" id="faq<?php echo $faq->faq_id ?>">
                    <span class="faq-content clearfix">
			<span class="faq-question-container">
			    <span class="faq-caption">Q:</span>
			    <span class="faq-question" onclick="toggleFAQ(<?php echo $faq->faq_id; ?>)">
				<?php echo $faq->faq_question; ?>
			    </span>
			</span>
			<span class="faq-answer-container">
			    <span class="faq-caption">A:</span>
			    <span class="faq-answer">
				
			    </span>
			</span>
                    </span>
		</span>
	    <?php }*/ ?>
	</div> <!-- content-left -->



    </div>
</section>