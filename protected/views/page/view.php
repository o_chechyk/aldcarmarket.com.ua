<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span><?php print $o_page['page_name']; ?></span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading"><?php print $o_page['page_name']; ?></h1>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="content-left">
            <article class="content-article clearfix">
                <?php print $o_page['page_text_1']; ?>
                <hr>
                <?php if (isset($o_page['image_1']['image_id'])) { ?>
                    <img src="<?php echo $o_page['image_1']['image_url']; ?>" alt="">
                <?php } ?>
                <?php print $o_page['page_text_2']; ?>
                <?php if (isset($o_page['image_2']['image_id'])) { ?>
                    <img src="<?php echo $o_page['image_2']['image_url']; ?>" alt="">
                <?php } ?>
                <?php print $o_page['page_text_3']; ?>
                <?php if (isset($o_page['image_3']['image_id'])) { ?>
                    <img src="<?php echo $o_page['image_3']['image_url']; ?>" alt="">
                <?php } ?>
                <?php print $o_page['page_text_4']; ?>
            </article>
        </div>
<!--        <div class="content-right">-->
<!--            <form class="content-right-form" method="POST" action="/form/salon">-->
<!--                <div class="content-right-form-header">-->
<!--                    <strong>Щоб завітати до нашого салону заповніть, <br>будь ласка, форму нижче.</strong>-->
<!--                </div>-->
<!--                <div class="content-right-form-body">-->
<!--                    <div class="content-right-form-phone">-->
<!--                        <div class="input-with-icon">-->
<!--                            <span><img src="/assets/img/form-phone.png" alt=""></span>-->
<!--                            <span><input name="salon[salon_phone]" type="text" required></span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="content-right-form-date">-->
<!--                        <div class="input-with-icon">-->
<!--                            <span><img src="/assets/img/form-date.png" alt=""></span>-->
<!--                            <span><input name="salon[salon_date]" type="text" required></span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="content-right-form-time">-->
<!--                        <div class="input-with-icon">-->
<!--                            <span><img src="/assets/img/form-time.png" alt=""></span>-->
<!--                            <span><input name="salon[salon_time]" type="text" required></span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <input type="submit" class="blue-submit" value="Відправити дані">-->
<!--                </div>-->
<!--            </form>-->
<!--        </div>-->
    </div>
</section>