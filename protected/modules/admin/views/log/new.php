<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Історія завантажень</h1>
    </div>
</div>

<div class="logFilters">
    <div class="caption">
        Фільтри
    </div>
    <div class="body">
	<form action="/admin/log/price" method="POST" autocomplete="off">
	    <div class="section">
		<div class="caption">
		    Дата
		</div>
		<div class="body">
		    З
		    <input name="dateFrom" id="dateFrom" value="<?=$dateFrom?>" autocomplete="off">
		    По
		    <input name="dateTo" id="dateTo" value="<?=$dateTo?>" autocomplete="off">
		</div>
	    </div>
	    <div class="footer">
		<input id="logSubmit" class="btn btn-default text-center" type="submit" value="Застосувати">
		<input type="button" class="btn btn-default text-center" onclick="clearFilter()" value="Скинути фільтр">
	    </div>
	</form>
    </div>
</div>

<div class="dataTable_wrapper">
    <table id="data-table" class="table table-striped table-bordered dataTable no-footer">
	<thead>
	    <tr>
		<th>Vehicle ID</th>
		<th>Виробник</th>
		<th>Модель</th>
		<th>Нова ціна</th>
		<th>Дата</th>
		<th>Користувач</th>
	    </tr>
	</thead>

	<?php foreach ($log as $record) { ?>

	    <tr>
		<td><?=$record['number']?></td>
		<td><?=$record['producer']?></td>
		<td><?=$record['model']?></td>
		<td><?=$record['new_price']?></td>
		<td><?=$record['dt']?></td>
		<td><?=$record['user']?></td>
	    </tr>

	<?php }?>

    </table>

</div>