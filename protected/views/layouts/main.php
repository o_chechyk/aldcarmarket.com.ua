<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?php print $this->seo_title; ?></title>
    <meta name="description" content="<?php print $this->seo_description; ?>">
    <meta name="keywords" content="<?php print $this->seo_keywords; ?>">
    <link rel="canonical" href="<?php print $this->seo_canonical . $this->action->Id . '/' . $this->view_id; ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/img/favicon.ico"
          type="image/x-icon"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&subset=latin,cyrillic' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="/assets/css/normalize.min.css">
    <link rel="stylesheet" href="/assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick.css">
    <link rel="stylesheet" href="/assets/js/vendor/slick/slick-theme.css">
    <link rel="stylesheet" href="/assets/css/libs.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script src="/assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscfMok9RfSXQJo5b92wmp7sVOKYNiyzM"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<header>
    <div class="header-top">
        <div class="wrap clearfix">
            <div class="header-mobile-top only-mobile">
                <div class="header-top-mobile-contacts">
                    <ul class="header-top-contacts">
                        <li><a href="tel:<?=$this->contacts['contacts_fax']?>"
                               class="header-fax"><?php print $this->contacts['contacts_fax']; ?></a></li>
                    </ul>
                    <a href="javascript:;" class="header-top-mobile-contacts-btn"></a>

                    <div class="header-top-mobile-contacts-dropdown">
                        <ul class="header-top-contacts">
                            <li><a href="tel:<?=$this->contacts['contacts_mob']?>"
                                   class="header-mobile"><?php print $this->contacts['contacts_mob']; ?></a></li>
                            <li><a href="tel:<?=$this->contacts['contacts_tel']?>"
                                   class="header-phone"><?php print $this->contacts['contacts_tel']; ?></a></li>
                            <li><a href="mailto:<?=$this->contacts['contacts_email']?>"
                                   class="header-mail"><?php print $this->contacts['contacts_email']; ?></a></li>
                        </ul>
                        <ul class="work-times">
                            <li><?php print implode(',</li><li>', $this->info_time); ?></li>
                        </ul>
                    </div>
                </div>
                <ul class="header-main-social">
                </ul>
                <div class="mobile-menu">
                    <a href="javascript:;" class="mobile-menu-btn"></a>

                    <div class="mobile-menu-dropdown">
                        <ul>
                            <?php foreach ($this->a_menu as $item) { ?>
                                <li><?php echo CHtml::link($item['menu_name'], array('/page/view', 'id' => $item['menu_url'])) ?></li>
                            <?php } ?>
                            <li><?php echo CHtml::link('Контакти', array('/contacts/index')) ?></li>
<!--                            <li><?php echo CHtml::link('Новини', array('/news/index')) ?></li>  -->
			    <li><?php echo CHtml::link('FAQ', array('/faq/index')) ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-top-left">
                <ul class="header-top-contacts">
                    <li><a href="javascript:;" class="header-fax"><?php print $this->contacts['contacts_fax']; ?></a>
                    </li>
                    <li><a href="javascript:;" class="header-mobile"><?php print $this->contacts['contacts_mob']; ?></a>
                    </li>
                    <li><a href="javascript:;" class="header-phone"><?php print $this->contacts['contacts_tel']; ?></a>
                    </li>
                    <li><a href="mailto:<?php print $this->contacts['contacts_email']; ?>"
                           class="header-mail"><?php print $this->contacts['contacts_email']; ?></a></li>
                </ul>
            </div>
            <div class="header-top-right">
                <ul class="work-times">
                    <li><?php print implode(',</li><li>', $this->info_time); ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-main">
        <div class="wrap clearfix">
            <div class="header-main-left">
                <a href="/" class="logo"><img src="/assets/img/logo2.png" alt=""></a>
            </div>
            <div class="header-main-right">
                <ul class="header-main-social">
                </ul>
                <ul class="header-main-nav">
                    <?php foreach ($this->a_menu as $item) { ?>
                        <li><?php echo CHtml::link($item['menu_name'], array('/page/view', 'id' => $item['menu_url'])) ?></li>
                    <?php } ?>
                    <li><?php echo CHtml::link('Контакти', array('/contacts/index')) ?></li>
<!--                    <li><?php echo CHtml::link('Новини', array('/news/index')) ?></li> -->
                    <li><?php echo CHtml::link('FAQ', array('/faq/index')) ?></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<?php print $content; ?>

<footer>
    <div class="wrap">
        <div class="footer-top clearfix">
            <?php print $this->seo_text; ?>
        </div>
        <div class="footer-bottom clearfix">
            <div class="footer-logo">
                <img src="/assets/img/footer-logo.png" alt="">
            </div>
            <div class="footer-contacts">
                <a href="javascript:;" class="footer-fax"><?php print $this->contacts['contacts_fax']; ?></a>
                <a href="javascript:;" class="footer-mobile"><?php print $this->contacts['contacts_mob']; ?></a>
                <a href="javascript:;" class="footer-phone"><?php print $this->contacts['contacts_tel']; ?></a>
            </div>
            <div class="footer-social">
<!--                <span>Приєднуйтесь до нас:</span> -->

                <div class="footer-social-icons">
                </div>
            </div>

            <div class="footer-last">
<!--
                <div class="footer-copy">© <?php print date('Y'); ?> Всі права захищено</div>
                <div class="footer-madeby">
                    <span>Створення сайтів —</span>
                    <a href="http://jaws.com.ua/" title="создание разработка сайтов киев" target="_blank">
                        <img src="/assets/img/jaws.png" alt="создание разработка сайтов киев">
                    </a>
                </div>
-->
                <div class="footer-madeby">Київ 2018</div>
            </div>

        </div>
    </div>

    <?php if(! isset(Yii::app()->session['code'])) { ?>
    <div class="cookieText">
	<div id="rules-container-cookies" class="rules-container" hidden></div>
	<span class="cookie-accept">
	    Я <a href="#" onclick="loadRules('cookies')">ознайомився з Правилами</a>, та погоджуюся на використання cookies
	</span>
	<span class="cookie-buttons">
	    <button type="button" class="cookie-submit" onclick="setCookie('yes')">Так</button>
	    <button type="button" class="cookie-submit" onclick="setCookie('no')">Ні</button>
	</span>
	<span class="cookie-closebutt" onclick="setCookie('no')";></span>
    </div>
    <?php } ?>

</footer>

<div class="subscribeModal modal hidden"></div>
<div class="subscribeContainer hidden">
    <div class="subscribeHeader">
	Дізнавайтесь першими про оновлення асортименту і спеціальні пропозиції!
	<span class="cookie-closebutt" onclick="hideSubscribe()"></span>
    </div>
    <div class="subscribeForm">
	<form id="formSubscription">
	    Ваш E-mail:
	    <input type="email" class="product-question-input" name="email" id="subscribeEmail" placeholder="user@example.com">
	    <div class="subscribe-buttons">
	    <button class="cookie-submit" id="subscribeSubmit" type="button" onclick="postSubscribe()" disabled>Підписатися</button>
	    <button class="cookie-submit" type="button" onclick="hideSubscribe()">Скасувати</button>
	    </div>
	</form>
    </div>
    <div class="subscribeAccept">
	<input type="checkbox" id="subscribeAccept" onclick="subscribeAccept()">
	<label for="subscribeAccept">Підписавшись я даю згоду на отримання <span>інформаційної розсилки*</span> з актуальними пропозиціями по автомобілях, що надійшли у продаж, і даю згоду на обробку моїх персональних даних відповідно до Положення про захист даних, зі змістом якого я ознайомлений.</label>
	<p>* Підписка на розсилку може бути скасована в будь-який час шляхом, передбаченим у розсилці через функцію «відписатися».</p>
    </div>
    <div id="rules-container-privacy" class="rules-container"></div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="/assets/js/vendor/jquery-ui.min.js"></script>
<script src="/assets/js/vendor/libs.js"></script>
<script src="/assets/js/vendor/slick/slick.min.js"></script>
<script src="/assets/js/main.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-4857006-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>