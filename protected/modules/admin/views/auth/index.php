<?php if (Yii::app()->user->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable text-center">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php print Yii::app()->user->getFlash('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Вход в административный раздел</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="/admin/auth/login">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Логин" name="data[login]" type="text" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Пароль" name="data[password]" type="password" value="">
                        </div>
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Войти" />
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>