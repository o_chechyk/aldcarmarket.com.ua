<?php

/****************************************
* Class to work with SpinCar API	*
*					*
* Loads data from SpinCar cloud		*
* and stores it in Redis cache		*
*					*
* !!! IMPORTANT !!!			*
* Requires PHP-Curl to be installed!	*
*					*
* !!! IMPORTANT !!!			*
* Requires PHP-Redis to be installed!	*
*					*
****************************************/
class SpinCar
{
    public static function getAll() {
	//
	// Loads all SRP data from SpinCar cloud
	// and stores that data in Redis cache
	// as JSON-encoded string.
	//
	// If the cache already exists,
	// this function returns the cached data.
	//
	// Cache expires in 1 hour.
	//
	// Input parameters
	//    - none
	//
	// Return values
	//    - SpinCar SRP data object
	//      or FALSE in case of any error
	//
	// Usage: $res = SpinCar::getAll();


	$spincar_customer_name = 'aldauto';
	$spincar_customer_site = 'aldcarmarket.com.ua';
	$spincar_api_key = 'qCHWmmBQNlX97PujWwz9';
	$redis_host = 'localhost';
	$redis_port = 6379;
	$redis_auth = 'Y308KXDC58EW0bR4wt2OvL9d1xdg0pQsaFgP9IiW+HhYvZ+iIH14WTAgY8rI7SzsDPBCjuCJu+91tRPb';
	$redis_spincar_key = 'spincar';


	$redis = new Redis();
	if( ! $redis->connect($redis_host, $redis_port) ) {
	    // Connect fail
	    return false;
	}
	if( ! $redis->auth($redis_auth) ) {
	    //Auth fail
	    $redis->close();
	    return false;
	}

	$buf = $redis->get( $redis_spincar_key );
	if( $buf ) {
	    $spincar_obj = json_decode($buf, true);
	    if($spincar_obj != NULL) {
		$redis->close();
		return $spincar_obj;
	    }
	}

	// When we'e get here, we still haven't $spincar_obj from Redis
	// Trying to get it from SpinCar cloud
	$auth = hash("sha512", $spincar_customer_name.$spincar_customer_site.$spincar_api_key);
	$url = "https://find-spin-api.spincar.com/provider-api/".$spincar_customer_name."/".$spincar_customer_site."?auth=".$auth;

	$handle = curl_init();
	if($handle == false) {
	    // CURL initialization failed
	    $redis->close();
	    return false;
	}
	curl_setopt($handle, CURLOPT_URL, $url);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($handle);
	curl_close($handle);
	if($output == false) {
	    // Empty answer
	    $redis->close();
	    return false;
	}

	$spincar_obj = json_decode($output, true);
	if($spincar_obj == NULL) {
	    // Invalid answer
	    $redis->close();
	    return false;
	}

	if($spincar_obj['status'] == 'ok') {
	    // Object Ok, save it to Redis for 1 hour
	    $redis->setEx($redis_spincar_key, 3600, $output);
	    $redis->close();
	    return $spincar_obj;
	} else {
	    // Our request rejected
	    $redis->close();
	    return false;
	}

    }

}
