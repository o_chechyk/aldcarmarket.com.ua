<?php

class TransmissionController extends AController
{
    public function actionIndex()
    {
        $a_transmission = TransmissionModel::model()->findAll();
        $h1             = 'КПП';

        $this->render('index', array('a_transmission' => $a_transmission, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id             = (int) $id;
        $o_transmission = TransmissionModel::model()->findByPk($id);
        $status         = $o_transmission->transmission_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_transmission->transmission_status = $new_status;
        $o_transmission->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/transmission');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_transmission = new TransmissionModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_transmission->$key = $value;
            }

            $c_transmission->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/transmission');
            exit;
        }

        $h1 = 'Редагування КПП';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id             = (int) $id;
        $o_transmission = TransmissionModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_transmission->$key = $value;
            }

            $o_transmission->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/transmission');
            exit;
        }

        $h1 = 'Редагування КПП';

        $this->render('form', array('o_transmission' => $o_transmission, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        TransmissionModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/transmission');
    }
}