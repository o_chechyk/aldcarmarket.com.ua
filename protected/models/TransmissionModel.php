<?php

class TransmissionModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'transmission';
    }

    public function primaryKey()
    {
        return 'transmission_id';
    }
}