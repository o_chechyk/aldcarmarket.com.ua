alter table question
modify column question_read int(11),
add question_manager int(11),
add question_reply text;

alter table ask
modify column ask_read int(11),
add ask_manager int(11),
add ask_reply text;
