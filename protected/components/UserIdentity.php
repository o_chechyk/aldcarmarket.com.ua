<?php

class UserIdentity extends CUserIdentity
{
    private $_id;
    public $user_type;

    public function authenticate()
    {
        $username   = strtolower($this->username);
        $user       = UserModel::model()->find('LOWER(user_login)=?', array($username));

        if ($user === null)
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        elseif (!$user->validatePassword($this->password))
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id          = $user->user_id;
            $this->username     = $user->user_login;
            $this->errorCode    = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}