<?php

class ModelModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'model';
    }

    public function primaryKey()
    {
        return 'model_id';
    }

    public function relations()
    {
        return array
        (
            'producer' => array(self::HAS_ONE, 'ProducerModel', array('producer_id' => 'model_producer_id')),
        );
    }
}