<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/body/create" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover" id="data-table">
                <thead>
                    <tr>
                        <th>Назва</th>
                        <th>Ключ імпорту</th>
                        <th class="col-lg-1">Статус</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($a_body as $item) { ?>
                        <tr>
                            <td><?php print $item['body_name']; ?></td>
                            <td><?php print $item['body_import_key']; ?></td>
                            <td>
                                <a href="/admin/body/status/<?php print $item['body_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['body_status']) { ?>danger<?php } else { ?>success<?php } ?> btn-circle">
                                        <i class="fa fa-power-off"></i>
                                    </button>
                                </a>
                            </td>
                            <td>
                                <a href="/admin/body/edit/<?php print $item['body_id']; ?>">
                                    <button type="button" class="btn btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="/admin/body/delete/<?php print $item['body_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->