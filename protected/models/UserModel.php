<?php

class UserModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'user';
    }

    public function primaryKey()
    {
        return 'user_id';
    }

    public function validatePassword($password)
    {
        return md5($password) == $this->user_password;
    }

    public function hashPassword($password)
    {
        return md5($password);
    }
}