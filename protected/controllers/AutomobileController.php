<?php

class AutomobileController extends Controller
{
    public function actionIndex($id, $alias)
    {
        $o_car = CarModel::model()->findByAttributes(array('car_sku' => $id));

//        if(isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'Проданий')
//            exit;
        if (!isset($o_car['car_id'])) {
            $this->redirect('/');
            exit;
        }

        if (!empty($o_car['seo_title'])) {
            $this->seo_title = $o_car['seo_title'];
        } else {
            $this->seo_title = $o_car['car_name'];
        }

        if (!empty($o_car['seo_keywords'])) {
            $this->seo_keywords = $o_car['seo_keywords'];
        } else {
            $this->seo_keywords = $o_car['car_name'];
        }

        if (!empty($o_car['seo_description'])) {
            $this->seo_description = $o_car['seo_description'];
        } else {
            $this->seo_description = $o_car['car_name'];
        }

        if (!empty($o_car['seo_h1'])) {
            $this->seo_h1 = $o_car['seo_h1'];
        } else {
            $this->seo_h1 = $o_car['car_name'];
        }

        if (!empty($o_car['seo_text'])) {
            $this->seo_text = $o_car['seo_text'];
        }

        $a_characteristic = CarCharacteristicModel::model()->with('char.group')->findAllByAttributes(array('carcharacteristic_car_id' => $o_car['car_id'], 'carcharacteristic_status' => 1), array('order' => 'characteristicgroup_order, characteristic_order'));

	// Check last viewed cars array to be initialized
	if( isset(Yii::app()->session['review']) ) {
	    $review = Yii::app()->session['review'];
	    if( array_search($o_car['car_id'],$review) === false) { // not seen yet
		$review[] = $o_car['car_id'];
	    }
	} else {
	    $review = array();
	    if( isset(Yii::app()->session['code']) ) { // load viewed car list from DB, if user is known
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));
		if( $cookie ) {
		    $rev = ReviewModel::model()->findAllByAttributes(array('cookie_id' => $cookie->cookie_id));
		    foreach($rev as $row) {
			$review[] = $row->car_id;
		    }
		}
	    }
	    if( array_search($o_car['car_id'],$review) === false) { // not seen yet
		$review[] = $o_car['car_id'];
	    }
	}
	Yii::app()->session['review'] = $review;

	// Check favorites to be initialized
	if( isset(Yii::app()->session['favorite_cars']) ) {
	    $favorite_cars = Yii::app()->session['favorite_cars'];
	} else {
	    $favorite_cars = array();
	    if( isset(Yii::app()->session['code']) ) { // Should we load favorites from DB?
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));
		if( $cookie ) {
		    $favorites = FavoritesModel::model()->findAllByAttributes(array('cookie_id' => $cookie->cookie_id));
		    foreach($favorites as $row) {
			$favorite_cars[] = $row->car_id;
		    }
		}
		Yii::app()->session['favorite_cars'] = $favorite_cars;
	    }
	}

	$fav_show = false;
	$cnt = 0;
	if( isset(Yii::app()->session['code']) ) {
	    $cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));

	    if( $cookie ) {
		$sql = "insert into review (cookie_id,car_id) ".
			"values (".$cookie->cookie_id.",".$o_car['car_id'].") ".
			"on duplicate key update last_seen=NOW();";
		Yii::app()->db->createCommand($sql)->execute();
	    }

	    if(isset(Yii::app()->session['favorite_cars']) and count(Yii::app()->session['favorite_cars'])) {
		$fav_show = true;

		// Update Last Seen timestamp in database ?
		if(array_search($o_car['car_id'], $favorite_cars) !== false) { // this car is one of favorites
		    if( $cookie ) {
			$sql = "update favorites set last_seen=NOW() where cookie_id=".$cookie->cookie_id." and car_id=".$o_car['car_id'];
			Yii::app()->db->createCommand($sql)->execute();
		    }
		}

		// Is there any changes in favorite automobiles during the time from last visit?
		$sql = "select ".
			    "count(*) cnt ".
			"from ".
			    "favorites f ".
			    "left join logCar l on f.car_id=l.car_id ".
			    "left join cookie c on c.cookie_id=f.cookie_id ".
			"where ".
			    "c.cookie_code='".Yii::app()->session['code']."' ".
			    "and l.dateOperation>f.last_seen ".
			    "and ".
				"(old_price != new_price ".
				"or old_price_old != new_price_old ".
				"or old_phase_id != new_phase_id)";

		$res = Yii::app()->db->createCommand($sql)->queryRow();
		$cnt = $res['cnt'];

	    } // if(isset(Yii::app()->session['favorite_cars']) and count(Yii::app()->session['favorite_cars']))
	} else { // if( isset(Yii::app()->session['code']) )
	    if(isset(Yii::app()->session['favorite_cars']) and count(Yii::app()->session['favorite_cars'])) {
		$fav_show = true;
	    }
	}

	// Load similar products list
	$sql = "select ".
	    "car_id ".
	"from ".
	    "car c ".
	    "left join carsmart cs on cs.carsmart_car_id=c.car_id ".
	"where ".
	    "cs.carsmart_smart_id in (select carsmart_smart_id from carsmart ".
					"where carsmart_car_id=".$o_car['car_id']." and carsmart_status=1) ".
	    "and c.car_phase_id=3 ".
	    "and c.car_id!=".$o_car['car_id'];
	$res = Yii::app()->db->createCommand($sql)->queryAll();
	$ids = array();
	foreach($res as $row) {
	    $ids[] = $row['car_id'];
	}

	$criteria = new CDbCriteria();
	$criteria->addInCondition("car_id", $ids);
	$criteria->order = "car_price";
	$s_car = CarModel::model()->findAll($criteria);

	$spin = array();
	$res = SpinCar::getAll();
	if( $res ) {
	    if( isset($res['vehicles']) ) {
		foreach($res['vehicles'] as $vehicle) {
		    $spin[$vehicle['vin']] = $vehicle['photo_url'];
		}
	    }
	}

	$this->render('view', array('o_car' => $o_car,
				    'a_characteristic' => $a_characteristic,
				    'fav_show' => $fav_show,
				    'fav_changed' => $cnt,
				    's_car' => $s_car,
				    'spin' => $spin));
    }

    public function actionPrint($id)
    {
        $this->layout = 'empty';
        $cars = array();
        if (is_array(Yii::app()->session['favorite_cars'])) {
            foreach ($_SESSION['favorite_cars'] as $car) {
                $cars[] = $this->getModel($car);
            }
            if (!in_array($id, $_SESSION['favorite_cars'])) {
                $cars[] = $this->getModel($id);
            }
            unset($_SESSION['favorite_cars']);
            $showImg = false;
        } else {
            $cars[] = $this->getModel($id);
            $showImg = true;
        }
        $this->render('print', array('cars' => $cars, 'showImg' => $showImg));
        exit;
    }

    private function getModel($id)
    {
        $car_model = CarModel::model()->findByAttributes(array('car_sku' => $id));
        $characteristic_model = CarCharacteristicModel::model()
            ->with('char.group')
            ->findAllByAttributes(
                array('carcharacteristic_car_id' => $car_model['car_id'], 'carcharacteristic_status' => 1),
                array('order' => 'characteristicgroup_order, characteristic_order'));

        return array('model' => $car_model, 'characteristic' => $characteristic_model);
    }

    public function actionFavorite($id)
    {
        if (!is_array(Yii::app()->session['favorite_cars'])) {
            $_SESSION['favorite_cars'] = array();
        }

        if (in_array($id, $_SESSION['favorite_cars'])) {
            $index = array_search($id, $_SESSION['favorite_cars']);
            unset($_SESSION['favorite_cars'][$index]);

	    if( isset(Yii::app()->session['code']) ) {
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));
		if( $cookie ) {
		    FavoritesModel::model()->deleteAll("cookie_id=:cookie_id and car_id=:car_id",
							array(':cookie_id' => $cookie->cookie_id, ':car_id' => $id));
		}
	    }

        } else {
            $_SESSION['favorite_cars'][] = $id;

	    if( isset(Yii::app()->session['code']) ) {
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));

		if( $cookie ) {
		    $favorites = new FavoritesModel;
		    $favorites->cookie_id = $cookie->cookie_id;
		    $favorites->car_id = $id;
		    $favorites->save();
		}
	    }

        }

        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionShow($id) {

        $car = CarModel::model()->findByPk($id,array('select' => 'car_sku,car_url,car_status'));

        if( $car ) {
	    if( $car['car_status'] ) {
		$url = "/automobile/".$car['car_sku']."/".$car['car_url']."#lang=uk";
	    } else {
		$review = $_SESSION['review'];
		$index = array_search($id, $review);
		if($index !== false) {
		    unset( $review[$index] );
		    if( count($review) ) {
			$_SESSION['review'] = $review;
		    } else {
			unset( $_SESSION['review'] );
		    }
		}
		$this->render( 'not_found' );
		exit;
	    }
        } else {
	    $url = "/";
	}

	$this->redirect( $url );

    }
}
