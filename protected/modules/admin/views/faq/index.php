<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Часті запитання (FAQ)</h1>
    </div>
</div>

<div class="notice">
    Перетягуйте блоки, щоб визначити правильну послідовність, потім не забудьте натиснути "Зберегти порядок"
</div>

<div class="row" style="margin-bottom:2em;">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">

	    <div class="column">
		<?php foreach ($a_faq as $order => $faq) { ?>

		    <div class="portlet" id="faq_container<?php echo $faq->faq_id; ?>">
			<div class="portlet-header">
			    Запис <?php echo $faq->faq_id; ?>
			</div>
			<div class="portlet-content">
			    <div<?php if(!$faq->faq_status) echo " style=\"color:lightgrey;\""; ?>>
				<input type="hidden" id="faq_id[<?php echo $faq->faq_id; ?>]" name="faq_id[<?php echo $faq->faq_id; ?>]" value="<?php echo $faq->faq_id; ?>">
				<table class="table table-striped table-bordered">
				    <tbody>
					<tr>
					    <td><?php echo $faq->faq_question ?></td>
					    <td rowspan="2" width="58px">
						<a href="/admin/faq/edit/<?php echo $faq->faq_id; ?>" title="Редагувати">
						    <button type="button" class="btn btn-default">
							<i class="fa fa-pencil"></i>
						    </button>
						</a>
					    </td>
					</tr>
					<tr>
					    <td><?php echo $faq->faq_answer ?></td>
					</tr>
				    </tbody>
				</table>
			    </div> <!-- container# -->
			</div> <!-- portlet-content -->
		    </div> <!-- portlet -->

		<?php }?>
	    </div> <!-- column -->

	    <form method="POST" action="/admin/faq/order">
		<a href="/admin/faq/edit" title="Додати нове запитання">
		    <button type="button" class="btn btn-default">
			<i class="fa fa-plus"></i>
		    </button>
		</a>

		<input type="hidden" id="faq_order_all" name="faq_order_all">
		<button type="submit" class="btn btn-default" title="Зберегти порядок">
		    <i class="fa fa-save"></i>
		</input>
	    </form>

        </div> <!-- dataTable_wrapper -->
    </div> <!-- /.col-lg-12 -->
</div> <!-- /.row -->