<?php

class ColorController extends AController
{
    public function actionIndex()
    {
        $a_color    = ColorModel::model()->findAll();
        $h1         = 'Кольори';

        $this->render('index', array('a_color' => $a_color, 'h1' => $h1));
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_color = new ColorModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_color->$key = $value;
            }

            $c_color->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/color');
            exit;
        }

        $h1 = 'Редагування кольору';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_color   = ColorModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_color->$key = $value;
            }

            $o_color->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/color');
            exit;
        }

        $h1 = 'Редагування кольору';

        $this->render('form', array('o_color' => $o_color, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        ColorModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/color');

        $this->render('form', array('o_color' => $o_color, 'h1' => $h1));
    }
}