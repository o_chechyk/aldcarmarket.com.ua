<?php

class CarCharacteristicModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'carcharacteristic';
    }

    public function primaryKey()
    {
        return 'carcharacteristic_id';
    }

    public function relations()
    {
        return array
        (
            'char' => array(self::HAS_ONE, 'CharacteristicModel', array('characteristic_id' => 'carcharacteristic_characteristic_id')),
        );
    }
}