<?php

class ContactsModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'contacts';
    }

    public function primaryKey()
    {
        return 'contacts_id';
    }
}