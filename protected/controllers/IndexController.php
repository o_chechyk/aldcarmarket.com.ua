<?php

class IndexController extends Controller
{
    public function actionIndex()
    {
        $o_info = InfoModel::model()->findByPk(1);

        $this->seo_title = $o_info['seo_title'];
        $this->seo_keywords = $o_info['seo_keywords'];
        $this->seo_description = $o_info['seo_description'];

        if (!empty($o_info['seo_text'])) {
            $this->seo_text = $o_info['seo_text'];
        }

	if(! isset(Yii::app()->session['favorite_cars']) ) {
	    $fav_exists = false;
	    if( isset(Yii::app()->session['code']) ) {
		// Load favorites from DB
		$favorite_cars = array();
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));
		if( $cookie ) {
		    $favorites = FavoritesModel::model()->findAllByAttributes(array('cookie_id' => $cookie->cookie_id));
		    foreach($favorites as $row) {
			$favorite_cars[] = $row->car_id;
		    }
		}
		Yii::app()->session['favorite_cars'] = $favorite_cars;

		if( count($favorite_cars) ) {
		    $fav_exists = true;
		}

	    } // if( isset(Yii::app()->session['code']) )
	} else { // if(! isset(Yii::app()->session['favorite_cars']) )
	    if( count(Yii::app()->session['favorite_cars']) ) {
		$fav_exists = true;
	    } else {
		$fav_exists = false;
	    }
	}

	// Is there any changes in favorite automobiles during the time from last visit?
	$fav_changed = false;
	if($fav_exists and isset(Yii::app()->session['code']) ) {
	    $sql = "select ".
			"count(*) cnt ".
		    "from ".
			"favorites f ".
			"left join logCar l on f.car_id=l.car_id ".
			"left join cookie c on c.cookie_id=f.cookie_id ".
		    "where ".
			"c.cookie_code='".Yii::app()->session['code']."' ".
			"and l.dateOperation>f.last_seen ".
			"and ".
			    "(old_price != new_price ".
			    "or old_price_old != new_price_old ".
			    "or old_phase_id != new_phase_id)";
	    $res = Yii::app()->db->createCommand($sql)->queryRow();
	    if($res['cnt'] > 0) {
		$fav_changed = true;
	    }
	}

        $attributes_array = array('car_status' => 1);
        $condition = '1';
        $limit = 10;
        $order_sql = 'car_price_old DESC';
        $order = array('car_price');
        $order_m = array('car_price');
        $offset = 0;
        $condition_array = array();
	$show_fav = false;

        if (isset($_GET['filter'])) {

            $filter = $_GET['filter'];
            $attributes_array['car_status'] = 1;

            if (isset($filter['check'])) {
                foreach ($filter['check'] as $key => $value) {
                    if (0 != $value[0]) {
                        $attributes_array['car_' . $key] = $value[0];
                    }
                }
            }

            if (isset($filter['smart'])) {
                $smart_array = array();

                foreach ($filter['smart'] as $item) {
                    $smart_array[] = $item;
                }

                $condition = $condition . ' AND carsmart_smart_id IN (' . implode(',', $smart_array) . ') AND carsmart_status=1';
            }

            if (isset($filter['year_min']) && !empty($filter['year_min']) && 0 != $filter['year_min']) {
                $condition = $condition . ' AND car_year>=' . (int)str_replace(' ', '', $filter['year_min']);
            }

            if (isset($filter['year_max']) && !empty($filter['year_max']) && 0 != $filter['year_max']) {
                $condition = $condition . ' AND car_year<=' . (int)str_replace(' ', '', $filter['year_max']);
            }

            if (isset($filter['price_min']) && !empty($filter['price_min']) && 0 != $filter['price_min']) {
                $condition = $condition . ' AND car_price>=' . (int)str_replace(' ', '', $filter['price_min']);
            }

            if (isset($filter['price_max']) && !empty($filter['price_max']) && 0 != $filter['price_max']) {
                $condition = $condition . ' AND car_price<=' . (int)str_replace(' ', '', $filter['price_max']);
            }

            if (isset($filter['engine_min']) && !empty($filter['engine_min']) && 0 != $filter['engine_min']) {
                $condition = $condition . ' AND car_engine_capacity>=' . ((float)str_replace(' ', '', $filter['engine_min']) * 1000);
            }

            if (isset($filter['engine_max']) && !empty($filter['engine_max']) && 0 != $filter['engine_max']) {
                $condition = $condition . ' AND car_engine_capacity<=' . ((float)str_replace(' ', '', $filter['engine_max']) * 1000);
            }

            if (isset($filter['special'])) {
                $condition = $condition . ' AND car_price_old!=0';
            }
	    //if (isset($filter['not_sold'])) {
		$condition = $condition . ' AND car_phase_id!=1';
	    //}
	} else if( isset($_GET['favorites']) ) {
	    $show_fav = true;
	    if(isset(Yii::app()->session['favorite_cars']) and count(Yii::app()->session['favorite_cars'])) {
		$condition = $condition . ' AND car_id in (0,'.implode(",",Yii::app()->session['favorite_cars']).')';
	    } else {
		$condition = $condition . ' AND FALSE';
	    }
	    $filter = array();
        } else {
            $filter = array('not_sold' => 1);
	    $condition = $condition . ' AND car_phase_id!=1';
        }

        $condition_array['condition'] = $condition;

        if (isset($_GET['on_page'])) {
            $limit = (int)$_GET['on_page'];
        }

        if (isset($_GET['order'])) {
            if (!is_array($_GET['order'])) {
                $order_get = array($_GET['order']);
            } else {
                $order_get = $_GET['order'];
            }

            $order_sql = implode(', ', $order_get);
            $order = $order_get;
            $order_m = $order_get;
        } else {
            $order_sql = $order_sql . ', car_price';
        }

        if (isset($_GET['page'])) {
            $offset = (int)$_GET['page'] - 1;
        }

        $condition_array['order'] = $order_sql;

        if (!isset($_GET['order']) && !isset($_GET['filter']) && !isset($_GET['favorites'])) {


            $sql = 'SELECT *
                    FROM (
                        SELECT *
                        FROM car
                        WHERE car_price_old != 0 AND car_phase_id!=1 AND car_status = 1
                        ORDER BY car_date DESC
                    ) AS a
                    UNION ALL

                    SELECT *
                    FROM (
                        SELECT *
                        FROM car
                        WHERE car_price_old = 0 AND car_phase_id!=1 AND car_status = 1
                        ORDER BY car_date DESC
                        ) AS n
		    ORDER BY car_date DESC
                    ';
            $a_car = CarModel::model()->with('smart')->findAllBySql($sql);


        } else
            $a_car = CarModel::model()->with('smart')->findAllByAttributes($attributes_array, $condition_array);

        if (0 != $limit) {
            $a_car = array_slice($a_car, $offset * $limit, $limit);
        }

        $count_car = CarModel::model()->with('smart')->countByAttributes($attributes_array, array('condition' => $condition));
        $a_body = BodyModel::model()->findAllByAttributes(array('body_status' => 1), array('order' => 'body_name'));
        $a_engine = EngineModel::model()->findAllByAttributes(array('engine_status' => 1), array('order' => 'engine_name'));
        $a_model = ModelModel::model()->findAllByAttributes(array('model_status' => 1), array('order' => 'model_name'));
        $a_producer = ProducerModel::model()->findAllByAttributes(array('producer_status' => 1), array('order' => 'producer_name'));
        $a_transmission = TransmissionModel::model()->findAllByAttributes(array('transmission_status' => 1), array('order' => 'transmission_name'));
//        $a_year = CarModel::model()->findAllByAttributes(array('car_status' => 1), array('group' => 'car_year', 'order' => 'car_year'));
        $a_year = CarModel::model()->findAllBySql("select distinct car_year from car where car_status=1 order by car_year");
//        $a_price = CarModel::model()->findAllByAttributes(array('car_status' => 1), array('group' => 'car_price', 'order' => 'car_price'));
        $a_price = CarModel::model()->findAllBySql("select distinct car_price from car where car_status=1 order by car_price");
        $a_smart = SmartModel::model()->findAllByAttributes(array('smart_status' => 1), array('order' => 'smart_order'));
        $a_slide = CarModel::model()->findAllByAttributes(array('car_status' => 1, 'car_slide' => 1));
        $slider = SliderModel::model()->findAllByAttributes(array('status' => 1));
        $on_page_array = array('10' => 10, '20' => 20, '50' => 50, 'Всі' => 0);
        $order_array = array();
        $gear_filter = GearModel::model()->findAll();

        if ('car_price' == $order[0]) {
            $order_array['Ціна'] = 'car_price DESC';
        } else {
            $order_array['Ціна'] = 'car_price';
        }

        if ('car_mileage' == $order[0]) {
            $order_array['Пробіг'] = 'car_mileage DESC';
        } else {
            $order_array['Пробіг'] = 'car_mileage';
        }

        if ('car_year' == $order[0]) {
            $order_array['Рік випуску'] = 'car_year DESC';
        } else {
            $order_array['Рік випуску'] = 'car_year';
        }

        if ('car_engine_capacity' == $order[0]) {
            $order_array['Об\'єм двигуна'] = 'car_engine_capacity DESC';
        } else {
            $order_array['Об\'єм двигуна'] = 'car_engine_capacity';
        }

        $mobile_asc = 'Від меншого до більшого';
        $mobile_desc = 'Від більшого до меншого';

        $mobile_order_array = array
        (
            'Ціна' => array(array('text' => $mobile_desc, 'value' => 'car_price DESC'), array('text' => $mobile_asc, 'value' => 'car_price')),
            'Пробіг' => array(array('text' => $mobile_desc, 'value' => 'car_mileage DESC'), array('text' => $mobile_asc, 'value' => 'car_mileage')),
            'Рік випуску' => array(array('text' => $mobile_desc, 'value' => 'car_year DESC'), array('text' => $mobile_asc, 'value' => 'car_year')),
            'Об\'єм двигуна' => array(array('text' => $mobile_desc, 'value' => 'car_engine_capacity DESC'), array('text' => $mobile_asc, 'value' => 'car_engine_capacity'))
        );

        if (0 != $limit) {
            $count_page = ceil($count_car / $limit);
        } else {
            $count_page = 1;
        }

        if (0 == $offset) {
            $prev = 1;
        } else {
            $prev = $offset;
        }

        if ($count_page == $offset + 1) {
            $next = $count_page;
        } else {
            $next = $offset + 2;
        }

        $a_pagination = array();

        for ($i = 1; $i <= $count_page; $i++) {
	    $buf = array('on_page' => $limit, 'page' => $i);
	    if( isset($_GET['filter']) ) {
		$buf['filter'] = $filter;
	    } else if( isset($_GET['favorites']) ) {
		$buf['favorites'] = 1;
	    }
	    if( isset($_GET['order']) ) {
		$buf['order'] = $order;
	    }

	    $a_pagination[] = array(
		'url' => '?' . urldecode(http_build_query($buf)),
		'page' => $i,
	    );

        }

        if (isset($_GET['order']) || isset($_GET['filter'])) {

            $prev = '?' . urldecode(http_build_query(array('filter' => $filter, 'order' => $order, 'on_page' => $limit, 'page' => $prev)));
            $next = '?' . urldecode(http_build_query(array('filter' => $filter, 'order' => $order, 'on_page' => $limit, 'page' => $next)));
        } else {
            $prev = '?' . urldecode(http_build_query(array('on_page' => $limit, 'page' => $prev)));
            $next = '?' . urldecode(http_build_query(array('on_page' => $limit, 'page' => $next)));
        }

	// Check last viewed cars array to be initialized
	if(! isset(Yii::app()->session['review'])) {
	    $review = array();
	    if( isset(Yii::app()->session['code']) ) { // load viewed car list from DB, if user is known
		$cookie = CookieModel::model()->findByAttributes(array('cookie_code' => Yii::app()->session['code']));
		if( $cookie ) {
		    $sql = "select r.car_id from review r left join car c on c.car_id=r.car_id where r.cookie_id=".
			    $cookie->cookie_id." and c.car_status=1";
//		    $rev = ReviewModel::model()->findAllByAttributes(array('cookie_id' => $cookie->cookie_id));
		    $rev = ReviewModel::model()->findAllBySql( $sql );
		    foreach($rev as $row) {
			$review[] = $row->car_id;
		    }
		}
	    }
	    Yii::app()->session['review'] = $review;
	}

	if( count(Yii::app()->session['review']) ) {
	    $criteria = new CDbCriteria();
	    $criteria->addInCondition("car_id", Yii::app()->session['review']);
	    $v_car = CarModel::model()->findAll($criteria);
	} else {
	    $v_car = array();
	}

	$spin = array();
	$res = SpinCar::getAll();
	if( $res ) {
	    if( isset($res['vehicles']) ) {
		foreach($res['vehicles'] as $vehicle) {
		    $spin[$vehicle['vin']] = $vehicle['photo_url'];
		}
	    }
	}

        $this->render('index', array(
            'a_body' => $a_body,
            'a_car' => $a_car,
            'a_engine' => $a_engine,
            'a_model' => $a_model,
            'a_producer' => $a_producer,
            'a_transmission' => $a_transmission,
            'a_price' => $a_price,
            'a_year' => $a_year,
            'a_smart' => $a_smart,
            'a_slide' => $a_slide,
            'slider' => $slider,
            'count_car' => $count_car,
            'filter' => $filter,
            'limit' => $limit,
            'order' => $order,
            'order_m' => $order_m,
            'offset' => $offset,
            'on_page_array' => $on_page_array,
            'order_array' => $order_array,
            'mobile_order_array' => $mobile_order_array,
            'a_pagination' => $a_pagination,
            'prev' => $prev,
            'next' => $next,
            'gear_filter' => $gear_filter,
	    'v_car' => $v_car,
	    'show_fav' => $show_fav,
	    'fav_exists' => $fav_exists,
	    'fav_changed' => $fav_changed,
	    'spin' => $spin
        ));
    }
}
