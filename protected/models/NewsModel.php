<?php

class NewsModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'news';
    }

    public function primaryKey()
    {
        return 'news_id';
    }

    public function relations()
    {
        return array
        (
            'image' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'news_image_id')),
        );
    }
}