<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/question" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Текст питання</td>
                        <td><?php if (isset ($o_question)) { print nl2br($o_question['question_text']); } ?></td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Відповідь</td>
                        <td>
			    <?php
				if($o_question['question_reply']) {
					$text = $o_question['question_reply'];
				} else {
					$text = "У відповідь на Вашe питання повідомляємо:<br/>";
				}
			    ?>
			    <textarea class="ckeditor" name="data[answer]"><?=$text?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Відповісти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->