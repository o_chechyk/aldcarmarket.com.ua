drop table if exists favorites;
drop table if exists cookie;

create table cookie (
cookie_id int(11) auto_increment primary key,
cookie_code varchar(32) not null,
cookie_created timestamp default now()
);

create table favorites (
cookie_id int(11),
car_id int(11) not null,
last_seen timestamp default NOW(),
constraint fav_fk_1 foreign key (cookie_id) references cookie(cookie_id) on delete cascade,
constraint fav_fk_2 foreign key (car_id) references car(car_id) on delete cascade
);
