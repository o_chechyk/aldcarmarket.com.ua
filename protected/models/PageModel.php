<?php

class PageModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'page';
    }

    public function primaryKey()
    {
        return 'page_id';
    }

    public function relations()
    {
        return array
        (
            'image_1' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'page_image_1')),
            'image_2' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'page_image_2')),
            'image_3' => array(self::HAS_ONE, 'ImageModel', array('image_id' => 'page_image_3')),
        );
    }
}