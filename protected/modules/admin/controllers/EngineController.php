<?php

class EngineController extends AController
{
    public function actionIndex()
    {
        $a_engine   = EngineModel::model()->findAll();
        $h1         = 'Двигуни';

        $this->render('index', array('a_engine' => $a_engine, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id         = (int) $id;
        $o_engine   = EngineModel::model()->findByPk($id);
        $status     = $o_engine->engine_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_engine->engine_status = $new_status;
        $o_engine->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/engine');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_engine = new EngineModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_engine->$key = $value;
            }

            $c_engine->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/engine');
            exit;
        }

        $h1 = 'Редагування двигуна';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_engine   = EngineModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_engine->$key = $value;
            }

            $o_engine->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/engine');
            exit;
        }

        $h1 = 'Редагування двигуна';

        $this->render('form', array('o_engine' => $o_engine, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        EngineModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/engine');
    }
}