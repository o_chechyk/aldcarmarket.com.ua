<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/slider/create" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th class="col-lg-2">Фото</th>
                        <th>Назва</th>
                        <th>Url</th>
                        <th class="col-lg-1">Статус</th>
                        <th class="col-lg-2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($models as $item) { ?>
                        <tr>
                            <td><?php print $item['slider_id']; ?></td>
                            <td>
                                <?php if (isset($item['img']['image_id'])) { ?>
                                    <a href="javascript:;" class="thumbnail">
                                        <img src="<?php ImageIgosja::resize($item['img']['image_id'], 200, 150, 0); ?>" />
                                    </a>
                                <?php } ?>
                            </td>
                            <td><?php print $item['slider_name']; ?></td>
                            <td><?php print $item['url']; ?> </td>
                            <td>
                                <a href="/admin/slider/status/<?php print $item['slider_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['status']) { ?>danger<?php } else { ?>success<?php } ?> btn-circle">
                                        <i class="fa fa-power-off"></i>
                                    </button>
                                </a>
                            </td>
                            <td>
                                <a href="/admin/slider/edit/<?php print $item['slider_id']; ?>">
                                    <button type="button" class="btn btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="/admin/slider/delete/<?php print $item['slider_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->