<?php

class NewsController extends AController
{
    public function actionIndex()
    {
        $a_news  = NewsModel::model()->findAll();
        $h1     = 'Новини';

        $this->render('index', array('a_news' => $a_news, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id     = (int) $id;
        $o_news  = NewsModel::model()->findByPk($id);
        $status = $o_news->news_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_news->news_status = $new_status;
        $o_news->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/news');
    }

    public function actionCreate()
    {
        $redirect   = 0;

        if (isset($_POST['data']))
        {
            $c_news = new NewsModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_news->$key = $value;
            }

            $c_news->news_date = time();
            $c_news->save();

            $news_id = $c_news->news_id;
            $o_news  = NewsModel::model()->findByPk($news_id);
            $o_news->news_url = $news_id . '_' . str_replace($this->rus, $this->lat, $o_news->news_name);
            $o_news->save();

            $id = $o_news->news_id;

            $redirect = 1;
        }

        if (isset($_FILES['image']['name']) &&
            !empty($_FILES['image']['name']))
        {
            $image = $_FILES['image'];

            $ext  = $image['name'];
            $ext  = explode('.', $ext);
            $ext  = end($ext);
            $file = $image['tmp_name'];

            $image_url = ImageIgosja::put_file($file, $ext);

            $o_image = new ImageModel();
            $o_image->image_url = $image_url;
            $o_image->save();

            $image_id = $o_image->image_id;

            $o_news  = NewsModel::model()->findByPk($news_id);
            $o_news->news_image_id = $image_id;
            $o_news->save();

            $redirect = 1;
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/news');
            exit;
        }

        $h1 = 'Редагування новини';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_news      = NewsModel::model()->findByPk($id);
        $redirect   = 0;

        if (isset($_GET['image']))
        {
            $image_id = (int) $_GET['image'];

            $o_image = ImageModel::model()->findByPk($image_id);

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
            {
                unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
            }

            ImageModel::model()->deleteByPk($image_id);

            $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

            foreach ($a_resize as $item)
            {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
                }
            }

            ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));

            $redirect = 1;
        }

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_news->$key = $value;
            }

            $o_news->save();

            $redirect = 1;
        }

        if (isset($_FILES['image']['name']) &&
            !empty($_FILES['image']['name']))
        {
            $image = $_FILES['image'];

            $ext  = $image['name'];
            $ext  = explode('.', $ext);
            $ext  = end($ext);
            $file = $image['tmp_name'];

            $image_url = ImageIgosja::put_file($file, $ext);

            $o_image = new ImageModel();
            $o_image->image_url = $image_url;
            $o_image->save();

            $image_id = $o_image->image_id;

            $o_news = NewsModel::model()->findByPk($id);
            $o_news->news_image_id  = $image_id;
            $o_news->save();

            $redirect = 1;
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/news');
            exit;
        }

        $h1 = 'Редагування новини';

        $this->render('form', array('o_news' => $o_news, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id       = (int) $id;
        $o_news   = NewsModel::model()->findByPk($id);
        $image_id = $o_news->news_image_id;
        $o_image  = ImageModel::model()->findByPk($image_id);

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
        {
            unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
        }

        ImageModel::model()->deleteByPk($image_id);

        $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

        foreach ($a_resize as $item)
        {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
            {
                unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
            }
        }

        ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));
        NewsModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/news');
    }
}