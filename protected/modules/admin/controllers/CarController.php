<?php

class CarController extends AController
{
    public function actionIndex()
    {
        $a_car  = CarModel::model()->findAll();
        $h1     = 'Автомобілі';

        $this->render('index', array('a_car' => $a_car, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id     = (int) $id;
        $o_car  = CarModel::model()->findByPk($id);
        $status = $o_car->car_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_car->car_status = $new_status;
        $o_car->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/car');
    }

    public function actionCreate()
    {
        $redirect   = 0;

        if (isset($_POST['data']))
        {
            $c_car = new CarModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_car->$key = $value;
            }

            $c_car->car_date = time();
	    if(! $c_car->car_comfort) {
		$c_car->car_comfort = "";
	    }
	    if(! $c_car->car_safety) {
		$c_car->car_safety = "";
	    }
            $c_car->save();

            $car_id = $c_car->car_id;
            $id     = $car_id;

            if (empty($c_car->car_url))
            {
                $o_car  = CarModel::model()->findByPk($car_id);
                $o_car->car_url = $car_id . '_' . str_replace($this->rus, $this->lat, $o_car->car_name);
                $o_car->save();

                $id = $o_car->car_id;
            }

            if (isset($_POST['value']))
            {
                foreach ($_POST['value'] as $item)
                {
                    $item = (int) $item;

                    $o_carcharacteristic = new CarCharacteristicModel();
                    $o_carcharacteristic->carcharacteristic_characteristic_id = $item;
                    $o_carcharacteristic->carcharacteristic_car_id = $id;
                    $o_carcharacteristic->save();
                }
            }

            if (isset($_POST['smart']))
            {
                foreach ($_POST['smart'] as $item)
                {
                    $item = (int) $item;

                    $o_carsmart = new CarSmartModel();
                    $o_carsmart->carsmart_car_id    = $id;
                    $o_carsmart->carsmart_smart_id  = $item;
                    $o_carsmart->carsmart_status    = 1;
                    $o_carsmart->save();
                }
            }

            $redirect = 1;
        }

        if (isset($_FILES['image']['tmp_name'][0]) &&
            !empty($_FILES['image']['tmp_name'][0]))
        {
            $image = $_FILES['image'];

            for ($i=0, $count_image=count($image['tmp_name']); $i<$count_image; $i++)
            {
                $ext  = $image['name'][$i];
                $ext  = explode('.', $ext);
                $ext  = end($ext);
                $file = $image['tmp_name'][$i];

                $image_url = ImageIgosja::put_file($file, $ext);

                $o_image = new ImageModel();
                $o_image->image_url = $image_url;
                $o_image->save();

                $image_id = $o_image->image_id;

                $this->watermark($image_id);

                $o_carimage = new CarImageModel();
                $o_carimage->carimage_image_id  = $image_id;
                $o_carimage->carimage_car_id    = $id;
                $o_carimage->save();
            }

            $redirect = 1;
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/car');
            exit;
        }

        $h1                 = 'Редагування автомобілю';
        $a_body             = BodyModel::model()->findAllByAttributes(array('body_status' => 1),array('order' => 'body_name'));
        $a_color            = ColorModel::model()->findAll(array('order' => 'color_name'));
        $a_gear             = GearModel::model()->findAll(array('order' => 'gear_name'));
        $a_engine           = EngineModel::model()->findAll(array('order' => 'engine_name'));
        $a_model            = ModelModel::model()->findAll(array('order' => 'model_name'));
        $a_phase            = PhaseModel::model()->findAll(array('order' => 'phase_name'));
        $a_producer         = ProducerModel::model()->findAll(array('order' => 'producer_name'));
        $a_transmission     = TransmissionModel::model()->findAll(array('order' => 'transmission_name'));
        $a_characteristic   = CharacteristicModel::model()->findAll(array('order' => 'characteristic_characteristicgroup_id, characteristic_order'));
        $a_smart            = SmartModel::model()->findAll(array('order' => 'smart_order'));

        $this->render('form', array(
            'h1'                => $h1,
            'a_body'            => $a_body,
            'a_color'           => $a_color,
            'a_engine'          => $a_engine,
            'a_gear'            => $a_gear,
            'a_model'           => $a_model,
            'a_phase'           => $a_phase,
            'a_producer'        => $a_producer,
            'a_transmission'    => $a_transmission,
            'a_characteristic'  => $a_characteristic,
            'a_smart'           => $a_smart
        ));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_car      = CarModel::model()->findByPk($id);
        $redirect   = 0;

        if (isset($_GET['image']))
        {
            $carimage_id = (int) $_GET['image'];
            $o_carimage = CarImageModel::model()->findByPk($carimage_id);
            $image_id   = $o_carimage['carimage_image_id'];

            CarImageModel::model()->deleteByPk($carimage_id);

            $o_image = ImageModel::model()->findByPk($image_id);

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
            {
                unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
            }

            ImageModel::model()->deleteByPk($image_id);

            $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

            foreach ($a_resize as $item)
            {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
                }
            }

            ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));

            $redirect = 1;
        }

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_car->$key = $value;
            }

            $o_car->save();

            CarCharacteristicModel::model()->updateAll(array('carcharacteristic_status' => 0), 'carcharacteristic_car_id=' . $id);

            if (isset($_POST['value']))
            {
                foreach ($_POST['value'] as $item)
                {
                    $item = (int) $item;

                    $o_carcharacteristic = CarCharacteristicModel::model()->findByAttributes(array('carcharacteristic_characteristic_id' => $item, 'carcharacteristic_car_id' => $id));

                    if (!isset($o_carcharacteristic['carcharacteristic_id']))
                    {
                        $o_carcharacteristic = new CarCharacteristicModel();
                        $o_carcharacteristic->carcharacteristic_car_id = $id;
                        $o_carcharacteristic->carcharacteristic_characteristic_id = $item;
                    }

                    $o_carcharacteristic->carcharacteristic_status = 1;

                    $o_carcharacteristic->save();
                }
            }

            CarSmartModel::model()->updateAll(array('carsmart_status' => 0), 'carsmart_car_id=' . $id);

            if (isset($_POST['smart']))
            {
                foreach ($_POST['smart'] as $item)
                {
                    $item = (int) $item;

                    $o_carsmart = CarSmartModel::model()->findByAttributes(array('carsmart_smart_id' => $item, 'carsmart_car_id' => $id));

                    if (!isset($o_carsmart['carsmart_id']))
                    {
                        $o_carsmart = new CarSmartModel();
                        $o_carsmart->carsmart_car_id    = $id;
                        $o_carsmart->carsmart_smart_id  = $item;
                    }

                    $o_carsmart->carsmart_status = 1;
                    $o_carsmart->save();
                }
            }

            $redirect = 1;
        }

        if (isset($_FILES['image']['tmp_name'][0]) &&
            !empty($_FILES['image']['tmp_name'][0]))
        {
            $image = $_FILES['image'];

            for ($i=0, $count_image=count($image['name']); $i<$count_image; $i++)
            {
                $ext  = $image['name'][$i];
                $ext  = explode('.', $ext);
                $ext  = end($ext);
                $file = $image['tmp_name'][$i];

                $image_url = ImageIgosja::put_file($file, $ext);

                $o_image = new ImageModel();
                $o_image->image_url = $image_url;
                $o_image->save();

                $image_id = $o_image->image_id;

                $this->smaller($image_id);
                $this->watermark($image_id);

                $o_carimage = new CarImageModel();
                $o_carimage->carimage_image_id  = $image_id;
                $o_carimage->carimage_car_id    = $id;
                $o_carimage->save();
            }

            $redirect = 1;
        }

        if (isset($_POST['image']))
        {
            $image = $_POST['image'];

            foreach($image as $key => $value)
            {
                $id = (int) $key;

                $o_carimage = CarImageModel::model()->findByPk($id);
                $o_carimage->carimage_order = $value;
                $o_carimage->save();
            }

            $redirect = 1;
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
            exit;
        }

        $h1                 = 'Редагування автомобілю';
        $a_body             = BodyModel::model()->findAllByAttributes(array('body_status' => 1),array('order' => 'body_name'));
        $a_color            = ColorModel::model()->findAll(array('order' => 'color_name'));
        $a_engine           = EngineModel::model()->findAll(array('order' => 'engine_name'));
        $a_gear             = GearModel::model()->findAll(array('order' => 'gear_name'));
        $a_model            = ModelModel::model()->findAll(array('order' => 'model_name'));
        $a_phase            = PhaseModel::model()->findAll(array('order' => 'phase_name'));
        $a_producer         = ProducerModel::model()->findAll(array('order' => 'producer_name'));
        $a_transmission     = TransmissionModel::model()->findAll(array('order' => 'transmission_name'));
        $a_characteristic   = CharacteristicModel::model()->findAll(array('order' => 'characteristic_characteristicgroup_id, characteristic_order'));
        $a_carchar          = CarCharacteristicModel::model()->findAllByAttributes(array('carcharacteristic_car_id' => $id, 'carcharacteristic_status' => 1));
        $a_smart            = SmartModel::model()->findAll(array('order' => 'smart_order'));

        $this->render('form', array(
            'o_car'             => $o_car,
            'h1'                => $h1,
            'a_body'            => $a_body,
            'a_color'           => $a_color,
            'a_engine'          => $a_engine,
            'a_gear'            => $a_gear,
            'a_model'           => $a_model,
            'a_phase'           => $a_phase,
            'a_producer'        => $a_producer,
            'a_transmission'    => $a_transmission,
            'a_characteristic'  => $a_characteristic,
            'a_carchar'         => $a_carchar,
            'a_smart'           => $a_smart
        ));
    }

    public function actionCopy($id)
    {
        $id                 = (int) $id;
        $o_car              = CarModel::model()->findByPk($id);
        $c_car              = new CarModel();

        foreach ($o_car->attributes as $key => $value)
        {
            $c_car->$key = $o_car->$key;
        }

        $c_car->car_id      = NULL;
        $c_car->car_date    = time();
        $c_car->car_number  = '';
        $c_car->car_sku     = 0;
        $c_car->save();

        $car_id = $c_car->car_id;
        $o_car  = CarModel::model()->findByPk($car_id);
        $o_car->car_url = $car_id . '_' . str_replace($this->rus, $this->lat, $o_car->car_name);
        $o_car->save();

        $a_char = CarCharacteristicModel::model()->findAllByAttributes(array('carcharacteristic_car_id' => $id));

        foreach ($a_char as $item)
        {
            $c_char = new CarCharacteristicModel();
            $c_char->carcharacteristic_characteristic_id    = $item->carcharacteristic_characteristic_id;
            $c_char->carcharacteristic_status               = $item->carcharacteristic_status;
            $c_char->carcharacteristic_car_id               = $car_id;
            $c_char->save();
        }

        $a_smart = CarSmartModel::model()->findAllByAttributes(array('carsmart_car_id' => $id));

        foreach ($a_smart as $item)
        {
            $c_smart = new CarSmartModel();
            $c_smart->carsmart_smart_id = $item->carsmart_smart_id;
            $c_smart->carsmart_status   = $item->carsmart_status;
            $c_smart->carsmart_car_id   = $car_id;
            $c_smart->save();
        }

        $a_carimage = CarImageModel::model()->findAllByAttributes(array('carimage_car_id' => $id));

        foreach ($a_carimage as $item)
        {
            $ext  = $item['image']['image_url'];
            $ext  = explode('.', $ext);
            $ext  = end($ext);
            $file = $_SERVER['DOCUMENT_ROOT'] . $item['image']['image_url'];

            $image_url = ImageIgosja::put_file($file, $ext);

            $o_image = new ImageModel();
            $o_image->image_url = $image_url;
            $o_image->save();

            $image_id = $o_image->image_id;

            $this->smaller($image_id);
            $this->watermark($image_id);

            $o_carimage = new CarImageModel();
            $o_carimage->carimage_image_id  = $image_id;
            $o_carimage->carimage_car_id    = $car_id;
            $o_carimage->carimage_order     = $item->carimage_order;
            $o_carimage->save();
        }

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/car/edit/' . $car_id);
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        CarModel::model()->deleteByPk($id);
        CarCharacteristicModel::model()->deleteAllByAttributes(array('carcharacteristic_car_id' => $id));
        CarSmartModel::model()->deleteAllByAttributes(array('carsmart_car_id' => $id));

        $a_image = CarImageModel::model()->findAllByAttributes(array('carimage_car_id' => $id));

        foreach ($a_image as $image)
        {
            $image_id = $image['carimage_image_id'];

            CarImageModel::model()->deleteByPk($image['carimage_id']);

            $o_image = ImageModel::model()->findByPk($image_id);

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
            {
                unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
            }

            ImageModel::model()->deleteByPk($image_id);

            $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

            foreach ($a_resize as $item)
            {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
                }
            }

            ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));
        }

        $this->redirect('/admin/car');
    }

    public function smaller($image_id)
    {
        $sizeh      = 600;
        $sizew      = 800;
        $cut        = 1;
        $o_image    = ImageModel::model()->findByPk($image_id);

        if (isset($o_image['image_url']))
        {
            $image_url      = $_SERVER['DOCUMENT_ROOT'] . $o_image['image_url'];
            $image_info     = getimagesize($image_url);
            $image_height   = $image_info[1];
            $image_width    = $image_info[0];
            $h_koef         = $sizeh / $image_height;
            $w_koef         = $sizew / $image_width;

            if ($image_height > 600 ||
                $image_width > 800)
            {
                if ($h_koef > $w_koef)
                {
                    $sizew_new = $image_width * $h_koef;
                    $sizeh_new = $sizeh;
                }
                else
                {
                    $sizeh_new = $image_height * $w_koef;
                    $sizew_new = $sizew;
                }

                if ($image_info[2] == IMAGETYPE_JPEG)
                {
                    $src = imagecreatefromjpeg($image_url);
                }
                elseif($image_info[2] == IMAGETYPE_GIF)
                {
                    $src = imagecreatefromgif($image_url);
                }
                elseif($image_info[2] == IMAGETYPE_PNG)
                {
                    $src = imagecreatefrompng($image_url);
                }

                $im     = imagecreatetruecolor($sizew, $sizeh);
                $back   = imagecolorallocate($im, 255, 255, 255);
                imagefill($im, 0, 0, $back);


                $offset_x = ($sizew_new - $sizew) / $h_koef / 2;

                if(0 > $offset_x)
                {
                    $offset_x = -$offset_x;
                }

                $offset_y = ($sizeh_new - $sizeh) / $w_koef / 2;

                if(0 > $offset_y)
                {
                    $offset_y = -$offset_y;
                }

                imagecopyresampled($im, $src, 0, 0, $offset_x, $offset_y, $sizew_new, $sizeh_new, imagesx($src), imagesy($src));

                if (imagejpeg($im, $image_url, 100))
                {
                    chmod($image_url, 0777);
                }

                imagedestroy($im);
            }
        }
    }

    public function watermark($image_id)
    {
        $o_image            = ImageModel::model()->findByPk($image_id);
        $image_url          = $_SERVER['DOCUMENT_ROOT'] . $o_image['image_url'];
        $watermark          = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/assets/img/watermark.png');
        $watermark_width    = imagesx($watermark);
        $watermark_height   = imagesy($watermark);
        $image              = imagecreatefromjpeg($image_url);

        if ($image === false)
        {
            return false;
        }

        $size   = getimagesize($image_url);
        $dest_x = ($size[0] - $watermark_width) / 2;
        $dest_y = ($size[1] - $watermark_height) / 2;

        imagealphablending($image, true);
        imagealphablending($watermark, true);
        imagecopy($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
        imagejpeg($image, $image_url, 100);
        imagedestroy($image);
        imagedestroy($watermark); 
    }
}