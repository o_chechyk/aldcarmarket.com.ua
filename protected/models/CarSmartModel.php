<?php

class CarSmartModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'carsmart';
    }

    public function primaryKey()
    {
        return 'carsmart_id';
    }
}