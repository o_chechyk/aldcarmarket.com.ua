<?php

class FaqController extends AController
{
    public function actionIndex() {

        $a_faq  = FaqModel::model()->findAll(array('order' => 'faq_order'));

        $this->render('index', array('a_faq' => $a_faq));
    } // public function actionIndex()

    public function actionEdit($id=0) {

	if( $id ) {
	    $faq = FaqModel::model()->findByPk($id);
	} else {
	    $faq = new FaqModel;
	    $faq->faq_id = 0;
	    $faq->faq_question = "";
	    $faq->faq_answer = "";
	}

	if( isset($_POST['faq_id']) ) {

	    if( $id ) {
		$faq = FaqModel::model()->findByPk($id);
	    } else {
		$faq = new FaqModel;
	    } // if( $id )

	    $faq->faq_question = $_POST['faq_question'];
	    $faq->faq_answer = $_POST['faq_answer'];
	    if( isset($_POST['faq_status']) ) {
		$faq->faq_status = 1;
	    } else {
		$faq->faq_status = 0;
	    }
	    $faq->save();

	    $this->redirect('/admin/faq');
	} // if( isset($_POST['faq_id']) )

	$this->render('form', array('faq' => $faq));
    } // public function actionEdit($id=0)

    public function actionOrder() {

	$a_faq  = FaqModel::model()->findAll(array('order' => 'faq_order')); // Load FAQ data from database
	if(isset($_POST['faq_order_all']) and $_POST['faq_order_all'] != "") { // Check if any order data passed

	    $arr = explode(" ", trim($_POST['faq_order_all'])); // Convert order data from string to array
	    if( $arr ) {

		foreach($arr as $order => $faq_str) {

		    $faq_id = substr($faq_str, 13); //Extract FAQ ID from supplied string
		    foreach($a_faq as $i => $faq) { // Search for the FAQ with this ID

			if($faq->faq_id == $faq_id ) { // FAQ found

			    $a_faq[$i]->faq_order = ($order+1);
			    $a_faq[$i]->save();
			    break;

			} // if($faq->faq_id == $faq_id )

		    } // foreach($a_faq as $i => $faq)

		} // foreach($arr as $faq_str)

	    } // if( $arr )

	} // if(isset($_POST['faq_order_all']) and $_POST['faq_order_all'] != "")

	$this->redirect('/admin/faq'); // Redirect user to the updated FAQ index

    }
}
