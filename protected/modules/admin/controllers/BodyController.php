<?php

class BodyController extends AController
{
    public function actionIndex()
    {
        $a_body = BodyModel::model()->findAll();
        $h1     = 'КПП';

        $this->render('index', array('a_body' => $a_body, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id     = (int) $id;
        $o_body = BodyModel::model()->findByPk($id);
        $status = $o_body->body_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_body->body_status = $new_status;
        $o_body->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/body');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_body = new BodyModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_body->$key = $value;
            }

            $c_body->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/body');
            exit;
        }

        $h1 = 'Редагування КПП';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id     = (int) $id;
        $o_body = BodyModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_body->$key = $value;
            }

            $o_body->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/body');
            exit;
        }

        $h1 = 'Редагування КПП';

        $this->render('form', array('o_body' => $o_body, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        BodyModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/body');
    }
}