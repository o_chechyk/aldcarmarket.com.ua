<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span>Скасування підписки</span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading">Скасування підписки</h1>
        </div>
        <div class="content-left">
            <article class="content-article clearfix">
		<p>
		    <strong><center>Вашу підписку скасовано.</center></strong><br>
		    Сайт <strong><?php echo $_SERVER['HTTP_HOST']; ?></strong>
		    більше не буде надсилати оновлення на адресу <?php echo $email; ?>.<br>
		    Якщо бажаєте відновити підписку, скористайтеся формою на головній сторінці.
		</p>
		<br>
		<p>
		    <center>
			<button class="product-question-submit" onclick="document.location='<?php echo $_SERVER['HTTP_HOST'] ?>'">
			    Повернутися на головну
			</button>
		    </center>
		</p>
            </article>
        </div>
    </div>
</section>