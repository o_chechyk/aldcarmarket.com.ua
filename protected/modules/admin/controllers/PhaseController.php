<?php

class PhaseController extends AController
{
    public function actionIndex()
    {
        $a_phase    = PhaseModel::model()->findAll();
        $h1         = 'Статуси';

        $this->render('index', array('a_phase' => $a_phase, 'h1' => $h1));
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_phase = new PhaseModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_phase->$key = $value;
            }

            $c_phase->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/phase');
            exit;
        }

        $h1 = 'Редагування статуса';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_phase   = PhaseModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_phase->$key = $value;
            }

            $o_phase->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/phase');
            exit;
        }

        $h1 = 'Редагування статуса';

        $this->render('form', array('o_phase' => $o_phase, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        PhaseModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/phase');
    }
}