<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span><?php print $this->seo_h1; ?></span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading"><?php print $this->seo_h1; ?></h1>

            <div class="page-header-right">
                <span>Код авто:</span>
                <strong><?php print $o_car['car_sku']; ?></strong>
            </div>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <?php if (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'Проданий')
            echo '<div class="product-left sold">';
        elseif (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'В резерві')
            echo '<div class="product-left reserved">';
        else
            echo '<div class="product-left">';
        ?>
        <div class="product-images">
		<div class="product-image-big spinReplace" data-vin="<?php print $o_car['car_number']; ?>" data-vin7="<?php print $o_car['car_sku']; ?>">
		    <div id="spin<?php print $o_car['car_sku']; ?>" class="spinCar spinShow hidden" data-vin="<?php print $o_car['car_number']; ?>" data-vin7="<?php print $o_car['car_sku']; ?>">
			<img src="/assets/img/360_3.png">
		    </div>
		    <?php if (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'Проданий') { ?>
			<div class="soldlabel">ПРОДАНИЙ</div>
		    <?php } ?>
		    <?php if (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'В резерві') { ?>
			<div class="reservedlabel">В РЕЗЕРВІ</div>
		    <?php } ?>
		    <?php if (isset($o_car['image'][0]['image']['image_id'])) { ?>
			<a href="<?php print $o_car['image'][0]['image']['image_url']; ?>" rel="gallery">
			    <img src="<?php ImageIgosja::resize($o_car['image'][0]['image']['image_id'], 540, 405); ?>" alt="">
			</a>
		    <?php } else if( isset($spin[$o_car['car_sku']]) ) { ?>
			    <img src="<?=$spin[$o_car['car_sku']]?>" class="imgBig">
		    <?php } else { ?>
			<img src="/assets/img/image-placeholder-big.jpg">
		    <?php } ?>
		</div>
		<div class="product-images-grid clearfix">
		    <?php foreach (array_slice($o_car['image'], 1) as $item) { ?>
			<a href="<?php print $item['image']['image_url']; ?>" rel="gallery">
			    <img src="<?php ImageIgosja::resize($item['image']['image_id'], 176, 132); ?>"/>
			</a>
		    <?php } ?>
		</div>
	</div>
        <div class="product-mobile-slider-wrap bx-unstyled only-mobile">
		<div class="product-mobile-slider spinReplace" data-vin="<?php print $o_car['car_number']; ?>" data-vin7="<?php print $o_car['car_sku']; ?>">
		    <?php if( count($o_car['image']) ) { ?>
			<?php foreach ($o_car['image'] as $item) { ?>
			    <?php if (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'Проданий')
				echo '<div class="product-left sold"><div class="soldlabel">ПРОДАНИЙ</div>';
			    elseif (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'В резерві')
				echo '<div class="product-left reserved"><div class="reservedlabel">В РЕЗЕРВІ</div>';
			    else
				echo '<div class="product-left">';
			    ?>
			    <div class="slide"><img
				src="<?php ImageIgosja::resize($item['image']['image_id'], 540, 405); ?>" alt=""></div>
			    </div>
			<?php } ?>
		    <?php } else if( isset($spin[$o_car['car_sku']]) ) { ?>
			    <img src="<?=$spin[$o_car['car_sku']]?>" class="imgBig">
		    <?php } else { ?>
			<img src="/assets/img/image-placeholder-big.jpg">
		    <?php } ?>
		</div>
        </div>
    </div>
    <div class="product-right">
        <div class="product-card">
            <div class="product-card-header">
                <?php if (0 != $o_car['car_price_old']) { ?>
                    <i class="product-card-special">
                        <span>Акція</span>
                    </i>
                <?php } ?>

                <div class="product-card-priceblock">

		    <?php if (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'Проданий') { ?>
			<div class="product-card-pricelabel">ПРОДАНИЙ</div>
		    <?php } elseif (isset($o_car['phase']) && $o_car['phase']['phase_name'] == 'В резерві') { ?>
			<div class="product-card-pricelabel">В РЕЗЕРВІ</div>
		    <?php } else { ?>



			<div class="product-card-pricelabel">Ціна:</div>
			<?php if (0 != $o_car['car_price_old']) { ?>
			    <div
				class="product-card-priceold"><?php print number_format($o_car['car_price_old'], 0, ',', ' '); ?></div>
			<?php } ?>
			<div
			    class="product-card-pricenew"><?php print number_format($o_car['car_price'], 0, ',', ' '); ?>
			    грн
			</div>

                    <br>

                    <div class="fb-like" data-layout="button" data-action="like" data-show-faces="false"
                         data-share="true"></div>

		    <?php } ?>

                </div>

                <div class="product-card-bodynum">
                    <span>Номер кузова:</span>
                    <strong><?php print $o_car['car_number']; ?></strong>
                </div>
            </div>
            <?php if ($o_car['phase']['phase_name'] != 'Проданий' && $o_car['phase']['phase_name'] != 'В резерві') { ?>
                <?php if (date('w') != 0 && date('w') != 6) : ?>
<!--
                    <form class="product-card-form" method="POST" action="/form/salon">
                        <p class="call-caption">Замовити зворотній дзвінок</p>

                        <div class="product-car-form-fields clearfix">
                            <div class="product-car-form-phone">
                                <div class="input-with-icon">
                                    <span><img src="/assets/img/form-phone.png" alt=""></span>
                                    <span><input name="salon[question_email]" type="text" required></span>
                                </div>
                            </div>
                            <div>
                                <div class="input-with-icon">
                                    <span><img src="/assets/img/user.png" alt=""></span>
                                    <span><input name="salon[question_name]" type="text" required
                                             placeholder="Ваше ім'я"></span>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="salon[question_text]" value="Замовлено зворотній дзвінок щодо <?php print $o_car['car_sku']." ".$o_car['car_name']; ?>">
                        <input type="submit" class="product-card-form-submit" value="Відправити дані">
                    </form>
-->
                <?php endif; ?>
            <?php } ?>
            <div class="product-card-specs">
                <div>
                    <div>
                        <span class="product-specs-color"><?php print $o_car['color']['color_name']; ?></span>
                    </div>
                    <div>
                            <span
                                class="product-specs-transmission"><?php print $o_car['transmission']['transmission_name']; ?></span>
                    </div>
                </div>
                <div>
                    <div>
                        <span class="product-specs-year"><?php print $o_car['car_year']; ?> р.</span>
                    </div>
                    <div>
                        <span class="product-specs-mileage"><?php print $o_car['car_mileage']; ?> тис.км</span>
                    </div>
                </div>
                <div>
                    <div>
                            <span
                                class="product-specs-engine"><?php print $o_car['engine']['engine_name'] . ', ' . round($o_car['car_engine_capacity'] / 1000, 1); ?>
                                л</span>
                    </div>
                    <div>
                        <span class="product-specs-body"><?php if($o_car['body']) {print $o_car['body']['body_name'];} else {print "-";} ?></span>
                    </div>
                </div>
                <div>
                    <div>
                        <span class="product-specs-engine-power"><?php ($o_car['car_power'] == '0') ? print 'Не вказано' : print $o_car['car_power'] . ' к/с' ?> </span>
                    </div>
                    <div>
                        <span class="product-specs-drive-type"><?php print $o_car['gear']['gear_name']; ?></span>
                    </div>
                </div>
            </div>
            <div class="product-card-funcs">
                <div>
                        <!--<span
                            class="pg-product-bottom-btn pg-product-date"><?php print date('d.m.Y', $o_car['car_date']); ?></span>-->
			<?php
				if( $fav_changed ) {
				    echo "<span class=\"fav-alarm\">!</span> ";
				}
			    if( $fav_show ) {
				echo CHtml::link("<img src=\"/assets/img/blink-fav.png\" class=\"blink-fav\">"."<span>Обране</span>",
				    Yii::app()->createUrl('/?favorites'),
				    array('class'=>'pg-product-bottom-btn pg-product-isfav'));
			    }
			?>
                </div>

                    <div>
                        <?php
                        if(is_array(Yii::app()->session['favorite_cars']) && in_array($o_car['car_id'], $_SESSION['favorite_cars'])) {
                            $span = "Прибрати з обраного";
			    $class = "pg-product-removefav";
                        } else {
                            $span = 'До&nbsp;обраного';
			    $class = 'pg-product-addfav';
			}
                        echo CHtml::link(
                            "<span>$span</span>",
                            Yii::app()->createUrl('automobile/favorite' , array('id' => $o_car['car_id'])),
                            array('class'=>'pg-product-bottom-btn '.$class));
                        ?>
                    </div>

                    <div>
                        <?php
                        echo CHtml::link(
                            '<span>Роздрукувати</span>',
                            Yii::app()->createUrl('automobile/print' , array('id' => $o_car['car_sku'])),
                            array('class'=>'pg-product-bottom-btn pg-product-print','target'=>'_blank'));
                        ?>
                    </div>

            </div>
            <div class="product-card-footer">
                <div class="product-card-footer-line">
                    <?php $group_id = ''; ?>
                    <?php foreach ($a_characteristic as $item) { ?><?php if ($item['char']['group']['characteristicgroup_id'] != $group_id) {
                    $group_id = $item['char']['group']['characteristicgroup_id']; ?>
                    </p>
                </div>
                <div class="product-card-footer-line">
                    <strong><?php print $item['char']['group']['characteristicgroup_name']; ?>:</strong>

                    <p>
                        <?php } else {
                            print ', ';
                        } ?><?php print $item['char']['characteristic_name']; ?><?php } ?>
                    </p>
                </div>
                <div class="product-card-footer-divider"></div>
                <div class="product-card-footer-line">
		    <strong>Зовнішній стан авто:</strong>
                    <p>
                        <?php if(isset($o_car['car_condition']) and $o_car['car_condition'] != "") {print "<a href=\"".$o_car['car_condition']."\" target=\"_blank\">Звіт</a>";} ?>
                    </p>
                </div>
                <?php print $o_car['car_description']; ?>
            </div>
        </div>
    </div>

    <?php if( count($s_car) ) { ?>
	<div class="content-bottom">
	    <div class="content-bottom-caption">
		Схожі авто
	    </div>
	    <div class="slick-container">
		<div class="slick similar">
		    <?php foreach($s_car as $item) { ?>
			<div>
			    <a href="/automobile/show/<?php echo $item['car_id']; ?>" title="<?php echo $item['car_name']; ?>">
			    <?php if (isset($item['image'][0]['image']['image_id'])) { ?>
				<img src="<?php ImageIgosja::resize($item['image'][0]['image']['image_id'], 235, 132); ?>" alt="<?php echo $item['car_name']; ?>">
			    <?php } else if( isset($spin[$item['car_sku']]) ) { ?>
				<img src="<?=$spin[$item['car_sku']]?>" class="imgSimilar">
			    <?php } else { ?>
				<img src="/assets/img/product-grid-placeholder1.jpg">
			    <?php } ?>
			    </a>
			    <span class="add-slick-price"><?php echo number_format($item['car_price'],0,"."," "); ?></span>
			</div>
		    <?php } ?>
		</div>
	    </div>
	</div>
    <?php } ?>

    </div>
</section>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v2.5&appId=1102444686474126";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript" src="https://integrator.swipetospin.com"></script>


<div>
