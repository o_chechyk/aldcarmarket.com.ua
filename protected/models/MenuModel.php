<?php

class MenuModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'menu';
    }

    public function primaryKey()
    {
        return 'menu_id';
    }
}