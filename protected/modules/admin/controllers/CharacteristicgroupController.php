<?php

class CharacteristicgroupController extends AController
{
    public function actionIndex()
    {
        $a_characteristicgroup = CharacteristicGroupModel::model()->findAll();
        $h1                    = 'Групи характеристик';

        $this->render('index', array('a_characteristicgroup' => $a_characteristicgroup, 'h1' => $h1));
    }

    public function actionOrder()
    {
        if (isset($_POST['data']))
        {
            foreach($_POST['data'] as $key => $value)
            {
                $id                     = (int) $key;
                $o_characteristicgroup  = CharacteristicGroupModel::model()->findByPk($id);
                $o_characteristicgroup->characteristicgroup_order = $value;
                $o_characteristicgroup->save();
            }

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/characteristicgroup');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_characteristicgroup = new CharacteristicGroupModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_characteristicgroup->$key = $value;
            }

            $c_characteristicgroup->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/characteristicgroup');
            exit;
        }

        $h1 = 'Редагування групи характеристик';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id                     = (int) $id;
        $o_characteristicgroup  = CharacteristicGroupModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_characteristicgroup->$key = $value;
            }

            $o_characteristicgroup->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/characteristicgroup');
            exit;
        }

        $h1 = 'Редагування групи характеристик';

        $this->render('form', array('o_characteristicgroup' => $o_characteristicgroup, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        CharacteristicGroupModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/characteristicgroup');
    }
}