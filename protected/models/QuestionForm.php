<?php

class QuestionForm extends CFormModel {

    public $question_name;
    public $question_email;
    public $question_text;
    public $verifyCode;

    public function rules() {

        return array(
            array('question_name, question_email, question_text', 'required'),
	    array('verifyCode', 'required'),
            array('verifyCode', 'ext.yiiReCaptcha.ReCaptchaValidator'),
        );
    }
}
