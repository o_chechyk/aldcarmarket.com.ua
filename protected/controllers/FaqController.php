<?php

class FaqController extends Controller
{
    public function actionIndex() {

	$o_faqseo = SeoModel::model()->findByPk(2); // 2 - FAQ SEO

        $o_faq = FaqModel::model()->findAll( array('condition' => 'faq_status=1', 'order' => 'faq_order') );

	if($o_faqseo and $o_faqseo['seo_title'] != "") {
	    $this->seo_title = $o_faqseo['seo_title'];
	} else {
	    $this->seo_title = "Часті питання";
	}

	if($o_faqseo and $o_faqseo['seo_description'] != "") {
		$this->seo_description = $o_faqseo['seo_description'];
	} else {
		$this->seo_description = '';
	}

	if($o_faqseo and $o_faqseo['seo_keywords'] != "") {
		$this->seo_keywords = $o_faqseo['seo_keywords'];
	} else {
		$this->seo_keywords = '';
	}

	if ($o_faqseo and ! empty($o_faqseo['seo_text'])) {
	    $this->seo_text = $o_faqseo['seo_text'];
	}

        $this->render('index', array('o_faq' => $o_faq));
    }
}
