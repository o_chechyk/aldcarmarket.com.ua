<?php

class CarModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'car';
    }

    public function primaryKey()
    {
        return 'car_id';
    }

    public function relations()
    {
        return array
        (
            'body'          => array(self::HAS_ONE, 'BodyModel', array('body_id' => 'car_body_id')),
            'color'         => array(self::HAS_ONE, 'ColorModel', array('color_id' => 'car_color_id')),
            'gear'          => array(self::HAS_ONE, 'GearModel', array('gear_id' => 'car_gear_id')),
            'image'         => array(self::HAS_MANY, 'CarImageModel', array('carimage_car_id' => 'car_id'), 'order' => 'carimage_order'),
            'engine'        => array(self::HAS_ONE, 'EngineModel', array('engine_id' => 'car_engine_id')),
            'phase'         => array(self::HAS_ONE, 'PhaseModel', array('phase_id' => 'car_phase_id')),
            'smart'         => array(self::HAS_MANY, 'CarSmartModel', array('carsmart_car_id' => 'car_id')),
            'transmission'  => array(self::HAS_ONE, 'TransmissionModel', array('transmission_id' => 'car_transmission_id')),
        );
    }
}