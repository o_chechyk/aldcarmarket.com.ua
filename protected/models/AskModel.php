<?php

class AskModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'ask';
    }

    public function primaryKey()
    {
        return 'ask_id';
    }

    public function relations()
    {
        return array
        (
            'car' => array(self::HAS_ONE, 'CarModel', array('car_id' => 'ask_car_id')),
	    'a_manager' => array(self::HAS_ONE, 'UserModel', array('user_id' => 'ask_manager')),
        );
    }
}