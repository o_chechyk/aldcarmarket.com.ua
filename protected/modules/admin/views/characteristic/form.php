<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/characteristic" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Група</td>
                        <td>
                            <select class="form-control" name="data[characteristic_characteristicgroup_id]">
                                <?php foreach ($a_characteristicgroup as $item) { ?>
                                    <option
                                        value="<?php print $item['characteristicgroup_id']; ?>"
                                        <?php if (isset ($o_characteristic) && $item['characteristicgroup_id'] == $o_characteristic['characteristic_characteristicgroup_id']) { ?>
                                            selected
                                        <?php } ?>
                                    >
                                        <?php print $item['characteristicgroup_name']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Назва</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[characteristic_name]"
                                type="text"
                                value="<?php if (isset ($o_characteristic)) { print $o_characteristic['characteristic_name']; }?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Ключ імпорту</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[characteristic_import_key]"
                                type="text"
                                value="<?php if (isset ($o_characteristic)) { print $o_characteristic['characteristic_import_key']; }?>"
                            />
                        </td>
                    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
</div>