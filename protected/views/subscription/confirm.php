<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span>Підтвердження підписки</span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading">Підтвердження підписки</h1>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="content-left">
            <article class="content-article clearfix">
		<p>
		    <center>
			<button class="product-question-submit" onclick="document.location='<?php echo $_SERVER['HTTP_HOST'] ?>'">
			    Повернутися на головну
			</button>
		    </center>
		</p>
            </article>
        </div>
    </div>
</section>