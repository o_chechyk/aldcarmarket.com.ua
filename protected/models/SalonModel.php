<?php

class SalonModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'salon';
    }

    public function primaryKey()
    {
        return 'salon_id';
    }

    public function relations()
    {
        return array
        (
            'car' => array(self::HAS_ONE, 'CarModel', array('car_id' => 'salon_car_id')),
        );
    }
}