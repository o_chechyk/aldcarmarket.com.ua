<?php

class ContactsController extends Controller
{
    public function actionIndex()
    {
        $this->seo_title        = $this->contacts['seo_title'];
        $this->seo_keywords     = $this->contacts['seo_keywords'];
        $this->seo_description  = $this->contacts['seo_description'];

        if (!empty($this->contacts['seo_text']))
        {
            $this->seo_text = $this->contacts['seo_text'];
        }

        $this->render('index');
    }
}
