<section class="content">
    <div class="wrap clearfix">
        <div class="breadcrumbs">
            <div xmlns:v="http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="/" title="">Головна</a>
                </span>
                <span>Новини</span>
            </div>
        </div>
        <div class="page-header">
            <h1 class="page-heading">Новини</h1>
        </div>
        <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message-success">
                <?php print Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php } ?>
        <?php if (Yii::app()->user->hasFlash('error')) { ?>
            <div class="message-error">
                <?php print Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php } ?>
        <div class="content-left">
            <?php foreach ($a_news as $item) { ?>
                <a href="/news/view/<?php print $item['news_url']; ?>" class="news-block">
                    <span class="news-block-content clearfix">
                        <span class="news-block-left">
                            <img src="<?php if (isset($item['image']['image_id'])) { ImageIgosja::resize($item['image']['image_id'], 280, 210); } else { print '/assets/img/news-block-placeholder.jpg'; } ?>" alt="">
                        </span>
                        <span class="news-block-right">
                            <span class="news-block-header">
                                <span class="news-block-name"><?php print $item['news_name']; ?></span>
                                <span class="news-block-date"><?php print date('d.m.Y', $item['news_date']); ?></span>
                            </span>
                            <span class="news-block-text">
                                <p><?php print mb_substr(strip_tags($item['news_text']), 0, 300); ?></p>
                            </span>
                            <span class="news-block-btn">Детальніше</span>
                        </span>
                    </span>
                </a>
            <?php } ?>
                <div class="products-pagination">
                    <div class="products-pagination-info">
                        Відображено новини з <?php print $limit * $offset + 1; ?> по <?php print $limit * $offset + count($a_news); ?> із <?php print $count_news; ?>
                    </div>
                    <a href="<?php print $prev; ?>" class="products-grid-pagination-prev">Попередня</a>
                    <span class="products-pagination-pages">
                        <?php foreach ($a_pagination as $item) { ?>
                            <?php if ($offset + 1 == $item['page']) { ?><b><?php } ?>
                                <a href="<?php print $item['url']; ?>" <?php if ($offset + 1 == $item['page']) { ?>class="active"<?php } ?>>
                                    <span><?php print $item['page']; ?></span>
                                </a>
                        <?php } ?>
                    </span>
                    <a href="<?php print $next; ?>" class="products-grid-pagination-next">Наступна</a>
                </div>
        </div>
        <!-- <div class="content-right">
            <form class="content-right-form" method="POST" action="/form/salon">
                <div class="content-right-form-header">
                    <strong>Щоб завітати до нашого салону заповніть, <br>будь ласка, форму нижче.</strong>
                </div>
                <div class="content-right-form-body">
                    <div class="content-right-form-phone">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-phone.png" alt=""></span>
                            <span><input name="salon[salon_phone]" type="text" required></span>
                        </div>
                    </div>
                    <div class="content-right-form-date">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-date.png" alt=""></span>
                            <span><input name="salon[salon_date]" type="text" required></span>
                        </div>
                    </div>
                    <div class="content-right-form-time">
                        <div class="input-with-icon">
                            <span><img src="/assets/img/form-time.png" alt=""></span>
                            <span><input name="salon[salon_time]" type="text" required></span>
                        </div>
                    </div>
                    <input type="submit" class="blue-submit" value="Відправити дані">
                </div>
            </form>
        </div> -->
    </div>
</section>