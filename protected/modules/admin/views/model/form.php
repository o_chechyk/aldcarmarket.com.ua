<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/model" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Назва</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[model_name]"
                                type="text"
                                value="<?php if (isset ($o_model)) { print $o_model['model_name']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Марка</td>
                        <td>
                            <select name="data[model_producer_id]" class="form-control">
                                <?php foreach ($a_producer as $item) { ?>
                                    <option
                                        value="<?php print $item['producer_id']; ?>"
                                        <?php if (isset($o_model) && $o_model['model_producer_id'] == $item['producer_id']) { ?>
                                            selected
                                        <?php } ?>
                                    >
                                        <?php print $item['producer_name']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->