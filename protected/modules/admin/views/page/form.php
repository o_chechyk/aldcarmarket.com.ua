<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/page" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" enctype="multipart/form-data">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab">Загальна інформація</a></li>
                <li><a href="#seo" data-toggle="tab">SEO</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="main">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">Назва</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[page_name]"
                                        type="text"
                                        value="<?php if (isset ($o_page)) { print $o_page['page_name']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">ЧПУ</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[page_url]"
                                        type="text"
                                        value="<?php if (isset ($o_page)) { print $o_page['page_url']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Текст 1</td>
                                <td>
                                    <textarea
                                        class="ckeditor"
                                        name="data[page_text_1]"
                                        rows="3"><?php if (isset ($o_page)) { print $o_page['page_text_1']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Текст 2</td>
                                <td>
                                    <textarea
                                        class="ckeditor"
                                        name="data[page_text_2]"
                                        rows="3"><?php if (isset ($o_page)) { print $o_page['page_text_2']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Текст 3</td>
                                <td>
                                    <textarea
                                        class="ckeditor"
                                        name="data[page_text_3]"
                                        rows="3"><?php if (isset ($o_page)) { print $o_page['page_text_3']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Текст 4</td>
                                <td>
                                    <textarea
                                        class="ckeditor"
                                        name="data[page_text_4]"
                                        rows="3"><?php if (isset ($o_page)) { print $o_page['page_text_4']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Зображення 1</td>
                                <td>
                                    <?php if (isset($o_page['image_1']['image_url'])) { ?>
                                        <div class="col-lg-3">
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php print $o_page['image_1']['image_url']; ?>" />
                                            </a>
                                        </div>
                                        <a href="?image_1=<?php print $o_page['page_image_1']; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    <?php } else { ?>
                                        <input type="file" name="image_1" />
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Зображення 2</td>
                                <td>
                                    <?php if (isset($o_page['image_2']['image_url'])) { ?>
                                        <div class="col-lg-3">
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php print $o_page['image_2']['image_url']; ?>" />
                                            </a>
                                        </div>
                                        <a href="?image_2=<?php print $o_page['page_image_2']; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    <?php } else { ?>
                                        <input type="file" name="image_2" />
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">Зображення 3</td>
                                <td>
                                    <?php if (isset($o_page['image_3']['image_url'])) { ?>
                                        <div class="col-lg-3">
                                            <a href="javascript:;" class="thumbnail">
                                                <img src="<?php print $o_page['image_3']['image_url']; ?>" />
                                            </a>
                                        </div>
                                        <a href="?image_3=<?php print $o_page['page_image_3']; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    <?php } else { ?>
                                        <input type="file" name="image_3" />
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="seo">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td class="col-lg-3">SEO title</td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="data[seo_title]"
                                        type="text"
                                        value="<?php if (isset ($o_page)) { print $o_page['seo_title']; } ?>"
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO description</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_description]"><?php if (isset ($o_page)) { print $o_page['seo_description']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO keywords</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_keywords]"><?php if (isset ($o_page)) { print $o_page['seo_keywords']; } ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-lg-3">SEO text</td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="data[seo_text]"><?php if (isset ($o_page)) { print $o_page['seo_text']; } ?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->