jQuery(document).ready(function($) {
    $('body').on('click', '.sidebar-filters-accordion-head', function() {
        $(this).parent().toggleClass('active');
    });

//    $('body').on('click', '.sidebar-filters-select', function(e) {
//        if ( !$(e.target).is('select') ) {
//            console.log('not select');
//            $(this).find('select').trigger('click');
//        }
//        console.log(e.target);
//    });

$('#model-select').parent().hover(function() {
    var self = $(this);
    $('#producer-select').val() == 0 ? self.addClass('inactiveselect') : self.removeClass('inactiveselect');
}, function() {
    var self = $(this);
    self.removeClass('inactiveselect');
});

    $('.main-slider').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        auto: true,
        pause: 10000
    });

    $('.product-mobile-slider').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        pager: false
    });

    $('.pg-product-labels-wrap').matchHeight();
    
    formatSidebarPrice();
    $('#sidebar-price-slider').each(function() {
        var self = $(this),
            min = self.data('min'),
            max = self.data('max')
            mincurrent = self.data('mincurrent'),
            maxcurrent = self.data('maxcurrent');
        self.slider({
            range: true,
            min: min,
            max: max,
            step: 100,
            values: [ mincurrent, maxcurrent ],
            slide: function( event, ui ) {
                $('#sidebar-price-from').val(ui.values[ 0 ]);
                $('#sidebar-price-to').val(ui.values[ 1 ]);
                formatSidebarPrice();
                filterButtonPosition($(this));
            }
        });
    });

    $('#sidebar-year-slider').each(function() {
        var self = $(this),
            min = self.data('min'),
            max = self.data('max')
            mincurrent = self.data('mincurrent'),
            maxcurrent = self.data('maxcurrent');
        self.slider({
            range: true,
            min: min,
            max: max,
            step: 1,
            values: [ mincurrent, maxcurrent ],
            slide: function( event, ui ) {
                $('#sidebar-year-from').val(ui.values[ 0 ]);
                $('#sidebar-year-to').val(ui.values[ 1 ]);
                formatSidebarYear();
                filterButtonPosition($(this));
            }
        });
    });

    $('#sidebar-capacity-slider').each(function() {
        var self = $(this),
            min = self.data('min'),
            max = self.data('max')
            mincurrent = self.data('mincurrent'),
            maxcurrent = self.data('maxcurrent');
        self.slider({
            range: true,
            min: min,
            max: max,
            step: 0.1,
            values: [ mincurrent, maxcurrent ],
            slide: function( event, ui ) {
                $('#sidebar-capacity-from').val(ui.values[ 0 ]);
                $('#sidebar-capacity-to').val(ui.values[ 1 ]);
                filterButtonPosition($(this));
            }
        });
    });
    
    $('body').on('keyup', '#sidebar-price-from', function() {
        var self = $(this),
            value = self.val(),
            price = numeral(value);
        self.val(price.format('0,0'));
        $('#sidebar-price-slider').slider('values', 0, numeral().unformat( price.value() ) );
    });
    $('body').on('keyup', '#sidebar-price-to', function() {
        var self = $(this),
            value = self.val(),
            price = numeral(value);
        self.val(price.format('0,0'));
        $('#sidebar-price-slider').slider('values', 1, numeral().unformat( price.value() ) );
    });

    $('body').on('keyup change input', '#sidebar-year-from', function() {
        var self = $(this),
            value = self.val(),
            min = self.data('min'),
            max = self.data('max'),
            year;
        if ( value.length === 4 && (value < min || value > max ) ) {
            value = min;
        } else if ( value.length > 4 ) {value = min;}
        year = numeral(value);
        self.val(year.format('0'));
        $('#sidebar-year-slider').slider('values', 0, numeral().unformat( year.value() ) );
    });
    $('body').on('keyup change input', '#sidebar-year-to', function() {
        var self = $(this),
            value = self.val(),
            min = self.data('min'),
            max = self.data('max'),
            year;
        if ( value.length === 4 && (value < min || value > max) ) {
            value = max;
        } else if ( value.length > 4 ) {value = max;}
        year = numeral(value);
        self.val(year.format('0'));
        $('#sidebar-year-slider').slider('values', 1, numeral().unformat( year.value() ) );
    });

    $('body').on('keyup change input', '#sidebar-capacity-from', function() {
        $('#sidebar-capacity-slider').slider('values', 0, $(this).val() );
    });
    $('body').on('keyup change input', '#sidebar-capacity-to', function() {
        $('#sidebar-capacity-slider').slider('values', 1, $(this).val() );
    });

    $('body').on('click', '.smartfilter-delete', function() {
        console.log( $(this).siblings('input') );
//        $(this).siblings('input').prop('checked', false);
	$(this).siblings('label').trigger('click');
    });

    $('.product-images a').colorbox({maxWidth: '95%', maxHeight: '95%'});

    $('.contacts-block-body').matchHeight();

    if ( $('#contacts-map').length ) {
        initializeMap();
    }

    $(".product-car-form-phone input, .content-right-form-phone input")
        .inputmask("(999) 999 99 99",{clearMaskOnLostFocus: false});
    $(".product-car-form-time input, .content-right-form-time input")
        .inputmask("99 : 99",{clearMaskOnLostFocus: false});
/*
    $(".product-car-form-date input, .content-right-form-date input")
        .inputmask("99 . 99 . 9999",{clearMaskOnLostFocus: false});

    $('.product-car-form-date input, .content-right-form-date input').datepicker({
        minDate: 0
    });
*/
    if ( $('.product-card-form').length ) {

        var currentHours = (new Date()).getHours(),
            startsHour = 9,
            endsHour = 18;

        if ( currentHours <= endsHour && currentHours >= startsHour ) {
            if ( currentHours > startsHour ) {
                startsHour = currentHours;
            }
            $('.product-car-form-time input').timepicker({
                showPeriodLabels: false,
                rows: 2,
                hours: {
                    starts: startsHour,
                    ends: endsHour
                },
                minutes: {
                    interval: 15
                }
            });
        } else {
            $('.product-card-form').hide();
        }
    }

    $('.content-right-form-time input').timepicker({
        showPeriodLabels: false,
        minutes: {
            interval: 15
        }
    });



    //Igosja start code
    $('#filter-empty').on('click', function()
    //Очищаем фильтры
    {
        $('input[type=checkbox]').prop('checked', false);
        $('select').val(0);
        $('input[type=text]').val('');
	$('.sidebar-filters-submit').trigger('click');
    });

    modelSelect();

    $('#producer-select').on('change', function()
    //Связка между маркой и моделью авто
    {
        $('#model-select').val(0);
	modelSelect();
         //var producer = $(this).val();
         //$('#model-select').val(0);
         //var option_list = $('#model-select option');
         //
         //option_list.hide();
         //
         //for (var i=0; i<option_list.length; i++)
         //{
         //    var data_producer = $(option_list[i]).data('producer');
         //
         //    if (data_producer == producer || data_producer == 0)
         //    {
         //        $(option_list[i]).show();
         //    }
         //}
    });

    $('.sidebar-floating-submit').on('click', function()
    //Подбор авто при клике на всплывающую форму
    {
        $('.sidebar-filters-submit').trigger('click');
    });
    //Igosja end code

    $('.header-top-mobile-contacts').on('click', function() {
        $('.header-top-mobile-contacts-dropdown').toggle();
    });

    $('.mobile-menu-btn').on('click', function() {
        $('.mobile-menu').toggleClass('active');
    });

    $('.sidebar-mobile-left').on('click', function() {
        $('.sidebar-filters-form').addClass('active');
    });

    $('.sidebar-filters-heading').on('click', function() {
        $('.sidebar-filters-form').removeClass('active');
    });

    $('.sidebar-mobile-right').on('click', function() {
        $('.sidebar-sorting-form').addClass('active');
    });

    $('.sidebar-sorting-heading').on('click', function() {
        $('.sidebar-sorting-form').removeClass('active');
    });

    $('body').on('change', '.sidebar-filters select', function() {
        filterButtonPosition($(this));
    });

    $('body').on('change input', '.sidebar-filters input[type="text"]', function() {
        filterButtonPosition($(this));
    });

    $('body').on('change input', '.sidebar-filter-checkbox input, .sidebar-filter-checkboxes input', function() {
        filterButtonPosition($(this).next());
    });

    $('body').on('click', '.sidebar-floating-close', function() {
        $('.filters-result').hide();
    });

    $('#g-recaptcha-response').attr("required", "required");

    var cf=document.getElementById("contacts-form");
    if( cf ) {
	cf.addEventListener("submit", function(event) {
	    check_if_capcha_is_filled(event);
	});
    }
    cf=document.getElementById("product-ask-form");
    if( cf ) {
	cf.addEventListener("submit", function(event) {
	    check_if_capcha_is_filled(event);
	});
    }

	$( "#accordion" ).accordion({
	    collapsible: true
	});

    if( $(".slick").hasClass("similar") ) {
	$('.slick').slick({
	    arrows: true,
	    slidesToShow: 3,
	    variableWidth: false,
	    useTransform: false
	});
    } else if( $(".slick").hasClass("review") ){
	$('.slick').slick({
	    arrows: true,
//	    prevArrow: '<button type="button" data-role="none" class="slick-prev">Previous</button>',
	    slidesToShow: 5,
	    variableWidth: false,
	    useTransform: false
	});
    }

//    var spinFrame=document.getElementById( 'spinFrame' );
//    if(spinFrame != undefined) {
//	window.addEventListener("resize", function() {
//	    resizeSpin();
//	});
//	resizeSpin();
//    }

});


$(window).on('scroll', function() {
    if ( $('.filters-result').is(':visible') ) {
        filterButtonScrollPosition();
    }
});

function modelSelect() {
	var producer = $('#producer-select').val();
	//$('#model-select').val(0);
	var option_list = $('#model-select option');
    option_list.hide();

    for (var i=0; i<option_list.length; i++)
    {
        var data_producer = $(option_list[i]).data('producer');

        if (data_producer == producer || data_producer == 0)
        {
            $(option_list[i]).show();
        }
    }
}

function filterButtonScrollPosition() {
    var offset = $('.filters-result').offset().top,
        bottomOffset = $('.sidebar-filters').offset().top + $('.sidebar-filters').height(),
        scroll = $(window).scrollTop(),
        bottomScroll = scroll + $(window).height();
        if (scroll >= offset && scroll < bottomOffset - 150) {
            $('.filters-result').css('top', scroll - 120);
        } else if (offset >= bottomScroll - 50) {
            $('.filters-result').css('top', bottomScroll - 200);
        }
    // console.log(offset, scroll, bottomScroll);
}

function filterButtonPosition(element) {
    $('.filters-result').css('top', (element.offset().top - 164) ).show();
}

// function formatSidebarCapacity() {
//     var capacityFrom = $('#sidebar-capacity-from'),
//         capacityTo = $('#sidebar-capacity-to');
//     capacityFrom.val( numeral(capacityFrom.val()).format('0.0') );
//     capacityTo.val( numeral(capacityTo.val()).format('0.0') );
// }

function formatSidebarPrice() {
    var priceFrom = $('#sidebar-price-from'),
        priceTo = $('#sidebar-price-to');
    priceFrom.val( numeral(priceFrom.val()).format('0,0') );
    priceTo.val( numeral(priceTo.val()).format('0,0') );
}

function formatSidebarYear() {
    var yearFrom = $('#sidebar-year-from'),
        yearTo = $('#sidebar-year-to');
    yearFrom.val( numeral(yearFrom.val()).format('0') );
    yearTo.val( numeral(yearTo.val()).format('0') );
}

function initializeMap() {
    var lat = $('#contacts-map').data('lat');
    var lng = $('#contacts-map').data('lng');
    var myLatlng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{
            featureType: "poi.business",
            elementType: "labels",
            stylers: [{visibility: "off"}]
        }],
        scrollwheel: false
    };
    var map = new google.maps.Map(document.getElementById("contacts-map"), mapOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        icon: '/assets/img/pin.png'
    });
}

$(window).resize(function() {});
$(window).load(function() {});


var allowSubmit = false;

function capcha_filled () {
    allowSubmit = true;
    return true;
}

function capcha_expired () {
    allowSubmit = false;
    grecaptcha.reset();
    return true;
}

function check_if_capcha_is_filled (event) {
    if(allowSubmit) {
	return true;
    } else {
	$(".reCaptcha-div-error").fadeIn("fast");
	event.preventDefault();
	setTimeout(function() { $(".reCaptcha-div-error").fadeOut("slow") }, 2000);
	return false;
    }
}


function setCookie( mode ) {

    $.get('/cookie/set?mode='+mode, function(data) {

    });

    $(".cookieText").addClass( "hidden" );

}

function loadRules( topic ) {

    var buf=$("#rules-container-"+topic).text();
    if(buf == '') {
	$.get('/cookie/rules?topic='+topic, function(data) {
	    $("#rules-container-"+topic).html( data );
	    $("#rules-container-"+topic).toggle();
	});
    } else {
	$("#rules-container-"+topic).toggle();
    }
}


function showSubscribe() {

    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $(".subscribeModal").removeClass( "hidden" );
    $(".subscribeContainer").removeClass( "hidden" );
    $("#subscribeAccept").prop("checked", false);
    subscribeAccept();

    var buf=$(".subscribeText").text();
    if(buf == '') {
	$.get('/cookie/rules?topic=privacy', function(data) {
	    $("#rules-container-privacy").html( data );
	});
    }

}


function hideSubscribe() {
    $(".subscribeModal").addClass( "hidden" );
    $(".subscribeContainer").addClass( "hidden" );
}


function postSubscribe() {

    if($("#subscribeEmail").val() == '') {
	alert("Будь-ласка, введіть адресу електронної пошти");
    } else {
	$.post('/subscription/subscribe',
	    $("#formSubscription").serialize(),
	    function(data) {
		if(data == 'Ok') {
		    alert( 'На Вашу поштову скриньку доправлено привітального листа, який містить посилання для підтвердження Вашої електронної адреси.\n'+
			    'Якщо Ви не отримали лист-підтвердження, перевірте папки зі спамом або небажаною поштою\n'+
			    '\nДякуємо за підписку!');
		    hideSubscribe();
		} else {
		    alert( data );
		}
	});
    }

}


function subscribeAccept() {
    if( $("#subscribeAccept").prop("checked") ) {
	$("#subscribeSubmit").prop("disabled", false);
	if($(".subscribeForm").css("display") == 'none') {
	    $(".subscribeAccept").css("display", "none");
	    $(".subscribeForm").css("display", "block");
	}
    } else {
	$("#subscribeSubmit").prop("disabled", true);
	if($(".subscribeAccept").css("display") == 'none') {
	    $(".subscribeForm").css("display", "none");
	    $(".subscribeAccept").css("display", "block");
	}
    }
}


function resizeSpin() {
    var newHeight;
    newHeight = Math.round((window.innerWidth - 20) / 1.34);
    $("#spinFrame").css("height", newHeight+"px");
}
