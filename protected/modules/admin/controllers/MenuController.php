<?php

class MenuController extends AController
{
    public function actionIndex()
    {
        $a_menu = MenuModel::model()->findAll(array('order' => 'menu_order ASC'));
        $h1     = 'Меню';

        $this->render('index', array('a_menu' => $a_menu, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id     = (int) $id;
        $o_menu = MenuModel::model()->findByPk($id);
        $status = $o_menu->menu_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_menu->menu_status = $new_status;
        $o_menu->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/menu');
    }

    public function actionOrder()
    {
        if (isset($_POST['data']))
        {
            foreach($_POST['data'] as $key => $value)
            {
                $id     = (int) $key;
                $o_menu = MenuModel::model()->findByPk($id);
                $o_menu->menu_order = $value;
                $o_menu->save();
            }

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/menu');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_menu = new MenuModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_menu->$key = $value;
            }

            $c_menu->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/menu');
            exit;
        }

        $h1 = 'Редагування меню';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id     = (int) $id;
        $o_menu = MenuModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_menu->$key = $value;
            }

            $o_menu->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/menu');
            exit;
        }

        $h1 = 'Редагування меню';

        $this->render('form', array('o_menu' => $o_menu, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        MenuModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/menu');
    }
}