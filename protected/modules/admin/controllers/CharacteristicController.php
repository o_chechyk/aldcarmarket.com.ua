<?php

class CharacteristicController extends AController
{
    public function actionIndex()
    {
        $a_characteristic = CharacteristicModel::model()->findAll();
        $h1               = 'Характеристики';

        $this->render('index', array('a_characteristic' => $a_characteristic, 'h1' => $h1));
    }

    public function actionOrder()
    {
        if (isset($_POST['data']))
        {
            foreach($_POST['data'] as $key => $value)
            {
                $id                = (int) $key;
                $o_characteristic  = CharacteristicModel::model()->findByPk($id);
                $o_characteristic->characteristic_order = $value;
                $o_characteristic->save();
            }

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/characteristic');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_characteristic = new CharacteristicModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_characteristic->$key = $value;
            }

            $c_characteristic->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/characteristic');
            exit;
        }

        $h1 = 'Редагування характеристики';
        $a_characteristicgroup = CharacteristicGroupModel::model()->findAll(array('order' => 'characteristicgroup_order'));

        $this->render('form', array('h1' => $h1, 'a_characteristicgroup' => $a_characteristicgroup));
    }

    public function actionEdit($id)
    {
        $id                = (int) $id;
        $o_characteristic  = CharacteristicModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_characteristic->$key = $value;
            }

            $o_characteristic->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/characteristic');
            exit;
        }

        $h1 = 'Редагування характеристики';
        $a_characteristicgroup = CharacteristicGroupModel::model()->findAll(array('order' => 'characteristicgroup_order'));

        $this->render('form', array('o_characteristic' => $o_characteristic, 'h1' => $h1, 'a_characteristicgroup' => $a_characteristicgroup));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        CharacteristicModel::model()->deleteByPk($id);
        CarCharacteristicModel::model()->deleteAllByAttributes(array('carcharacteristic_characteristic_id' => $id));

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/characteristic');
    }
}