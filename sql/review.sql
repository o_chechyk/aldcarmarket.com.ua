drop table if exists review;

create table review (
cookie_id int(11),
car_id int(11) not null,
last_seen timestamp default NOW(),
unique key (cookie_id, car_id),
constraint rev_fk_1 foreign key (cookie_id) references cookie(cookie_id) on delete cascade,
constraint rev_fk_2 foreign key (car_id) references car(car_id) on delete cascade
);
