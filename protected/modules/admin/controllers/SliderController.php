<?php

/**
 * Created by PhpStorm.
 * User: Игорь Бойко
 * Date: 22.03.2016
 * Time: 10:50
 */
class SliderController extends AController
{
    /**
     *
     */
    public function actionIndex()
    {
        $models = SliderModel::model()->findAll();
        $h1 = 'Слайдер';

        $this->render('index', array('models' => $models, 'h1' => $h1));
    }

    /**
     *
     */
    public function actionCreate()
    {
        $redirect = 0;

        if (isset($_POST['data'])) {
            $model = new SliderModel();


            foreach ($_POST['data'] as $key => $value) {
                $model->$key = $value;
            }

		// Пока ставим заглушку, правильный ID запишем после сохранения файла.
		$model-> img_id = 0;

            $model->save();

            $slider_id = $model->slider_id;

            $redirect = 1;
        }

        if (isset($_FILES['image']['name']) &&
            !empty($_FILES['image']['name'])
        ) {
            $image = $_FILES['image'];

            $ext = $image['name'];
            $ext = explode('.', $ext);
            $ext = end($ext);
            $file = $image['tmp_name'];

            $image_url = ImageIgosja::put_file($file, $ext);

            $o_image = new ImageModel();
            $o_image->image_url = $image_url;
            $o_image->save();

            $image_id = $o_image->image_id;

            $slider_model = SliderModel::model()->findByPk($slider_id);
            $slider_model->img_id = $image_id;
            $slider_model->save();

            $redirect = 1;
        }

        if (1 == $redirect) {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/slider');
            exit;
        }

        $h1 = 'Редагування слайдеру';

        $this->render('form', array('h1' => $h1));

    }

    /**
     * @param $id
     */
    public function actionEdit($id)
    {
        $id = (int)$id;
        $slider_model = SliderModel::model()->findByPk($id);
        $redirect = 0;

        if (isset($_GET['image'])) {
            $image_id = (int)$_GET['image'];

            $o_image = ImageModel::model()->findByPk($image_id);

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url'])) {
                unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
            }

            ImageModel::model()->deleteByPk($image_id);


            $redirect = 1;
        }

        if (isset($_POST['data'])) {
            foreach ($_POST['data'] as $key => $value) {
                $slider_model->$key = $value;
            }

            $slider_model->save();

            $redirect = 1;
        }

        if (isset($_FILES['image']['name']) &&
            !empty($_FILES['image']['name'])
        ) {
            $image = $_FILES['image'];

            $ext = $image['name'];
            $ext = explode('.', $ext);
            $ext = end($ext);
            $file = $image['tmp_name'];

            $image_url = ImageIgosja::put_file($file, $ext);

            $o_image = new ImageModel();
            $o_image->image_url = $image_url;
            $o_image->save();

            $image_id = $o_image->image_id;

            $slider_model = SliderModel::model()->findByPk($id);
            $slider_model->img_id = $image_id;
            $slider_model->save();

            $redirect = 1;
        }

        if (1 == $redirect) {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/slider');
            exit;
        }

        $h1 = 'Редагування новини';

        $this->render('form', array('model' => $slider_model, 'h1' => $h1));

    }

    /**
     * @param $id
     */
    public function actionDelete($id)
    {
        $id = (int)$id;
        $slider = SliderModel::model()->findByPk($id);

        if($slider->img_id != 0) {
            $img = ImageModel::model()->findByPk($slider->img_id);
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $img['image_url'])) {
                unlink($_SERVER['DOCUMENT_ROOT'] . $img['image_url']);
            }
            $img->delete();
        }
        $slider->delete();

        $this->redirect('/admin/slider');

    }

    /**
     * @param $id
     */
    public function actionStatus($id)
    {
        $id = (int)$id;
        $model = SliderModel::model()->findByPk($id);
        $status = $model->status;

        if (0 == $status) {
            $new_status = 1;
        } else {
            $new_status = 0;
        }

        $model->status = $new_status;
        $model->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/slider');
    }

    /**
     * @param $image_id
     */
    public function smaller($image_id)
    {
        $sizeh = 600;
        $sizew = 800;
        $cut = 1;
        $o_image = ImageModel::model()->findByPk($image_id);

        if (isset($o_image['image_url'])) {
            $image_url = $_SERVER['DOCUMENT_ROOT'] . $o_image['image_url'];
            $image_info = getimagesize($image_url);
            $image_height = $image_info[1];
            $image_width = $image_info[0];
            $h_koef = $sizeh / $image_height;
            $w_koef = $sizew / $image_width;

            if ($image_height > 600 ||
                $image_width > 800
            ) {
                if ($h_koef > $w_koef) {
                    $sizew_new = $image_width * $h_koef;
                    $sizeh_new = $sizeh;
                } else {
                    $sizeh_new = $image_height * $w_koef;
                    $sizew_new = $sizew;
                }

                if ($image_info[2] == IMAGETYPE_JPEG) {
                    $src = imagecreatefromjpeg($image_url);
                } elseif ($image_info[2] == IMAGETYPE_GIF) {
                    $src = imagecreatefromgif($image_url);
                } elseif ($image_info[2] == IMAGETYPE_PNG) {
                    $src = imagecreatefrompng($image_url);
                }

                $im = imagecreatetruecolor($sizew, $sizeh);
                $back = imagecolorallocate($im, 255, 255, 255);
                imagefill($im, 0, 0, $back);


                $offset_x = ($sizew_new - $sizew) / $h_koef / 2;

                if (0 > $offset_x) {
                    $offset_x = -$offset_x;
                }

                $offset_y = ($sizeh_new - $sizeh) / $w_koef / 2;

                if (0 > $offset_y) {
                    $offset_y = -$offset_y;
                }

                imagecopyresampled($im, $src, 0, 0, $offset_x, $offset_y, $sizew_new, $sizeh_new, imagesx($src), imagesy($src));

                if (imagejpeg($im, $image_url, 100)) {
                    chmod($image_url, 0777);
                }

                imagedestroy($im);
            }
        }
    }

    /**
     * @param $image_id
     * @return bool
     */
    public function watermark($image_id)
    {
        $o_image = ImageModel::model()->findByPk($image_id);
        $image_url = $_SERVER['DOCUMENT_ROOT'] . $o_image['image_url'];
        $watermark = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/assets/img/watermark.png');
        $watermark_width = imagesx($watermark);
        $watermark_height = imagesy($watermark);
        $image = imagecreatefromjpeg($image_url);

        if ($image === false) {
            return false;
        }

        $size = getimagesize($image_url);
        $dest_x = ($size[0] - $watermark_width) / 2;
        $dest_y = ($size[1] - $watermark_height) / 2;

        imagealphablending($image, true);
        imagealphablending($watermark, true);
        imagecopy($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
        imagejpeg($image, $image_url, 100);
        imagedestroy($image);
        imagedestroy($watermark);
    }

}