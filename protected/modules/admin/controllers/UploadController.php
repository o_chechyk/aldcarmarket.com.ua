<?php

//require 'vendor/autoload.php';

//use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class UploadController extends AController
{
    public function actionAuto_header()
    {
        $this->render( 'auto_header' );
    }


    public function actionAuto()
    {
        $user = UserModel::model()->findByPk( Yii::app()->user->id );
        if(($user['user_role'] & 2) == 0) {
            echo "Недостатній рівень доступу! Лише Super User може завантажувати вміст.";
            exit;
        }

	if( isset($_FILES['autoCSV']) ) {
	    $path   = array();
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);

	    $upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . implode('/', $path);
	    if (!is_dir($upload_dir))
	    {
		mkdir($upload_dir, 0777, 1);
	    }

	    $filename = $upload_dir . '/' . $_FILES['autoCSV']['name'];
	} else {
	    echo "Ошибка: не могу загрузить файл";
	    return;
	}

	if ( ! move_uploaded_file($_FILES["autoCSV"]["tmp_name"], $filename) ) {
	    echo "Не могу сохранить ".$filename;
	    return;
	}

	// Check uploaded file format
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        finfo_close( $finfo );

	// Load Excel library
        require __DIR__.'/../vendor/autoload.php';

	// Set custom error handler
	$old_eh = null;
	$old_eh = set_error_handler( "my_err" );

        // Open a newly uploaded file
	try {

	    if($type == "text/plain") {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
	    } else if(strpos($type,"spreadsheetml") !== false) {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	    } else if(strpos($type,"ms-excel") !== false) {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
	    } else {
		echo "Неверный формат файла (должен быть .csv, .xlsx или .xls)";
		return;
	    }

	    $spreadsheet = $reader->load( $filename );
	    $sheet = $spreadsheet->getActiveSheet();
	    $rows = $sheet->getHighestRow();
	} catch(\PhpOffice\PhpSpreadsheet\Exception $e) {
	    echo $e->getMessage();
	    return;
        } catch (Error $e) {
	    echo "Here";
	    return;
	}

	// Return error handler to its rightful place
	if( $old_eh ) {
	    set_error_handler( $old_eh );
	}

	// File was opened successfully. Load cars info from DB to perform some checks.
	// Also load dictionaries for another checks;

	$known_cars = CarModel::model()->findAll();
	$producers = ProducerModel::model()->findAll();
	$models = ModelModel::model()->findAll();
	$colors = ColorModel::model()->findAll();
	$engines = EngineModel::model()->findAll();
	$transmissions = TransmissionModel::model()->findAll();
	$bodies = BodyModel::model()->findAll();
	$characteristics = CharacteristicModel::model()->findAll();
	$classes = SmartModel::model()->findAll();
	$gears = GearModel::model()->findAll();
/*
	$columns = array('A' => array('car_sku',0),
			'B' => array('car_number',0),
			'C' => array('raw_name',0),
			'D' => array('car_mileage',0),
			'E' => array('car_color_code',0),
			'F' => array('car_engine_code',0),
			'G' => array('car_price',0),
			'H' => array('car_year',0),
			'I' => array('car_engine_capacity',0),
			'J' => array('car_power',0),
			'K' => array('car_transmission_code',0),
			'L' => array('car_body_code',0),
			'M' => array('car_description',0),
			'N' => array('car_smart_code',0),
			'O' => array('raw_options',0));
*/
	echo "Оброблені дані з файлу '<strong>".$_FILES['autoCSV']['name']."</strong>': <br><br>".
	    "<form id=\"formUploadData\" method=\"POST\">".
	    "<table class=\"table table-bordered\">".
	    "<tr>".
		"<td><input type=\"checkbox\" id=\"set_all_upload\" onchange=\"formUploadCheckAll('upload')\"></td>".
		"<td>Назва</td>".
		"<td>Код авто</td>".
		"<td>Марка</td>".
		"<td>Модель</td>".
		"<td>Номер кузова</td>".
		"<td><input type=\"checkbox\" id=\"set_all_mileage\" onchange=\"formUploadCheckAll('mileage')\"> Пробіг, тис.км.</td>".
		"<td><input type=\"checkbox\" id=\"set_all_color\" onchange=\"formUploadCheckAll('color')\"> Колір</td>".
		"<td><input type=\"checkbox\" id=\"set_all_engine\" onchange=\"formUploadCheckAll('engine')\"> Тип двигуна</td>".
		"<td><input type=\"checkbox\" id=\"set_all_price\" onchange=\"formUploadCheckAll('price')\"> Ціна</td>".
		"<td><input type=\"checkbox\" id=\"set_all_year\" onchange=\"formUploadCheckAll('year')\"> Рік випуску</td>".
		"<td><input type=\"checkbox\" id=\"set_all_engine_capacity\" onchange=\"formUploadCheckAll('engine_capacity')\"> Об'єм двигуна</td>".
		"<td><input type=\"checkbox\" id=\"set_all_power\" onchange=\"formUploadCheckAll('power')\"> Потужність</td>".
		"<td><input type=\"checkbox\" id=\"set_all_transmission\" onchange=\"formUploadCheckAll('transmission')\"> Трансмісія</td>".
		"<td><input type=\"checkbox\" id=\"set_all_gear\" onchange=\"formUploadCheckAll('gear')\"> Привід</td>".
		"<td><input type=\"checkbox\" id=\"set_all_body\" onchange=\"formUploadCheckAll('body')\"> Кузов</td>".
		"<td>Клас</td>".
		"<td>Додатково</td>".
		"<td>Зовнішній стан</td>".
	    "</tr>";

	$count = 0;
	for($i=0; $i<$rows; $i++) {

	    $err = "";
	    $warn = "";
	    // Checking CAR_SKU
	    $car_sku = trim($sheet->getCell( 'B'.($i+1) )->getValue());
	    if( preg_match("/^\d+$/", $car_sku) ) {
		$car_sku = (int)$car_sku;
		foreach($known_cars as $car) {
		    if((int)$car->car_sku == $car_sku) {
			$err = "Авто з кодом '".$car_sku."' вже є в базі\n";
			break;
		    }
		}
	    } else { // if(preg_match("/^\d+$/", $car_sku) === 0)
		if($i == 0) { // The very first row, just skip the header
		    continue;
		} else {
		    $err .= "Невірний код авто: '".htmlspecialchars($car_sku)."'\n";
		}
	    } // if(preg_match("/^\d+$/", $car_sku) === 0) ... else

	    // Check CAR_NUMBER
	    $car_number = trim($sheet->getCell( 'C'.($i+1) )->getValue());

	    if(strlen($car_number) == 17 and ctype_alnum($car_number)) {
		foreach($known_cars as $car) {
		    if($car['car_number'] == $car_number) {
			$err = "Авто з номером кузова '".$car_number."' вже є в базі\n";
			break;
		    }
		}
	    } else {
		$err .= "Невірний номер кузова: '".htmlspecialchars($car_number)."'\n";
	    }

	    // Check producer & model
	    $car_raw_name = trim($sheet->getCell( 'D'.($i+1) )->getValue());
	    $buf = preg_split("/\s+/", $car_raw_name);

	    if(is_array($buf) and count($buf) >= 2) {
		$found = false;
		$car_producer_name = $buf[0];
		foreach($producers as $producer) {
		    if(strtolower($producer['producer_name']) == strtolower($buf[0])) {
			$found = true;
			$car_producer_id = $producer['producer_id'];
			$car_producer_name = $producer['producer_name'];
			break;
		    }
		}
		if(! $found) {
		    $err .= "Марку '".htmlspecialchars($buf[0])."' не знайдено в довіднику\n";
		    $car_producer_id = 'null';
		}

		$found = false;
		$car_model_name = $buf[1];
		foreach($models as $model) {
		    if(mb_strtolower($model['model_name']) == mb_strtolower($buf[1])) {
			$found = true;
			$car_model_id = $model['model_id'];
			$car_model_name = $model['model_name'];
			break;
		    }
		}
		if(! $found) {
		    $err .= "Модель '".htmlspecialchars($buf[1])."' не знайдено в довіднику\n";
		    $car_model_id = 'null';
		}
	    } else {
		$err .= "Не можу виявити марку та модель в назві '".htmlspecialchars($car_raw_name)."'\n";
		$car_producer_name = $car_model_name = $car_raw_name;
		$car_producer_id = $car_model_id = 'null';
	    }

	    // Checking CAR_MILEAGE
	    $car_mileage = trim($sheet->getCell( 'E'.($i+1) )->getValue());
	    $car_mileage = str_replace(" ", "", $car_mileage);

	    if(preg_match("/^\d+$/", $car_mileage) and $car_mileage > 0) {
		$car_mileage = round($car_mileage/1000);
		$mileage_class = "";
	    } else {
		$warn .= "Невірний пробіг: '".$car_mileage."'\n";
		$mileage_class = "uploadErr";
	    }

	    // Checking CAR_COLOR
	    $car_color = trim($sheet->getCell( 'I'.($i+1) )->getValue());
	    $found = false;
	    foreach($colors as $color) {
		if(mb_strtolower($color['color_import_key']) == mb_strtolower($car_color)) {
		    $found = true;
		    $car_color_id = $color['color_id'];
		    $car_color_name = $color['color_name'];
		    $color_class = "";
		    break;
		}
	    }
	    if(! $found) {
		$warn .= "Не знайдено ключ імпорту для: '".htmlspecialchars($car_color)."'\n";
		$car_color_id = 'null';
		$color_class = "uploadErr";
		$car_color_name = $car_color;
	    }

	    // Checking CAR_ENGINE
	    $car_engine = trim($sheet->getCell( 'K'.($i+1) )->getValue());
	    $found = false;
	    foreach($engines as $engine) {
		if(mb_strtolower($engine['engine_import_key']) == mb_strtolower($car_engine)) {
		    $found = true;
		    $car_engine_id = $engine['engine_id'];
		    $car_engine_name = $engine['engine_name'];
		    $engine_class = "";
		    break;
		}
	    }
	    if(! $found) {
		$warn .= "Не знайдено ключ імпорту для двигуна: '".htmlspecialchars($car_engine)."'\n";
		$car_engine_id = 'null';
		$engine_class = "uploadErr";
		$car_engine_name = $car_engine;
	    }

	    // Checking CAR_PRICE
	    $car_price = trim($sheet->getCell( 'T'.($i+1) )->getValue());
	    $car_price = str_replace(" ", "", $car_price);

	    if( preg_match("/^\d+$/", $car_price) ) {
		$price_class = "";
	    } else {
		$warn .= "Невірно задано ціну: '".$car_price."'\n";
		$price_class = "uploadErr";
	    }

	    // Checking CAR_YEAR
	    $car_year = trim($sheet->getCell( 'AS'.($i+1) )->getValue());
	    if( preg_match("/^\d{4}$/", $car_year) ) {
		$year_class = "";
	    } else {
		$warn .= "Невірно задано рік випуску: '".$car_year."'\n";
		$year_class = "uploadErr";
	    }

	    // Checking CAR_ENGINE_CAPACITY
//	    $car_engine_capacity = trim($sheet->getCell( 'I'.($i+1) )->getValue());
	    $car_engine_capacity = "";
	    if( preg_match("/^\d+$/", $car_engine_capacity) ) {
		$engine_capacity_class = "";
	    } else {
		$warn .= "Невірно задано об'єм двигуна: '".$car_engine_capacity."'\n";
		$engine_capacity_class = "uploadErr";
	    }

	    // Checking CAR_power
	    $car_power = trim($sheet->getCell( 'AJ'.($i+1) )->getValue());
	    if(preg_match("/^\d+$/", $car_power) and $car_power > 0) {
		$power_class = "";
	    } else {
		$warn .= "Невірно задано потужність: '".$car_power."'\n";
		$power_class = "uploadErr";
	    }

	    // Checking CAR_TRANSMISSION
	    $car_transmission = trim($sheet->getCell( 'J'.($i+1) )->getValue());
		$found = false;
		foreach($transmissions as $transmission) {
		    if(mb_strtolower($transmission['transmission_import_key']) == mb_strtolower($car_transmission)) {
			$found = true;
			$car_transmission_id = $transmission['transmission_id'];
			$car_transmission_name = $transmission['transmission_name'];
			$transmission_class = "";
			break;
		    }
		}
		if(! $found) {
		    $warn .= "Не знайдено ключ імпорту для КПП: '".htmlspecialchars($car_transmission)."'\n";
		    $car_transmission_id = 2;
		    $transmission_class = "uploadErr";
		    $car_transmission_name = $car_transmission;
		}

	    // Checking CAR_BODY
	    $car_body = trim($sheet->getCell( 'G'.($i+1) )->getValue());
	    $found = false;
	    foreach($bodies as $body) {
		if(mb_strtolower($body['body_import_key']) == mb_strtolower($car_body)) {
		    $found = true;
		    $car_body_id = $body['body_id'];
		    $car_body_name = $body['body_name'];
		    $body_class = "";
		    break;
		}
	    }
	    if(! $found) {
		$warn .= "Не знайдено ключ імпорту для кузова: '".htmlspecialchars($car_body)."'\n";
		$car_body_id = 'null';
		$body_class = "uploadErr";
		$car_body_name = $car_body;
	    }

	    // Loading CAR_CLASS
	    $car_class = trim($sheet->getCell( 'BH'.($i+1) )->getValue());
	    $car_classes = array();
	    $label = array();
	    $class_class = "";
	    if($car_class != "") {
		$buf = preg_split("/[\s,]+/", $car_class);
		foreach($buf as $car_class) {
		    $found = false;
		    foreach($classes as $class) {
			if($class['smart_id'] == $car_class) {
			    $found = true;
			    $label[] = str_replace(" ","&nbsp;",$class['smart_name']);
			    break;
			}
		    }
		    if( $found ) {
			$car_classes[] = $car_class;
		    } else {
			$warn .= "Не знайдено ID класу авто: '".$car_class."'\n";
			$class_class = "uploadErr";
		    }
		} // foreach($car_classes as $car_class)
	    } // if($car_class != "")
	    if( count($car_classes) ) { // if($car_class != "")
		$car_class_name = implode(", ", $label);
	    } else if($class_class != "") {
		$car_class_name = "-не&nbsp;розпізнано-";
	    } else {
		$car_class_name = "-не&nbsp;вказано-";
	    }

	    // Loading CAR_CHARACTERISTIC
	    $columns = array("BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR",
			     "BS","BT","BU","BV","BW","BX","BY","BZ","CA","CB",
			     "CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL");
	    $buf = array();
	    foreach($columns as $col) { // Load all features into buffer
		$tmp = trim($sheet->getCell( $col.($i+1) )->getValue());
		if( $tmp ) {
		    $buf[] = $tmp;
		} else {
		    break;
		}
	    } // foreach($columns as $col)

//	    $car_characteristic = trim($sheet->getCell( 'BI'.($i+1) )->getValue());
	    $car_characteristic_id = array();
	    $label = array();
	    $characteristic_class = "";

	    if( count($buf) ) {
		foreach($buf as $car_characteristic) {
		    $found = false;
		    foreach($characteristics as $characteristic) {
			if(mb_strtolower($characteristic['characteristic_import_key']) == mb_strtolower($car_characteristic)) {
			    $found = true;
			    $car_characteristic_id[] = $characteristic['characteristic_id'];
			    $label[] = str_replace(" ","&nbsp;",$characteristic['characteristic_name']);
			    break;
			}
		    }
		    if(! $found) {
			$warn .= "Не знайдено ключ імпорту для характеристики: '".htmlspecialchars($car_characteristic)."'\n";
			$car_characteristic_name = $car_characteristic;
			$characteristic_class = "uploadErr";
		    }
		} // foreach($buf as $car_characteristic)
	    } else { // if( count($buf) )
		$car_characteristic_name = "-не&nbsp;вказано-";
	    }
	    if( count($car_characteristic_id) ) { // if($car_class != "")
		$car_characteristic_name = implode(", ", $label);
	    } else if($characteristic_class != "") {
		$car_characteristic_name = "-не&nbsp;розпізнано-";
	    } else {
		$car_characteristic_name = "-не&nbsp;вказано-";
	    }

	    // Link to car condition report
	    $tmp = trim($sheet->getCell( 'AV'.($i+1) )->getValue());
	    $link = array();
	    preg_match("/(http.*)$/", $tmp, $link);
	    if( count($link) ) {
		$car_condition = $link[0];
	    } else {
		$car_condition = "";
	    }

	    if($err == "") {
		if($warn == "") {
		    $tr_head = "class=\"uploadOk\"";
		    $checked = " checked";
		} else {
		    $tr_head = "class=\"uploadWarn\" title=\"".$warn."\"";
		    $checked = "";
		}
		echo "<tr ".$tr_head."><td id=\"control".$count."\">".
		    "<input type=\"checkbox\" class=\"all-upload\" name=\"form[".$count."][check]\"$checked>".
		    "</td>";
	    } else {
		echo "<tr class=\"uploadErr\" title=\"".$err."\"><td><strong>Помилка</strong></td>";
	    }
	    echo "<td>".$car_raw_name."<input type=\"hidden\" name=\"form[".$count."][car_name]\" value=\"".htmlspecialchars($car_raw_name,ENT_QUOTES)."\"></td>".
		    "<td>".$car_sku."<input type=\"hidden\" name=\"form[".$count."][car_sku]\" value=\"".$car_sku."\"></td>".
		    "<td>".$car_producer_name."<input type=\"hidden\" name=\"form[".$count."][car_producer_id]\" value=\"".$car_producer_id."\"></td>".
		    "<td>".$car_model_name."<input type=\"hidden\" name=\"form[".$count."][car_model_id]\" value=\"".$car_model_id."\"></td>".
		    "<td>".$car_number."<input type=\"hidden\" name=\"form[".$count."][car_number]\" value=\"".$car_number."\"></td>";
	    if( $mileage_class ) {
		echo "<td class=\"".$mileage_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-mileage\" title=\"Перезаписати\"> ".
		    "<input type=\"text\" name=\"form[".$count."][car_mileage]\" value=\"".$car_mileage."\" size=\"5\" ".
		    "onkeyup=\"changeAll(this,'mileage')\" class=\"edit-mileage\"></td>";
	    } else {
		echo "<td>".$car_mileage."<input type=\"hidden\" name=\"form[".$count."][car_mileage]\" value=\"".$car_mileage."\"></td>";
	    }
	    if( $color_class ) {
		echo "<td class=\"".$color_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-color\" title=\"Перезаписати\"> ".
		    "<select name=\"form[".$count."][car_color_id]\" value=\"".$car_color_id."\" ".
		    "onchange=\"changeAll(this,'color')\" class=\"edit-color\">".
			"<option value=\"null\">-не&nbsp;вибрано-</option>";
		foreach($colors as $color) {
		    if($car_color_id == $color['color_id']) {
			$str = " selected";
		    } else {
			$str = "";
		    }
		    echo "<option value=\"".$color['color_id']."\"$str>".$color['color_name']."</option>";
		}
		echo "</select></td>";
	    } else {
		echo "<td>".$car_color_name."<input type=\"hidden\" name=\"form[".$count."][car_color_id]\" value=\"".$car_color_id."\"></td>";
	    }
	    if( $engine_class ) {
		echo "<td class=\"".$engine_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-engine\" title=\"Перезаписати\"> ".
		    "<select name=\"form[".$count."][car_engine_id]\" value=\"".$car_engine_id."\" ".
		    "onchange=\"changeAll(this,'engine')\" class=\"edit-engine\">".
			"<option value=\"null\">-не&nbsp;вибрано-</option>";
		foreach($engines as $engine) {
		    if($car_engine_id == $engine['engine_id']) {
			$str = " selected";
		    } else {
			$str = "";
		    }
		    echo "<option value=\"".$engine['engine_id']."\"$str>".$engine['engine_name']."</option>";
		}
		echo "</select></td>";
	    } else {
		echo "<td>".$car_engine_name."<input type=\"hidden\" name=\"form[".$count."][car_engine_id]\" value=\"".$car_engine_id."\"></td>";
	    }
	    if( $price_class ) {
		echo "<td class=\"".$price_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-price\" title=\"Перезаписати\"> ".
		    "<input type=\"text\" name=\"form[".$count."][car_price]\" value=\"".$car_price."\" ".
		    "size=\"8\" onkeyup=\"changeAll(this,'price')\" class=\"edit-price\"></td>";
	    } else {
		echo "<td>".$car_price."<input type=\"hidden\" name=\"form[".$count."][car_price]\" value=\"".$car_price."\"></td>";
	    }
	    if( $year_class ) {
		echo "<td class=\"".$year_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-year\" title=\"Перезаписати\"> ".
		    "<input type=\"text\" name=\"form[".$count."][car_year]\" value=\"".$car_year."\" ".
		    "size=\"5\" onkeyup=\"changeAll(this,'year')\" class=\"edit-year\"></td>";
	    } else {
		echo "<td>".$car_year."<input type=\"hidden\" name=\"form[".$count."][car_year]\" value=\"".$car_year."\"></td>";
	    }
	    if( $engine_capacity_class ) {
		echo "<td class=\"".$engine_capacity_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-engine_capacity\" title=\"Перезаписати\"> ".
		    "<input type=\"text\" name=\"form[".$count."][car_engine_capacity]\" value=\"".$car_engine_capacity."\" ".
		    "size=\"5\" onkeyup=\"changeAll(this,'engine_capacity')\" class=\"edit-engine_capacity\"></td>";
	    } else {
		echo "<td>".$car_engine_capacity."<input type=\"hidden\" name=\"form[".$count."][car_engine_capacity]\" value=\"".$car_engine_capacity."\"></td>";
	    }
	    if( $power_class ) {
		echo "<td class=\"".$power_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-power\" title=\"Перезаписати\"> ".
		    "<input type=\"text\" name=\"form[".$count."][car_power]\" value=\"".$car_power."\" ".
		    "size=\"4\" onkeyup=\"changeAll(this,'power')\" class=\"edit-power\"></td>";
	    } else {
		echo "<td>".$car_power."<input type=\"hidden\" name=\"form[".$count."][car_power]\" value=\"".$car_power."\"></td>";
	    }
	    if( $transmission_class ) {
		echo "<td class=\"".$transmission_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-transmission\" title=\"Перезаписати\"> ".
		    "<select name=\"form[".$count."][car_transmission_id]\" value=\"".$car_transmission_id."\" ".
		    "onchange=\"changeAll(this,'transmission')\" class=\"edit-transmission\">".
			"<option value=\"null\">-не&nbsp;вибрано-</option>";
		foreach($transmissions as $transmission) {
		    if($car_transmission_id == $transmission['transmission_id']) {
			$str = " selected";
		    } else {
			$str = "";
		    }
		    echo "<option value=\"".$transmission['transmission_id']."\"$str>".$transmission['transmission_name']."</option>";
		}
	    echo "</select></td>";
	    } else {
		echo "<td>".$car_transmission_name."<input type=\"hidden\" name=\"form[".$count."][car_transmission_id]\" value=\"".$car_transmission_id."\"></td>";
	    }

	    echo "<td class=\"no-wrap\">".
		"<input type=\"checkbox\" class=\"all-gear\" title=\"Перезаписати\"> ".
		"<select name=\"form[".$count."][car_gear_id]\" value=\"1\" ".
		"onchange=\"changeAll(this,'gear')\" class=\"edit-gear\">";
	    foreach($gears as $gear) {
		echo "<option value=\"".$gear['gear_id']."\">".$gear['gear_name']."</option>";
	    }
	    echo "</select></td>";

	    if( $body_class ) {
		echo"<td class=\"".$body_class." no-wrap\">".
		    "<input type=\"checkbox\" class=\"all-body\" title=\"Перезаписати\"> ".
		    "<select name=\"form[".$count."][car_body_id]\" value=\"".$car_body_id."\" ".
		    "onchange=\"changeAll(this,'body')\" class=\"edit-body\">".
			"<option value=\"null\">-не&nbsp;вибрано-</option>";
		foreach($bodies as $body) {
		    if($car_body_id == $body['body_id']) {
			$str = " selected";
		    } else if($body['body_status'] == 0) { // This type of body disabled in dictionary
			continue; // skip this option
		    } else {
			$str = "";
		    }
		    echo "<option value=\"".$body['body_id']."\"$str>".$body['body_name']."</option>";
		}
		echo "</select></td>";
	    } else {
		echo "<td>".$car_body_name."<input type=\"hidden\" name=\"form[".$count."][car_body_id]\" value=\"".$car_body_id."\"></td>";
	    }

	    echo "<td class=\"".$class_class."\">".$car_class_name."<input type=\"hidden\" name=\"form[".$count."][car_class]\" value=\"".htmlspecialchars(json_encode($car_classes),ENT_QUOTES)."\"></td>";
	    echo "<td class=\"".$characteristic_class."\">".$car_characteristic_name."<input type=\"hidden\" name=\"form[".$count."][car_characteristic]\" value=\"".htmlspecialchars(json_encode($car_characteristic_id),ENT_QUOTES)."\">".
		    "<input type=\"hidden\" name=\"form[".$count."][car_description]\" value=\"\">".
		    "</td>";
	    echo "<td>".htmlspecialchars($car_condition)."<input type=\"hidden\" name=\"form[".$count."][car_condition]\" value=\"".htmlspecialchars($car_condition,ENT_QUOTES)."\"></td>";

	    if($err == "") {
		$count++;
	    }
	    echo "</tr>";

	} // for($i=0; $i<$rows; $i++)

	echo "</table></form><br>".
	    "<button onclick=\"uploadPost('car')\">Зберегти дані</button>";

	unlink( $filename );

    }


    public function actionPrice_header() {
	$this->render( 'price_header' );
    }


    public function actionPrice()
    {
        $user = UserModel::model()->findByPk( Yii::app()->user->id );
        if(($user['user_role'] & 2) == 0) {
            echo "Недостатній рівень доступу! Лише Super User може завантажувати ціни.";
            exit;
        }

	if( isset($_FILES['priceCSV']) ) {
	    $path   = array();
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);

	    $upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . implode('/', $path);
	    if (!is_dir($upload_dir))
	    {
		mkdir($upload_dir, 0777, 1);
	    }

	    $filename = $upload_dir . '/' . $_FILES['priceCSV']['name'];
	} else {
	    echo "Ошибка: не могу загрузить файл";
	    return;
	}

	if ( ! move_uploaded_file($_FILES["priceCSV"]["tmp_name"], $filename) ) {
	    echo "Не могу сохранить ".$filename;
	    return;
	}

	// Check uploaded file format
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        finfo_close( $finfo );

	// Load Excel library
        require __DIR__.'/../vendor/autoload.php';

	// Set custom error handler
	$old_eh = null;
	$old_eh = set_error_handler( "my_err" );

        // Open a newly uploaded file
	try {

	    if($type == "text/plain") {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
	    } else if(strpos($type,"spreadsheetml") !== false) {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	    } else {
		echo "Неверный формат файла (должен быть .csv или .xlsx)";
		return;
	    }

	    $spreadsheet = $reader->load( $filename );
	    $sheet = $spreadsheet->getActiveSheet();
	    $rows = $sheet->getHighestRow();
	} catch(\PhpOffice\PhpSpreadsheet\Exception $e) {
	    echo $e->getMessage();
	    return;
        } catch (Error $e) {
	    echo $e->getMessage();
	    return;
	}

	// Return error handler to its rightful place
	if( $old_eh ) {
	    set_error_handler( $old_eh );
	}

	// File was opened successfully. Load cars info from DB to perform some checks.
	// Also load dictionaries for another checks;

	$known_cars = CarModel::model()->findAll();

	echo "Оброблені дані з файлу '<strong>".$_FILES['priceCSV']['name']."</strong>': <br><br>".
	    "<form id=\"formUploadData\" method=\"POST\">".
	    "<table class=\"table table-bordered\">".
	    "<tr>".
		"<td><input type=\"checkbox\" id=\"formUploadChecker\" onchange=\"formUploadCheckAll()\"></td>".
		"<td>Назва</td>".
		"<td>Код авто</td>".
		"<td>Номер кузова</td>".
		"<td>Стара ціна</td>".
		"<td>Нова ціна</td>".
	    "</tr>";

	$count = 0;
	for($i=0; $i<$rows; $i++) {

	    $err = "";
	    $car_name = "-не&nbsp;визначено-";
	    $car_id = 'null';
	    // Checking CAR_SKU
	    $car_sku = trim($sheet->getCell( 'A'.($i+1) )->getValue());
	    if( preg_match("/^\d+$/", $car_sku) ) {
		$car_sku = (int)$car_sku;
		$found = false;
		foreach($known_cars as $car) {
		    if((int)$car->car_sku == $car_sku) {
			$found = true;
			$tmp_car_number = $car->car_number;
			$car_name = $car->car_name;
			$car_id = $car->car_id;
			break;
		    }
		}
		if(! $found) {
		    $err .= "Авто з кодом '".$car_sku."' не знайдено в базі\n";
		}
	    } else { // if(preg_match("/^\d+$/", $car_sku) === 0)
		if($i == 0) { // The very first row, just skip the header
		    continue;
		} else {
		    $err .= "Невірний код авто: '".htmlspecialchars($car_sku)."'\n";
		}
	    } // if(preg_match("/^\d+$/", $car_sku) === 0) ... else

	    // Check CAR_NUMBER
	    $car_number = trim($sheet->getCell( 'B'.($i+1) )->getValue());

	    if(strlen($car_number) == 17 and ctype_alnum($car_number)) {
		if( isset($tmp_car_number) ) { // previous check was successful
		    if($car_number != $tmp_car_number) {
			$err .= "Не співпадає номер кузова! ".
				"В базі для цього авто вказано номер '".$tmp_car_number."',".
				" а в файлі '".$car_number."'\n";
		    }
		}
	    } else {
		$err .= "Невірний номер кузова: '".htmlspecialchars($car_number)."'\n";
	    }

	    // Checking CAR_PRICE
	    $car_price = trim($sheet->getCell( 'C'.($i+1) )->getValue());
	    $car_price = str_replace(" ", "", $car_price);

	    if(! preg_match("/^\d+$/", $car_price) ) {
		$err .= "Невірно задано ціну: '".$car_price."'\n";
	    }

	    // Checking CAR_OLD_PRICE
	    $car_price_old = trim($sheet->getCell( 'D'.($i+1) )->getValue());
	    $car_price_old = str_replace(" ", "", $car_price_old);

	    if(! preg_match("/^\d+$/", $car_price_old) ) {
		$err .= "Невірно задано стару ціну: '".$car_price_old."'\n";
	    }

	    if($err == "") {
		echo "<tr class=\"uploadOk\"><td id=\"control".$count."\">".
		    "<input type=\"checkbox\" class=\"formUploadCheck\" name=\"form[".$count."][check]\">".
		    "</td>";
	    } else {
		echo "<tr class=\"uploadErr\" title=\"".$err."\"><td>Помилка</td>";
	    }
	    echo "<td>".$car_name."<input type=\"hidden\" name=\"form[".$count."][car_id]\" value=\"".$car_id."\"></td>".
		    "<td>".$car_sku."<input type=\"hidden\" name=\"form[".$count."][car_sku]\" value=\"".$car_sku."\"></td>".
		    "<td>".$car_number."<input type=\"hidden\" name=\"form[".$count."][car_number]\" value=\"".$car_number."\"></td>".
		    "<td>".$car_price_old."<input type=\"hidden\" name=\"form[".$count."][car_price_old]\" value=\"".$car_price_old."\"></td>".
		    "<td>".$car_price."<input type=\"hidden\" name=\"form[".$count."][car_price]\" value=\"".$car_price."\"></td>";

	    if($err == "") {
		$count++;
	    }
	    echo "</tr>";

	} // for($i=0; $i<$rows; $i++)

	echo "</table></form><br>".
	    "<button onclick=\"uploadPost('price')\">Зберегти дані</button>";

	unlink( $filename );

    }

    public function actionSavecar() {

	$ret = array();
	foreach($_POST['form'] as $count => $row) {
	    if( isset($row['check']) ) {
		try {
		    $car = new CarModel();

		    foreach($row as $key => $value) {
			if($key == "check" or $key == "car_class" or $key == "car_characteristic") { // skip these
			    continue;
			}
			$car->$key = $value;
		    }

		    $car->car_comfort = '';
		    $car->car_date = time();
		    $car->car_phase_id = 3;
		    $car->car_safety = '';
		    $car->car_slide = 0;
		    $car->car_status = 1;
		    $car->car_url = '';
		    $car->seo_description = '';
		    $car->seo_h1 = '';
		    $car->seo_keywords = '';
		    $car->seo_text = '';
		    $car->seo_title = '';
		    $car->save();
		    $new_id = $car->car_id;

		    $characteristics = json_decode($row['car_characteristic'], true);
		    if( is_array($characteristics) ) {
			foreach($characteristics as $characteristic) {
			    $o_carcharacteristic = new CarCharacteristicModel();
			    $o_carcharacteristic->carcharacteristic_characteristic_id = $characteristic;
			    $o_carcharacteristic->carcharacteristic_car_id = $new_id;
			    $o_carcharacteristic->save();
			}
		    }

		    $classes = json_decode($row['car_class'], true);
		    if( is_array($classes) ) {
			foreach($classes as $class) {
			    $o_carsmart = new CarSmartModel();
			    $o_carsmart->carsmart_smart_id = $class;
			    $o_carsmart->carsmart_car_id = $new_id;
			    $o_carsmart->save();
			}
		    }

		    $o_car  = CarModel::model()->findByPk($new_id);
		    $o_car->car_url = $new_id . '_' . str_replace($this->rus, $this->lat, $o_car->car_name);
		    $o_car->save();

		    $ret[$count] = $new_id;
		} catch (Exception $e) {
		    $ret[$count] = "Помилка запису в базу\n".$e->getMessage();
		}
	    } // if( isset($row['check']) )
	} // foreach($_POST as $row)

	echo json_encode( array('data' => $ret) );
    }


    public function actionSaveprice() {

	$ret = array();
	foreach($_POST['form'] as $count => $row) {
	    if( isset($row['check']) ) {
		try {
		    if(isset($row['car_id']) and is_numeric($row['car_id'])) {
			$car_id = $row['car_id'];
		    } else {
			$ret[$count] = "Невірний або порожній ID авто\n";
			continue;
		    }

		    $car = CarModel::model()->findByPk($car_id);

		    $car->car_price = $row['car_price'];
		    $car->car_price_old = $row['car_price_old'];
		    $car->save();

		    $ret[$count] = $car_id;
		} catch (Exception $e) {
		    $ret[$count] = "Помилка запису в базу\n".$e->getMessage();
		}
	    } // if( isset($row['check']) )
	} // foreach($_POST as $row)

	echo json_encode( array('data' => $ret) );
    }



    public function actionComment()
    {
        $user = UserModel::model()->findByPk( Yii::app()->user->id );
        if(($user['user_role'] & 2) == 0) {
            echo "Недостатній рівень доступу! Лише Super User може завантажувати вміст.";
            exit;
        }

	if( isset($_FILES['commentCSV']) ) {
	    $path   = array();
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);
	    $path[] = substr(md5(uniqid(rand(), 1)), -3);

	    $upload_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . implode('/', $path);
	    if (!is_dir($upload_dir))
	    {
		mkdir($upload_dir, 0777, 1);
	    }

	    $filename = $upload_dir . '/' . $_FILES['commentCSV']['name'];
	} else {
	    echo "Ошибка: не могу загрузить файл";
	    return;
	}

	if ( ! move_uploaded_file($_FILES["commentCSV"]["tmp_name"], $filename) ) {
	    echo "Не могу сохранить ".$filename;
	    return;
	}

	// Check uploaded file format
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $filename);
        finfo_close( $finfo );

	// Load Excel library
        require __DIR__.'/../vendor/autoload.php';

	// Set custom error handler
	$old_eh = null;
	$old_eh = set_error_handler( "my_err" );

        // Open a newly uploaded file
	try {

	    if($type == "text/plain") {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
	    } else if(strpos($type,"spreadsheetml") !== false) {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
	    } else if(strpos($type,"ms-excel") !== false) {
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
	    } else {
		echo "Неверный формат файла (должен быть .csv, .xlsx или .xls)";
		return;
	    }

	    $spreadsheet = $reader->load( $filename );
	    $sheet = $spreadsheet->getActiveSheet();
	    $rows = $sheet->getHighestRow();
	} catch(\PhpOffice\PhpSpreadsheet\Exception $e) {
	    echo $e->getMessage();
	    return;
        } catch (Error $e) {
	    echo "Here";
	    return;
	}

	// Return error handler to its rightful place
	if( $old_eh ) {
	    set_error_handler( $old_eh );
	}

	$known_cars = CarModel::model()->findAll();

	echo "Оброблені дані з файлу '<strong>".$_FILES['commentCSV']['name']."</strong>': <br><br>".
	    "<form id=\"formUploadData\" method=\"POST\">".
	    "<table class=\"table table-bordered\">".
	    "<tr>".
		"<td><input type=\"checkbox\" id=\"set_all_upload\" onchange=\"formUploadCheckAll('upload')\"></td>".
		"<td>Назва</td>".
		"<td>Код авто</td>".
		"<td>Номер кузова</td>".
		"<td>Технічний стан</td>".
	    "</tr>";


	$count = 0;
	for($i=0; $i<$rows; $i++) {

	    $err = "";
	    $warn = "";

	    // Checking CAR_SKU
	    $car_sku = trim($sheet->getCell( 'B'.($i+1) )->getValue());
	    if( preg_match("/^\d+$/", $car_sku) ) {
		$car_sku = (int)$car_sku;
		$found = false;
		foreach($known_cars as $car) {
		    if((int)$car->car_sku == $car_sku) {
			$found = true;
			$tmp_car_number = $car->car_number;
			$car_name = $car->car_name;
			$car_id = $car->car_id;
			break;
		    }
		}
		if(! $found) {
		    $err .= "Авто з кодом '".$car_sku."' не знайдено в базі\n";
		}
	    } else { // if(preg_match("/^\d+$/", $car_sku) === 0)
		if($i == 0) { // The very first row, just skip the header
		    continue;
		} else {
		    $err .= "Невірний код авто: '".htmlspecialchars($car_sku)."'\n";
		}
	    } // if(preg_match("/^\d+$/", $car_sku) === 0) ... else

	    // Check CAR_NUMBER
	    $car_number = trim($sheet->getCell( 'C'.($i+1) )->getValue());

	    if(strlen($car_number) == 17 and ctype_alnum($car_number)) {
		if( isset($tmp_car_number) ) { // previous check was successful
		    if($car_number != $tmp_car_number) {
			$err .= "Не співпадає номер кузова! ".
				"В базі для цього авто вказано номер '".$tmp_car_number."',".
				" а в файлі '".$car_number."'\n";
		    }
		}
	    } else {
		$err .= "Невірний номер кузова: '".htmlspecialchars($car_number)."'\n";
	    }

	    $car_raw_name = trim($sheet->getCell( 'D'.($i+1) )->getValue());
	    $buf = preg_split("/\s+/", $car_raw_name);


	    if($err == "") {
		if($warn == "") {
		    $tr_head = "class=\"uploadOk\"";
		    $checked = " checked";
		} else {
		    $tr_head = "class=\"uploadWarn\" title=\"".$warn."\"";
		    $checked = "";
		}
		echo "<tr ".$tr_head."><td id=\"control".$count."\">".
		    "<input type=\"checkbox\" class=\"all-upload\" name=\"form[".$count."][check]\"$checked>".
		    "</td>";
	    } else {
		echo "<tr class=\"uploadErr\" title=\"".$err."\"><td><strong>Помилка</strong></td>";
	    }

	    $car_comment = trim($sheet->getCell( 'AO'.($i+1) )->getValue());

	    $link = array();
	    preg_match("/(http.*)$/", $car_comment, $link);

	    echo "<td>".$car_raw_name."<input type=\"hidden\" name=\"form[".$count."][car_id]\" value=\"".$car_id."\"></td>".
		    "<td>".$car_sku."<input type=\"hidden\" name=\"form[".$count."][car_sku]\" value=\"".$car_sku."\"></td>".
		    "<td>".$car_number."<input type=\"hidden\" name=\"form[".$count."][car_number]\" value=\"".$car_number."\"></td>".
		    "<td>".htmlspecialchars($link[0])."<input type=\"hidden\" name=\"form[".$count."][car_comment]\" value=\"".htmlspecialchars($link[0])."\"></td>".
		    "</tr>\n";

	    if($err == "") {
		$count++;
	    }

	} // for($i=0; $i<$rows; $i++)

	if( $count ) {
	    echo "</table></form><br>".
		"<button onclick=\"uploadPost('comment')\">Зберегти дані</button>";
	}

	unlink( $filename );

    } // public function actionComment



    public function actionComment_header() {
	$this->render( 'comment_header' );
    }


    public function actionSavecomment() {

	$ret = array();
	foreach($_POST['form'] as $count => $row) {
	    if( isset($row['check']) ) {
		try {
		    if(isset($row['car_id']) and is_numeric($row['car_id'])) {
			$car_id = $row['car_id'];
		    } else {
			$ret[$count] = "Невірний або порожній ID авто\n";
			continue;
		    }

		    $car = CarModel::model()->findByPk($car_id);

		    $car->car_condition = $row['car_comment'];
		    $car->save();

		    $ret[$count] = $car_id;
		} catch (Exception $e) {
		    $ret[$count] = "Помилка запису в базу\n".$e->getMessage();
		}
	    } // if( isset($row['check']) )
	} // foreach($_POST as $row)

	echo json_encode( array('data' => $ret) );
    }

}


function my_err($errno, $errstr, $errfile, $errline) {
//    echo "Ошибка чтения файла (неверный формат?)<br>".$errstr."<br>".$errfile."<br>".$errline;
    echo "Помилка формату файлу<br>".$errstr."<br>".$errfile."<br>".$errline;
    exit;
}