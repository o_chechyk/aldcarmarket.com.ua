<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/menu/create" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="/admin/menu/order">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="data-table">
                    <thead>
                        <tr>
                            <th class="col-lg-1">Порядок</th>
                            <th>Меню</th>
                            <th>Посилання</th>
                            <th class="col-lg-1">Статус</th>
                            <th class="col-lg-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($a_menu as $item) { ?>
                            <tr>
                                <td>
                                    <span style="display:none;">
                                        <?php print $item['menu_order']; ?>
                                    </span>
                                    <input
                                        class="form-control"
                                        name="data[<?php print $item['menu_id']; ?>]"
                                        size="1"
                                        type="text"
                                        value="<?php print $item['menu_order']; ?>"
                                    />
                                </td>
                                <td><?php print $item['menu_name']; ?></td>
                                <td><?php print $item['menu_url']; ?></td>
                                <td>
                                    <a href="/admin/menu/status/<?php print $item['menu_id']; ?>">
                                        <button type="button" class="btn btn-<?php if (0 == $item['menu_status']) { ?>danger<?php } else { ?>success<?php } ?> btn-circle">
                                            <i class="fa fa-power-off"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>
                                    <a href="/admin/menu/edit/<?php print $item['menu_id']; ?>">
                                        <button type="button" class="btn btn-info">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </a>
                                    <a href="/admin/menu/delete/<?php print $item['menu_id']; ?>">
                                        <button type="button" class="btn btn-danger">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->