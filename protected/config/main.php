<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'vt',

    'preload' => array('log'),

    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.vendor.*',
        'application.modules.admin.components.*',
        'application.modules.admin.views.*',
        'ext.igosja.*',
        'ext.yiiReCaptcha.*',
        'ext.spincar.*'
    ),

    'modules' => array(
        'admin'
    ),


    'components' => array(
        'admin' => array(
            'allowAutoLogin' => true,
        ),

	'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'ext.yiiReCaptcha.ReCaptcha',
            'key' => '6LfNEHwUAAAAAI08-FTavJs1ZvsbP3v0SRii6qHG',
            'secret' => '6LfNEHwUAAAAAOtoDgFD6CsQpHD2Y-eD7tE4wjFS',
        ],

        'urlManager' => array(
            'urlFormat' => 'path',
//            'class'=>'UrlManager',
            'rules' => array(
                'news'=>'news/index',
                'contacts'=>'contacts/index',
                'admin' => 'admin/index',
                'automobile/show/<id>' => 'automobile/show',
                'automobile/<id>/<alias>' => 'automobile/index',
                'faq'=>'faq/index',
                '<id>'=>'page/view',

                'admin/<controller>' => 'admin/<controller>',
                'admin/<controller>/<action>' => 'admin/<controller>/<action>',
                'admin/<controller>/<action>/<id>' => 'admin/<controller>/<action>',

		'.git' => 'index',
                '' => 'index',

                '<controller>/<action>' => '<controller>/<action>',
                '<controller>/<action>/<id>' => '<controller>/<action>',


            ),
            'showScriptName'=>false
        ),

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=ald',
            'emulatePrepare' => true,
            //'username' => 'root',
             'username' => 'ald',
            //'password' => '',
             'password' => 'gjyRIrX7',
            'charset' => 'utf8',
        ),

        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),

	'SpinCar' => array(
	    'spincar_customer_name' => 'aldauto',
	    'spincar_customer_site' => 'aldcarmarket.com.ua',
	    'spincar_api_key' => 'qCHWmmBQNlX97PujWwz9',
	    'redis_host' => 'localhost',
	    'redis_port' => 6379,
	    'redis_auth' => 'Y308KXDC58EW0bR4wt2OvL9d1xdg0pQsaFgP9IiW+HhYvZ+iIH14WTAgY8rI7SzsDPBCjuCJu+91tRPb',
	    'redis_spincar_key' => 'spincar'
        ),


    ),

    'params' => array(),
);
