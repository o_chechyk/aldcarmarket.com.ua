<?php

class ModelController extends AController
{
    public function actionIndex()
    {
        $a_model    = ModelModel::model()->findAll();
        $h1         = 'Моделі';

        $this->render('index', array('a_model' => $a_model, 'h1' => $h1));
    }

    public function actionStatus($id)
    {
        $id         = (int) $id;
        $o_model    = ModelModel::model()->findByPk($id);
        $status     = $o_model->model_status;

        if (0 == $status)
        {
            $new_status = 1;
        }
        else
        {
            $new_status = 0;
        }

        $o_model->model_status = $new_status;
        $o_model->save();

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/model');
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_model = new ModelModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_model->$key = $value;
            }

            $c_model->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/model');
            exit;
        }

        $h1         = 'Редагування моделі';
        $a_producer = ProducerModel::model()->findAll(array('order' => 'producer_name'));

        $this->render('form', array('h1' => $h1, 'a_producer' => $a_producer));
    }

    public function actionEdit($id)
    {
        $id      = (int) $id;
        $o_model = ModelModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_model->$key = $value;
            }

            $o_model->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/model');
            exit;
        }

        $h1         = 'Редагування моделі';
        $a_producer = ProducerModel::model()->findAll(array('order' => 'producer_name'));

        $this->render('form', array('o_model' => $o_model, 'h1' => $h1, 'a_producer' => $a_producer));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        ModelModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/model');
    }
}