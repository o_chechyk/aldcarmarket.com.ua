<?php

class SmartModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'smart';
    }

    public function primaryKey()
    {
        return 'smart_id';
    }
}