<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Історія змін стану автомобілей</h1>
    </div>
</div>

<div class="logFilters">
    <div class="caption">
        Фільтри
    </div>
    <div class="body">
	<form action="/admin/log/phase" method="POST" autocomplete="off">
	    <div class="section">
		<div class="caption">
		    Дата
		</div>
		<div class="body">
		    З
		    <input name="dateFrom" id="dateFrom" value="<?=$dateFrom?>" autocomplete="off">
		    По
		    <input name="dateTo" id="dateTo" value="<?=$dateTo?>" autocomplete="off">
		</div>
	    </div>
	    <div class="section">
		<div class="caption">
		    Стан
		</div>
		<div class="body">
		    <select name="phase" id="phase" value="<?=$phase?>">
			<option value="0">Всі</option>
			<?php foreach($phases as $p) {
			    if($phase == $p['phase_id']) $selected=" selected";
			    else $selected="";
			?>
			    <option value="<?=$p['phase_id']?>"<?=$selected?>><?=$p['phase_name']?></option>

			<?php } ?>
		    </select>
		</div>
	    </div>
	    <div class="section">
		<div class="caption">
		    Vehicle ID
		</div>
		<div class="body">
		    <input name="number" id="number" value="<?=$number?>" autocomplete="off">
		</div>
	    </div>
	    <div class="footer">
		<input id="logSubmit" class="btn btn-default text-center" type="submit" value="Застосувати">
		<input type="button" class="btn btn-default text-center" onclick="clearFilter()" value="Скинути фільтр">
	    </div>
	</form>
    </div>
</div>

<div class="dataTable_wrapper">
    <table id="data-table" class="table table-striped table-bordered dataTable no-footer">
	<thead>
	    <tr>
		<th>Vehicle ID</th>
		<th>Виробник</th>
		<th>Модель</th>
		<th>Старий стан</th>
		<th>Протягом</th>
		<th>Новий стан</th>
		<th>Дата</th>
		<th>Користувач</th>
	    </tr>
	</thead>

	<?php foreach ($log as $record) { ?>

	    <tr>
		<td>
		    <?php echo "<a href=\"?number=".urlencode($record['number'])."\">".$record['number']."</a>"; ?>
		</td>
		<td><?=$record['producer']?></td>
		<td><?=$record['model']?></td>
		<td><?=$record['old_phase']?></td>
		<td><?=$stages[$record['id']]?></td>
		<td><?=$record['new_phase']?></td>
		<td><?=$record['dt']?></td>
		<td><?=$record['user']?></td>
	    </tr>

	<?php }?>

    </table>

</div>