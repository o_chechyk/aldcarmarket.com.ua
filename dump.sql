-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: ald_test
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ask`
--

DROP TABLE IF EXISTS `ask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ask` (
  `ask_id` int(11) NOT NULL AUTO_INCREMENT,
  `ask_car_id` int(11) DEFAULT '0',
  `ask_date` int(11) DEFAULT '0',
  `ask_email` varchar(50) DEFAULT NULL,
  `ask_read` int(11) DEFAULT NULL,
  `ask_text` text,
  `ask_manager` int(11) DEFAULT NULL,
  `ask_reply` text,
  PRIMARY KEY (`ask_id`),
  KEY `ask_car_id` (`ask_car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ask`
--

LOCK TABLES `ask` WRITE;
/*!40000 ALTER TABLE `ask` DISABLE KEYS */;
INSERT INTO `ask` VALUES (55,2718,1547123202,'nadiia.zelentsova@aldautomotive.com',1547123889,'My question is ??',3,'<p>У відповідь на Вашe питання щодо автомобілю SKODA Octavia A7 1.2 M/T Ambition повідомляємо:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>test</p>\r\n'),(56,2718,1547123652,'nadiia.zelentsova@aldautomotive.com',1547123855,'Так что, снимите авто с резерва?????',3,'<p>У відповідь на Вашe питання щодо автомобілю SKODA Octavia A7 1.2 M/T Ambition повідомляємо:</p>\r\n\r\n<p>test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;testtest&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test&nbsp;test</p>\r\n\r\n<p>&nbsp;</p>\r\n');
/*!40000 ALTER TABLE `ask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `body`
--

DROP TABLE IF EXISTS `body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `body` (
  `body_id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `body_name` varchar(255) DEFAULT NULL,
  `body_status` tinyint(1) DEFAULT '1',
  `body_import_key` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`body_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `body`
--

LOCK TABLES `body` WRITE;
/*!40000 ALTER TABLE `body` DISABLE KEYS */;
INSERT INTO `body` VALUES (1,'Седан',1,'Sedan'),(2,'Універсал',1,'Mini MVP'),(3,'Хетчбек',1,'Hatchback'),(4,'Купе',1,'Coupe'),(5,'Ліфтбек',1,'Liftback'),(6,'Фургон',1,'Van'),(7,'Пікап',1,'Pick-up'),(8,'Автобус',0,'Bus'),(9,'Комбі',1,'Combi'),(10,'Джип',0,'Jeep'),(11,'Лімузин',0,'Limousine'),(12,'Мінівен',1,'MiniVan'),(13,'Кросовер',1,'Crossover');
/*!40000 ALTER TABLE `body` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_body_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_color_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_comfort` text NOT NULL,
  `car_date` int(11) NOT NULL DEFAULT '0',
  `car_description` text NOT NULL,
  `car_engine_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_engine_capacity` int(11) NOT NULL DEFAULT '0',
  `car_gear_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_mileage` int(11) NOT NULL DEFAULT '0',
  `car_model_id` int(11) NOT NULL DEFAULT '0',
  `car_name` varchar(255) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `car_phase_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_power` int(11) NOT NULL DEFAULT '0',
  `car_price` int(11) NOT NULL DEFAULT '0',
  `car_price_old` int(11) NOT NULL DEFAULT '0',
  `car_producer_id` int(11) NOT NULL DEFAULT '0',
  `car_safety` text NOT NULL,
  `car_sku` int(11) NOT NULL DEFAULT '0',
  `car_slide` tinyint(1) NOT NULL DEFAULT '0',
  `car_status` tinyint(4) DEFAULT '1',
  `car_transmission_id` tinyint(4) NOT NULL DEFAULT '0',
  `car_url` varchar(255) NOT NULL,
  `car_year` smallint(4) NOT NULL DEFAULT '0',
  `seo_description` text NOT NULL,
  `seo_h1` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_text` text NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  PRIMARY KEY (`car_id`),
  KEY `car_engine_id` (`car_engine_id`),
  KEY `car_transmission_id` (`car_transmission_id`),
  KEY `car_body_id` (`car_body_id`),
  KEY `car_year` (`car_year`),
  KEY `car_model_id` (`car_model_id`),
  KEY `car_producer_id` (`car_producer_id`),
  KEY `car_gear_id` (`car_gear_id`),
  KEY `car_color_id` (`car_color_id`),
  KEY `car_phase_id` (`car_phase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2858 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (2704,5,3,'',1537962777,'',2,1395,1,146,101,'SKODA Octavia A7 1.4 M/T Ambition','TMBAC2NF4GB006023',3,0,320000,335000,2,'',15358,0,0,2,'2704_SKODA_Octavia_A7_14_MT_Ambition',2015,'','','','',''),(2709,5,1,'',1538045533,'',2,1198,1,97,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE3FB006080',3,0,265000,280000,2,'',14130,0,1,2,'2709_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2718,5,1,'',1538656499,'',2,1199,1,91,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE5FB006095',2,0,260000,270000,2,'',14132,0,1,2,'2718_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2729,5,3,'',1539696700,'',2,1198,1,69,101,'SKODA Octavia A7 1.2 M/T Active','TMBAB2NE8FB006057',3,0,275000,290000,2,'',14073,0,1,2,'2729_SKODA_Octavia_A7_12_MT_Active',2014,'','','','',''),(2730,5,1,'',1539696765,'',2,1198,1,142,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE6FB006073',3,0,255000,270000,2,'',14138,0,1,2,'2730_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2731,5,1,'',1539696845,'',2,1198,1,136,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE5FB006078',3,0,255000,270000,2,'',14135,0,1,2,'2731_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2732,5,1,'',1539696873,'',2,1198,1,111,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE9FB006066',1,0,295000,305000,2,'',14137,0,1,2,'2732_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2733,5,1,'',1539696923,'',2,1198,1,92,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE8FB006074',3,0,270000,285000,2,'',14136,0,1,2,'2733_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2735,5,3,'',1539697166,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','TMBAC2NE9EB006239',3,0,330000,350000,2,'',13174,0,0,1,'2735_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','',''),(2736,1,2,'',1539780712,'',1,1968,1,24,8,'AUDI A6 2.0 TDI S TRONIC','WAUZZZ4G0HN035089',1,0,890000,950000,11,'',152864,0,1,1,'2736_AUDI_A6_20_TDI_S_TRONIC',2016,'','','','',''),(2746,1,2,'',1540908274,'',2,1798,1,51,7,'AUDI A4 1.8 A/T','WAUZZZ8K2FN032075',3,0,645000,680000,11,'',15445,0,1,1,'2746_AUDI_A4_18_AT',2015,'','','','',''),(2749,1,3,'',1540909619,'',2,998,1,68,2,'FORD Focus 1.0 M/T Comfort','WF04XXGCC4FD88599',3,0,270000,0,1,'',15384,0,0,2,'2749_FORD_Focus_10_MT_Comfort',2015,'','','','',''),(2750,1,3,'',1540909632,'',2,998,1,62,2,'FORD Focus 1.0 M/T Comfort','WF04XXGCC4FK40876',3,0,255000,270000,1,'',15386,0,1,2,'2750_FORD_Focus_10_MT_Comfort',2015,'','','','',''),(2757,5,1,'',1540982747,'',2,1395,1,121,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE3EB006186',3,0,290000,295000,2,'',13144,0,0,2,'2757_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','',''),(2765,5,1,'',1541585736,'',2,1198,1,70,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE7FB006079',3,0,275000,290000,2,'',14143,0,1,2,'2765_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2766,5,1,'',1541585849,'',2,1198,1,64,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NEXFB006092',3,0,280000,295000,2,'',14141,0,1,2,'2766_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2769,1,3,'',1542185432,'',2,999,1,59,2,'FORD Focus 1.0 M/T Comfort','WF04XXGCC4FL48957',3,0,275000,290000,1,'',15538,0,0,2,'2769_FORD_Focus_10_MT_Comfort',2015,'','','','',''),(2777,5,1,'',1542191357,'',2,1198,1,95,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE2FB006104',3,0,270000,285000,2,'',14144,0,1,2,'2777_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2778,5,1,'',1542191366,'',2,1198,1,81,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE2FB006099',3,0,275000,290000,2,'',14156,0,1,2,'2778_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2779,2,1,'',1542192410,'',2,1395,1,179,101,'SKODA Octavia A7 1.4 M/T Ambition COMBI','TMBJC2NE2GB006179',2,0,310000,320000,2,'',15173,0,1,2,'2779_SKODA_Octavia_A7_12_MT_Ambition',2015,'','','','',''),(2780,3,1,'',1542193465,'',2,1395,1,107,19,'VOLKSWAGEN Golf VII 1.4 M/T','WVWZZZAUZEW064954',3,0,340000,0,10,'',13257,0,1,2,'2780_VOLKSWAGEN_Golf_VII_14_MT',2013,'','','','',''),(2781,5,1,'',1542291247,'',2,1395,1,114,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE2EB006356',3,0,340000,0,2,'',13148,0,1,2,'2781_SKODA_Octavia_A7_14_TSI_MT',2013,'','','','',''),(2791,5,1,'',1542728522,'',2,1198,1,86,101,'SKODA Octavia A7 1.2 M/T Ambition','TMBAB2NE3FB006077',3,0,275000,290000,2,'',14142,0,1,2,'2791_SKODA_Octavia_A7_12_MT_Ambition',2014,'','','','',''),(2792,5,3,'',1542728563,'',2,1398,1,111,101,'SKODA Octavia A7 1.4 M/T Ambition','TMBAC2NEXEB007612',3,0,325000,0,2,'',13470,0,1,2,'2792_SKODA_Octavia_A7_12_MT_Ambition',2013,'','','','',''),(2793,5,1,'',1542794708,'',2,1395,1,155,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE4EB006374',3,0,310000,0,2,'',13147,0,1,2,'2793_SKODA_Octavia_A7_14_TSI_MT',2013,'','','','',''),(2795,5,1,'',1542794743,'',2,1395,1,85,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006354',3,0,330000,0,2,'',13149,0,1,2,'2795_SKODA_Octavia_A7_14_TSI_MT',2013,'','','','',''),(2796,5,2,'',1542794817,'',2,1798,1,131,101,'SKODA Octavia A7 1.8 TSI M/T Ambition','TMBAD2NE0EB008930',3,0,370000,0,2,'',14560,0,1,2,'2796_SKODA_Octavia_A7_14_TSI_MT',2014,'','','','',''),(2801,5,1,'',1542981207,'',2,1395,1,153,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE1EB006462',3,0,310000,315000,2,'',13145,0,1,2,'2801_SKODA_Octavia_A7_14_TSI_MT',2013,'','','','',''),(2805,5,2,'',1542981534,'',2,1395,1,96,101,'SKODA Octavia A7 1.4 TSI DSG','TMBAC2NE8EB007527',3,0,360000,0,2,'',13464,0,0,1,'2805_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','',''),(2808,2,1,'',1543243952,'',2,1798,2,204,141,'SKODA Yeti 1.8 TSI DSG','TMBLB25L2GB700008',3,0,380000,410000,2,'',15341,0,0,1,'2808_SKODA_Yeti_18_TSI_DSG',2015,'','','','',''),(2809,3,3,'',1543497290,'',2,1395,1,84,19,'VOLKSWAGEN Golf VII 1.4 TSI M/T Trendline','WVWZZZAUZEW065008',3,0,350000,0,10,'',13459,0,1,2,'2809_VOLKSWAGEN_Golf_VII_14_TSI_MT_Trendline',2013,'','','','',''),(2810,5,1,'',1543497394,'',2,1396,1,97,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006340',3,0,330000,0,2,'',13151,0,0,2,'2810_SKODA_Octavia_A7_14_TSI_MT',2013,'','','','',''),(2811,1,1,'',1543569310,'',2,1798,1,157,4,'SKODA Superb 1.8 M/T Elegance','TMBAB43T1FB300008',3,160,360000,0,2,'',14067,0,0,2,'2811_SKODA_Superb_18_MT_Elegance',2014,'','','','',''),(2813,3,3,'',1543578745,'',2,1396,1,83,15,'SKODA Fabia 1.4 M/T Active','TMBEC15J2FB500226',1,0,215000,0,2,'',14257,0,1,2,'2813_SKODA_Fabia_14_MT_Active',2014,'','','','',''),(2814,3,3,'',1543578773,'',2,1396,1,79,15,'SKODA Fabia 1.4 M/T Active','TMBEC15J6FB500228',3,0,215000,0,2,'',14259,0,0,2,'2814_SKODA_Fabia_14_MT_Active',2014,'','','','',''),(2815,3,3,'',1543578812,'',2,1396,1,65,15,'SKODA Fabia 1.4 M/T Active','TMBEC15J3FB500185',2,0,220000,0,2,'',14272,0,1,2,'2815_SKODA_Fabia_14_MT_Active',2014,'','','','',''),(2816,3,3,'',1543578844,'',2,1396,1,61,15,'SKODA Fabia 1.4 M/T Active','TMBEC15J8FB500196',2,0,220000,0,2,'',14248,0,1,2,'2816_SKODA_Fabia_14_MT_Active',2014,'','','','',''),(2817,3,3,'',1543578874,'',2,1396,1,59,15,'SKODA Fabia 1.4 M/T Active','TMBEC15J8FB500182',3,0,220000,0,2,'',14270,0,0,2,'2817_SKODA_Fabia_14_MT_Active',2014,'','','','',''),(2821,1,8,'',1543583162,'',2,1798,1,147,103,'HONDA Civic 1.8 A/T ES','19XFB2670DE800218',3,0,340000,0,22,'',13680,0,0,1,'2821_HONDA_Civic_18_AT_ES',2013,'','','','',''),(2822,1,1,'',1543583687,'',1,1498,1,169,14,'RENAULT Fluence 1.5 M/T Authentique','VF1LZBB0653681364',3,0,295000,0,4,'',15540,0,1,2,'2822_RENAULT_Fluence_15_MT_Authentique',2015,'','','','',''),(2823,1,1,'',1543583696,'',2,1598,1,61,14,'RENAULT Fluence 1.6 M/T Authentique','VF1LZBR0551496224',3,0,270000,285000,4,'',14174,0,0,2,'2823_RENAULT_Fluence_15_MT_Authentique',2014,'','','','',''),(2828,2,3,'',1543584415,'',2,1587,1,131,16,'SKODA FABIA 1.6 M/T Combi Elegance','TMBJD45J6DB502359',3,0,240000,244999,2,'',12775,0,1,2,'2828_SKODA_FABIA_16_MT_Combi_Elegance',2013,'','','','',''),(2829,2,1,'',1543832726,'',1,2360,2,63,126,'VOLVO XC60 2.4 A/T','YV1DZ8756E2544949',3,0,715000,0,7,'',13303,0,1,1,'2829_VOLVO_XC60_24_AT',2013,'','','','',''),(2830,5,1,'',1543846373,'',2,1398,1,41,101,'SKODA Octavia A7 1.4 TSI DSG','TMBAC2NE3EB007600',1,0,375000,0,2,'',13463,0,1,1,'2830_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','',''),(2831,5,9,'',1543846535,'',2,1598,1,204,34,'SKODA Rapid 1.6 M/T Ambition MPI','TMBAD2NH5EB101727',1,0,255000,0,2,'',14522,0,1,2,'2831_SKODA_Rapid_16_MT_Ambition_MPI',2013,'','','','',''),(2832,1,1,'',1543846746,'',2,1329,1,95,28,'TOYOTA Corolla 1.3 M/T Business','NMTBT9JE40R010525',1,0,320000,0,13,'',14558,0,1,2,'2832_TOYOTA_Corolla_13_MT_Business',2014,'','','','',''),(2833,1,1,'',1543846786,'',2,1329,1,66,28,'TOYOTA Corolla 1.3 M/T Business','NMTBT9JE60R010509',1,0,325000,0,13,'',14544,0,1,2,'2833_TOYOTA_Corolla_13_MT_Business',2014,'','','','',''),(2835,1,3,'',1544013444,'',2,1798,1,82,43,'HYUNDAI ELANTRA 1.8 COMFORT AUTO','KMHDH41EBDU772964',3,0,340000,0,17,'',150083,0,0,1,'2835_HYUNDAI_ELANTRA_18_COMFORT_AUTO',2013,'','','','',''),(2836,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,1,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015,'','','','',''),(2837,7,1,'',1544013783,'',2,2494,2,122,60,'MITSUBISHI L200 2.5 TD MT INVITE 4WD','MMCJNKB40EDZ06641',3,0,510000,0,19,'',13632,0,1,2,'2837_MITSUBISHI_L200_25_TD_MT_INVITE_4WD',2013,'','','','',''),(2843,1,1,'',1544016741,'',1,1968,1,76,18,'VOLKSWAGEN Passat B7 2.0 TDI DSG Life','WVWZZZ3CZFE060212',3,0,500000,0,10,'',14578,0,0,1,'2843_VOLKSWAGEN_Passat_B7_20_TDI_DSG_Life',2014,'','','','',''),(2845,3,1,'',1544019253,'',2,1198,1,84,15,'SKODA FABIA 1.2 M/T Active','TMBJH15J8FB500103',3,0,200000,0,2,'',13792,0,0,2,'2845_SKODA_FABIA_12_MT_Active',2014,'','','','',''),(2846,5,1,'',1544019341,'',2,1395,1,54,101,'SKODA Octavia A7 1.4 TSI DSG','TMBAC2NE2EB007667',3,0,375000,0,2,'',13469,0,0,1,'2846_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','',''),(2847,3,1,'',1544531281,'',2,998,1,57,2,'FORD Focus 1.0 M/T Trend','WF0KXXGCBKEY86169',3,0,270000,0,1,'',14513,0,1,2,'2847_FORD_Focus_10_MT_Trend',2014,'','','','',''),(2848,3,1,'',1544531315,'',2,998,1,57,2,'FORD Focus 1.0 M/T Trend','WF0KXXGCBKEY86189',3,0,270000,0,1,'',14514,0,1,2,'2848_FORD_Focus_10_MT_Trend',2014,'','','','',''),(2849,7,3,'',1544531836,'',1,2488,2,123,60,'MITSUBISHI L200 2.5 TD MT INVITE 4WD','MMCJNKB40EDZ09466',3,0,490000,0,19,'',13634,0,1,2,'2849_MITSUBISHI_L200_25_TD_MT_INVITE_4WD',2013,'','','','',''),(2850,3,2,'',1544532389,'',2,1568,1,63,6,'RENAULT Megane 1.6 M/T Authentique','VF1BZAR0552449998',3,0,295000,0,4,'',15567,0,0,2,'2850_RENAULT_Megane_16_MT_Authentique',2015,'','','','',''),(2851,1,8,'',1544536509,'',1,1968,1,46,17,'VOLKSWAGEN Passat CC 2.0 A/T','WVWZZZ3CZFE822407',2,0,640000,0,10,'',15570,0,1,1,'2851_VOLKSWAGEN_Passat_CC_20_AT',2015,'','','','',''),(2852,5,8,'',1544537425,'',2,1398,1,52,101,'SKODA Octavia A7 1.4 DSG Ambition TFSI','TMBAC2NE3FB006559',3,0,400000,0,2,'',15069,0,0,1,'2852_SKODA_Octavia_A7_14_DSG_Ambition_TFSI',2015,'','','','',''),(2853,5,3,'',1544537436,'',2,1398,1,95,101,'SKODA Octavia A7 1.4 M/T Elegance','TMBAC4NE1EB007638',3,0,335000,0,2,'',13496,0,1,2,'2853_SKODA_Octavia_A7_14_DSG_Ambition_TFSI',2013,'','','','',''),(2854,4,1,'',1544538050,'',2,1198,1,195000,15,'SKODA FABIA 1.2  ACTIVE','TMBEH15J6DB502740',1,0,195000,0,2,'',12839,0,1,2,'2854_SKODA_FABIA_12__ACTIVE',2013,'','','','',''),(2855,2,9,'',1544616415,'',1,2198,2,114,76,'TOYOTA RAV-4 2.2 TDI A/T','JTMDAREV30D031971',3,0,595000,0,13,'',13309,0,0,2,'2855_TOYOTA_RAV4_22_TDI_AT',2013,'','','','',''),(2856,1,8,'',1544617685,'',1,1968,1,97,18,'VOLKSWAGEN Passat B7 2.0 DSG Comfort','WVWZZZ3CZEP004094',1,0,470000,0,10,'',13284,0,1,1,'2856_VOLKSWAGEN_Passat_B7_20_DSG_Comfort',2013,'','','','',''),(2857,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',2,0,310000,330000,2,'',0,0,1,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013,'','','','','');
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `logCarTrigger_ins` AFTER INSERT ON `car` FOR EACH ROW BEGIN

    insert into logCar(
	car_id,
	user_id,
	dateOperation,
	operation,

	old_body_id,
	old_color_id,
	old_comfort,
	old_date,
	old_description,
	old_engine_id,
	old_engine_capacity,
	old_gear_id,
	old_mileage,
	old_model_id,
	old_name,
	old_number,
	old_phase_id,
	old_power,
	old_price,
	old_price_old,
	old_producer_id,
	old_safety,
	old_sku,
	old_slide,
	old_status,
	old_transmission_id,
	old_url,
	old_year,

	new_body_id,
	new_color_id,
	new_comfort,
	new_date,
	new_description,
	new_engine_id,
	new_engine_capacity,
	new_gear_id,
	new_mileage,
	new_model_id,
	new_name,
	new_number,
	new_phase_id,
	new_power,
	new_price,
	new_price_old,
	new_producer_id,
	new_safety,
	new_sku,
	new_slide,
	new_status,
	new_transmission_id,
	new_url,
	new_year

    ) values (

	NEW.car_id,
	@user_id,
	NOW(),
	1,

	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,

	NEW.car_body_id,
	NEW.car_color_id,
	NEW.car_comfort,
	NEW.car_date,
	NEW.car_description,
	NEW.car_engine_id,
	NEW.car_engine_capacity,
	NEW.car_gear_id,
	NEW.car_mileage,
	NEW.car_model_id,
	NEW.car_name,
	NEW.car_number,
	NEW.car_phase_id,
	NEW.car_power,
	NEW.car_price,
	NEW.car_price_old,
	NEW.car_producer_id,
	NEW.car_safety,
	NEW.car_sku,
	NEW.car_slide,
	NEW.car_status,
	NEW.car_transmission_id,
	NEW.car_url,
	NEW.car_year
    );

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `logCarTrigger_upd` AFTER UPDATE ON `car` FOR EACH ROW BEGIN

    insert into logCar(
	car_id,
	user_id,
	dateOperation,
	operation,

	old_body_id,
	old_color_id,
	old_comfort,
	old_date,
	old_description,
	old_engine_id,
	old_engine_capacity,
	old_gear_id,
	old_mileage,
	old_model_id,
	old_name,
	old_number,
	old_phase_id,
	old_power,
	old_price,
	old_price_old,
	old_producer_id,
	old_safety,
	old_sku,
	old_slide,
	old_status,
	old_transmission_id,
	old_url,
	old_year,

	new_body_id,
	new_color_id,
	new_comfort,
	new_date,
	new_description,
	new_engine_id,
	new_engine_capacity,
	new_gear_id,
	new_mileage,
	new_model_id,
	new_name,
	new_number,
	new_phase_id,
	new_power,
	new_price,
	new_price_old,
	new_producer_id,
	new_safety,
	new_sku,
	new_slide,
	new_status,
	new_transmission_id,
	new_url,
	new_year

    ) values (

	NEW.car_id,
	@user_id,
	NOW(),
	2,

	OLD.car_body_id,
	OLD.car_color_id,
	OLD.car_comfort,
	OLD.car_date,
	OLD.car_description,
	OLD.car_engine_id,
	OLD.car_engine_capacity,
	OLD.car_gear_id,
	OLD.car_mileage,
	OLD.car_model_id,
	OLD.car_name,
	OLD.car_number,
	OLD.car_phase_id,
	OLD.car_power,
	OLD.car_price,
	OLD.car_price_old,
	OLD.car_producer_id,
	OLD.car_safety,
	OLD.car_sku,
	OLD.car_slide,
	OLD.car_status,
	OLD.car_transmission_id,
	OLD.car_url,
	OLD.car_year,

	NEW.car_body_id,
	NEW.car_color_id,
	NEW.car_comfort,
	NEW.car_date,
	NEW.car_description,
	NEW.car_engine_id,
	NEW.car_engine_capacity,
	NEW.car_gear_id,
	NEW.car_mileage,
	NEW.car_model_id,
	NEW.car_name,
	NEW.car_number,
	NEW.car_phase_id,
	NEW.car_power,
	NEW.car_price,
	NEW.car_price_old,
	NEW.car_producer_id,
	NEW.car_safety,
	NEW.car_sku,
	NEW.car_slide,
	NEW.car_status,
	NEW.car_transmission_id,
	NEW.car_url,
	NEW.car_year
    );

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `logCarTrigger_del` AFTER DELETE ON `car` FOR EACH ROW BEGIN

    insert into logCar(
	car_id,
	user_id,
	dateOperation,
	operation,

	old_body_id,
	old_color_id,
	old_comfort,
	old_date,
	old_description,
	old_engine_id,
	old_engine_capacity,
	old_gear_id,
	old_mileage,
	old_model_id,
	old_name,
	old_number,
	old_phase_id,
	old_power,
	old_price,
	old_price_old,
	old_producer_id,
	old_safety,
	old_sku,
	old_slide,
	old_status,
	old_transmission_id,
	old_url,
	old_year,

	new_body_id,
	new_color_id,
	new_comfort,
	new_date,
	new_description,
	new_engine_id,
	new_engine_capacity,
	new_gear_id,
	new_mileage,
	new_model_id,
	new_name,
	new_number,
	new_phase_id,
	new_power,
	new_price,
	new_price_old,
	new_producer_id,
	new_safety,
	new_sku,
	new_slide,
	new_status,
	new_transmission_id,
	new_url,
	new_year

    ) values (

	OLD.car_id,
	@user_id,
	NOW(),
	0,

	OLD.car_body_id,
	OLD.car_color_id,
	OLD.car_comfort,
	OLD.car_date,
	OLD.car_description,
	OLD.car_engine_id,
	OLD.car_engine_capacity,
	OLD.car_gear_id,
	OLD.car_mileage,
	OLD.car_model_id,
	OLD.car_name,
	OLD.car_number,
	OLD.car_phase_id,
	OLD.car_power,
	OLD.car_price,
	OLD.car_price_old,
	OLD.car_producer_id,
	OLD.car_safety,
	OLD.car_sku,
	OLD.car_slide,
	OLD.car_status,
	OLD.car_transmission_id,
	OLD.car_url,
	OLD.car_year,

	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
    );

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `carcharacteristic`
--

DROP TABLE IF EXISTS `carcharacteristic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carcharacteristic` (
  `carcharacteristic_id` int(11) NOT NULL AUTO_INCREMENT,
  `carcharacteristic_car_id` int(11) DEFAULT '0',
  `carcharacteristic_characteristic_id` int(11) DEFAULT '0',
  `carcharacteristic_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`carcharacteristic_id`),
  KEY `carcharacteristic_car_id` (`carcharacteristic_car_id`),
  KEY `carcharacteristic_characteristic_id` (`carcharacteristic_characteristic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26902 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carcharacteristic`
--

LOCK TABLES `carcharacteristic` WRITE;
/*!40000 ALTER TABLE `carcharacteristic` DISABLE KEYS */;
INSERT INTO `carcharacteristic` VALUES (25394,2704,18,1),(25395,2704,19,1),(25396,2704,2,1),(25397,2704,6,1),(25398,2704,8,1),(25399,2704,9,1),(25400,2704,10,1),(25401,2704,24,1),(25402,2704,11,1),(25403,2704,21,1),(25443,2709,18,1),(25444,2709,19,1),(25445,2709,2,1),(25446,2709,7,0),(25447,2709,9,1),(25448,2709,10,1),(25449,2709,24,1),(25450,2709,11,1),(25451,2709,21,1),(25463,2709,6,1),(25549,2718,18,1),(25550,2718,19,1),(25551,2718,2,1),(25552,2718,6,1),(25553,2718,8,1),(25554,2718,9,1),(25555,2718,10,1),(25556,2718,24,1),(25557,2718,11,1),(25558,2718,21,1),(25580,2704,31,1),(25671,2729,18,1),(25672,2729,19,0),(25673,2729,2,1),(25674,2729,6,0),(25675,2729,8,0),(25676,2729,9,1),(25677,2729,10,1),(25678,2729,24,1),(25679,2729,11,1),(25680,2729,21,1),(25681,2730,18,1),(25682,2730,19,1),(25683,2730,2,1),(25684,2730,6,1),(25685,2730,8,1),(25686,2730,9,1),(25687,2730,10,1),(25688,2730,24,1),(25689,2730,11,1),(25690,2730,21,1),(25691,2731,18,1),(25692,2731,19,1),(25693,2731,2,1),(25694,2731,6,1),(25695,2731,8,1),(25696,2731,9,1),(25697,2731,10,1),(25698,2731,24,1),(25699,2731,11,1),(25700,2731,21,1),(25701,2732,18,1),(25702,2732,19,1),(25703,2732,2,1),(25704,2732,6,1),(25705,2732,8,1),(25706,2732,9,1),(25707,2732,10,1),(25708,2732,24,1),(25709,2732,11,1),(25710,2732,21,1),(25711,2733,18,1),(25712,2733,19,1),(25713,2733,2,1),(25714,2733,6,1),(25715,2733,8,1),(25716,2733,9,1),(25717,2733,10,1),(25718,2733,24,1),(25719,2733,11,1),(25720,2733,21,1),(25731,2735,18,1),(25732,2735,19,1),(25733,2735,2,1),(25734,2735,6,1),(25735,2735,8,1),(25736,2735,9,1),(25737,2735,10,1),(25738,2735,24,1),(25739,2735,11,1),(25740,2735,21,1),(25741,2735,20,1),(25742,2736,18,1),(25743,2736,19,1),(25744,2736,2,1),(25745,2736,22,1),(25746,2736,23,1),(25747,2736,26,1),(25748,2736,6,1),(25749,2736,8,1),(25750,2736,9,1),(25751,2736,10,1),(25752,2736,24,1),(25753,2736,11,1),(25754,2736,12,1),(25755,2736,20,1),(25756,2736,21,1),(25757,2736,27,1),(25758,2736,28,1),(25759,2729,7,1),(25867,2746,18,1),(25868,2746,19,1),(25869,2746,2,1),(25870,2746,16,1),(25871,2746,22,1),(25872,2746,23,1),(25873,2746,26,1),(25874,2746,6,1),(25875,2746,8,1),(25876,2746,9,1),(25877,2746,10,1),(25878,2746,24,1),(25879,2746,11,1),(25880,2746,12,1),(25881,2746,20,1),(25882,2746,21,1),(25883,2746,29,1),(25911,2749,18,1),(25912,2749,19,1),(25913,2749,2,1),(25914,2749,25,1),(25915,2749,4,1),(25916,2749,7,1),(25917,2749,9,1),(25918,2749,10,1),(25919,2749,24,1),(25920,2749,21,1),(25921,2750,18,1),(25922,2750,19,1),(25923,2750,2,1),(25924,2750,25,1),(25925,2750,4,1),(25926,2750,7,1),(25927,2750,9,1),(25928,2750,10,1),(25929,2750,24,1),(25930,2750,21,1),(26002,2757,18,1),(26003,2757,19,1),(26004,2757,2,1),(26005,2757,6,1),(26006,2757,8,1),(26007,2757,9,1),(26008,2757,10,1),(26009,2757,24,1),(26010,2757,11,1),(26011,2757,21,1),(26089,2765,18,1),(26090,2765,19,1),(26091,2765,2,1),(26092,2765,6,1),(26093,2765,8,1),(26094,2765,9,1),(26095,2765,10,1),(26096,2765,24,1),(26097,2765,11,1),(26098,2765,21,1),(26099,2766,18,1),(26100,2766,19,1),(26101,2766,2,1),(26102,2766,6,1),(26103,2766,8,1),(26104,2766,9,1),(26105,2766,10,1),(26106,2766,24,1),(26107,2766,11,1),(26108,2766,21,1),(26126,2769,18,1),(26127,2769,2,1),(26128,2769,7,1),(26129,2769,9,1),(26130,2769,10,1),(26131,2769,24,1),(26132,2769,21,1),(26182,2777,18,1),(26183,2777,19,1),(26184,2777,2,1),(26185,2777,6,1),(26186,2777,8,1),(26187,2777,9,1),(26188,2777,10,1),(26189,2777,24,1),(26190,2777,11,1),(26191,2777,21,1),(26192,2778,18,1),(26193,2778,19,1),(26194,2778,2,1),(26195,2778,6,1),(26196,2778,8,1),(26197,2778,9,1),(26198,2778,10,1),(26199,2778,24,1),(26200,2778,11,1),(26201,2778,21,1),(26202,2779,18,1),(26203,2779,19,1),(26204,2779,2,1),(26205,2779,6,1),(26206,2779,8,1),(26207,2779,9,1),(26208,2779,10,1),(26209,2779,24,1),(26210,2779,11,1),(26211,2779,21,1),(26212,2780,18,1),(26213,2780,19,1),(26214,2780,2,1),(26215,2780,6,1),(26216,2780,8,1),(26217,2780,9,1),(26218,2780,10,1),(26219,2780,24,1),(26220,2780,11,1),(26221,2780,20,1),(26222,2780,21,1),(26223,2781,18,1),(26224,2781,2,1),(26225,2781,6,1),(26226,2781,8,1),(26227,2781,9,1),(26228,2781,10,1),(26229,2781,24,1),(26230,2781,11,1),(26231,2781,21,1),(26305,2791,18,1),(26306,2791,19,1),(26307,2791,2,1),(26308,2791,6,1),(26309,2791,8,1),(26310,2791,9,1),(26311,2791,10,1),(26312,2791,24,1),(26313,2791,11,1),(26314,2791,21,1),(26315,2792,18,1),(26316,2792,19,1),(26317,2792,2,1),(26318,2792,6,1),(26319,2792,8,1),(26320,2792,9,1),(26321,2792,10,1),(26322,2792,24,1),(26323,2792,11,1),(26324,2792,21,1),(26325,2793,18,1),(26326,2793,2,1),(26327,2793,6,1),(26328,2793,8,1),(26329,2793,9,1),(26330,2793,10,1),(26331,2793,24,1),(26332,2793,11,1),(26333,2793,21,1),(26343,2795,18,1),(26344,2795,2,1),(26345,2795,6,1),(26346,2795,8,1),(26347,2795,9,1),(26348,2795,10,1),(26349,2795,24,1),(26350,2795,11,1),(26351,2795,21,1),(26352,2796,18,1),(26353,2796,2,1),(26354,2796,6,1),(26355,2796,8,1),(26356,2796,9,1),(26357,2796,10,1),(26358,2796,24,1),(26359,2796,11,1),(26360,2796,21,1),(26412,2801,18,1),(26413,2801,19,1),(26414,2801,2,1),(26415,2801,6,1),(26416,2801,8,1),(26417,2801,9,1),(26418,2801,10,1),(26419,2801,24,1),(26420,2801,11,1),(26421,2801,21,1),(26427,2801,13,1),(26438,2805,18,1),(26439,2805,19,1),(26440,2805,2,1),(26441,2805,6,1),(26442,2805,8,1),(26443,2805,9,1),(26444,2805,10,1),(26445,2805,24,1),(26446,2805,11,1),(26447,2805,21,1),(26448,2805,20,1),(26449,2805,13,1),(26468,2808,18,1),(26469,2808,19,1),(26470,2808,2,1),(26471,2808,6,1),(26472,2808,8,1),(26473,2808,9,1),(26474,2808,10,1),(26475,2808,24,1),(26476,2808,11,1),(26477,2808,20,1),(26478,2808,21,1),(26479,2809,18,1),(26480,2809,19,1),(26481,2809,2,1),(26482,2809,7,1),(26483,2809,9,1),(26484,2809,10,1),(26485,2809,24,1),(26486,2809,21,1),(26487,2810,18,1),(26488,2810,19,1),(26489,2810,2,1),(26490,2810,6,1),(26491,2810,8,1),(26492,2810,9,1),(26493,2810,10,1),(26494,2810,24,1),(26495,2810,11,1),(26496,2810,21,1),(26497,2811,18,1),(26498,2811,19,1),(26499,2811,2,1),(26500,2811,16,1),(26501,2811,23,1),(26502,2811,4,1),(26503,2811,6,1),(26504,2811,8,1),(26505,2811,9,1),(26506,2811,10,1),(26507,2811,24,1),(26508,2811,11,1),(26509,2811,12,1),(26510,2811,20,1),(26511,2811,21,1),(26517,2813,2,1),(26518,2813,7,1),(26519,2813,9,1),(26520,2813,10,1),(26521,2813,21,1),(26522,2814,2,1),(26523,2814,7,1),(26524,2814,9,1),(26525,2814,10,1),(26526,2814,21,1),(26527,2815,2,1),(26528,2815,7,1),(26529,2815,9,1),(26530,2815,10,1),(26531,2815,21,1),(26532,2816,2,1),(26533,2816,7,1),(26534,2816,9,1),(26535,2816,10,1),(26536,2816,21,1),(26537,2817,2,1),(26538,2817,7,1),(26539,2817,9,1),(26540,2817,10,1),(26541,2817,21,1),(26557,2821,18,1),(26558,2821,19,1),(26559,2821,2,1),(26560,2821,15,1),(26561,2821,16,1),(26562,2821,6,1),(26563,2821,8,1),(26564,2821,9,1),(26565,2821,10,1),(26566,2821,11,1),(26567,2821,20,1),(26568,2821,21,1),(26569,2821,27,1),(26570,2822,18,1),(26571,2822,2,1),(26572,2822,6,1),(26573,2822,8,1),(26574,2822,9,1),(26575,2822,10,1),(26576,2822,24,1),(26577,2822,11,1),(26578,2822,21,1),(26579,2823,18,1),(26580,2823,2,1),(26581,2823,6,1),(26582,2823,8,1),(26583,2823,9,1),(26584,2823,10,1),(26585,2823,24,1),(26586,2823,11,1),(26587,2823,21,1),(26588,2823,20,1),(26589,2822,20,1),(26622,2828,18,1),(26623,2828,2,1),(26624,2828,7,1),(26625,2828,9,1),(26626,2828,10,1),(26627,2828,24,1),(26628,2828,11,1),(26629,2828,20,1),(26630,2828,21,1),(26631,2829,18,1),(26632,2829,19,1),(26633,2829,2,1),(26634,2829,16,1),(26635,2829,22,1),(26636,2829,23,1),(26637,2829,25,1),(26638,2829,26,1),(26639,2829,4,1),(26640,2829,6,1),(26641,2829,8,1),(26642,2829,9,1),(26643,2829,10,1),(26644,2829,24,1),(26645,2829,11,1),(26646,2829,12,1),(26647,2829,20,1),(26648,2829,21,1),(26649,2830,18,1),(26650,2830,19,1),(26651,2830,2,1),(26652,2830,6,1),(26653,2830,8,1),(26654,2830,9,1),(26655,2830,10,1),(26656,2830,24,1),(26657,2830,11,1),(26658,2830,20,0),(26659,2830,21,1),(26660,2831,18,1),(26661,2831,2,1),(26662,2831,6,1),(26663,2831,8,1),(26664,2831,9,1),(26665,2831,10,1),(26666,2831,24,1),(26667,2831,11,1),(26668,2831,21,1),(26669,2832,18,1),(26670,2832,19,0),(26671,2832,2,1),(26672,2832,7,1),(26673,2832,9,1),(26674,2832,10,1),(26675,2832,24,1),(26676,2832,21,1),(26677,2833,18,1),(26678,2833,19,0),(26679,2833,2,1),(26680,2833,7,1),(26681,2833,9,1),(26682,2833,10,1),(26683,2833,24,1),(26684,2833,21,1),(26693,2835,18,1),(26694,2835,19,1),(26695,2835,2,1),(26696,2835,6,1),(26697,2835,8,1),(26698,2835,9,1),(26699,2835,10,1),(26700,2835,24,1),(26701,2835,11,1),(26702,2835,20,1),(26703,2835,21,1),(26704,2836,18,1),(26705,2836,19,1),(26706,2836,2,1),(26707,2836,6,1),(26708,2836,8,1),(26709,2836,9,1),(26710,2836,10,1),(26711,2836,24,1),(26712,2836,11,1),(26713,2836,21,1),(26714,2837,18,1),(26715,2837,2,1),(26716,2837,7,1),(26717,2837,9,1),(26718,2837,10,1),(26719,2837,24,1),(26720,2837,11,1),(26721,2837,20,1),(26722,2837,21,1),(26750,2836,16,1),(26751,2843,18,1),(26752,2843,19,1),(26753,2843,2,1),(26754,2843,22,1),(26755,2843,25,1),(26756,2843,4,1),(26757,2843,6,1),(26758,2843,8,1),(26759,2843,9,1),(26760,2843,10,1),(26761,2843,11,1),(26762,2843,20,1),(26763,2843,21,1),(26769,2845,2,1),(26770,2845,7,1),(26771,2845,9,1),(26772,2845,10,1),(26773,2845,21,1),(26774,2846,18,1),(26775,2846,19,1),(26776,2846,2,1),(26777,2846,6,1),(26778,2846,8,1),(26779,2846,9,1),(26780,2846,10,1),(26781,2846,24,1),(26782,2846,11,1),(26783,2846,21,1),(26784,2846,20,0),(26785,2846,13,1),(26786,2847,18,1),(26787,2847,19,1),(26788,2847,2,1),(26789,2847,6,1),(26790,2847,9,1),(26791,2847,10,1),(26792,2847,24,1),(26793,2847,11,1),(26794,2847,21,1),(26795,2848,18,1),(26796,2848,19,1),(26797,2848,2,1),(26798,2848,6,1),(26799,2848,9,1),(26800,2848,10,1),(26801,2848,24,1),(26802,2848,11,1),(26803,2848,21,1),(26804,2849,18,1),(26805,2849,2,1),(26806,2849,7,1),(26807,2849,9,1),(26808,2849,10,1),(26809,2849,11,1),(26810,2849,21,1),(26811,2850,18,1),(26812,2850,2,1),(26813,2850,6,1),(26814,2850,9,1),(26815,2850,10,1),(26816,2850,24,1),(26817,2850,11,1),(26818,2850,20,1),(26819,2850,21,1),(26820,2851,18,1),(26821,2851,19,1),(26822,2851,2,1),(26823,2851,15,1),(26824,2851,16,1),(26825,2851,23,1),(26826,2851,25,1),(26827,2851,26,1),(26828,2851,4,1),(26829,2851,6,1),(26830,2851,8,1),(26831,2851,9,1),(26832,2851,10,1),(26833,2851,24,1),(26834,2851,11,1),(26835,2851,12,1),(26836,2851,13,1),(26837,2851,20,1),(26838,2851,21,1),(26839,2851,29,1),(26840,2852,18,1),(26841,2852,19,1),(26842,2852,2,1),(26843,2852,6,1),(26844,2852,8,1),(26845,2852,9,1),(26846,2852,10,1),(26847,2852,24,1),(26848,2852,11,1),(26849,2852,21,1),(26850,2853,18,1),(26851,2853,19,1),(26852,2853,2,1),(26853,2853,6,1),(26854,2853,8,1),(26855,2853,9,1),(26856,2853,10,1),(26857,2853,24,1),(26858,2853,11,1),(26859,2853,21,1),(26860,2853,16,1),(26861,2853,23,1),(26862,2853,25,0),(26863,2853,20,1),(26864,2854,2,1),(26865,2854,7,1),(26866,2854,9,1),(26867,2854,10,1),(26868,2854,21,1),(26869,2855,18,1),(26870,2855,19,1),(26871,2855,2,1),(26872,2855,6,1),(26873,2855,8,1),(26874,2855,9,1),(26875,2855,10,1),(26876,2855,24,1),(26877,2855,11,1),(26878,2855,20,1),(26879,2855,21,1),(26880,2855,27,1),(26881,2856,18,1),(26882,2856,19,1),(26883,2856,2,1),(26884,2856,6,1),(26885,2856,9,1),(26886,2856,10,1),(26887,2856,24,1),(26888,2856,11,1),(26889,2856,20,1),(26890,2856,21,1),(26891,2857,18,1),(26892,2857,19,1),(26893,2857,2,1),(26894,2857,6,1),(26895,2857,8,1),(26896,2857,9,1),(26897,2857,10,1),(26898,2857,24,1),(26899,2857,11,1),(26900,2857,21,1),(26901,2857,20,1);
/*!40000 ALTER TABLE `carcharacteristic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carimage`
--

DROP TABLE IF EXISTS `carimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carimage` (
  `carimage_id` int(11) NOT NULL AUTO_INCREMENT,
  `carimage_car_id` int(11) DEFAULT '0',
  `carimage_image_id` int(11) DEFAULT '0',
  `carimage_order` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`carimage_id`),
  KEY `carimage_car_id` (`carimage_car_id`),
  KEY `carimage_image_id` (`carimage_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20417 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carimage`
--

LOCK TABLES `carimage` WRITE;
/*!40000 ALTER TABLE `carimage` DISABLE KEYS */;
INSERT INTO `carimage` VALUES (19722,2709,19773,8),(19723,2709,19774,4),(19724,2709,19775,3),(19725,2709,19776,2),(19726,2709,19777,1),(19727,2709,19778,6),(19728,2709,19779,7),(19729,2709,19780,5),(19760,2704,19811,4),(19761,2704,19812,3),(19762,2704,19813,2),(19763,2704,19814,1),(19764,2704,19815,6),(19765,2704,19816,7),(19766,2704,19817,5),(19767,2704,19818,8),(19806,2718,19857,7),(19807,2718,19858,5),(19808,2718,19859,8),(19809,2718,19860,4),(19810,2718,19861,3),(19811,2718,19862,2),(19812,2718,19863,1),(19813,2718,19864,6),(19914,2736,19965,4),(19915,2736,19966,3),(19916,2736,19967,1),(19917,2736,19968,2),(19918,2736,19969,6),(19919,2736,19970,7),(19920,2736,19971,8),(19921,2736,19972,5),(19944,2735,19995,3),(19945,2735,19996,2),(19946,2735,19997,1),(19947,2735,19998,6),(19948,2735,19999,7),(19949,2735,20000,5),(19950,2735,20001,8),(19951,2735,20002,4),(19960,2732,20011,8),(19961,2732,20012,4),(19962,2732,20013,3),(19963,2732,20014,2),(19964,2732,20015,1),(19965,2732,20016,6),(19966,2732,20017,7),(19967,2732,20018,5),(19968,2731,20019,4),(19969,2731,20020,3),(19970,2731,20021,1),(19971,2731,20022,2),(19972,2731,20023,6),(19973,2731,20024,7),(19974,2731,20025,5),(19975,2731,20026,8),(19976,2729,20027,8),(19977,2729,20028,4),(19978,2729,20029,3),(19979,2729,20030,1),(19980,2729,20031,2),(19981,2729,20032,6),(19982,2729,20033,7),(19983,2729,20034,5),(19992,2757,20043,0),(19993,2757,20044,0),(19994,2757,20045,0),(19995,2757,20046,0),(19996,2757,20047,0),(19997,2757,20048,0),(19998,2757,20049,0),(19999,2757,20050,0),(20000,2733,20051,0),(20001,2733,20052,0),(20002,2733,20053,0),(20003,2733,20054,0),(20004,2733,20055,0),(20005,2733,20056,0),(20006,2733,20057,0),(20007,2733,20058,0),(20016,2730,20067,0),(20017,2730,20068,0),(20018,2730,20069,0),(20019,2730,20070,0),(20020,2730,20071,0),(20021,2730,20072,0),(20022,2730,20073,0),(20023,2730,20074,0),(20024,2749,20075,0),(20025,2749,20076,0),(20026,2749,20077,0),(20027,2749,20078,0),(20028,2749,20079,0),(20029,2749,20080,0),(20030,2749,20081,0),(20031,2749,20082,0),(20050,2750,20101,0),(20051,2750,20102,0),(20052,2750,20103,0),(20053,2750,20104,0),(20054,2750,20105,0),(20055,2750,20106,0),(20056,2750,20107,0),(20057,2750,20108,0),(20058,2750,20109,0),(20059,2746,20110,0),(20060,2746,20111,0),(20061,2746,20112,0),(20062,2746,20113,0),(20063,2746,20114,0),(20064,2746,20115,0),(20065,2746,20116,0),(20148,2765,20200,4),(20149,2765,20201,3),(20150,2765,20202,1),(20151,2765,20203,2),(20152,2765,20204,7),(20153,2765,20205,5),(20154,2765,20206,6),(20155,2766,20207,8),(20156,2766,20208,1),(20157,2766,20209,2),(20158,2766,20210,6),(20159,2766,20211,7),(20160,2766,20212,5),(20161,2766,20213,4),(20162,2766,20214,3),(20190,2777,20242,3),(20191,2777,20243,1),(20192,2777,20244,2),(20193,2777,20245,6),(20194,2777,20246,7),(20195,2777,20247,5),(20196,2777,20248,8),(20197,2777,20249,4),(20198,2778,20250,6),(20199,2778,20251,7),(20200,2778,20252,5),(20201,2778,20253,1),(20202,2778,20254,2),(20203,2778,20255,4),(20204,2778,20256,3),(20205,2779,20257,4),(20206,2779,20258,3),(20207,2779,20259,2),(20208,2779,20260,1),(20209,2779,20261,5),(20210,2779,20262,6),(20211,2779,20263,7),(20212,2780,20264,8),(20213,2780,20265,4),(20214,2780,20266,3),(20215,2780,20267,2),(20216,2780,20268,1),(20217,2780,20269,6),(20218,2780,20270,7),(20219,2780,20271,5),(20228,2769,20280,3),(20229,2769,20281,1),(20230,2769,20282,2),(20231,2769,20283,6),(20232,2769,20284,7),(20233,2769,20285,5),(20234,2769,20286,8),(20235,2769,20287,4),(20236,2781,20288,5),(20237,2781,20289,8),(20238,2781,20290,4),(20239,2781,20291,3),(20240,2781,20292,1),(20241,2781,20293,2),(20242,2781,20294,6),(20243,2781,20295,7),(20260,2791,20315,4),(20261,2791,20316,3),(20262,2791,20317,1),(20263,2791,20318,2),(20264,2791,20319,6),(20265,2791,20320,7),(20266,2791,20321,5),(20267,2791,20322,8),(20268,2792,20323,3),(20269,2792,20324,2),(20270,2792,20325,1),(20271,2792,20326,6),(20272,2792,20327,7),(20273,2792,20328,5),(20274,2792,20329,8),(20275,2792,20330,4),(20276,2808,20331,4),(20277,2808,20332,3),(20278,2808,20333,1),(20279,2808,20334,2),(20280,2808,20335,6),(20281,2808,20336,7),(20282,2808,20337,5),(20283,2808,20338,8),(20301,2795,20356,5),(20302,2795,20357,8),(20303,2795,20358,4),(20304,2795,20359,3),(20305,2795,20360,1),(20306,2795,20361,2),(20307,2795,20362,6),(20308,2795,20363,7),(20309,2796,20364,8),(20310,2796,20365,4),(20311,2796,20366,3),(20312,2796,20367,1),(20313,2796,20368,2),(20314,2796,20369,6),(20315,2796,20370,7),(20316,2796,20371,5),(20318,2813,20373,0),(20319,2813,20374,0),(20320,2813,20375,0),(20321,2813,20376,0),(20322,2813,20377,0),(20323,2813,20378,0),(20324,2814,20379,0),(20325,2814,20380,0),(20326,2814,20381,0),(20327,2814,20382,0),(20328,2814,20383,0),(20329,2814,20384,0),(20330,2814,20385,0),(20331,2814,20386,0),(20340,2845,20395,0),(20341,2845,20396,0),(20342,2845,20397,0),(20343,2845,20398,0),(20344,2845,20399,0),(20345,2845,20400,0),(20346,2845,20401,0),(20347,2845,20402,0),(20356,2823,20411,0),(20357,2823,20412,0),(20358,2823,20413,0),(20359,2823,20414,0),(20360,2823,20415,0),(20361,2823,20416,0),(20362,2823,20417,0),(20363,2822,20418,0),(20364,2822,20419,0),(20365,2822,20420,0),(20366,2822,20421,0),(20367,2822,20422,0),(20368,2822,20423,0),(20369,2822,20424,0),(20370,2793,20425,0),(20371,2793,20426,0),(20372,2793,20427,0),(20373,2793,20428,0),(20374,2793,20429,0),(20375,2793,20430,0),(20376,2793,20431,0),(20377,2793,20432,0),(20378,2801,20433,0),(20379,2801,20434,0),(20380,2801,20435,0),(20381,2801,20436,0),(20382,2801,20437,0),(20383,2801,20438,0),(20384,2801,20439,0),(20385,2801,20440,0),(20386,2835,20441,0),(20387,2835,20442,0),(20388,2835,20443,0),(20389,2835,20444,0),(20390,2835,20445,0),(20391,2835,20446,0),(20392,2835,20447,0),(20393,2835,20448,0),(20394,2809,20449,0),(20395,2809,20450,0),(20396,2809,20451,0),(20397,2809,20452,0),(20398,2809,20453,0),(20399,2809,20454,0),(20400,2809,20455,0),(20401,2805,20456,0),(20402,2805,20457,0),(20403,2805,20458,0),(20404,2805,20459,0),(20405,2805,20460,0),(20406,2805,20461,0),(20407,2805,20462,0),(20408,2805,20463,0),(20409,2857,20464,3),(20410,2857,20465,2),(20411,2857,20466,1),(20412,2857,20467,6),(20413,2857,20468,7),(20414,2857,20469,5),(20415,2857,20470,8),(20416,2857,20471,4);
/*!40000 ALTER TABLE `carimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carsmart`
--

DROP TABLE IF EXISTS `carsmart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carsmart` (
  `carsmart_id` int(11) NOT NULL AUTO_INCREMENT,
  `carsmart_car_id` int(11) DEFAULT '0',
  `carsmart_smart_id` int(11) DEFAULT '0',
  `carsmart_status` int(11) DEFAULT '1',
  PRIMARY KEY (`carsmart_id`),
  KEY `carsmart_car_id` (`carsmart_car_id`),
  KEY `carsmart_smart_id` (`carsmart_smart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1197 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carsmart`
--

LOCK TABLES `carsmart` WRITE;
/*!40000 ALTER TABLE `carsmart` DISABLE KEYS */;
INSERT INTO `carsmart` VALUES (1049,2704,2,1),(1054,2709,2,1),(1063,2718,2,1),(1074,2729,2,1),(1075,2730,2,1),(1076,2731,2,1),(1077,2732,2,1),(1078,2733,2,1),(1080,2736,4,1),(1090,2746,4,1),(1093,2749,2,1),(1094,2750,2,1),(1101,2757,2,1),(1109,2765,2,1),(1110,2766,2,1),(1113,2769,2,1),(1120,2777,2,1),(1121,2778,2,1),(1122,2779,2,1),(1123,2780,2,1),(1124,2781,2,1),(1134,2791,2,1),(1135,2792,2,1),(1136,2793,2,1),(1138,2795,2,1),(1139,2796,2,1),(1144,2801,2,1),(1149,2808,5,1),(1150,2809,2,1),(1151,2810,2,1),(1152,2811,4,1),(1154,2813,3,1),(1155,2814,3,1),(1156,2815,3,1),(1157,2816,3,1),(1158,2817,3,1),(1162,2821,2,1),(1163,2822,2,1),(1164,2823,2,1),(1169,2828,3,1),(1170,2829,4,1),(1171,2830,2,1),(1172,2832,2,1),(1173,2833,2,1),(1175,2835,2,1),(1176,2836,2,1),(1177,2837,5,1),(1183,2843,4,1),(1185,2845,1,1),(1186,2846,2,1),(1187,2847,2,1),(1188,2848,2,1),(1189,2849,5,1),(1190,2850,2,1),(1191,2851,4,1),(1192,2852,2,1),(1193,2853,2,1),(1194,2854,3,1),(1195,2855,5,1),(1196,2856,4,1);
/*!40000 ALTER TABLE `carsmart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characteristic`
--

DROP TABLE IF EXISTS `characteristic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characteristic` (
  `characteristic_id` int(11) NOT NULL AUTO_INCREMENT,
  `characteristic_characteristicgroup_id` int(11) DEFAULT '0',
  `characteristic_name` varchar(50) DEFAULT NULL,
  `characteristic_order` int(11) DEFAULT '0',
  `characteristic_import_key` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`characteristic_id`),
  KEY `characteristic_characteristicgroup_id` (`characteristic_characteristicgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characteristic`
--

LOCK TABLES `characteristic` WRITE;
/*!40000 ALTER TABLE `characteristic` DISABLE KEYS */;
INSERT INTO `characteristic` VALUES (1,2,'Підігрів керма',10,'HSW'),(2,1,'Подушки безпеки',3,'AIR'),(4,2,'Підігрів скла',11,'HW'),(6,2,'Клімат-контроль',12,'ACC'),(7,2,'Кондиціонер',13,'AC'),(8,2,'Підігрів сидінь',14,'HS'),(9,2,'Електричні склопідйомники',15,'EW'),(10,2,'Центральний замок',16,'CZ'),(11,4,'Протитуманні фари',18,'Fog'),(12,4,'Ксенонові фари',19,'Xenon'),(13,4,'Повнорозмірне запасне колесо',20,'FSST'),(14,4,'Тимчасове запасне колесо',21,'TST'),(15,2,'Шкіряний салон',4,'LS'),(16,2,'Круїз-контроль',5,'CC'),(17,4,'Сигналізація',22,'VTA'),(18,1,'ABS',1,'ABS'),(19,1,'ESP',2,'ESP'),(20,4,'Легкосплавні диски',23,'WL'),(21,4,'Бортовий комп\'ютер',24,'BC'),(22,2,'Датчик світла',6,'Light'),(23,2,'Датчик дощу',7,'Rain'),(24,2,'Підігрів дзеркал',17,'HM'),(25,2,'Задній парктронік',8,'RPS'),(26,2,'Передній парктронік',9,'FPS'),(27,4,'Камера заднього виду',25,'RVC'),(28,4,'Навігація',26,'Nav'),(29,4,'Електричне стоянкове гальмо',27,'EPB'),(30,2,'Панорманий дах',28,NULL),(31,4,'Фаркоп',0,NULL),(32,2,'Люк',0,NULL),(33,2,'Панорманий дах',28,'PS'),(34,4,'Фаркоп',29,'TH'),(35,2,'Люк',30,'Sun');
/*!40000 ALTER TABLE `characteristic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characteristicgroup`
--

DROP TABLE IF EXISTS `characteristicgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characteristicgroup` (
  `characteristicgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `characteristicgroup_name` varchar(255) DEFAULT NULL,
  `characteristicgroup_order` int(11) DEFAULT '0',
  PRIMARY KEY (`characteristicgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characteristicgroup`
--

LOCK TABLES `characteristicgroup` WRITE;
/*!40000 ALTER TABLE `characteristicgroup` DISABLE KEYS */;
INSERT INTO `characteristicgroup` VALUES (1,'Безпека',1),(2,'Комфорт',2),(4,'Функцональність',0);
/*!40000 ALTER TABLE `characteristicgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(255) DEFAULT NULL,
  `color_import_key` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'Білий','White'),(2,'Чорний','Black'),(3,'Сірий','Grey'),(4,'Срібний','Silver'),(5,'Синій','Blue'),(6,'Бежевий','Beige'),(7,'Помаранчевий','Orange'),(8,'Коричневий','Brown'),(9,'Червоний','Red'),(10,'Зелений','Green'),(11,'Жовтий','Yellow'),(12,'Фіолетовий','Violet');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `contacts_id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `contacts_address` varchar(50) NOT NULL,
  `contacts_city` varchar(50) NOT NULL,
  `contacts_email` varchar(50) NOT NULL,
  `contacts_email_manager` varchar(50) NOT NULL,
  `contacts_fax` varchar(50) NOT NULL,
  `contacts_friday` varchar(50) NOT NULL,
  `contacts_lat` varchar(50) NOT NULL,
  `contacts_lng` varchar(50) NOT NULL,
  `contacts_mob` varchar(50) NOT NULL,
  `contacts_monday` varchar(50) NOT NULL,
  `contacts_sunday` varchar(50) NOT NULL,
  `contacts_tel` varchar(50) NOT NULL,
  `seo_description` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_text` text NOT NULL,
  PRIMARY KEY (`contacts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'вул. Корабельна, 1','м. Київ','ucs_ua@aldautomotive.com','Denys.FEDASH@aldautomotive.com','+38067-555-06-32','9:00 - 17:00','50.481904','30.490155','+38067-555-06-21','9:00 - 18:00','вихідний ','+38044-247-69-09','','','','');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cookie`
--

DROP TABLE IF EXISTS `cookie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cookie` (
  `cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_code` varchar(32) NOT NULL,
  `cookie_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cookie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cookie`
--

LOCK TABLES `cookie` WRITE;
/*!40000 ALTER TABLE `cookie` DISABLE KEYS */;
INSERT INTO `cookie` VALUES (1,'df2b473ccf8394420011623d65fb7837','2018-12-14 07:04:39'),(2,'3a7aeb046590798c1317ac9f09e7baa0','2018-12-14 07:13:38'),(3,'0b3ba0c752c8bf7d9010bbafaef4eb47','2018-12-14 07:18:38'),(4,'b03b2ea82d919c5eac3963dd96529892','2018-12-14 07:23:59'),(5,'e24a64522d549dc6f03dc3ee0c10d3e5','2018-12-14 07:25:42'),(6,'f3604dc69ff1d97cf7ea9917f177a2a3','2018-12-14 07:33:55'),(7,'6847d331f4064565aca9f03d91d3567f','2018-12-14 07:34:37'),(8,'858e01195f7087c878bbdbf4b20935f4','2018-12-14 07:42:57'),(9,'38d05b3fe18757deb22b6a4d24207557','2018-12-14 07:45:31'),(10,'1a641626c31c8cda1150fd12ba662e7f','2018-12-14 07:46:59'),(11,'fb21ccc753c526337b317a1f50c4bf33','2018-12-14 07:56:14'),(12,'a904fd5558acdf5cea9b08a505482c65','2018-12-14 08:09:21'),(13,'9cad6f5ceecb6910c7f690af5278cfc6','2018-12-14 08:12:08'),(14,'d00d3132bd7a71100773ca7ba9567363','2018-12-14 08:16:05'),(15,'403210586c16ac4b23f6cbb62451b8d6','2018-12-14 08:26:11'),(16,'095718a559ea79b5b2533d7474245de6','2018-12-14 08:30:09'),(17,'e629edf2cfd6d106cd2a68bf9b3338c4','2018-12-14 08:30:43'),(18,'ecbbe6439a8b9f10db11bd937182c932','2018-12-14 08:36:54'),(19,'1695ae59da4b121610bfe6c27f5242e2','2018-12-14 08:38:40'),(20,'e1db706111ea3f68d21e2edff6dee72c','2018-12-14 08:50:48'),(21,'fcf3e36258becf7c5daa0f37697c41f7','2018-12-14 08:55:55'),(22,'6d0ffefed8a1f4d048962488c50b012a','2018-12-14 08:56:04'),(23,'2a06157151de1b8a3d9357636031c1cd','2018-12-14 08:59:56'),(24,'5508f5d1d242e46f7ba9902330037f08','2018-12-14 09:05:26'),(25,'96d4c115ddd88138b358eaef7ab09e11','2018-12-14 09:14:30'),(26,'50564148a6a87d77545bc6e5066a23fb','2018-12-14 09:17:00'),(27,'b8d913566219074b07bd8bbf39ae8d16','2018-12-14 09:21:29'),(28,'fb5d8356ca0886e0697cdcf8c9fda077','2018-12-14 09:21:44'),(29,'ce2ed741b6160e9c35ace8e0df0e01e9','2018-12-14 09:26:16'),(30,'303da7363bf1fc990faeb508e2179993','2018-12-14 09:28:21'),(31,'f60074d76f9bcc4994a07d8b3971f259','2018-12-14 09:38:40'),(32,'1c35614c48095992b41e4fa82ac65a1c','2018-12-14 09:39:15'),(33,'fcb19ce64e82ab92bff572596d1a301c','2018-12-14 09:41:39'),(34,'31628bfff6b60eb0ab82d31cad667cfc','2018-12-14 09:54:42'),(35,'caa672422ae39eb0f580033790649345','2018-12-14 10:10:13'),(36,'4d0b25388edea9a02b05c7c24626ec3e','2018-12-14 10:12:54'),(37,'c2a165dcf5da483d1a5dd43017a8e5ea','2018-12-14 10:27:31'),(38,'7dfa49670f226ddf3f3e922f67be751f','2018-12-14 10:30:37'),(39,'b67e25dfffddc3162be16f72df99123a','2018-12-14 10:31:01'),(40,'e4211f63dd2a1f968bbc3596dc67cec2','2018-12-14 10:36:44');
/*!40000 ALTER TABLE `cookie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `engine`
--

DROP TABLE IF EXISTS `engine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `engine` (
  `engine_id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `engine_name` varchar(255) DEFAULT NULL,
  `engine_status` tinyint(1) DEFAULT '1',
  `engine_import_key` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`engine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `engine`
--

LOCK TABLES `engine` WRITE;
/*!40000 ALTER TABLE `engine` DISABLE KEYS */;
INSERT INTO `engine` VALUES (1,'Дизель',1,'D'),(2,'Бензин',1,'P'),(3,'Електро',0,'E'),(4,'Газ',1,'G');
/*!40000 ALTER TABLE `engine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_order` int(5) NOT NULL DEFAULT '0',
  `faq_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'<p>Чи надаєте ви послугу тест драйву?</p>\r\n','<p>Ви можете протестувати наш автомобіль на нашому парковому майданчику, але за кермом буде представник ALD Ukraine.</p>\r\n',0,1),(2,'<p>Чи можу я оплатити автомобіль частинами?</p>\r\n','<p>Так, у нас є партнери, що надають послуги з кредитування та лізингу.</p>\r\n',0,1),(3,'<p>Можу я здійснити оплату в іноземній валюті?</p>\r\n','<p>Оплата можлива тільки в національній валюті (гривні).</p>\r\n',0,1),(4,'<p>Як здійснюється оплата за автомобіль?</p>\r\n','<p>Оплата здійснюється у формі банківського переказу.</p>\r\n',0,1),(5,'<p>Ці автомобілі привезені із-за кордону?</p>\r\n','<p>Всі наші автомобілі куплені у офіційних дилерів в Україні, та мають повну історію обслуговування.</p>\r\n',0,1),(6,'<p>Що за автомобілі Ви продаєте?</p>\r\n','<p>Це наші власні автомобілі, які було повернуто з оперативного лізингу, та які мають повну історію обслуговування та справжній пробіг.</p>\r\n',0,1),(7,'<p>Чи надаєте Ви послуги кредиту та/або лізингу?</p>\r\n','<p>Ви можете придбати наші автомобілі в кредит або в лізинг через наших партнерів.</p>\r\n',0,1),(8,'<p>Яка кінцева ціна на автомобіль?</p>\r\n','<p>Ціни, що вказані на сайті, є остаточними.</p>\r\n',0,1);
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `cookie_id` int(11) DEFAULT NULL,
  `car_id` int(11) NOT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `fav_fk_1` (`cookie_id`),
  KEY `fav_fk_2` (`car_id`),
  CONSTRAINT `fav_fk_1` FOREIGN KEY (`cookie_id`) REFERENCES `cookie` (`cookie_id`) ON DELETE CASCADE,
  CONSTRAINT `fav_fk_2` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gear`
--

DROP TABLE IF EXISTS `gear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gear` (
  `gear_id` int(11) NOT NULL AUTO_INCREMENT,
  `gear_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gear_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gear`
--

LOCK TABLES `gear` WRITE;
/*!40000 ALTER TABLE `gear` DISABLE KEYS */;
INSERT INTO `gear` VALUES (1,'Передній'),(2,'Повний'),(3,'Задній');
/*!40000 ALTER TABLE `gear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20472 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (30,'/uploads/1fb/4eb/4b5/f915c4d20762982ddfcb.jpg'),(422,'/uploads/055/ad6/0d6/f1ab51309fad004ef6ba.jpg'),(439,'/uploads/2df/9d7/da2/b3c0cc38a2be547be350.jpg'),(657,'/uploads/173/3aa/aab/8d8b3c1bdc610d0d2e0b.JPG'),(660,'/uploads/460/0df/772/7f389be651b57a0aee83.JPG'),(813,'/uploads/162/559/4b5/4ea38d275fcaf8e3fe17.jpg'),(2115,'/uploads/06b/7bf/5da/89ce86deca5d8c1c8f0f.JPG'),(2116,'/uploads/86e/558/7c0/84d376c3db833cfe3976.JPG'),(2117,'/uploads/245/68a/5b2/80336071c4e0be722460.JPG'),(2119,'/uploads/1f7/b5f/311/6137da4d657b5dcc2d61.JPG'),(2123,'/uploads/78a/3f2/242/7a467a15d63521cabbfd.JPG'),(3153,'/uploads/04a/7df/384/31826a57fe49117525bf.jpg'),(3569,'/uploads/b03/f92/225/0977a04638455c30e9c0.jpg'),(4492,'/uploads/a35/ee3/e9b/715d0b2d8224c7df12fa.png'),(5250,'/uploads/ef2/619/093/5082e9c3eeccd20b7ea3.jpg'),(5251,'/uploads/65c/add/387/f972d5ac5321a0ccfec3.jpg'),(15855,'/uploads/d28/0dd/1e3/489a2972f8cb60edca54.jpg'),(18471,'/uploads/0ee/5bc/f07/f7884f694f46fbbf7aca.jpg'),(19773,'/uploads/6b9/bff/ca4/d3a15e72476297ac0696.JPG'),(19774,'/uploads/060/c1f/d04/9bfd9870d8cdf05f4913.JPG'),(19775,'/uploads/267/01b/103/056bd9ec42ac2dac50a1.JPG'),(19776,'/uploads/dc8/aad/75a/547ba2a0e84dc0b17a46.JPG'),(19777,'/uploads/21c/6a7/e77/024009dca9742f25d222.JPG'),(19778,'/uploads/b12/4a1/57b/75c538ab1cfdb8037471.JPG'),(19779,'/uploads/280/31f/410/7135fe330c973f5dce9b.JPG'),(19780,'/uploads/f93/a11/6aa/c0975e0d737fbd8b795e.JPG'),(19811,'/uploads/583/159/30b/3d5e5218c472f9ca10e3.JPG'),(19812,'/uploads/bb3/892/a0d/4b0f1d8565c01d6c123b.JPG'),(19813,'/uploads/8a1/06f/d9d/e307e88699d3e8b6b88c.JPG'),(19814,'/uploads/da3/ba5/ad1/9b87a13c1e093ab7ab0c.JPG'),(19815,'/uploads/fbd/12e/c09/b118bc50269b95ac293f.JPG'),(19816,'/uploads/aa0/aeb/c3f/d3b8f954685380765010.JPG'),(19817,'/uploads/2dc/799/d6a/82586cdc973907920eb6.JPG'),(19818,'/uploads/9b4/c67/cdf/ec8a7c8b97bcda7b0df1.JPG'),(19857,'/uploads/227/88d/138/f172d7ed50e847fdf46e.JPG'),(19858,'/uploads/66a/dff/33e/4f22e79f462d1a95a8e1.JPG'),(19859,'/uploads/e66/659/dc7/5e484405077907781169.JPG'),(19860,'/uploads/ec0/9b5/b66/739830c8ed3ca114ada3.JPG'),(19861,'/uploads/55b/922/2e1/0edff93389c17143e6ce.JPG'),(19862,'/uploads/808/31a/fe5/61832e4609af88ba86a8.JPG'),(19863,'/uploads/5a4/32c/17b/85cce662654822dc78ea.JPG'),(19864,'/uploads/936/4ae/807/8e03e8d3655925170c1a.JPG'),(19965,'/uploads/239/345/d3b/c89fc969d848a3231a59.JPG'),(19966,'/uploads/12f/804/92a/f86bad59e775d812c1ab.JPG'),(19967,'/uploads/f43/0fa/079/f9883ad6b7671e02768b.JPG'),(19968,'/uploads/727/f08/81d/61b50450aae39b0116f2.JPG'),(19969,'/uploads/3be/381/cf4/e98531bcfd403efc493e.JPG'),(19970,'/uploads/73c/1a3/bf5/d5fd605495d7a8caca62.JPG'),(19971,'/uploads/e49/d52/04e/811bbcb888e4f1357f4e.JPG'),(19972,'/uploads/695/d89/d09/a1e9e0fa1c9d7db59525.JPG'),(19995,'/uploads/312/606/662/2f855af73a13422404fc.JPG'),(19996,'/uploads/f42/6b3/914/ec44de266cc700fa401b.JPG'),(19997,'/uploads/d3e/777/0db/c5ef92cdb49c99d1a005.JPG'),(19998,'/uploads/c9d/cc9/fb4/d6deb7b2248845409d95.JPG'),(19999,'/uploads/e1b/fef/3d2/e5e56b93da14634b904e.JPG'),(20000,'/uploads/c78/0e1/34b/8f37e105ba1d4543dcc2.JPG'),(20001,'/uploads/d7a/543/6d6/74a4de61e2f72305dca7.JPG'),(20002,'/uploads/e16/dc0/e33/20511f51c2217514d20c.JPG'),(20011,'/uploads/92c/e2c/211/c4d5e9e735ad42b9e57c.JPG'),(20012,'/uploads/fd2/0d9/baa/46b1e1be15fdced02bfc.JPG'),(20013,'/uploads/91e/7aa/f2d/99f802770f7fd782f2ce.JPG'),(20014,'/uploads/75f/9f9/f4d/8173c37ee42ec3fc1036.JPG'),(20015,'/uploads/f38/261/1b2/ed92f75f14ca54166db2.JPG'),(20016,'/uploads/cf5/b1a/5ac/09febd8fcb56d6deb66f.JPG'),(20017,'/uploads/428/262/891/f2fd0e72490bf2840bd9.JPG'),(20018,'/uploads/dcc/7a8/841/7a3a8fb171b24a0ca7c9.JPG'),(20019,'/uploads/8fe/847/9cc/74cbab4b57aca4da5159.JPG'),(20020,'/uploads/d6b/3a6/2c2/cff5107b83eef8c6e01f.JPG'),(20021,'/uploads/624/3ee/72d/4411beb98d7478254779.JPG'),(20022,'/uploads/9b2/f91/d37/8c9ddf1b321497fb821b.JPG'),(20023,'/uploads/46b/d01/e34/2c9f4bdd5df7e54e14b6.JPG'),(20024,'/uploads/6a7/5aa/20a/2901440bdced3ebd28e8.JPG'),(20025,'/uploads/42a/abf/57b/72667d14c7f5bf555df7.JPG'),(20026,'/uploads/2d1/73e/6a3/5a6602b313fc7640ec29.JPG'),(20027,'/uploads/6ec/411/5a3/71271b2e2d49d21ecc6a.JPG'),(20028,'/uploads/961/1e2/46c/3d7d59bb759568078f07.JPG'),(20029,'/uploads/acf/037/511/88456aeaebb88d57c78e.JPG'),(20030,'/uploads/371/2e1/74b/5e48edef50372bd09429.JPG'),(20031,'/uploads/f08/cf1/df5/506de6e2cb8efcdee797.JPG'),(20032,'/uploads/baf/d2f/099/4b62459805a5521c5367.JPG'),(20033,'/uploads/8bd/f3c/ff4/e2ede07aeb1ea387d547.JPG'),(20034,'/uploads/d49/493/ae0/dfedca4ae2c399f376eb.JPG'),(20043,'/uploads/a10/94a/e58/1ef8d4c44e8cff110b22.JPG'),(20044,'/uploads/47e/994/056/8dfa2267f0691f98d33c.JPG'),(20045,'/uploads/137/8e7/93d/1d76d184afeeed4200d9.JPG'),(20046,'/uploads/ccb/fb1/850/8d2495b3c40e7b637d8d.JPG'),(20047,'/uploads/e28/bd1/5bf/709053cfa698b42f6e33.JPG'),(20048,'/uploads/652/2fa/84b/b6e15565f870c658a5ec.JPG'),(20049,'/uploads/b50/0dc/1e2/f498583322d8c63d86cf.JPG'),(20050,'/uploads/a3c/f93/74a/e8d69df017419a0aec45.JPG'),(20051,'/uploads/ec1/d93/14a/07df5026df4b3f969de7.JPG'),(20052,'/uploads/4e5/3c6/d29/0e7268badcd0f4b09354.JPG'),(20053,'/uploads/2fd/7a4/bd2/ae5465f0e496b6246f80.JPG'),(20054,'/uploads/c8c/124/8bf/f8cc16d218769d345bc2.JPG'),(20055,'/uploads/37e/953/1e9/0c06f532d01ce9d09399.JPG'),(20056,'/uploads/c03/124/82f/7dd50a5734a9d205f7cf.JPG'),(20057,'/uploads/142/12b/1de/86cbc4f6522d37fbfb65.JPG'),(20058,'/uploads/229/d1d/231/0ff7be427cb5702b60d3.JPG'),(20067,'/uploads/559/aeb/10e/5b090ff1cb843ca11997.JPG'),(20068,'/uploads/f58/19b/252/4602610eb52fb78de46e.JPG'),(20069,'/uploads/038/1ea/221/54a91412a58abc7e628b.JPG'),(20070,'/uploads/cc5/fd5/e6f/962b0415539e6ac02921.JPG'),(20071,'/uploads/0d2/61b/378/3a43f35fa25c4b07c982.JPG'),(20072,'/uploads/612/0cb/a38/86dfd60c986ae5abc2c0.JPG'),(20073,'/uploads/4a2/6c4/c9a/a2e7f8c829e302519fed.JPG'),(20074,'/uploads/f20/432/791/5d8c9a66a25ba6df7f02.JPG'),(20075,'/uploads/a5a/00f/b73/0e96530706b6b6a2e1e1.JPG'),(20076,'/uploads/c0b/5d6/4a6/1e06cd51e364678e492c.JPG'),(20077,'/uploads/002/d7e/284/7d809f1fed124446c7d4.JPG'),(20078,'/uploads/fa1/2f0/da7/249ff45110b1a0f28d35.JPG'),(20079,'/uploads/0a5/4d2/c5e/f0470229b0649eec0a7f.JPG'),(20080,'/uploads/846/f1a/54f/0ab89b5412f5a5811401.JPG'),(20081,'/uploads/06a/2fc/853/b9c9ba0aebc14266dbb3.JPG'),(20082,'/uploads/b97/bce/48e/94dd5afad0ed25bd91a7.JPG'),(20101,'/uploads/c4e/250/1e6/df7f8674b90a62911c8d.JPG'),(20102,'/uploads/1ae/62a/745/4361b60ee2198f9a76e9.JPG'),(20103,'/uploads/00c/0c3/bae/2b09968b1dbafb5b68dd.JPG'),(20104,'/uploads/fc8/18d/c09/cc7db05623ebec80abec.JPG'),(20105,'/uploads/f0c/158/aa3/8bbd229486dc64999e7b.JPG'),(20106,'/uploads/653/793/2d3/e687cdbbceb6c3667b4b.JPG'),(20107,'/uploads/a2c/c65/540/a3c53bfc0e30bb7463dc.JPG'),(20108,'/uploads/70b/ce9/513/b6b0095e61d25e03c7c2.JPG'),(20109,'/uploads/b9a/37c/518/4fb8697d58c1d6372c7f.JPG'),(20110,'/uploads/9db/8db/e15/0bdca645df24e23b4541.JPG'),(20111,'/uploads/1df/429/dff/3eb4e56f6769d2e053fa.JPG'),(20112,'/uploads/140/2d7/4ee/624167d1ffa268e6b8c9.JPG'),(20113,'/uploads/972/0e0/9a8/b9f2877c04053580c086.JPG'),(20114,'/uploads/19d/8a9/1ce/5ba30db6fe42b893cad3.JPG'),(20115,'/uploads/348/b8a/ca0/129077b6632e1e0bd4ec.JPG'),(20116,'/uploads/c7a/aa9/3ea/3fce8049995eb9e7ec1c.JPG'),(20199,'/uploads/9b2/e87/eae/9f4cf520d60c4a36197a.jpg'),(20200,'/uploads/2ed/83f/074/5c4b31c84474b77853d0.JPG'),(20201,'/uploads/12e/0bf/adc/8deea0642ab0583c0941.JPG'),(20202,'/uploads/e43/fcb/2b1/3f6957a5fa673ca034f2.JPG'),(20203,'/uploads/481/0dc/fd7/f64f2970f891f48425cf.JPG'),(20204,'/uploads/b62/4fc/627/31ce134956515acbfaae.JPG'),(20205,'/uploads/161/674/791/1fb497e02812e032ec4a.JPG'),(20206,'/uploads/de4/cfb/a21/4e15b530f3685e95e8e5.JPG'),(20207,'/uploads/43b/af5/d3a/bf89eeb53d0c4aa48831.JPG'),(20208,'/uploads/97c/72f/71a/2688c50a560f37a5b401.JPG'),(20209,'/uploads/d26/c44/5a2/522c7a8e7578dd8207c2.JPG'),(20210,'/uploads/5ef/60d/098/04aa6af56004ef71720e.JPG'),(20211,'/uploads/3cc/a5c/dc3/4db811527db4b001ccf4.JPG'),(20212,'/uploads/751/5c2/3f0/783ceea5515e5f6fb77b.JPG'),(20213,'/uploads/073/f59/c61/3783f1a26de2279a23e5.JPG'),(20214,'/uploads/7ab/ab2/165/b42cc6a6215b59e6a6c5.JPG'),(20242,'/uploads/170/57a/935/920a66c449f0c62d94fb.JPG'),(20243,'/uploads/793/c00/2a8/2e29db582d9ef3ca2fdb.JPG'),(20244,'/uploads/5f1/382/74a/224259a9aa68c876886e.JPG'),(20245,'/uploads/050/082/4fc/ff1324cbde1ff34477bd.JPG'),(20246,'/uploads/ff0/9b1/505/eb96665208ad516065af.JPG'),(20247,'/uploads/2df/628/7a4/2244b046a77289d0f80b.JPG'),(20248,'/uploads/fe8/cdf/bf4/596c26cc1fb7ade4f88b.JPG'),(20249,'/uploads/acd/21a/aa3/4f81e4ceaa31a873927d.JPG'),(20250,'/uploads/b5e/d41/b13/939495f6849c301684a2.JPG'),(20251,'/uploads/196/895/486/2bb5c4f52406554e5dca.JPG'),(20252,'/uploads/3d3/3c2/1aa/3ba54d36faa646c61640.JPG'),(20253,'/uploads/174/063/d3f/ef9d5288bfbfc44e3ba4.JPG'),(20254,'/uploads/a03/6b4/9f0/6031112c151505f562bd.JPG'),(20255,'/uploads/4af/ccb/ddb/09506646dfd75c113d60.JPG'),(20256,'/uploads/ebf/993/d6c/3bf2277fc0920ef78979.JPG'),(20257,'/uploads/c41/9d6/ff9/6581b7678af455f95765.JPG'),(20258,'/uploads/32f/a13/57e/cc66dfcee4590f6320bb.JPG'),(20259,'/uploads/b49/f95/110/74c3be9322f505cf39d8.JPG'),(20260,'/uploads/088/cef/d9e/7979688d0ecadbc2589b.JPG'),(20261,'/uploads/474/237/621/954727f2549a557e6830.JPG'),(20262,'/uploads/0dd/6ab/a4a/0cd1b4fdbaeb4bd884be.JPG'),(20263,'/uploads/691/5be/01a/5a664bd89208d81cb8fc.JPG'),(20264,'/uploads/96d/b53/c7e/894aca3eec039da74cd2.JPG'),(20265,'/uploads/818/48c/d9c/630d145b41facef4e084.JPG'),(20266,'/uploads/98c/866/b90/0902508f937c0adf6af1.JPG'),(20267,'/uploads/70f/1e4/0b3/9ac1b8a1f1fb21ccde38.JPG'),(20268,'/uploads/87f/a41/1ee/b0a5a3192d7f49c5e11c.JPG'),(20269,'/uploads/507/8d1/ac1/8bfe91f90aa1813485b4.JPG'),(20270,'/uploads/5e2/e8b/4dd/f9886782a1aeb7706fdf.JPG'),(20271,'/uploads/cb3/179/eb6/0a05ef51196fa6419b02.JPG'),(20280,'/uploads/533/64f/4ab/89cc4a3ca08f06017dca.JPG'),(20281,'/uploads/b84/b73/b2e/7e12a223b2718668f8f9.JPG'),(20282,'/uploads/cce/063/550/d7a0fa1e8c4f6be2b2c9.JPG'),(20283,'/uploads/7cd/017/10a/2c2d35187786a09f66cb.JPG'),(20284,'/uploads/d47/61e/b52/c8b7100dbf1397f15622.JPG'),(20285,'/uploads/ff5/8c9/de3/8a70162b7cac37bf9eaf.JPG'),(20286,'/uploads/cf6/088/53f/5ce95d7fad198abb75e1.JPG'),(20287,'/uploads/d5b/453/259/63d7846cceabda5e66ed.JPG'),(20288,'/uploads/024/74d/69b/09e221d509f39690adfe.JPG'),(20289,'/uploads/287/3b6/d68/7635bd2e224f829902f0.JPG'),(20290,'/uploads/84b/12a/930/50125a7ed3dc2dc846f4.JPG'),(20291,'/uploads/c1b/600/8c6/3beb555f6689c35475b6.JPG'),(20292,'/uploads/377/8af/b41/eb522b6b9e9904b4715a.JPG'),(20293,'/uploads/1bb/3f9/d4d/d8fdb65d2182ede09f4b.JPG'),(20294,'/uploads/245/1f5/150/9889aefb4fe2e7474c2f.JPG'),(20295,'/uploads/869/f1c/e27/2cb739c3dcd3f2ef0857.JPG'),(20298,'/uploads/a60/d83/2fe/5379daff2301fa05f2f7.PNG'),(20315,'/uploads/9d6/af6/567/9b51b93b82533ece7b4f.JPG'),(20316,'/uploads/67e/acf/9a3/d4cb4d8597b9d071d75f.JPG'),(20317,'/uploads/ccb/2d6/b20/24ce5ff914493f173956.JPG'),(20318,'/uploads/b2d/73f/8e9/d8d7a9b9ea29bebe0e33.JPG'),(20319,'/uploads/923/887/c5e/7469026016512b6f467b.JPG'),(20320,'/uploads/e36/0b8/5bd/c4b9e5663baeac978e8c.JPG'),(20321,'/uploads/001/e2e/34c/01b94771dd9f6db1a302.JPG'),(20322,'/uploads/585/fdb/af6/a1b71d73f94fbfcf0982.JPG'),(20323,'/uploads/152/5df/16f/c38e7148bff43e9e40a9.JPG'),(20324,'/uploads/5f6/f70/7e6/51bf3f18996ed03c8c63.JPG'),(20325,'/uploads/4aa/90b/062/d84fb154756014533a4e.JPG'),(20326,'/uploads/6d9/63a/96e/5977e27cb516db730e0b.JPG'),(20327,'/uploads/916/95c/7ff/653a8809fb611f5a7c14.JPG'),(20328,'/uploads/728/b94/3f9/979064843f443f6f5823.JPG'),(20329,'/uploads/92a/552/967/8862db971043aa2d41ee.JPG'),(20330,'/uploads/21c/d44/247/26ebd6b00ec1e44632ad.JPG'),(20331,'/uploads/d2c/7b3/90f/6a66251aa61490ade0d8.JPG'),(20332,'/uploads/858/b5f/970/90251ad72462359a02a7.JPG'),(20333,'/uploads/dcd/f55/b33/aa5eb52bb4c3a31f7dde.JPG'),(20334,'/uploads/fd9/363/56f/c46c503bb488b74ad6b4.JPG'),(20335,'/uploads/423/6a5/350/da1d027ed67179b53eb9.JPG'),(20336,'/uploads/fbf/adb/d5a/fdf6c041fa18e74ef0c6.JPG'),(20337,'/uploads/a05/235/923/9fd07703e1f28768aa9b.JPG'),(20338,'/uploads/5a7/001/b20/0dbea1c26617147bab57.JPG'),(20356,'/uploads/675/0ba/fdf/b2f4191f05b773bfff4d.JPG'),(20357,'/uploads/e9d/ffe/30d/35bd5398cdc3e9b8d1d9.JPG'),(20358,'/uploads/6f3/f9c/7d2/ca28dc3eb73ee9240fbe.JPG'),(20359,'/uploads/e74/e7f/5bf/bb7159bab2796fc4a0f5.JPG'),(20360,'/uploads/d6d/be6/6ef/c613980e8af1f6a34a1e.JPG'),(20361,'/uploads/689/4a9/899/c9d88be63dba867ab54c.JPG'),(20362,'/uploads/5c9/7ad/b58/f971f18cc7273e95dced.JPG'),(20363,'/uploads/e43/fbe/691/dd9b1756f905463be4a7.JPG'),(20364,'/uploads/806/c39/917/bf52407ed93449c481d0.JPG'),(20365,'/uploads/f25/1dc/be4/71d6961e6c995d88ca7c.JPG'),(20366,'/uploads/379/1fa/547/b11d734ce08ad644e517.JPG'),(20367,'/uploads/ffb/153/b17/c09424177a1cf8ce65b3.JPG'),(20368,'/uploads/ff6/0b9/ae9/8cb33b61a373c3043415.JPG'),(20369,'/uploads/9c0/c7d/292/7978e856711e310f28fe.JPG'),(20370,'/uploads/255/f61/402/7d6636e80a155d0c77ba.JPG'),(20371,'/uploads/c3c/26f/48c/6addf7b413de721f8c75.JPG'),(20373,'/uploads/fdf/277/2b3/cb42d6dbdf68c6a38a99.JPG'),(20374,'/uploads/eca/c4c/7fb/88cf174e20c38bbe9e2d.JPG'),(20375,'/uploads/3f3/7c7/b2b/35a3487a1eff8b573cfb.JPG'),(20376,'/uploads/af7/f17/7ce/c93c08d01afe189a463b.JPG'),(20377,'/uploads/f19/9db/8ec/ee74e08c416f763e4658.JPG'),(20378,'/uploads/2c9/43b/69c/ede2ea0eb7325b9a5a06.JPG'),(20379,'/uploads/55b/738/0ee/2e94fab186fd6106df0c.JPG'),(20380,'/uploads/64c/e2c/01f/f31dba1ff79fdde1b9eb.JPG'),(20381,'/uploads/c93/400/69c/644599c25f8b4ee49995.JPG'),(20382,'/uploads/656/19c/c42/40400bd309a2b82f0a76.JPG'),(20383,'/uploads/e5b/f79/45c/47780a435b6b46d27054.JPG'),(20384,'/uploads/9ea/b5f/cc0/6f9473719d563637908e.JPG'),(20385,'/uploads/fab/389/e3c/3c114c13aba6b9e9208a.JPG'),(20386,'/uploads/2e7/bc3/32d/8350a9022589a64cb469.JPG'),(20395,'/uploads/acf/f43/70d/dbea643473ee95942f47.JPG'),(20396,'/uploads/cdb/4ba/f6a/d19822fecb0ec2490107.JPG'),(20397,'/uploads/f6c/3d7/c6b/b367b6f91073df06e270.JPG'),(20398,'/uploads/2c1/97d/7b8/28b28c951fbe490c5383.JPG'),(20399,'/uploads/22a/5df/d96/137dec2d20512b5b5ff0.JPG'),(20400,'/uploads/c75/9c3/695/17af31a28aa0b04c3093.JPG'),(20401,'/uploads/ab3/84c/7fc/6b1dcfcfed1e59f1e476.JPG'),(20402,'/uploads/208/cb6/8d1/d6535fe4ff4191513c06.JPG'),(20411,'/uploads/7ac/430/754/238e580e4996fed82714.JPG'),(20412,'/uploads/06c/01a/f81/1dab7fdd14d33e7a3500.JPG'),(20413,'/uploads/325/093/da0/6dc387340d7d2ceb9658.JPG'),(20414,'/uploads/3b0/c72/226/7b83ff84afa8fd9075e9.JPG'),(20415,'/uploads/b9c/7ad/a1e/1d7ad98d9d3b109f9a5a.JPG'),(20416,'/uploads/478/fa9/cae/2a745cab2176b4550d4a.JPG'),(20417,'/uploads/659/103/5ae/09f3f88a2058bd57cea7.JPG'),(20418,'/uploads/e62/a79/dd1/a5287283a3ba8d44a696.JPG'),(20419,'/uploads/8e1/273/f20/f47bc7fbaba366a98ced.JPG'),(20420,'/uploads/ba6/38c/046/c4c776a301223d7ba463.JPG'),(20421,'/uploads/da8/713/d4d/3cb4e8d48b5b60a13ed7.JPG'),(20422,'/uploads/ca2/822/b7e/96431f4b748ec9d7403d.JPG'),(20423,'/uploads/715/aeb/a52/9baa546c22aeae7a85cd.JPG'),(20424,'/uploads/fd9/2f1/b12/462c491798579d0bd475.JPG'),(20425,'/uploads/eae/7bf/815/405dfc13fb8865931bc5.JPG'),(20426,'/uploads/8f4/a8b/92f/64d0e801b2584b1e2dc9.JPG'),(20427,'/uploads/9af/40e/b6b/ecd31da4fa4cd81beecd.JPG'),(20428,'/uploads/4f9/23c/5a5/2bec8e856819396a25e0.JPG'),(20429,'/uploads/f44/d13/519/dd3773cedd6b83debc0b.JPG'),(20430,'/uploads/deb/d11/ba1/ed7dcbc330b0f3b8c48a.JPG'),(20431,'/uploads/fa3/79c/de4/5939a6b9a39988e900ed.JPG'),(20432,'/uploads/335/8d2/06c/6676b1329b824ec3d992.JPG'),(20433,'/uploads/096/0ba/6fb/858ec0350ebd3376747f.JPG'),(20434,'/uploads/546/be4/fec/35dc90afeedc1f766f9e.JPG'),(20435,'/uploads/e99/719/661/175e31aad8258339cb73.JPG'),(20436,'/uploads/596/ebd/4d9/4bab9bc04cd54d33d4fa.JPG'),(20437,'/uploads/873/7aa/341/d781a34868aed4304a7e.JPG'),(20438,'/uploads/574/8d0/74d/cb123fbaeb2768a0e74d.JPG'),(20439,'/uploads/2a4/ec9/a59/1dab9909c537c8d01037.JPG'),(20440,'/uploads/b32/f76/265/c4cb35f7fa3ab4622e3b.JPG'),(20441,'/uploads/f27/381/a7a/76839cb1966b3f6fe1d1.JPG'),(20442,'/uploads/295/194/25e/7a19dbe8621ea68d0ab0.JPG'),(20443,'/uploads/071/831/f9d/b1e1eb17394e747e2c36.JPG'),(20444,'/uploads/bc1/ac2/d7c/79f233919bce1d98a47d.JPG'),(20445,'/uploads/7cd/466/ab0/4f0b8f91d838f9ec4f5d.JPG'),(20446,'/uploads/90b/7c9/ba0/d5b2e296784ecd034f43.JPG'),(20447,'/uploads/878/43e/116/f47974589afe8ccab370.JPG'),(20448,'/uploads/313/19b/46e/1b956853eac534c802e5.JPG'),(20449,'/uploads/6d2/775/8b6/62f36d0d745bd923402e.JPG'),(20450,'/uploads/67e/2b2/a48/39c4e7b932e36497f47a.JPG'),(20451,'/uploads/565/6a7/fda/a5c3a07eb1017a889ca4.JPG'),(20452,'/uploads/f77/fdc/61b/b20fe0baefe296a5356c.JPG'),(20453,'/uploads/f75/802/ac7/a7a7b7051c3e2a72effc.JPG'),(20454,'/uploads/7a8/20d/56b/968853fb7452e29572c6.JPG'),(20455,'/uploads/2f8/913/801/df106809a29693a9cf13.JPG'),(20456,'/uploads/f09/3bd/c21/7205d3c948f6334e97d0.JPG'),(20457,'/uploads/651/f61/76f/6a4634609a19a559ed14.JPG'),(20458,'/uploads/fcc/55d/d34/e0764e95c69b024b443f.JPG'),(20459,'/uploads/d78/02e/b55/c18d2eaec5777b08d5c8.JPG'),(20460,'/uploads/0d8/3a6/4ae/3b326e31993929aab62d.JPG'),(20461,'/uploads/970/41e/da0/cb6f25ded49f48dceb33.JPG'),(20462,'/uploads/7f8/7f6/e18/74d15555640c083e1844.JPG'),(20463,'/uploads/89b/d17/a1e/fa435a7786215fe0a24e.JPG'),(20464,'/uploads/aad/6c7/737/4d274e4e478774e6b1e6.JPG'),(20465,'/uploads/11c/edb/66f/a69d3eee31c19c0f2c49.JPG'),(20466,'/uploads/fea/56c/465/f87d3f8fc8a1fa3c75c7.JPG'),(20467,'/uploads/c41/6c5/5ea/6c4e27b422b8927a8502.JPG'),(20468,'/uploads/b78/fae/d52/dfc2a1e709725882ef6f.JPG'),(20469,'/uploads/6af/a93/6dd/d59e839bf88932e8af31.JPG'),(20470,'/uploads/2b5/24f/f42/4f95e64670083e31dc25.JPG'),(20471,'/uploads/a82/67d/7ff/6489c2b8812c82ceaa85.JPG');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT,
  `info_facebook` varchar(255) DEFAULT NULL,
  `info_instagram` varchar(255) DEFAULT NULL,
  `info_time` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` text,
  `seo_text` text,
  `seo_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES (1,'http://www.facebook.com/ald.carmarket.ukraine','','ПН-ЧТ: 9:00 - 18:00 ПТ: 9:00 - 17:00 СБ-НД: Вихідний','АЛД Автомотів / ТОВ «Перша лізингова компанія» пропонує для продажу легкові автомобілі, які повернулися з лізингу та знаходяться у відмінному технічному стані.','AUDI,ауди,BMW,,FORD,форд,форт,NISSAN,нісан,ніссан,ниссан,нисан,OPEL,опел,опель,PEUGEOT,пежо,RENAULT,рено,SKODA,шкода,SUZUKI,,TOYOTA,тойота,VOLKSWAGEN,vw,фолькс,фольксваген,вольксваген,VOLVO,вольво,волво,ZAZ,,Автомобили, Автомобили б/у, купить автомобиль, купить машину, купить машину Киев, придбати автомобіль, б/у автомобілі, б/в автомобілі, авто после лизинга, авто після лізингу, алд, ALD, первая лизинговая компания','<div>АЛД Автомотів/ТОВ «Перша лізингова компанія» пропонує для продажу легкові автомобілі, які повернулися з лізингу.</div>\r\n<div>Всі автомобілі придбані та обслуговуються у офіційних дилерів. Автомобiлі мають історію обслуговування та справжній пробіг.</div>\r\n<div>Про будь які факти вчинення корупційних дій,  які можуть бути пов’язані із діяльністю АЛД Автомотів/ТОВ «Перша Лізингова Компанія»,  просимо відправляти повідомлення за адресою: ua.compliance@aldautomotive.com \r\nДетальну інформацію можна отримати на сайті www.aldautomotive.ua</div>','ALD автомаркет — Продаж автомобілів після лізингу');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logCar`
--

DROP TABLE IF EXISTS `logCar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logCar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dateOperation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `operation` tinyint(1) DEFAULT NULL,
  `old_body_id` tinyint(4) DEFAULT NULL,
  `old_color_id` tinyint(4) DEFAULT NULL,
  `old_comfort` text,
  `old_date` int(11) DEFAULT NULL,
  `old_description` text,
  `old_engine_id` tinyint(4) DEFAULT NULL,
  `old_engine_capacity` int(11) DEFAULT NULL,
  `old_gear_id` tinyint(4) DEFAULT NULL,
  `old_mileage` int(11) DEFAULT NULL,
  `old_model_id` int(11) DEFAULT NULL,
  `old_name` varchar(255) DEFAULT NULL,
  `old_number` varchar(255) DEFAULT NULL,
  `old_phase_id` tinyint(4) DEFAULT NULL,
  `old_power` int(11) DEFAULT NULL,
  `old_price` int(11) DEFAULT NULL,
  `old_price_old` int(11) DEFAULT NULL,
  `old_producer_id` int(11) DEFAULT NULL,
  `old_safety` text,
  `old_sku` int(11) DEFAULT NULL,
  `old_slide` tinyint(1) DEFAULT NULL,
  `old_status` tinyint(4) DEFAULT NULL,
  `old_transmission_id` tinyint(4) DEFAULT NULL,
  `old_url` varchar(255) DEFAULT NULL,
  `old_year` smallint(4) DEFAULT NULL,
  `new_body_id` tinyint(4) DEFAULT NULL,
  `new_color_id` tinyint(4) DEFAULT NULL,
  `new_comfort` text,
  `new_date` int(11) DEFAULT NULL,
  `new_description` text,
  `new_engine_id` tinyint(4) DEFAULT NULL,
  `new_engine_capacity` int(11) DEFAULT NULL,
  `new_gear_id` tinyint(4) DEFAULT NULL,
  `new_mileage` int(11) DEFAULT NULL,
  `new_model_id` int(11) DEFAULT NULL,
  `new_name` varchar(255) DEFAULT NULL,
  `new_number` varchar(255) DEFAULT NULL,
  `new_phase_id` tinyint(4) DEFAULT NULL,
  `new_power` int(11) DEFAULT NULL,
  `new_price` int(11) DEFAULT NULL,
  `new_price_old` int(11) DEFAULT NULL,
  `new_producer_id` int(11) DEFAULT NULL,
  `new_safety` text,
  `new_sku` int(11) DEFAULT NULL,
  `new_slide` tinyint(1) DEFAULT NULL,
  `new_status` tinyint(4) DEFAULT NULL,
  `new_transmission_id` tinyint(4) DEFAULT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `new_year` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_logCar_date` (`car_id`,`dateOperation`),
  KEY `i_logCar_carid_date` (`car_id`,`dateOperation`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logCar`
--

LOCK TABLES `logCar` WRITE;
/*!40000 ALTER TABLE `logCar` DISABLE KEYS */;
INSERT INTO `logCar` VALUES (1,2826,2,'2018-12-14 11:54:23',0,3,3,'',1543584147,'',1,1498,1,34,156,'RENAULT Sandero 1.5 M/T Stepway','VF15SRAWG53540473',1,0,275000,0,4,'',15404,0,1,2,'2826_RENAULT_Sandero_15_MT_Stepway',2015,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2824,2,'2018-12-14 11:54:25',0,3,3,'',1543584082,'',1,1498,1,34,156,'RENAULT Sandero 1.5 M/T Stepway','VF15SRAWG53540464',1,0,275000,0,4,'',15406,0,1,2,'2824_RENAULT_Sandero_15_MT_Stepway',2015,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,2836,1,'2018-12-17 17:45:44',2,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,1,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,0,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015),(4,2836,1,'2018-12-17 17:55:05',2,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,0,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,1,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015),(5,2836,1,'2018-12-17 17:55:08',2,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,1,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,0,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015),(6,2836,1,'2018-12-17 17:55:33',2,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,0,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015,3,1,'',1544013533,'',1,1568,1,42,149,'KIA CEED 1.6 GSL BUSINESS','U5YHN516AFL207085',3,0,350000,0,20,'',15347,0,1,2,'2836_KIA_CEED_16_GSL_BUSINESS',2015),(7,2810,1,'2018-12-20 13:57:07',2,5,1,'',1543497394,'',2,1396,1,97,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006340',3,0,330000,0,2,'',13151,0,1,2,'2810_SKODA_Octavia_A7_14_TSI_MT',2013,5,1,'',1543497394,'',2,1396,1,97,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006340',3,0,330000,0,2,'',13151,0,0,2,'2810_SKODA_Octavia_A7_14_TSI_MT',2013),(8,2857,1,'2018-12-20 13:57:14',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,330000,350000,2,'',0,0,0,1,'2735_SKODA_Octavia_A7_14_TSI_DSG',2013),(9,2857,1,'2018-12-20 13:57:14',2,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,330000,350000,2,'',0,0,0,1,'2735_SKODA_Octavia_A7_14_TSI_DSG',2013,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,330000,350000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013),(10,2810,1,'2018-12-20 13:57:32',2,5,1,'',1543497394,'',2,1396,1,97,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006340',3,0,330000,0,2,'',13151,0,0,2,'2810_SKODA_Octavia_A7_14_TSI_MT',2013,5,1,'',1543497394,'',2,1396,1,97,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE9EB006340',3,0,330000,0,2,'',13151,0,0,2,'2810_SKODA_Octavia_A7_14_TSI_MT',2013),(11,2857,1,'2018-12-20 13:58:07',2,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,330000,350000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,310000,330000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013),(12,2857,1,'2018-12-20 13:58:58',2,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',3,0,310000,330000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',2,0,310000,330000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013),(13,2757,1,'2018-12-20 13:59:22',2,5,1,'',1540982747,'',2,1395,1,121,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE3EB006186',3,0,295000,315000,2,'',13144,0,0,2,'2757_SKODA_Octavia_A7_14_TSI_DSG',2013,5,1,'',1540982747,'',2,1395,1,121,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE3EB006186',3,0,290000,295000,2,'',13144,0,0,2,'2757_SKODA_Octavia_A7_14_TSI_DSG',2013),(14,2828,5,'2018-12-20 14:27:29',2,2,3,'',1543584415,'',2,1587,1,131,16,'SKODA FABIA 1.6 M/T Combi Elegance','TMBJD45J6DB502359',3,0,245000,0,2,'',12775,0,1,2,'2828_SKODA_FABIA_16_MT_Combi_Elegance',2013,2,3,'',1543584415,'',2,1587,1,131,16,'SKODA FABIA 1.6 M/T Combi Elegance','TMBJD45J6DB502359',3,0,244999,0,2,'',12775,0,1,2,'2828_SKODA_FABIA_16_MT_Combi_Elegance',2013),(15,2857,1,'2018-12-20 14:34:25',2,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',2,0,310000,330000,2,'',0,0,0,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013,5,3,'',1545307034,'',2,1395,1,74,101,'SKODA Octavia A7 1.4 TSI DSG','',2,0,310000,330000,2,'',0,0,1,1,'2857_SKODA_Octavia_A7_14_TSI_DSG',2013),(16,2854,1,'2018-12-20 14:34:33',2,4,1,'',1544538050,'',2,1198,1,195000,15,'SKODA FABIA 1.2  ACTIVE','TMBEH15J6DB502740',3,0,195000,0,2,'',12839,0,0,2,'2854_SKODA_FABIA_12__ACTIVE',2013,4,1,'',1544538050,'',2,1198,1,195000,15,'SKODA FABIA 1.2  ACTIVE','TMBEH15J6DB502740',3,0,195000,0,2,'',12839,0,1,2,'2854_SKODA_FABIA_12__ACTIVE',2013),(17,2854,1,'2018-12-20 14:34:39',2,4,1,'',1544538050,'',2,1198,1,195000,15,'SKODA FABIA 1.2  ACTIVE','TMBEH15J6DB502740',3,0,195000,0,2,'',12839,0,1,2,'2854_SKODA_FABIA_12__ACTIVE',2013,4,1,'',1544538050,'',2,1198,1,195000,15,'SKODA FABIA 1.2  ACTIVE','TMBEH15J6DB502740',1,0,195000,0,2,'',12839,0,1,2,'2854_SKODA_FABIA_12__ACTIVE',2013),(18,2828,1,'2018-12-20 14:35:02',2,2,3,'',1543584415,'',2,1587,1,131,16,'SKODA FABIA 1.6 M/T Combi Elegance','TMBJD45J6DB502359',3,0,244999,0,2,'',12775,0,1,2,'2828_SKODA_FABIA_16_MT_Combi_Elegance',2013,2,3,'',1543584415,'',2,1587,1,131,16,'SKODA FABIA 1.6 M/T Combi Elegance','TMBJD45J6DB502359',3,0,240000,244999,2,'',12775,0,1,2,'2828_SKODA_FABIA_16_MT_Combi_Elegance',2013),(19,2801,1,'2018-12-20 15:31:13',2,5,1,'',1542981207,'',2,1395,1,153,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE1EB006462',3,0,315000,0,2,'',13145,0,1,2,'2801_SKODA_Octavia_A7_14_TSI_MT',2013,5,1,'',1542981207,'',2,1395,1,153,101,'SKODA Octavia A7 1.4 TSI M/T','TMBAC2NE1EB006462',3,0,310000,315000,2,'',13145,0,1,2,'2801_SKODA_Octavia_A7_14_TSI_MT',2013);
/*!40000 ALTER TABLE `logCar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_order` tinyint(2) NOT NULL DEFAULT '0',
  `menu_status` tinyint(1) NOT NULL DEFAULT '1',
  `menu_url` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'ALD Automotive',1,1,'pro-ald-automotive'),(2,'Дилерам',2,1,'dileram'),(4,'Контакти',4,0,'contacts'),(5,'Новини',3,0,'news');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_producer_id` int(11) DEFAULT '0',
  `model_name` varchar(255) DEFAULT NULL,
  `model_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`model_id`),
  KEY `model_producer_id` (`model_producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (1,1,'Fusion',0),(2,1,'Focus',1),(3,2,'Octavia',0),(4,2,'Superb',0),(5,3,'X5',0),(6,4,'Megane ',0),(7,11,'A4',1),(8,11,'A6',1),(9,11,'A8',0),(10,9,'Insignia',0),(11,5,'207',0),(13,4,'Clio Symbol',0),(14,4,'Fluence',1),(15,2,'Fabia',1),(16,2,'Fabia Combi',0),(17,10,'Passat CC',1),(18,10,'Passat',0),(19,10,'Golf',1),(20,10,'Caddy',0),(21,6,'X-Trail',1),(22,4,'Logan',0),(23,4,'Duster',0),(24,7,'XC90',0),(25,8,'Lanos',1),(26,1,'Mondeo',0),(27,12,'SX-4',0),(28,13,'Corolla',1),(29,13,'Auris',0),(30,6,'Qashqai',1),(31,10,'Tiguan',0),(32,14,'DS3',0),(33,15,'Doblo',0),(34,2,'Rapid',0),(35,16,'XV',0),(36,7,'S60',0),(37,10,'Polo',0),(38,9,'Vectra',0),(39,13,'Camry',0),(40,4,' Koleos ',0),(41,4,' Latitude',0),(42,16,'Outback ',0),(43,17,'Elantra',1),(44,5,'4007',0),(46,18,'3',0),(47,4,'Kangoo',0),(48,10,'Jetta',0),(49,10,'Touran',0),(50,13,'Avensis',0),(51,14,'C-Elysee',1),(52,12,'Grand Vitara',0),(53,17,'ix35 ',0),(55,19,' Outlander XL    ',0),(56,13,'Prado',0),(57,2,'Roomster             ',0),(58,19,'Lancer        ',0),(59,14,'C 3',0),(60,19,'L-200              ',1),(61,17,'Santa Fe',0),(62,20,'Sportage',0),(63,12,'Swift                   ',0),(64,9,'Astra',1),(66,1,'C-Max',0),(67,5,'4008',0),(68,13,'Highlander ',0),(69,6,'Tiida',0),(70,1,'Kuga',0),(73,3,'520      ',0),(74,14,'Nemo',0),(75,21,'3302',0),(76,13,'RAV-4',1),(77,22,'CR-V',0),(78,6,'Murano',0),(79,10,'Touareg       ',0),(80,7,'V60',0),(81,17,'Sonata',0),(82,14,'Berlingo',1),(83,5,'3008',0),(84,5,'208',0),(85,14,'C 4',0),(86,14,'DS4                     ',0),(87,18,'6',1),(89,17,'Tucson',0),(90,19,'I-MIEV',0),(92,3,'X1',0),(93,18,'CX-5',0),(95,10,'СС',0),(96,23,'FreeLander              ',1),(97,3,'528i         ',0),(98,19,'Outlander  ',0),(99,6,'Teana',0),(100,15,'Linea        ',0),(101,2,'Octavia A7',1),(102,24,'Captiva ',0),(103,22,'Civic',0),(104,25,'Niva          ',1),(105,24,'Cruze',0),(106,3,'4 Series',0),(107,19,'ASX',0),(108,7,'XC70',0),(109,10,'Multivan',0),(110,13,'Land Cruiser',0),(111,11,'Q5',0),(112,1,'Transit',0),(114,13,'Hilux',0),(115,11,'Q7',0),(116,5,'301',1),(117,14,'DS5',0),(118,20,'Mohave',0),(120,17,'Grandeur',0),(121,27,'ML350',0),(122,3,'320',0),(123,27,'GLE 400',0),(124,28,'QX60',1),(125,1,'Fiesta',0),(126,7,'XC60',1),(127,19,'Pajero',0),(128,5,'508',0),(129,4,'Dokker',0),(130,2,'Spaceback',0),(131,14,'Picasso  ',0),(132,1,'Connect',0),(133,17,'Accent',0),(134,16,'Forester',1),(135,4,'Scenic',0),(136,5,'308',0),(137,6,'Pathfinder',0),(138,17,'i30',0),(139,13,'Yaris',0),(140,11,'A3',0),(141,2,'Yeti',1),(142,3,'325',0),(143,3,'525',0),(144,5,'408',0),(146,13,'Venza',0),(147,12,'Jimny',1),(148,22,'Accord',0),(149,20,'Ceed',1),(150,3,'X3',1),(151,27,'GL400',1),(152,29,'Toledo',0),(153,30,'ES 250',1),(154,15,'500',1),(155,22,'Crosstour',1),(156,4,'Sandero',0);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_date` int(11) DEFAULT '0',
  `news_image_id` int(11) DEFAULT '0',
  `news_text` text,
  `news_name` varchar(255) DEFAULT NULL,
  `news_status` tinyint(1) DEFAULT '1',
  `news_url` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` text,
  `seo_text` text,
  `seo_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (2,1457633859,30,'<table style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">На КПП &quot;Ужгород&quot; на украинско-словацкой границе около 100 автомобилей с иностранными номерами заблокировали движение. Как сообщает УНИАН, поводом стало сообщение Закарпатской таможни ГФС Украины в Закарпатской области от 4 марта, что справка с СТО о ремонте автомобиля больше не является основанием для отсрочки технического пересечения границы, которое обязательно для автомобилей с иностранной регистрацией в соответствии со ст. 95 Таможенного кодекса.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">Напомним, что владельцы авто на иностранных номерах обязаны пересекать границу раз в 5 дней. Но определенной поблажкой стала справка из СТО, что автомобиль не мог покинуть территорию Украины якобы по причине ремонта. Такой &quot;лазейкой&quot; пользовались почти все, кто приобрел авто в соседней стране и ездил на нем в Украине без &quot;растаможки&quot;. А с 4 марта начала действовать новая норма, когда владелец автомобиля должен уведомлять таможню в письменном виде о возникновении такой проблемы - еще до того, как началась &quot;просрочка&quot;&nbsp;<br />\r\n			<br />\r\n			<img src=\"http://www.autoconsulting.com.ua/pictures/_upload/14576046219bxZ_h.jpg\" />&nbsp;<br />\r\n			<br />\r\n			<img src=\"http://www.autoconsulting.com.ua/pictures/_upload/1457604634mPAK_h.jpg\" />&nbsp;<br />\r\n			<br />\r\n			Организаторы акции протеста призвали парламентариев увеличить срок пребывания авто на иностранной регистрации в Украине хотя бы до 10 суток. Но звучали и более радикальные призывы, вплоть до полной отмены таможенных платежей. В качестве альтернативы владельцы &quot;иностранцев&quot; предлагают ввести платный временный ввоз на территорию Украины авто сроком на 1 год. Блокада сделала невозможным проезд через КПП сотен грузовиков, которые следовали в Украину из Словакии и обратно.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n','Владельцы авто на иностранных номерах заблокировали границу в Закарпатье',0,'2_vladelycy_avto_na_inostrannyh_nomerah_zablokirovali_granicu_v_zakarpatye','','','',''),(3,1458204262,422,'<p>Кожен заїзд на заправку передбачає схуднення вашого бюджету на 200-300 і більше гривень, після кількох таких заправок виникає питання: як же можна заощадити паливо? Якщо машина для вас незамінний засіб пересування і відмовитися від неї не можна, скористайтеся нашими порадами, як економити бензин, а, отже, і сімейний бюджет. Звичайно, найрозумніше рішення для економії палива - вибрати економну машину. Але якщо у вас вже є автомобіль і змінювати ви його не плануєте, то скористайтеся корисними рекомендаціями.<br />\r\n<br />\r\n<strong>ТОП-12 способів економії бензину</strong></p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Слідкуйте за справністю машини. Вчасно проходіть ТО, тому що будь-яка несправність автомобіля збільшує витрату палива (іноді вдвічі). Наприклад, зношений двигун збільшує витрату палива до 20%, засмічений повітряний фільтр - до 10%, а невідрегульований розвал-сходження - ще 10%.</p>\r\n	</li>\r\n	<li>\r\n	<p>Позбудьтеся зайвих кілограмів в багажнику авто. Якщо на даху вашого автомобіля встановлений багажник, подумайте, чи дійсно він вам потрібен щодня. Зніміть його і заощадите 10% бензину.</p>\r\n	</li>\r\n	<li>\r\n	<p>Користуйтеся найекономічнішим режимом їзди на максимальній передачі з мінімальною швидкістю. Якщо ваш автомобіль передбачає економічний або зимовий режим - не забудьте його ввімкнути. Плавна їзда з рівномірною швидкістю дозволяє заощадити до 30% бензину.</p>\r\n	</li>\r\n	<li>\r\n	<p>Слідкуйте за тиском у колесах. Зниження тиску на 2 кг/см2 збільшить витрату палива приблизно на 2-3%. Але не перекачуйте колеса, адже тоді збільшиться навантаження на підвіску, а значить, ремонт не змусить себе довго чекати. Крім того, перекачане колесо на дорозі може розірватися з усіма можливими наслідками. Тиск в шинах повинен бути завжди в нормі.</p>\r\n	</li>\r\n	<li>\r\n	<p>Закривайте вікна і вимикайте кондиціонер. Відкриті вікна при русі на швидкості більше 50 км/год призводять до перевитрати палива на 8%, а ввімкнений кондиціонер збільшує витрату бензину на 15%.</p>\r\n	</li>\r\n	<li>\r\n	<p>Глушіть мотор, якщо час очікування більше хвилини. Уникайте пробок, нехай краще ваша дорога буде довшою, але й економія палива наприкінці місяця виявиться істотнішою. Час стояння в пробці за витратою палива еквівалентний проїзду 60 км по трасі зі швидкістю 80-90 км/год. Яндекс-Пробки дозволять вам уникнути пробок і скористатися незавантаженим автомобілями маршрутом.</p>\r\n	</li>\r\n	<li>\r\n	<p>Не прогрівайте двигун 10-15 хвилин. Достатньо, щоб зрушилася з місця стрілка покажчика температури, і вже можна не поспішаючи їхати. Поки ви прогріваєте двигун - бензин безповоротно згорає.</p>\r\n	</li>\r\n	<li>\r\n	<p>Максимально використовуйте кінетичну енергію автомобіля, що їде. Натискайте на педаль газу і гальма якомога рідше, адже після гальмування доведеться розганятися, а це збільшує витрату бензину.</p>\r\n	</li>\r\n	<li>\r\n	<p>Вимикайте в автомобілі все зайве! У першу чергу фари, протитуманки і гучну музику, адже все це збільшує витрату палива на 5-10%.</p>\r\n	</li>\r\n	<li>\r\n	<p>Заливайте якісне паливо. Не варто заливати 92-й бензин замість 95-го. Вартість цих марок відрізняється приблизно на 5%, але 95-й дозволить знизити витрату палива на 10-15%. Крім того, заправляйтеся лише на перевірених заправках, це вбереже від неякісного палива і, отже, від можливого ремонту надалі.</p>\r\n	</li>\r\n	<li>\r\n	<p>Заведіть маршрутний комп&#39;ютер і ввімкніть на ньому режим миттєвої витрати палива. Цей прилад навчить, як заощадити приблизно 10% палива.</p>\r\n	</li>\r\n	<li>\r\n	<p>Допоможіть заощадити бензин сусідові або другу. Запропонуйте йому, якщо вам по дорозі, їздити по черзі - на роботу, в супермаркет, в школу за дітьми, так ви скоротите поїздки на власному автомобілі, а, значить, заощадите паливо.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Всі рекомендації легкі у виконанні, якщо взяти їх за правило. Зверніть увагу на способи економії палива 2, 3 і 5. Якщо ви скористаєтеся ними негайно, то вже завтра заощадите 15-20% бензину.</p>\r\n','Як економити бензин і дизельне паливо',0,'3_yak_ekonomiti_benzin_i_dizelyne_palivo','','','',''),(4,1460106046,0,'','',0,'','','','',''),(5,1460106108,813,'<p><em>31 березня відбулась зустріч ALD Automotive з партнерами в напрямку оптового продажу автомобілів з пробігом.</em></p>\r\n\r\n<p>&nbsp;<em>Серед присутніх були, як діючі партнери, так і потенційні дилери ALD Carmarket.&nbsp; Більшість компаній - це представники найбільших дилерських центрів в Україні, які спеціалізуються саме на торгівлі </em>&nbsp;<em>автомобілів з пробігом</em><em>, адже на сьогоднішній день це є перспективним напрямком в автобізнесі. </em></p>\r\n\r\n<p><em>Зустріч пройшла в комфортній атмосфері затишного кафе готелю Radisson в Києві. Учасники заходу мали можливість познайомитися з діяльністю компанії ALD Automotive ближче , дізнатися про плани на найближчий час, а також обговорити актуальні проблеми та отримати відповіді на ключові питання від керівництва компанії.</em></p>\r\n\r\n<p><em>Наприкінці презентації &nbsp;гостей приємно здивував конкурс від ALD Automotive із заохочувальними призами для активних дилерів, які придбають найбільшу кількість авто за наступні півроку.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n','Презентація ALD carmarket',0,'5_31_bereznya_vidbulasy_zustrich_ALD_Automotive_z_partnerami_v_napryamku_optovogo_prodaghu_avtomobiliv_z_probigom_','','','',''),(6,1467790075,3153,'<p>Президент Петр Порошенко внес изменения в закон о снижении акциза на подержанные авто: только одна машина до конца действия закона, запрет на перепродажи и авто владелец должен завезти его в Украину лично.</p>\r\n\r\n<p>Об этом говорится на&nbsp;<a href=\"http://www.president.gov.ua/news/prezident-pidpisav-zakon-shodo-kupivli-vzhivanih-avto-37551\">официальном сайте президента</a>.</p>\r\n\r\n<p>Как отмечается, подписание состоялось в присутствии соавторов - народных депутатов, представляющих разные фракции, и по результатам нахождения компромисса и достижения четких договоренностей о том, что в проголосованный и подписанный президентом закон будут внесены изменения.</p>\r\n\r\n<p>&quot;Президент и депутаты договорились о том, что изменения в закон, которые в неотложном порядке внесет Глава государства, будут поддержаны всеми фракциями и проголосованы в Верховной Раде в ближайший четверг&quot;, - отмечается в сообщении.</p>\r\n\r\n<p>В сообщении сказано, что &quot;изменения коснутся четкого регламентирования действия закона&quot;, а именно - одна машина до конца действия закона, введение запрета перепродажи и при условии, что владелец авто должен завезти его в Украину лично.</p>\r\n\r\n<p>&quot;Это будет по-честному со стороны Президента, парламента, правительства к людям и по-честному по отношению к государству в необходимости финансирования его расходов в условиях войны, которая не закончена&quot;, - сказал Порошенко.</p>\r\n\r\n<p><a href=\"http://www.epravda.com.ua/rus/news/2016/07/6/598217/\">Экономическая правда</a></p>\r\n','Порошенко изменил закон о снижении акциза на подержанные авто',0,'6_poroshenko_izmenil_zakon_o_snighenii_akciza_na_poderghannye_avto','','','',''),(7,1476954337,5250,'<p>&nbsp;</p>\r\n\r\n<p><em>Наприкінці вересня у затишному ресторані &nbsp;&laquo;Тераса&raquo;, що знаходиться в історичному центрі Києва, відбулась чергова зустріч ALD Automotive та партнерів ALD Carmarket.</em></p>\r\n\r\n<p><em>За останні пів року більшість учасників стали надійними партнерами ALD Carmarket, тому вечір проходив в неформальній та дружній атмосфері.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Гості мали змогу дізнатися більше інформації щодо &nbsp;планів ALD Carmarket, перспектив співпраціі та отримати відповіді на питання від менеджменту компанії. Після офіційної частини відбулося &nbsp;урочисте нагородження переможців програми лояльності ALD Automotive. Нагадаємо, що за умовами конкурсу представники трьох найбільш активних компаній &ndash; партнерів з продажу автомобілів ALD Automotive вирушать у мандрівку до сонячного Кіпру.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>3 місце &ndash; ТОВ &laquo;Делкар&raquo;, м. Рівне</em></p>\r\n\r\n<p><em>2 місце &ndash; ТОВ &laquo;Авто і Компанія&raquo;, м. Львів</em></p>\r\n\r\n<p><em>1 місце &ndash; ТОВ &laquo;Кобус&raquo;, м. Львів</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Після офіційної частини та привітань гостей очікували смачні коктейлі від професійних барменів. </em></p>\r\n\r\n<p><em>ALD Automotive </em><em>і надалі планує розвивати програму лояльності та влаштовувати подібні заходи для постійних та надійних партнерів.</em></p>\r\n','Зустріч з дилерами ALD Carmarket',0,'7_zustrich_z_dilerami_ALD_Carmarket','','','','');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_image_1` int(11) DEFAULT '0',
  `page_image_2` int(11) DEFAULT '0',
  `page_image_3` int(11) DEFAULT '0',
  `page_name` varchar(255) DEFAULT NULL,
  `page_text_1` text,
  `page_text_2` text,
  `page_text_3` text,
  `page_text_4` text,
  `page_url` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` text,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_text` text,
  PRIMARY KEY (`page_id`),
  KEY `page_image_1` (`page_image_1`),
  KEY `page_image_2` (`page_image_2`),
  KEY `page_image_3` (`page_image_3`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,439,18,19,'Дилерам','<p><strong>ALD Automotive</strong> орієнтується на оптовий продаж своїх автомобілів та пропонує компаніям &ndash; автомобільним дилерам будувати перспективний та прибутковий бізнес разом.</p>\r\n\r\n<p>Для дилерів передбачений&nbsp;ряд преференцій та додаткових інструментів для подальшого продажу наших авто кінцевому покупцю:</p>\r\n\r\n<p><!--[if !supportLists]-->&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->Спеціальні цінові пропозиції</p>\r\n\r\n<p><!--[if !supportLists]-->&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->Детальні звіти та фотографії від експертної компанії&nbsp;по стану кожного авто</p>\r\n\r\n<p><!--[if !supportLists]-->&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <!--[endif]-->Повна інформація по технічній історії та страхових випадках усіх автомобілів</p>\r\n\r\n<p>Для того, щоб отримати детальну інформацію, як стати партнером <strong>ALD Automotive</strong>&nbsp;з&nbsp;продажу автомобілів, будь ласка, надсилайте запит на електронну адресу відділу продажу <strong>ALD Carmarket&nbsp;</strong>&nbsp;<a href=\"mailto:ucs_ua@aldautomotive.com\">ucs_ua@aldautomotive.com</a></p>\r\n\r\n<p>&nbsp;</p>\r\n','','','','dileram','Дилерам','Дилерам','Дилерам','Дилерам'),(2,657,660,440,'ALD Automotive','<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ALD Automotive Україна</strong> &ndash; підрозділ ALD International, що входить до складу банківської групи <strong>Soci</strong><strong>&eacute;</strong><strong>t</strong><strong>&eacute; </strong><strong>G</strong><strong>&eacute;</strong><strong>n</strong><strong>&eacute;</strong><strong>rale</strong>, та надає послуги&nbsp; лізингу автомобілів з повним сервісним покриттям та управлінням автопарком.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ALD Automotive</strong> є світовим лідером в галузі фінансування транспортних засобів вже понад <strong>50 років</strong> на налічує:</p>\r\n\r\n<ul>\r\n	<li>Автопарк понад <strong>1&nbsp;200&nbsp;000 авто</strong></li>\r\n	<li><strong>5000 співробітників</strong></li>\r\n	<li>Понад <strong>100&nbsp;000 клієнтів</strong></li>\r\n	<li>Пряма присутність в <strong>43&nbsp;країні</strong> світу</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Компанія <strong>ALD Automotive</strong> здійснює діяльність в Україні з 2005 року і надає послуги фінансування та сервісної підтримки автопарків корпоративних клієнтів.&nbsp;<strong>ALD Automotive</strong> &ndash; єдина міжнародна корпорація, що безупинно діє на ринку України та спеціалізується на оперативному лізингу з повним сервісним покриттям, що включає стандартний пакет послуг і додаткове технічне обслуговування/ремонти.&nbsp;Завдяки постійному удосконаленню в сфері обслуговування клієнтів та покращенню якості продукту, компанія міцно закріпила за собою позицію лідера та на сьогодні налічує понад<strong> 200 клієнтів </strong>і близько <strong>4500 автомобілів</strong>.</p>\r\n\r\n<p><strong>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong><strong><a href=\"http://www.aldautomotive.ua/\">http://www.aldautomotive.ua/</a></strong></p>\r\n','<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong>&nbsp;<strong>ALD Automotiv</strong>e пропонує для продажу легкові автомобілі, які повернулися з лізингу.&nbsp;Всі авто, представлені на сайті,&nbsp; є власністю компанії ALD Automotive та мають історію обслуговування, що проводилось згідно з умовами заводу виробника тільки у офіційних дилерів.</p>\r\n\r\n<h3><strong>ALD carmarket</strong> пропонує&nbsp; широкий вибір авто та ряд переваг для своїх клієнтів:</h3>\r\n\r\n<ul>\r\n	<li>Доступна ціна в національній валюті</li>\r\n	<li>Покупець сплачує лише вартість автомобіля та послуги банку, всі податки при продажу авто сплачує компанія ALD Automotive&nbsp; Україна</li>\r\n	<li>Усі ціни на авто вказані з урахуванням ПДВ</li>\r\n	<li>Всі автомобілі придбані та обслуговуються у офіційних дилерів</li>\r\n	<li>Покупець отримує авто в належному технічному стані та з справжнім пробігом</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Для того, щоб залишити запитання стосовно пропозицій, що вас зацікавили або відправити запит на огляд авто в зручний для вас час, будь ласка, заповніть анкету на нашому сайті чи зателефонуйте&nbsp; до відділу продажу ALD carmarket: &nbsp;<strong>+38044-247-69-09</strong></p>\r\n','','','pro-ald-automotive','Хто ми','Хто ми','Хто ми','Хто ми'),(3,0,0,0,'Лізінг та кредит','<p>...</p>\r\n','','','','lizing_ta_kredit','','','','');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase` (
  `phase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`phase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (1,'Проданий'),(2,'В резерві'),(3,'');
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producer`
--

DROP TABLE IF EXISTS `producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `producer_name` varchar(255) DEFAULT NULL,
  `producer_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producer`
--

LOCK TABLES `producer` WRITE;
/*!40000 ALTER TABLE `producer` DISABLE KEYS */;
INSERT INTO `producer` VALUES (1,'Ford',1),(2,'Skoda',1),(3,'BMW',0),(4,'Renault',1),(5,'Peugeot',0),(6,'Nissan',0),(7,'Volvo',0),(8,'Zaz',0),(9,'Opel',0),(10,'Volkswagen',1),(11,'Audi',1),(12,'Suzuki',0),(13,'Toyota',1),(14,'Citroen',0),(15,'Fiat',0),(16,'Subaru',0),(17,'Hyundai',1),(18,'Mazda',0),(19,'MITSUBISHI',1),(20,'KIA',1),(21,'GAZ ',0),(22,'Honda',0),(23,'LAND ROVER',0),(24,'CHEVROLET',0),(25,'VAZ',0),(27,'Mercedes-BENZ',0),(28,'INFINITI',0),(29,'Seat',0),(30,'Lexus',0);
/*!40000 ALTER TABLE `producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_date` int(11) DEFAULT '0',
  `question_email` varchar(255) DEFAULT NULL,
  `question_name` varchar(255) DEFAULT NULL,
  `question_read` int(11) DEFAULT NULL,
  `question_text` text,
  `question_manager` int(11) DEFAULT NULL,
  `question_reply` text,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (3200,1547123165,'nadiia.zelentsova@aldautomotive.com','Nadiia',1547123587,'Моє питання',3,'<p>У відповідь на Вашe питання повідомляємо:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>test</p>\r\n'),(3201,1547123940,'(067) 555 06 38','Тфвшшф',1547124047,'Замовлено зворотній дзвінок щодо 14130 SKODA Octavia A7 1.2 M/T Ambition',3,'<p>У відповідь на Вашe питання повідомляємо:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>dkafnad;kbna;fbn;adfjka;dfjbn;adfjb;adb;adjnalfbk;adfbl;dlfba;la&#39;lfnklbaldfnbalbnbnaobnbn;adlfbnaldfkadfnklbad&#39;lbn&#39;adlb&#39;bn&#39;bn&#39;bn&#39;abn&#39;adlb&#39;bn</p>\r\n');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resize`
--

DROP TABLE IF EXISTS `resize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resize` (
  `resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `resize_height` smallint(6) DEFAULT '0',
  `resize_image_id` int(11) DEFAULT '0',
  `resize_url` varchar(255) DEFAULT NULL,
  `resize_width` smallint(6) DEFAULT '0',
  PRIMARY KEY (`resize_id`),
  KEY `resize_image_id` (`resize_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73524 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resize`
--

LOCK TABLES `resize` WRITE;
/*!40000 ALTER TABLE `resize` DISABLE KEYS */;
INSERT INTO `resize` VALUES (97,210,30,'/uploads/776/611/878/2095b63a39fc885f4b60.jpg',280),(1018,210,422,'/uploads/6e2/8fb/592/63216c2f92353e7f3679.jpg',280),(1457,150,475,'/uploads/9ab/a9d/ea0/96abe8f80e91bed2cc8c.jpg',200),(1458,296,475,'/uploads/849/68b/15a/7ec1982ce3e1c3910e57.jpg',820),(2738,210,813,'/uploads/ee8/c8b/3f0/c12667a937129660278c.jpg',280),(6335,150,1923,'/uploads/88e/6ef/6c3/bcc2ad2e8c2ae74bda39.jpg',200),(6336,296,1923,'/uploads/f35/80f/531/c659b655449d502349e1.jpg',820),(6415,150,1975,'/uploads/01d/194/d2f/c67618c8b1a1f73f3820.jpg',200),(6416,296,1975,'/uploads/ced/7a3/7b3/3e5915fae2ae4fa3a8b5.jpg',820),(10536,210,3153,'/uploads/b02/56d/ff0/c826426a7d8eaa07fea5.jpg',280),(11864,210,792,'/uploads/2f1/5fa/924/5c8a0d9f144f221a09c5.jpg',280),(11865,68,795,'/uploads/989/d6f/5bf/30dd4cc3a32803474982.jpg',90),(12016,150,3568,'/uploads/09b/f25/560/362fe33a0d99b7d29c4f.jpg',200),(12017,296,3568,'/uploads/547/cee/d80/4e4ef4112dcb40978b08.jpg',820),(12043,150,3569,'/uploads/535/e61/438/9dd94cefef188408bf1a.jpg',200),(12044,296,3569,'/uploads/be3/108/d60/ddf7c48a8b4190951ba7.jpg',820),(14020,150,4192,'/uploads/c7c/2e2/0e6/5f9033afe19668b368b1.jpg',200),(14021,296,4192,'/uploads/113/5a6/f0f/2a1bc4561683d346fd70.jpg',820),(14778,150,4388,'/uploads/02b/129/fb9/9e8392388923a3a487d0.jpg',200),(14779,296,4388,'/uploads/79b/fc9/478/04bb8be39ad8031081f8.jpg',820),(17734,210,5250,'/uploads/b5f/2e6/dd7/2243487203281345ae75.jpg',280),(17735,150,5251,'/uploads/a4a/8e6/d3d/80d8d5aa734726e67789.jpg',200),(17736,296,5251,'/uploads/664/325/661/4827579cc9f5caedf72d.jpg',820),(23367,150,6852,'/uploads/732/e57/4ff/e698b17e4caa7a179663.jpg',200),(23368,296,6852,'/uploads/e36/8f5/c9d/5d57d6745fc81e7e3dc9.jpg',820),(23598,150,6936,'/uploads/026/d6c/a09/7f3935bb5b66e0f71d10.jpg',200),(23599,296,6936,'/uploads/7f7/ef0/052/0010c1e4f107c4f7e54a.jpg',820),(32041,150,9237,'/uploads/834/9eb/484/593a89990835b9b6e9ab.jpg',200),(32042,296,9237,'/uploads/077/d28/07e/b0f678ad26e72d438107.jpg',820),(56284,150,15855,'/uploads/42c/df0/c3b/79f978c4611d8c60b4e8.jpg',200),(57101,296,15855,'/uploads/f06/d45/bba/b817b014551eb92ac7eb.jpg',820),(60633,405,16992,'/uploads/fb1/ad6/2c8/eb2010f50123b1242f08.jpg',540),(61015,210,15311,'/uploads/69a/87a/3a0/17b4b59f4010617e4d55.jpg',280),(66133,150,18469,'/uploads/463/1d0/ae4/7283152564c8a907ada3.jpg',200),(66134,296,18469,'/uploads/d08/672/20b/1203785d333aa912ea26.jpg',820),(66135,150,18470,'/uploads/caf/7f2/562/36575e09613c7008a687.jpg',200),(66136,296,18470,'/uploads/ea2/d28/a50/efd40b16fbcf46ba16e9.jpg',820),(66137,150,18471,'/uploads/32d/48d/698/92760322b341efe74bad.jpg',200),(66138,296,18471,'/uploads/3b6/345/6c6/7c110021ea3b66d40a56.jpg',820),(70880,135,19773,'/uploads/3e4/9b6/6fe/fb30856df9a075eebb31.jpg',180),(70881,135,19774,'/uploads/aee/d55/d4b/77eb2e076676a2d8cd36.jpg',180),(70882,135,19775,'/uploads/8a8/b5d/92b/6abe3b26a129253ec134.jpg',180),(70883,135,19776,'/uploads/99d/dc2/1b3/a073d960bdfbf95e0b58.jpg',180),(70884,135,19777,'/uploads/cb4/635/33d/0e62034e0b6654235646.jpg',180),(70885,135,19778,'/uploads/298/d14/24d/5642755fdbdf988901df.jpg',180),(70886,135,19779,'/uploads/a8e/182/dc4/d467895a1a6faa1180ba.jpg',180),(70887,135,19780,'/uploads/d8e/ff1/e6b/6999d333c12b658b6210.jpg',180),(70888,210,19773,'/uploads/b8c/4b5/df9/f8744dbed831cb3b9cbc.jpg',280),(70889,68,19774,'/uploads/c85/17b/bfc/febbb85af379d211e528.jpg',90),(70890,68,19775,'/uploads/dfa/eac/067/c96a03bcdfa5e4b22381.jpg',90),(70891,68,19776,'/uploads/316/022/5b0/835b22fad5296b1a5035.jpg',90),(70892,210,19777,'/uploads/c16/5ff/be3/e623090c4160dec92e55.jpg',280),(70893,150,19777,'/uploads/4f4/d2a/8bd/758066512e26786ea09b.jpg',200),(70993,405,19777,'/uploads/c1d/676/dfb/a59f439ddcf696879c61.jpg',540),(70994,132,19776,'/uploads/d0b/a3c/bde/6a0c73032b6b3a141a2d.jpg',176),(70995,132,19775,'/uploads/a93/30a/08d/63a81dfe2b54b4145b24.jpg',176),(70996,132,19774,'/uploads/589/3f1/6a2/6ca394fcb74e6fb52c5a.jpg',176),(70997,132,19780,'/uploads/3a5/cda/578/e4b8a99d206dded14278.jpg',176),(70998,132,19778,'/uploads/d93/6f3/6f2/21a1b16a24976ba2eb2c.jpg',176),(70999,132,19779,'/uploads/0e0/005/060/fbad1270dc2b2e14aaa0.jpg',176),(71000,132,19773,'/uploads/4e5/f25/b82/b958ee954d633171b2e6.jpg',176),(71001,405,19776,'/uploads/5f5/750/b2e/be52fd48bd4fcf468f18.jpg',540),(71002,405,19775,'/uploads/a3e/c92/45b/d5b2811ab2064ea2df4a.jpg',540),(71003,405,19774,'/uploads/45c/991/a88/35bfcbe43d1219e4d7e4.jpg',540),(71004,405,19780,'/uploads/a7b/871/20c/727fad8cb3d04e6fd2cb.jpg',540),(71005,405,19778,'/uploads/903/50c/41e/dc3f28f4ee5ff28ed73f.jpg',540),(71006,405,19779,'/uploads/3b5/b06/dc1/72925c843276fd3807ad.jpg',540),(71007,405,19773,'/uploads/b21/b0d/812/a9aeca3c8e9012e2d9a6.jpg',540),(71050,135,19811,'/uploads/c7b/a08/a34/3fb7246567a648717d36.jpg',180),(71051,135,19812,'/uploads/6af/d21/969/398312b26fc4b2fe72e5.jpg',180),(71052,135,19813,'/uploads/b29/f8d/805/b02e66435b36567057b7.jpg',180),(71053,135,19814,'/uploads/0c7/0e7/8f5/08b286cb5cca7d689370.jpg',180),(71054,135,19815,'/uploads/f95/415/44a/c37a1095274741527b70.jpg',180),(71055,135,19816,'/uploads/d97/1c8/11c/9a162507131299d53856.jpg',180),(71056,135,19817,'/uploads/fae/19b/708/dc56012f33c9a10c7ae5.jpg',180),(71057,135,19818,'/uploads/fa1/be3/ad9/5c83b3b37b90caf357d9.jpg',180),(71058,210,19811,'/uploads/826/662/a3e/9930ed91988c57af97f8.jpg',280),(71059,68,19812,'/uploads/0f9/fc2/b78/f166a286306cf066b563.jpg',90),(71060,68,19813,'/uploads/88e/806/30d/10146b4897e03156e6dc.jpg',90),(71061,68,19814,'/uploads/186/8bf/24c/02d30420faabe10b051b.jpg',90),(71062,150,19814,'/uploads/4a1/ffc/786/4ab0d34631675db7db7a.jpg',200),(71063,210,19814,'/uploads/7fd/b4b/cf2/0ec031bb33dcec0c912c.jpg',280),(71064,68,19811,'/uploads/b1a/486/f71/83064c54219bca759cf1.jpg',90),(71093,405,19814,'/uploads/4d8/6f7/5da/63797736e33c96bd4f2f.jpg',540),(71094,132,19813,'/uploads/7ee/021/0f7/b50b13a763f31afd8151.jpg',176),(71095,132,19812,'/uploads/de9/657/6fa/c6ab40401640dbfd32a9.jpg',176),(71096,132,19811,'/uploads/ea5/f24/3d0/241d0e0d82e17e612069.jpg',176),(71097,132,19817,'/uploads/d7d/967/69f/22a542e9ec4f4506f77c.jpg',176),(71098,132,19815,'/uploads/242/8bb/93e/572a6187a0da85d7d096.jpg',176),(71099,132,19816,'/uploads/219/e09/e64/d4572866d8e56da0bf6b.jpg',176),(71100,132,19818,'/uploads/3cc/08c/56d/8f9fb7ade4b25e31b19b.jpg',176),(71101,405,19813,'/uploads/d58/328/552/a70c8ee8f5d37f6dfb64.jpg',540),(71102,405,19812,'/uploads/687/b69/588/439ea27a5461baf1cb86.jpg',540),(71103,405,19811,'/uploads/428/df0/86a/7f482917e107225ce3a4.jpg',540),(71104,405,19817,'/uploads/3e1/063/3a6/215f95365e619f08a9cc.jpg',540),(71105,405,19815,'/uploads/510/cbd/248/013ccfc1466b84f71ee9.jpg',540),(71106,405,19816,'/uploads/6b7/dd9/32a/1fda8f4caa697150d360.jpg',540),(71107,405,19818,'/uploads/57f/305/564/5fffdcfe025697ccbfff.jpg',540),(71136,132,19814,'/uploads/4db/8a0/7bf/28fa242057dfe06f0c5e.jpg',176),(71142,132,19777,'/uploads/326/02a/245/efc6ec8a5f543e1c7755.jpg',176),(71234,135,19857,'/uploads/aa7/10a/25d/9bb6cfd7ddda2aec25ef.jpg',180),(71235,135,19858,'/uploads/90d/e4f/8aa/c97387e95b4e3fe361cb.jpg',180),(71236,135,19859,'/uploads/bba/93f/fb7/10d95d64c17a52a3ec66.jpg',180),(71237,135,19860,'/uploads/2a5/a20/a6f/fec77af734901c0830a0.jpg',180),(71238,135,19861,'/uploads/1a5/163/005/36eac913315a641c5f6c.jpg',180),(71239,135,19862,'/uploads/b95/72c/0e9/2f2ff4fc057715b209c7.jpg',180),(71240,135,19863,'/uploads/328/29c/8f2/3cb288ae472b9f2a2787.jpg',180),(71241,135,19864,'/uploads/0c4/702/d47/5c39d28a4380556ca68d.jpg',180),(71242,210,19857,'/uploads/9da/94d/055/815a1a5c25b09a7fa439.jpg',280),(71243,68,19858,'/uploads/229/cd1/c64/fb71cffb8fa6553f0d56.jpg',90),(71244,68,19859,'/uploads/6cf/8c2/389/4fb6e0a7fde03bab505c.jpg',90),(71245,68,19860,'/uploads/2fc/ea4/c24/8b813a31ad068cedcaef.jpg',90),(71246,150,19863,'/uploads/1c0/4ae/fab/63391cb0fb8382f07cab.jpg',200),(71247,210,19863,'/uploads/352/025/9d1/cb7ab11b1af09c76cad0.jpg',280),(71248,68,19862,'/uploads/d56/9c9/a4e/aa6a10ce69d400e0161d.jpg',90),(71249,68,19861,'/uploads/10a/b88/ada/db98976bed516e081611.jpg',90),(71291,405,19863,'/uploads/b77/6e1/82d/85284ed43b5ec58ce4dd.jpg',540),(71292,132,19862,'/uploads/29c/ed2/61e/530955dce8dddd5cc9f2.jpg',176),(71293,132,19861,'/uploads/f3b/f2d/0cd/9ec600427a85ea14604d.jpg',176),(71294,132,19860,'/uploads/9b6/3af/db2/0525d92fe8dbb6b50aed.jpg',176),(71295,132,19858,'/uploads/2f1/7de/d89/c0227f2b77a69a3ff931.jpg',176),(71296,132,19864,'/uploads/103/ccc/6a8/684dec27112a2e0b999b.jpg',176),(71297,132,19857,'/uploads/75d/73f/ff6/58a70ed4d13ecc46d882.jpg',176),(71298,132,19859,'/uploads/91e/0e6/293/e152d81e0681d54333e7.jpg',176),(71299,405,19862,'/uploads/bc3/329/d77/e6f40ac4c8f38b66cea8.jpg',540),(71300,405,19861,'/uploads/482/bef/db5/2e9b59e88f368c218fc1.jpg',540),(71301,405,19860,'/uploads/253/63a/f2e/dea45a96dd39395629b7.jpg',540),(71302,405,19858,'/uploads/80a/fcd/8d7/3c3b1bd6ca60e3b1a893.jpg',540),(71303,405,19864,'/uploads/29d/f7f/f1e/b8b215c10d49f9260b13.jpg',540),(71304,405,19857,'/uploads/5e2/d69/99f/69aa171e1109463e0de9.jpg',540),(71305,405,19859,'/uploads/223/7d1/bc4/d8300337ec06795f445c.jpg',540),(71534,132,19863,'/uploads/7e0/3cb/723/7283a155d5784b6e6004.jpg',176),(71626,135,19965,'/uploads/e97/90f/eb6/ca59d2bf69dfb910074e.jpg',180),(71627,135,19966,'/uploads/002/e29/78b/3028db8b0c3648f8bdd6.jpg',180),(71628,135,19967,'/uploads/83b/2d4/a29/e139ab9d839a674831d4.jpg',180),(71629,135,19968,'/uploads/7ae/f21/226/42869c809b4c4e41e664.jpg',180),(71630,135,19969,'/uploads/710/bfc/149/c9e5766163558adee199.jpg',180),(71631,135,19970,'/uploads/631/4d4/396/4d69dc465bac844885b3.jpg',180),(71632,135,19971,'/uploads/dec/9e6/5a5/ae390a2fbfb7656f6d3a.jpg',180),(71633,135,19972,'/uploads/b6a/eea/805/914a19c638e8005cbf54.jpg',180),(71634,210,19967,'/uploads/4e1/3d2/b07/08f6c72034b6ae61a16a.jpg',280),(71635,68,19968,'/uploads/2e5/253/cd8/ff33abac741a3e7d6e67.jpg',90),(71636,68,19966,'/uploads/669/a79/3ca/2c85611d18c0096cd6ae.jpg',90),(71637,68,19965,'/uploads/6a1/1b5/cb8/96ae1eed610c15002684.jpg',90),(71638,150,19967,'/uploads/49c/9c9/d6d/4504cfda35bed2b07ce1.jpg',200),(71653,405,19967,'/uploads/704/977/782/700eaa526aeee565913f.jpg',540),(71654,132,19968,'/uploads/537/a7d/785/f5d9e0dd3e3339b5da01.jpg',176),(71655,132,19966,'/uploads/75a/98b/ec9/560ee9f9c9ebeccc9195.jpg',176),(71656,132,19965,'/uploads/5c3/c09/cb8/180ce586d83c9500d2d0.jpg',176),(71657,132,19972,'/uploads/70f/c32/3ed/044f1816b82580dcd74d.jpg',176),(71658,132,19969,'/uploads/cb2/095/d2d/1872ad521f1c23e84daf.jpg',176),(71659,132,19970,'/uploads/ff9/256/dbd/1574972dffbe19180849.jpg',176),(71660,132,19971,'/uploads/9c3/613/0b8/66b9bf5339f22e37b41d.jpg',176),(71661,405,19968,'/uploads/20c/be3/d61/6bc5136c917e5a92536f.jpg',540),(71662,405,19966,'/uploads/ffe/b66/9e8/3c58f0d092d8455d791c.jpg',540),(71663,405,19965,'/uploads/c95/4b8/838/963a7d04a65796f3973f.jpg',540),(71664,405,19972,'/uploads/c31/4a0/58e/5dfb2baeb173ceec4569.jpg',540),(71665,405,19969,'/uploads/ab5/f8d/a0f/3ecd2704f75ee827b738.jpg',540),(71666,405,19970,'/uploads/daa/cfc/1ac/82f94879fef23b8078ae.jpg',540),(71667,405,19971,'/uploads/bcf/887/601/1a4620fff5c875fb5773.jpg',540),(71754,210,19995,'/uploads/88d/fde/617/df0f93d9b6ba2d547168.jpg',280),(71755,68,19996,'/uploads/74f/ee0/46b/fd54f057a4047df02c74.jpg',90),(71756,68,19997,'/uploads/5b2/09e/d50/e609f8d1a7273470bad5.jpg',90),(71757,68,19998,'/uploads/d89/786/a25/05e03049a89ae46294e7.jpg',90),(71758,135,19995,'/uploads/4da/804/4ac/43ca04b0144acb9dd3aa.jpg',180),(71759,135,19996,'/uploads/0f8/c4e/cda/85d46fb20e8baeb0ae3b.jpg',180),(71760,135,19997,'/uploads/ea4/a97/71c/bdad20de505ff6c8abf4.jpg',180),(71761,135,19998,'/uploads/195/3c7/ba8/7195e7f14584e4e847d5.jpg',180),(71762,135,19999,'/uploads/88c/5ad/d3d/bae504c5bbdb72de5a85.jpg',180),(71763,135,20000,'/uploads/0a6/df2/ef4/cf18609af46f4690a28a.jpg',180),(71764,135,20001,'/uploads/ddb/4e3/1ad/7f4996fd9dfd16376383.jpg',180),(71765,135,20002,'/uploads/1c7/a0a/2fa/4a5599b5a7dccd8e5880.jpg',180),(71766,150,19997,'/uploads/8eb/ad0/b89/1ce61fb581a0414f0de6.jpg',200),(71767,210,19997,'/uploads/b3b/769/4e9/06c108492b5f3e58f83f.jpg',280),(71768,68,19995,'/uploads/84e/c57/ab5/172fdd5af64d8d48ea61.jpg',90),(71769,68,20002,'/uploads/32b/1a0/3db/c21f5809622834eafc92.jpg',90),(71770,405,19997,'/uploads/dea/a0a/d3f/3916cfe54d39f9aada72.jpg',540),(71771,132,19996,'/uploads/e1b/1c9/223/87451868b6b0ba7e3be0.jpg',176),(71772,132,19995,'/uploads/47e/c72/11f/0d3fad562a84d0878962.jpg',176),(71773,132,20002,'/uploads/eca/ed6/190/dc471446a2ce56319fb1.jpg',176),(71774,132,20000,'/uploads/b1a/462/4c2/47bf0a2bf669a424dc08.jpg',176),(71775,132,19998,'/uploads/d34/7cb/62e/ccab478968b80084592f.jpg',176),(71776,132,19999,'/uploads/bf9/05f/49a/1c6819e6ffac37e2bb5e.jpg',176),(71777,132,20001,'/uploads/de3/cf1/134/b00be6da702a37e3a499.jpg',176),(71778,405,19996,'/uploads/254/193/0aa/6381a32dc7072ec9c623.jpg',540),(71779,405,19995,'/uploads/58b/fca/457/ad1fe630c4f9ce1f7b11.jpg',540),(71780,405,20002,'/uploads/6b3/1f6/d1b/9b0ef5ed0dd9756f623d.jpg',540),(71781,405,20000,'/uploads/886/e8c/af3/c873fd1fac73f2b06663.jpg',540),(71782,405,19998,'/uploads/6b4/796/c8c/00395abf6f38004b3e53.jpg',540),(71783,405,19999,'/uploads/53f/b9f/b8e/f88464f40d3f592e623d.jpg',540),(71784,405,20001,'/uploads/5e1/db9/dd0/ee41faedaa7757e37d25.jpg',540),(71813,210,20011,'/uploads/fd3/710/fa5/e5128f720050c5473d14.jpg',280),(71814,68,20012,'/uploads/e01/ec6/82f/ac099f5b9991553d1132.jpg',90),(71815,68,20013,'/uploads/4a2/4a4/953/1fbf8dcb54264b1f05b4.jpg',90),(71816,135,20011,'/uploads/7a6/6ab/5c0/4d44ff92e020a9ed425c.jpg',180),(71817,68,20014,'/uploads/33d/695/bfb/73c9f89736715c280388.jpg',90),(71818,135,20012,'/uploads/3ab/cb7/982/456bb5fb663f5a5cf9d3.jpg',180),(71819,135,20013,'/uploads/131/54a/15d/689e006ecd432530d5a0.jpg',180),(71820,135,20014,'/uploads/eed/370/138/bfc98438831ceedf1e29.jpg',180),(71821,135,20015,'/uploads/e0b/708/dde/2d4a69a79d39737c7993.jpg',180),(71822,135,20016,'/uploads/64c/8a0/0db/ead792ba2de93b88294c.jpg',180),(71823,135,20017,'/uploads/4b7/3b8/803/c5ff304b32d49e07ce39.jpg',180),(71824,135,20018,'/uploads/294/7be/079/cfedc2ab60da4df18721.jpg',180),(71825,210,20015,'/uploads/d8b/980/f3d/3ed2797d847e3196b389.jpg',280),(71826,150,20015,'/uploads/51e/e89/fe1/17c72e14fb70a3c620b4.jpg',200),(71827,135,20019,'/uploads/9e7/413/7fa/faf5926032e2d1888963.jpg',180),(71828,135,20020,'/uploads/eef/1a7/b27/12e87d3b261963b542b4.jpg',180),(71829,135,20021,'/uploads/2d0/3dc/10b/afe7b168c463af29361a.jpg',180),(71830,135,20022,'/uploads/489/b44/507/af7c972443aa87f67a05.jpg',180),(71831,135,20023,'/uploads/624/4b4/b70/6dc9b6923efe8fc99058.jpg',180),(71832,135,20024,'/uploads/4ff/1ac/019/d266643e9728e4ba7063.jpg',180),(71833,135,20025,'/uploads/a74/886/c08/14a5276c15b9c2599cec.jpg',180),(71834,135,20026,'/uploads/147/ba2/710/2da2eecbb1d9138efe74.jpg',180),(71835,210,20019,'/uploads/c69/a76/533/e67a835836f82eb15756.jpg',280),(71836,68,20020,'/uploads/380/3f9/6a2/e23234237172debbbcce.jpg',90),(71837,68,20021,'/uploads/6bb/364/3d0/882eecd430230064a3bc.jpg',90),(71838,68,20022,'/uploads/726/1ba/d0b/6e1849f0d8c75ba0a82a.jpg',90),(71839,150,20021,'/uploads/a0a/d57/6c7/a61e94ee5c39810369e5.jpg',200),(71840,210,20021,'/uploads/e40/9f7/90f/62b90f8b36706c9a0c84.jpg',280),(71841,68,20019,'/uploads/477/078/a18/082f6b314a3eea0ec3ff.jpg',90),(71842,135,20027,'/uploads/81c/058/e4d/1f0d00cdd55c315c6b1a.jpg',180),(71843,135,20028,'/uploads/561/176/dba/c1b8f6b8971ab02e3aa8.jpg',180),(71844,135,20029,'/uploads/a14/ae0/0b5/c14283e6285c0724c243.jpg',180),(71845,135,20030,'/uploads/ded/596/926/1fb1d18abb46da5d649f.jpg',180),(71846,135,20031,'/uploads/6cb/55c/fb7/87b375153d069fc4f46c.jpg',180),(71847,135,20032,'/uploads/eab/7ea/a8f/13831f5d3e803c8507ef.jpg',180),(71848,135,20033,'/uploads/426/1bd/04d/31de7738baee4e481ae1.jpg',180),(71849,135,20034,'/uploads/32d/5e2/205/eb0751f59a5f8ed1bf00.jpg',180),(71850,150,20030,'/uploads/8c6/24b/c9f/e64ead2982c18290b621.jpg',200),(71851,210,20030,'/uploads/c02/25e/4a1/ad702aef55d9df6bc881.jpg',280),(71852,68,20031,'/uploads/69f/877/42f/fd7932f5d1f8ec7b1532.jpg',90),(71853,68,20029,'/uploads/302/84d/aa5/ad0e8035ee76057c8309.jpg',90),(71854,68,20028,'/uploads/717/ca4/4f1/df5beac2f05393f75023.jpg',90),(71855,405,20030,'/uploads/d7d/1a0/c7d/9c1a6c8e2ded8366038b.jpg',540),(71856,132,20031,'/uploads/932/a2d/0bb/00b317aaa8e19348a9bc.jpg',176),(71857,132,20029,'/uploads/fe4/0e5/4fa/824f6595304a1c53e99c.jpg',176),(71858,132,20028,'/uploads/f33/f14/ddd/0e33ba9be88750a8a171.jpg',176),(71859,132,20034,'/uploads/271/adb/727/73de311222787cd824d4.jpg',176),(71860,132,20032,'/uploads/a19/f11/f2f/de4aec56e382ebffb01d.jpg',176),(71861,132,20033,'/uploads/f08/1b6/99a/e2eff9583cc1da879aa2.jpg',176),(71862,132,20027,'/uploads/0a6/2f8/c38/e0a6108724592e5aff01.jpg',176),(71863,405,20031,'/uploads/df0/8b4/181/828d0330549f778fed82.jpg',540),(71864,405,20029,'/uploads/111/05a/372/fed6a1fa7a8245275ba0.jpg',540),(71865,405,20028,'/uploads/21e/c37/73d/3686d862f4b31f11b3f1.jpg',540),(71866,405,20034,'/uploads/c37/7d1/e7d/282350e53eaf2f924230.jpg',540),(71867,405,20032,'/uploads/139/d0c/e25/a208d3ec11598a332e9f.jpg',540),(71868,405,20033,'/uploads/3c6/546/18b/3c29a32fda1c0a0181c1.jpg',540),(71869,405,20027,'/uploads/f8c/6ee/098/3892006195e398b19ba0.jpg',540),(71870,405,20015,'/uploads/942/159/0f8/c043ee33920a9ea11d93.jpg',540),(71871,132,20014,'/uploads/9c2/afd/5ca/9ab1267cbcbee37b7d9f.jpg',176),(71872,132,20013,'/uploads/90d/bd3/fdb/faa099535f1d08008b8c.jpg',176),(71873,132,20012,'/uploads/370/e83/88b/2bffb80784e4821d41e0.jpg',176),(71874,132,20018,'/uploads/bee/100/331/ed477df88267550cc074.jpg',176),(71875,132,20016,'/uploads/ed5/faa/2a9/6ce74d98c003f31550be.jpg',176),(71876,132,20017,'/uploads/0f0/9bd/11a/033474cbb9396647232a.jpg',176),(71877,132,20011,'/uploads/7a3/6f2/7df/f1288db4b7b0c24a454f.jpg',176),(71878,405,20014,'/uploads/2b9/a84/7c9/b8138bf99e043ff4f6b0.jpg',540),(71879,405,20013,'/uploads/ec5/c76/d02/0df2b0018d437f22e9b6.jpg',540),(71880,405,20012,'/uploads/eb0/306/8bf/fa9bbd40e404274141ac.jpg',540),(71881,405,20018,'/uploads/97b/742/2da/0f5234a29f5af7ee8c5b.jpg',540),(71882,405,20016,'/uploads/0fe/681/a42/0d62448595b3e07ba8b6.jpg',540),(71883,405,20017,'/uploads/fb0/5e5/d22/ee8cf7c687d849ec8ac5.jpg',540),(71884,405,20011,'/uploads/f0b/507/d57/1fa1fdea43bc9d10f364.jpg',540),(71885,405,20021,'/uploads/df3/f79/4f1/0061380a41a94e9694bb.jpg',540),(71886,132,20022,'/uploads/45d/ad3/a8c/d0a479efa0fe7c73959b.jpg',176),(71887,132,20020,'/uploads/aff/a9e/81f/6970153c46583ed7cb66.jpg',176),(71888,132,20019,'/uploads/82d/8e7/6b8/0e4a21386ff1b16070d2.jpg',176),(71889,132,20025,'/uploads/473/a91/ece/fcaf9ccca57646807708.jpg',176),(71890,132,20023,'/uploads/571/60b/d95/df62b385089c408af087.jpg',176),(71891,132,20024,'/uploads/0d0/5ec/840/5eb0b4a99863a29e5ad4.jpg',176),(71892,132,20026,'/uploads/068/126/847/32356709654520cfac55.jpg',176),(71893,405,20022,'/uploads/237/6f2/90a/fda478ce69ce7773c52d.jpg',540),(71894,405,20020,'/uploads/f70/2e0/c7a/f8db469f850ecd31fc77.jpg',540),(71895,405,20019,'/uploads/33d/30f/9da/ea2189f052f8dc790f8a.jpg',540),(71896,405,20025,'/uploads/d97/5e3/4e4/be9f5979e1dc8849c34a.jpg',540),(71897,405,20023,'/uploads/e4d/a39/ec6/9a2740beda08911ea5f6.jpg',540),(71898,405,20024,'/uploads/f8d/796/aa7/ce1328d053148d68947c.jpg',540),(71899,405,20026,'/uploads/6d3/69a/c25/2a4bb8c4c05b626db79f.jpg',540),(71900,132,19967,'/uploads/b18/7be/f14/4392681e35f48fbc16c2.jpg',176),(71901,132,20030,'/uploads/c16/e07/910/f976c54acc1a92d6c762.jpg',176),(71930,132,20021,'/uploads/a36/404/080/18b2e6d3e001df2f3c82.jpg',176),(71933,132,19997,'/uploads/34d/cbe/9a7/470f3ddce833fcad0c2b.jpg',176),(71935,132,20015,'/uploads/9a1/95e/dd5/8d7f783749abcbee0eb1.jpg',176),(71936,135,20043,'/uploads/6b1/2f4/0d1/58ca772c5ef076a2bddb.jpg',180),(71937,135,20044,'/uploads/12b/78d/ab8/17d9df1c678feb952b3b.jpg',180),(71938,135,20045,'/uploads/b45/3a2/93b/9fd77f021de985d95feb.jpg',180),(71939,135,20046,'/uploads/113/4d2/c50/c6cde87fbc5f53d08f00.jpg',180),(71940,135,20047,'/uploads/e59/ff7/c25/759ece629b9af1ead8bb.jpg',180),(71941,135,20048,'/uploads/0a9/560/a94/9a61ce500c269479102a.jpg',180),(71942,135,20049,'/uploads/b20/52f/4b6/830813606fb3affca53e.jpg',180),(71943,135,20050,'/uploads/987/c46/2a9/699611f436402ba31313.jpg',180),(71944,150,20043,'/uploads/109/af2/ded/504b1b0e069f3415dede.jpg',200),(71945,210,20043,'/uploads/910/295/cac/5a4bc1fdc9e2df7a25d6.jpg',280),(71946,68,20044,'/uploads/371/292/f1f/032edeb8b078e541366b.jpg',90),(71947,68,20045,'/uploads/d9f/e4d/6f3/e838aaac604bb900ecf8.jpg',90),(71948,68,20046,'/uploads/113/b73/776/1453cabfa9a47ba3c6b7.jpg',90),(71949,135,20051,'/uploads/df5/097/9ab/b9ba77c410a844ae4817.jpg',180),(71950,135,20052,'/uploads/298/3bc/fa6/35cd29ef77631f44eb1f.jpg',180),(71951,135,20053,'/uploads/186/f19/045/b868a29f6b3aaf3119ff.jpg',180),(71952,135,20054,'/uploads/ef5/a3b/5df/a072d8fbd5182d662d70.jpg',180),(71953,135,20055,'/uploads/7b1/020/76f/c0e3c279cf4d6e64c408.jpg',180),(71954,135,20056,'/uploads/db0/f98/9d6/302264929b2c4f8fdea0.jpg',180),(71955,135,20057,'/uploads/3f2/031/f2d/ce3ea663ed28ca1b2c92.jpg',180),(71956,135,20058,'/uploads/301/b25/296/5aaa4e879d4b5336c4fa.jpg',180),(71957,210,20051,'/uploads/d22/0ca/0c6/eca859dbbbb9ff0441f0.jpg',280),(71958,68,20052,'/uploads/bfe/a16/aef/d8e83840bf7329723166.jpg',90),(71959,68,20053,'/uploads/613/4f2/cf8/9ba41c34e3146a019501.jpg',90),(71960,68,20054,'/uploads/45b/391/f7f/eae5e6ede420e8f043da.jpg',90),(71961,150,20051,'/uploads/406/472/0cc/1293344488921e6e2184.jpg',200),(71990,135,20067,'/uploads/8a7/4ed/601/42a5b9a0e5d44b133ddc.jpg',180),(71991,135,20068,'/uploads/792/ee0/9d8/221867ca2fcfe34de3ea.jpg',180),(71992,135,20069,'/uploads/a61/087/5eb/3dd720ff35613d662ef1.jpg',180),(71993,135,20070,'/uploads/f42/b61/e7e/4067bce38dd105939b4b.jpg',180),(71994,135,20071,'/uploads/2f0/c1f/c66/97d3d47a3a3298da552f.jpg',180),(71995,135,20072,'/uploads/6bd/7e4/3e8/84439ca15b7e60e1552b.jpg',180),(71996,135,20073,'/uploads/d42/1e5/1e5/bc6fdfa1492b9f426193.jpg',180),(71997,135,20074,'/uploads/049/746/b8d/f26e9b16331f5e5fd32c.jpg',180),(71998,210,20067,'/uploads/28b/416/9d3/8480438a9f90d40f80ce.jpg',280),(71999,68,20068,'/uploads/9f9/350/47c/ea1c44eb64a9c9bb5c7e.jpg',90),(72000,68,20069,'/uploads/515/aea/179/6ad5024cbbf85d1b5e05.jpg',90),(72001,68,20070,'/uploads/d24/01d/2d1/f480eb72b3d5f76b1f1a.jpg',90),(72002,150,20067,'/uploads/525/884/f3c/2e56b4fd430796ef0a73.jpg',200),(72003,135,20075,'/uploads/e1b/17a/517/5f387a6a506b85b471c5.jpg',180),(72004,135,20076,'/uploads/06b/f45/948/5ad9c7c9a9649e378da9.jpg',180),(72005,135,20077,'/uploads/e96/440/910/f891265331030a05d7a1.jpg',180),(72006,135,20078,'/uploads/841/40f/7d7/d54f33d8799026c9a889.jpg',180),(72007,135,20079,'/uploads/a05/ffa/767/46f90c4dc28747f9688b.jpg',180),(72008,135,20080,'/uploads/a05/930/5b9/04db59e1dccf2d5c7fc7.jpg',180),(72009,135,20081,'/uploads/5be/4f8/3d5/22a640ae9957e25b9838.jpg',180),(72010,135,20082,'/uploads/faf/659/9d3/b7f0e4a08d84284c99e9.jpg',180),(72011,150,20075,'/uploads/3d5/6a0/450/1247fce5adbc5ef20c5f.jpg',200),(72012,210,20075,'/uploads/e99/0de/bde/4602e0025795d608b3a7.jpg',280),(72013,68,20076,'/uploads/a34/a03/498/647a462d57ab956b8af4.jpg',90),(72014,68,20077,'/uploads/f9c/178/cf1/c2da3ecd3d9a07a1f2e0.jpg',90),(72015,68,20078,'/uploads/2a2/568/69e/988adc621a8ae27bfe8d.jpg',90),(72055,405,20075,'/uploads/17e/6ca/0cd/74d5a898394e593f9a71.jpg',540),(72056,132,20076,'/uploads/a08/d39/53d/ccd8fa0b05902eea3011.jpg',176),(72057,132,20077,'/uploads/f03/655/faa/8232d516b30005de55f0.jpg',176),(72058,132,20078,'/uploads/b4d/654/67d/ba301ed95d063ed9be8d.jpg',176),(72059,132,20079,'/uploads/eca/d67/d05/1b8858da6ef200a637b0.jpg',176),(72060,132,20080,'/uploads/881/994/324/cd324c12c200de22d8fb.jpg',176),(72061,132,20081,'/uploads/37c/b5a/442/9f0b880eec6c9c1ce0c2.jpg',176),(72062,132,20082,'/uploads/f4b/199/496/9d3f5f93e3cc9a587815.jpg',176),(72063,405,20076,'/uploads/a40/672/f6b/85fc2c1108fa2a175d77.jpg',540),(72064,405,20077,'/uploads/3a8/8bd/349/4fd1fe970a6c23bb9333.jpg',540),(72065,405,20078,'/uploads/b99/775/13f/82d47bbdf9254ccd8604.jpg',540),(72066,405,20079,'/uploads/b42/1bd/d96/3b4629b89661714903dd.jpg',540),(72067,405,20080,'/uploads/889/c6c/b7d/608d4cf197bbd6d7ac9d.jpg',540),(72068,405,20081,'/uploads/901/316/bcc/d88af794fc505808a7c5.jpg',540),(72069,405,20082,'/uploads/4e6/c6a/2bf/f557ef3ac044a0f47ff9.jpg',540),(72070,135,20101,'/uploads/7eb/804/329/685970f7bdcb8412e05f.jpg',180),(72071,135,20102,'/uploads/9d9/f22/366/f5d60cd96caa43fe3ae6.jpg',180),(72072,135,20103,'/uploads/a11/e89/f38/71f2f68eeda092bf7dc0.jpg',180),(72073,135,20104,'/uploads/f50/295/5ca/0ede5bb60b2fba253b38.jpg',180),(72074,135,20105,'/uploads/66f/4a5/b35/695f2b228adef1daba61.jpg',180),(72075,135,20106,'/uploads/c20/56a/e19/a35ee50ccadbcb44eecb.jpg',180),(72076,135,20107,'/uploads/a6a/d86/c79/32a93fc8738eb9182e40.jpg',180),(72077,135,20108,'/uploads/7aa/909/f62/9d76da5f516a8123b80f.jpg',180),(72078,135,20109,'/uploads/9cf/749/0a5/743beb97497ab583f6fd.jpg',180),(72079,150,20101,'/uploads/1f2/4ac/fc3/5b78e8b1612ccf5aba7b.jpg',200),(72080,210,20101,'/uploads/4a5/f5b/cd8/7b58f7a357d30a328a0f.jpg',280),(72081,68,20102,'/uploads/730/c9a/6ef/cca235db627751f677c0.jpg',90),(72082,68,20103,'/uploads/09b/fe5/87c/5ddabfc0a998b91819af.jpg',90),(72083,68,20104,'/uploads/7d8/a9d/30a/c6fd3b834405c445c7da.jpg',90),(72084,405,20101,'/uploads/aa5/52d/95c/286eff83ca41a9194102.jpg',540),(72085,132,20102,'/uploads/81a/5a6/e82/48393ca3d9373f806598.jpg',176),(72086,132,20103,'/uploads/618/142/530/f68a4217bd0553245c4c.jpg',176),(72087,132,20104,'/uploads/8e7/f82/701/13c973c975747c6b2248.jpg',176),(72088,132,20105,'/uploads/f9e/de6/7b8/c0ce7c3ad343a8430f88.jpg',176),(72089,132,20106,'/uploads/77a/03b/f44/83e149ea507645920633.jpg',176),(72090,132,20107,'/uploads/e1d/296/64b/2a06cf151dc846dcf5f9.jpg',176),(72091,132,20108,'/uploads/88f/ff9/106/08ec1738483014dcdfea.jpg',176),(72092,132,20109,'/uploads/0ce/bc6/d4a/b61181d950422fffb9fd.jpg',176),(72093,405,20102,'/uploads/d46/da7/1ed/b3f04c79ae5d44fa7281.jpg',540),(72094,405,20103,'/uploads/d3b/1d7/d2c/c04db96cc7ba4656418f.jpg',540),(72095,405,20104,'/uploads/21c/bcd/49c/603f795976b095d65659.jpg',540),(72096,405,20105,'/uploads/a93/5e8/d1e/941779a41c54c6b1aa75.jpg',540),(72097,405,20106,'/uploads/665/244/a85/6fd3aca96986f2fc275b.jpg',540),(72098,405,20107,'/uploads/459/05a/382/13778a989ed36fe46db5.jpg',540),(72099,405,20108,'/uploads/1ba/439/0f4/845746123bfca2c1d7d6.jpg',540),(72100,405,20109,'/uploads/8cd/e22/779/c67a6f7ca17108bfff99.jpg',540),(72101,135,20110,'/uploads/5b5/064/37c/a5bcc068c28f5c42d5fe.jpg',180),(72102,135,20111,'/uploads/41b/7a8/f5d/b179e781bc28a4259ecc.jpg',180),(72103,135,20112,'/uploads/cdb/18e/232/eb2cfd5be81abd4403c9.jpg',180),(72104,135,20113,'/uploads/ba0/176/da5/2dcee03eba1f1c0329bc.jpg',180),(72105,135,20114,'/uploads/17d/904/522/c6777fa1a581ccc79349.jpg',180),(72106,135,20115,'/uploads/3a0/b1d/2d9/69716ba9dff93664e08c.jpg',180),(72107,135,20116,'/uploads/1f5/09c/e23/d09f290035ee442958d7.jpg',180),(72108,210,20110,'/uploads/e93/3a6/47a/0fc9ff438dd8d1cafc0b.jpg',280),(72109,68,20111,'/uploads/9a1/e9f/0aa/f25f4de8d0f0b089abb7.jpg',90),(72110,68,20112,'/uploads/896/7b1/fe4/64909eeb52c4ffb4e003.jpg',90),(72111,68,20113,'/uploads/30a/50f/635/e6990a5f8235e5669b43.jpg',90),(72112,150,20110,'/uploads/cf4/66f/76d/9cb842134e9f8c8a14a1.jpg',200),(72113,405,20110,'/uploads/fb6/5d3/74c/5c43449e0189591a1f9f.jpg',540),(72114,132,20111,'/uploads/44c/8ee/d50/22a6736f36eaa2e9b773.jpg',176),(72115,132,20112,'/uploads/3b6/17e/468/b83ffc2ed9dfae53c809.jpg',176),(72116,132,20113,'/uploads/394/09b/928/adf041cdb567cf96b41f.jpg',176),(72117,132,20114,'/uploads/568/766/eef/af88ef355e37cb320a65.jpg',176),(72118,132,20115,'/uploads/cbd/af0/104/ba70de4ba85f3db437fe.jpg',176),(72119,132,20116,'/uploads/444/d25/42f/eee9624ff26bd989b3ee.jpg',176),(72120,405,20111,'/uploads/7ca/b0b/450/629151eb6de1d85cea76.jpg',540),(72121,405,20112,'/uploads/37b/1d9/204/951b629b3c1d0d92f5b2.jpg',540),(72122,405,20113,'/uploads/f66/be4/77e/fe44e12eedd6b694e48b.jpg',540),(72123,405,20114,'/uploads/7cb/ba8/671/4028e6ffa7c3f49d065d.jpg',540),(72124,405,20115,'/uploads/315/328/a56/2b396c0af03c6beb8465.jpg',540),(72125,405,20116,'/uploads/81e/4b3/fea/febd738329d77090cfa4.jpg',540),(72268,405,20043,'/uploads/573/1d7/c03/f2f3e0c93a6ed9227c5f.jpg',540),(72269,132,20044,'/uploads/c60/e39/ebc/aa40a88009cda3b77765.jpg',176),(72270,132,20045,'/uploads/c28/5e5/af1/dbd5ef46b08802f8a454.jpg',176),(72271,132,20046,'/uploads/c04/3ae/8a9/8cb8c3007e09c6b58b64.jpg',176),(72272,132,20047,'/uploads/0e8/fd5/f42/9937151ac46c42669b62.jpg',176),(72273,132,20048,'/uploads/505/b28/0ab/74a7ac299e82312d4ba6.jpg',176),(72274,132,20049,'/uploads/f8c/00b/f3b/cd4f98c4693ded7720f5.jpg',176),(72275,132,20050,'/uploads/730/375/f65/09ca7d6f30351ccfbbfc.jpg',176),(72276,405,20044,'/uploads/6ae/e1d/ffe/fd2f3368749372d0c26c.jpg',540),(72277,405,20045,'/uploads/a87/fda/fc3/e7c7d719ab1058e0e09a.jpg',540),(72278,405,20046,'/uploads/e2c/ab5/d2f/ad3a0ed3c97525e1e529.jpg',540),(72279,405,20047,'/uploads/c3c/ee3/a42/8d0e1cdd53d908f618ce.jpg',540),(72280,405,20048,'/uploads/e7a/3ae/1d2/5f1915cc979fa2f7d927.jpg',540),(72281,405,20049,'/uploads/01f/6fe/55f/57ac36c425d48a4cb147.jpg',540),(72282,405,20050,'/uploads/4da/935/b3e/c544efa5aba067e41bfb.jpg',540),(72283,405,20067,'/uploads/10d/bd9/f7a/d77699404de13ebdf122.jpg',540),(72284,132,20068,'/uploads/bcd/ae3/8b2/c4c3ce41f34d4f4a0cba.jpg',176),(72285,132,20069,'/uploads/8ac/db8/c8c/aaf39dd47bd5ef2c73ce.jpg',176),(72286,132,20070,'/uploads/299/e28/a2e/32fc22956fc9a6693f7f.jpg',176),(72287,132,20071,'/uploads/815/706/785/c804ef765e3a742ed5fb.jpg',176),(72288,132,20072,'/uploads/b8a/4f6/e5c/82480bbeaaeb2c641d85.jpg',176),(72289,132,20073,'/uploads/727/b29/ddb/55338a7de9d5294582a0.jpg',176),(72290,132,20074,'/uploads/3a6/fd7/4a3/a29ee5f4740f40d07fd3.jpg',176),(72291,405,20068,'/uploads/24d/335/8e6/0f0439d0b29b4fa0760d.jpg',540),(72292,405,20069,'/uploads/d07/583/f54/680e7a9fa11bfd4c263b.jpg',540),(72293,405,20070,'/uploads/e3d/894/672/d77595e7954d32bb9233.jpg',540),(72294,405,20071,'/uploads/ddc/f4e/516/49bd5ac6e5698fcb3b7f.jpg',540),(72295,405,20072,'/uploads/7b7/341/b3f/376c133af338cf45a292.jpg',540),(72296,405,20073,'/uploads/079/87f/bb2/312ecf5378a8453f1d63.jpg',540),(72297,405,20074,'/uploads/165/590/1a3/8b5dd76ab7a4597d7ea4.jpg',540),(72298,132,20067,'/uploads/623/b98/205/f885b133755bd2cb7baa.jpg',176),(72299,405,20051,'/uploads/d63/32d/678/f3fe1235a8aea96a4ac5.jpg',540),(72300,132,20052,'/uploads/8a7/b56/92e/0b6431a2bab796932abc.jpg',176),(72301,132,20053,'/uploads/bbe/309/40b/6cf1368dfd753b173f13.jpg',176),(72302,132,20054,'/uploads/283/253/e88/091927962085ef4512c1.jpg',176),(72303,132,20055,'/uploads/587/906/929/f859037afa05044ed1ed.jpg',176),(72304,132,20056,'/uploads/cb0/75d/e70/a219f9c99b5d26fd7634.jpg',176),(72305,132,20057,'/uploads/48a/5fe/325/8579ad0bb094073fce99.jpg',176),(72306,132,20058,'/uploads/464/feb/3c3/f80fe8bfc583d6aca278.jpg',176),(72307,405,20052,'/uploads/ecc/719/b07/b49af38e8a2ab867023f.jpg',540),(72308,405,20053,'/uploads/86d/834/90d/5a72bf312e5f2824d43b.jpg',540),(72309,405,20054,'/uploads/a47/0d2/7de/b49e1b6df031894baae9.jpg',540),(72310,405,20055,'/uploads/94c/422/417/27282725f339166abe51.jpg',540),(72311,405,20056,'/uploads/295/b03/33e/d432fdee1afff698fae8.jpg',540),(72312,405,20057,'/uploads/fad/397/292/3de65edf62b8680f20e6.jpg',540),(72313,405,20058,'/uploads/cd6/2d9/9f6/8bfda5c96e9afc03c421.jpg',540),(72334,132,20110,'/uploads/e3d/859/3fd/10998de682a48eadd879.jpg',176),(72337,132,20051,'/uploads/82f/76f/8be/a1726d765c646334927c.jpg',176),(72338,132,20043,'/uploads/062/dc1/525/64c0f9d43320caa5fdb4.jpg',176),(72340,132,20075,'/uploads/68d/ab9/ec9/ac380976cec19ed11159.jpg',176),(72485,150,20199,'/uploads/bb0/d6f/a43/fe2e6334536100c9b03f.jpg',200),(72486,296,20199,'/uploads/2e9/930/cd6/debe69d32c6116c810d2.jpg',820),(72490,135,20200,'/uploads/266/dd8/94b/b784ee809575c3a1e5a6.jpg',180),(72491,135,20201,'/uploads/983/6ed/633/e8b26039d571f17335a1.jpg',180),(72492,135,20202,'/uploads/4e4/880/b5f/9943d17a906d3a064e50.jpg',180),(72493,135,20203,'/uploads/520/406/05e/d1213f58be797b2f3413.jpg',180),(72494,135,20204,'/uploads/52c/c6f/5c6/c7ed190f3ead07ca89dc.jpg',180),(72495,135,20205,'/uploads/b8e/03d/626/b26f63c1c136ed2a4246.jpg',180),(72496,135,20206,'/uploads/54e/440/9a4/1a51ee4cdbaef4084353.jpg',180),(72497,150,20202,'/uploads/45b/b55/970/a73f3fa4540e42b87b63.jpg',200),(72498,210,20202,'/uploads/eb2/2ff/922/5702790b1aa019aca734.jpg',280),(72499,68,20203,'/uploads/7fa/35e/1e0/639236cf3a0d236901df.jpg',90),(72500,68,20201,'/uploads/a46/e0e/b92/e5ed9ecbf5c6c776e7f1.jpg',90),(72501,68,20200,'/uploads/83f/33d/385/6b1b7d571d37fcac3b4c.jpg',90),(72502,135,20207,'/uploads/799/081/dff/9e014f5984c9f3dfed89.jpg',180),(72503,135,20208,'/uploads/648/9d7/c55/cf5631808990831715cf.jpg',180),(72504,135,20209,'/uploads/39c/191/0ea/8590801937656aeb0c6c.jpg',180),(72505,135,20210,'/uploads/a64/d1e/ed5/89fee7426c1d1a808cc2.jpg',180),(72506,135,20211,'/uploads/bf1/d8c/8c8/a9a59741d62f58e3e686.jpg',180),(72507,135,20212,'/uploads/360/ba1/362/d789403df201bac3f758.jpg',180),(72508,135,20213,'/uploads/11c/c8d/7a3/fab21b4887f3214cd714.jpg',180),(72509,135,20214,'/uploads/a38/052/b44/039507c9cb90e5c25240.jpg',180),(72510,210,20207,'/uploads/982/105/9ab/d3089b51a8124cea88a6.jpg',280),(72511,68,20208,'/uploads/15b/84e/afa/df05cf1bd52b173c412a.jpg',90),(72512,68,20209,'/uploads/fc5/27b/157/a4480deb760b704243c6.jpg',90),(72513,68,20210,'/uploads/aeb/adb/c50/dd2017958c4eefca6b7e.jpg',90),(72514,150,20208,'/uploads/bb0/cd1/675/af20414cbace10a54263.jpg',200),(72515,210,20208,'/uploads/7b3/f40/ff8/6417fc2d436270ec7648.jpg',280),(72516,68,20214,'/uploads/8b0/b04/2b2/004e29f16f177903c691.jpg',90),(72517,68,20213,'/uploads/a36/808/17a/dfc5eaa6d82261d91607.jpg',90),(72530,405,20208,'/uploads/b96/9f1/e9c/c11933c77f1b7c9e967c.jpg',540),(72531,132,20209,'/uploads/dbb/2f3/186/38b28e841c4806accd59.jpg',176),(72532,132,20214,'/uploads/ce0/89f/f33/9dca02621acc5703d0e1.jpg',176),(72533,132,20213,'/uploads/da3/b62/438/d835f1da42925e89218a.jpg',176),(72534,132,20212,'/uploads/279/aa1/90a/09e3c172bb19ae42494f.jpg',176),(72535,132,20210,'/uploads/6ff/f6a/335/0900ced324e45398709e.jpg',176),(72536,132,20211,'/uploads/709/05f/dd3/40d90331df481f99ecc3.jpg',176),(72537,132,20207,'/uploads/eaf/619/844/adb70cca60c861f0770c.jpg',176),(72538,405,20209,'/uploads/79d/527/a82/b6aa7bdf0c9ea03ade5c.jpg',540),(72539,405,20214,'/uploads/25f/db5/f61/d7301d7ffe5900eb4e7c.jpg',540),(72540,405,20213,'/uploads/af2/014/788/655f516bfef2a75e036f.jpg',540),(72541,405,20212,'/uploads/5b7/985/760/63ce0cefb1debea4c3bb.jpg',540),(72542,405,20210,'/uploads/f02/e18/673/50bfe06be7266d872a1d.jpg',540),(72543,405,20211,'/uploads/cea/089/8f0/d260c72647fe4aac3838.jpg',540),(72544,405,20207,'/uploads/b30/0ca/b04/d898676697617f9140ce.jpg',540),(72545,405,20202,'/uploads/df1/626/f34/9cfb34e455fe758b9883.jpg',540),(72546,132,20203,'/uploads/b9d/5d9/209/8ba1265ee0670408db47.jpg',176),(72547,132,20201,'/uploads/cb7/497/8ef/055fea4869099c87befb.jpg',176),(72548,132,20200,'/uploads/611/4ba/bff/6b341aee7a18b7e4faf1.jpg',176),(72549,132,20205,'/uploads/60c/987/eca/40746bb8be65659e852d.jpg',176),(72550,132,20206,'/uploads/52d/1b5/a36/adf98cda063db8f82c3c.jpg',176),(72551,132,20204,'/uploads/d54/1cd/140/7d8ad16c3677049afd8b.jpg',176),(72552,405,20203,'/uploads/d7a/d35/67c/a75da5451feb73e20459.jpg',540),(72553,405,20201,'/uploads/d0c/617/d39/0b371a4b8c69d9d04963.jpg',540),(72554,405,20200,'/uploads/9c7/943/d10/4f39597112c810fa7ae8.jpg',540),(72555,405,20205,'/uploads/3c6/421/1ce/27b4d0fbe3c5e2e75d27.jpg',540),(72556,405,20206,'/uploads/20f/a26/7cb/1d7abac811d94cb3addc.jpg',540),(72557,405,20204,'/uploads/c8e/798/4a9/e145a8f82e7c248dc180.jpg',540),(72568,132,20208,'/uploads/6ce/d3a/d73/ed9def5e8314a27000a6.jpg',176),(72569,132,20202,'/uploads/084/cf1/b78/ae6d7edfff16c70d61fc.jpg',176),(72570,132,20101,'/uploads/c11/9d1/33d/a647f81a6a3edae164eb.jpg',176),(72660,135,20242,'/uploads/478/d59/238/f2665ebb7c0e7fc0d0c8.jpg',180),(72661,135,20243,'/uploads/110/831/6d3/bc934b9d7b76a4d0f2e9.jpg',180),(72662,135,20244,'/uploads/891/b66/87f/d2cba9a07012912e389b.jpg',180),(72663,135,20245,'/uploads/6ef/024/15a/b924b7ec681fe55b1847.jpg',180),(72664,135,20246,'/uploads/859/c3e/d85/e2fbe7c33e8a36421193.jpg',180),(72665,135,20247,'/uploads/aa7/ccd/039/72dc56d7551eda82fa8b.jpg',180),(72666,135,20248,'/uploads/2c8/cff/18f/f21fe9d24fe0b7b790be.jpg',180),(72667,135,20249,'/uploads/428/2da/14e/3a5a0e68ea3bf3020340.jpg',180),(72668,150,20243,'/uploads/7c2/b9c/630/77c81c6a85b39f01f71c.jpg',200),(72669,135,20250,'/uploads/299/954/3dd/20627dd3fbc01ee0bdf4.jpg',180),(72670,135,20251,'/uploads/81a/4c9/c6d/37a6d97b28f70763054f.jpg',180),(72671,135,20252,'/uploads/8ca/bf0/836/3d688a8728fc5c16ce2c.jpg',180),(72672,135,20253,'/uploads/c47/6d0/041/c4c151e5f3ac98c05829.jpg',180),(72673,135,20254,'/uploads/54a/95d/8ca/fea96026f9e06ae804a4.jpg',180),(72674,135,20255,'/uploads/90d/364/bff/abc10c3d3e70e61e3472.jpg',180),(72675,135,20256,'/uploads/86e/cd0/a49/0d56b22eb77f2293b315.jpg',180),(72676,150,20253,'/uploads/ca7/e0c/0d1/fa406236324be8d7fb0f.jpg',200),(72677,135,20257,'/uploads/6c9/423/b5f/c3be8a7396befdd4efcf.jpg',180),(72678,135,20258,'/uploads/652/8e4/027/1c705e631a30b755f94e.jpg',180),(72679,135,20259,'/uploads/d50/890/044/318ce823f0082a62e709.jpg',180),(72680,135,20260,'/uploads/113/b2b/a8a/5b5d445e3d5c2d663d0f.jpg',180),(72681,135,20261,'/uploads/487/172/948/1b2cb80f30a5d8b63dfe.jpg',180),(72682,135,20262,'/uploads/d43/60a/f10/6f09f66a034bc474270e.jpg',180),(72683,135,20263,'/uploads/99a/dc8/41a/f689bb12eef74c79ba15.jpg',180),(72684,210,20257,'/uploads/37f/612/abc/90f329865dd682e0121a.jpg',280),(72685,68,20258,'/uploads/e90/0ef/647/e817cf5496c0612ebebe.jpg',90),(72686,68,20259,'/uploads/64a/c9a/651/3537cc9a4bd0f18cfae4.jpg',90),(72687,68,20260,'/uploads/62b/8d4/4b2/13130b50a94d6aefb627.jpg',90),(72688,210,20243,'/uploads/f69/b8f/3b2/660891409f0e72d462a7.jpg',280),(72689,68,20244,'/uploads/0e0/b26/ec3/f65701955faaf3992363.jpg',90),(72690,68,20242,'/uploads/609/a62/5ab/155e8aaa9fcf9dab34c1.jpg',90),(72691,68,20249,'/uploads/bad/ff0/f5a/fadd5086ae425f9a2f00.jpg',90),(72692,210,20253,'/uploads/d2b/688/e98/2e49af3102c392cd2db3.jpg',280),(72693,68,20254,'/uploads/fc2/24f/4b5/3469e13ccfd81b86d20f.jpg',90),(72694,68,20256,'/uploads/770/03b/b8a/ca8c622805e6c7f50aa9.jpg',90),(72695,68,20255,'/uploads/b43/321/a78/68fac3046d9574d5bab1.jpg',90),(72696,405,20257,'/uploads/3c1/bb4/b48/b5f3e3568b27f198a5f4.jpg',540),(72697,132,20258,'/uploads/b87/e08/9eb/1a19f89a6a8ed0e87e61.jpg',176),(72698,132,20259,'/uploads/ebf/6b2/cff/b3fab24600eae9e1dea5.jpg',176),(72699,132,20260,'/uploads/984/06a/696/46869f2bfdcee4f8edc6.jpg',176),(72700,132,20261,'/uploads/1f0/9fc/716/8bee2b0509bde497ff9d.jpg',176),(72701,132,20262,'/uploads/630/ef9/0cd/5b71da3c5e1f0314df06.jpg',176),(72702,132,20263,'/uploads/13e/f3f/f40/958fbf079cb89e38791a.jpg',176),(72703,405,20258,'/uploads/948/2f3/d4d/a50e4b90b95c06db4812.jpg',540),(72704,405,20259,'/uploads/851/79b/b8f/d879e58f5ff5d8b6affa.jpg',540),(72705,405,20260,'/uploads/616/ed0/a23/d7833b920e7f3e3e2550.jpg',540),(72706,405,20261,'/uploads/a52/3df/50b/553cff7c8a8a4ade3426.jpg',540),(72707,405,20262,'/uploads/73c/3ee/f8e/2fc06b42b84b43a6cb15.jpg',540),(72708,405,20263,'/uploads/9e0/856/9f2/ec02ca57d0c861266cbf.jpg',540),(72709,150,20260,'/uploads/5f5/7ae/562/d960ef1e07427b0d0c83.jpg',200),(72710,135,20264,'/uploads/0cc/ef6/386/ced150e104c80a81d4c9.jpg',180),(72711,135,20265,'/uploads/425/f0c/894/e336ef335ade312ae6fd.jpg',180),(72712,135,20266,'/uploads/0d4/13f/572/01bba1e782c7f9b2afb7.jpg',180),(72713,135,20267,'/uploads/6c4/d8d/cd7/6abeb9d48a5455756cef.jpg',180),(72714,135,20268,'/uploads/144/b53/707/71cc301a44b233731a03.jpg',180),(72715,135,20269,'/uploads/3f8/c57/f91/3ec6cca0b63f1fa6eb7c.jpg',180),(72716,135,20270,'/uploads/ad1/f93/fb8/e845e4b587b76f7ac6e6.jpg',180),(72717,135,20271,'/uploads/6d6/659/610/b1b2cd23d2755883fc02.jpg',180),(72718,150,20268,'/uploads/02a/b82/e26/92c2daf022611cfba96d.jpg',200),(72719,210,20260,'/uploads/c1e/a55/cce/45ea123f15c5dee28289.jpg',280),(72720,68,20257,'/uploads/834/833/fee/c90328a88b6bacd0cadb.jpg',90),(72721,210,20268,'/uploads/5bb/6ef/608/b0f3ed137545c371078d.jpg',280),(72722,68,20267,'/uploads/106/af7/14c/83bfdf97dee4f0e1ed4a.jpg',90),(72723,68,20266,'/uploads/599/49b/edc/203e21dc5621b2d6f408.jpg',90),(72724,68,20265,'/uploads/484/7c8/65f/6f5a9ad2c1866cbbc2a1.jpg',90),(72725,405,20268,'/uploads/dc6/567/fd1/5c6c53311e006181ef8f.jpg',540),(72726,132,20267,'/uploads/249/561/dc9/d3583597a8d7ee00d22b.jpg',176),(72727,132,20266,'/uploads/c7b/ada/e8a/42feaa673b4e934c21ca.jpg',176),(72728,132,20265,'/uploads/6d5/dbc/e40/0fb7785c6deecd13b69a.jpg',176),(72729,132,20271,'/uploads/3a9/41c/7ba/c5e21726f857205cbc7c.jpg',176),(72730,132,20269,'/uploads/97a/efd/5c0/af25e4bf641032d713a9.jpg',176),(72731,132,20270,'/uploads/ef8/447/6f2/700c96d4d1a81361c660.jpg',176),(72732,132,20264,'/uploads/74d/401/ae4/e2944045856ff42eaa8c.jpg',176),(72733,405,20267,'/uploads/d14/a8c/b12/c763e087184fd03edfc6.jpg',540),(72734,405,20266,'/uploads/56b/554/ce9/c2bd60799b42e7afe812.jpg',540),(72735,405,20265,'/uploads/06e/ef6/d50/89c469a894263351ed1f.jpg',540),(72736,405,20271,'/uploads/028/505/335/692a3c7c2ce7f3b422f4.jpg',540),(72737,405,20269,'/uploads/5cd/f25/e85/3d12ba4cc969961f9966.jpg',540),(72738,405,20270,'/uploads/165/d3a/423/7d4cc53d52080fb50455.jpg',540),(72739,405,20264,'/uploads/77e/cc2/5dd/5262b4c0e3e21641681c.jpg',540),(72740,132,20257,'/uploads/0eb/e65/85a/af2f9a0bcbc4c1e72591.jpg',176),(72741,405,20253,'/uploads/52c/da8/ea6/75b468640b32ba1ad227.jpg',540),(72742,132,20254,'/uploads/cf2/20b/138/501ffc2769f7319ec983.jpg',176),(72743,132,20256,'/uploads/f4b/14e/4ff/61bb75dc23c25c4218be.jpg',176),(72744,132,20255,'/uploads/9e1/60f/840/5f1f751f6b7d48678c54.jpg',176),(72745,132,20252,'/uploads/c24/b2f/976/28ec8f21b200584a9f5c.jpg',176),(72746,132,20250,'/uploads/419/385/220/b88dbaf801067a25e6db.jpg',176),(72747,132,20251,'/uploads/f74/46b/04e/80c1bcb21b686383205e.jpg',176),(72748,405,20254,'/uploads/5a4/27b/e55/a99db984fb5ee9c65d81.jpg',540),(72749,405,20256,'/uploads/13e/0cb/60d/6b666274f2287c9d884c.jpg',540),(72750,405,20255,'/uploads/928/46c/8ed/f9834d93b60abd760ad6.jpg',540),(72751,405,20252,'/uploads/7f2/756/ee7/9a19571458a5f48d5684.jpg',540),(72752,405,20250,'/uploads/292/382/2f6/eb132630e4a825ecfcba.jpg',540),(72753,405,20251,'/uploads/d76/409/06e/1d143e9e423ecccb4cfb.jpg',540),(72754,405,20243,'/uploads/5c5/906/b3f/76f22c8497d8dfa853c6.jpg',540),(72755,132,20244,'/uploads/021/725/635/487007239285963b5acd.jpg',176),(72756,132,20242,'/uploads/83d/086/98c/7ea2ba0815e9f7349ad1.jpg',176),(72757,132,20249,'/uploads/57d/974/972/f5eb38933e3daa306428.jpg',176),(72758,132,20247,'/uploads/ab7/ad4/d42/800f324177f58a0e6d87.jpg',176),(72759,132,20245,'/uploads/adf/241/430/6099ee593f13724f0cfc.jpg',176),(72760,132,20246,'/uploads/8a4/289/fba/7ee3f05c3bad364f41e3.jpg',176),(72761,132,20248,'/uploads/935/9b6/487/3e186a551bb1b36e0c76.jpg',176),(72762,405,20244,'/uploads/134/da7/304/c539b917a5be6424100c.jpg',540),(72763,405,20242,'/uploads/ece/504/a79/500c8f33d64eb296ac26.jpg',540),(72764,405,20249,'/uploads/a77/e96/57a/e6e7cf12092d0da57166.jpg',540),(72765,405,20247,'/uploads/c2f/70f/980/2be0361f86a017d15ea9.jpg',540),(72766,405,20245,'/uploads/6ca/cff/2c3/43db7f9601786ccf53ea.jpg',540),(72767,405,20246,'/uploads/9fe/ed0/0cc/7c2bbd9b9e8f6976dffc.jpg',540),(72768,405,20248,'/uploads/148/43a/29e/e00bb3ebaf83fb87e615.jpg',540),(72778,135,20280,'/uploads/21e/4b8/917/1a1fbd43aeaed9576081.jpg',180),(72779,135,20281,'/uploads/e78/368/0ff/19b2ecfe07867732c47b.jpg',180),(72780,135,20282,'/uploads/753/2cd/3d5/a335160688eef0d4708e.jpg',180),(72781,135,20283,'/uploads/5e4/481/fab/77cf8d66fe3de11b1c42.jpg',180),(72782,135,20284,'/uploads/035/54f/104/32ce431948e8bb53b6aa.jpg',180),(72783,135,20285,'/uploads/3dd/d2c/0c4/e816cd11da7cfaf74f7c.jpg',180),(72784,135,20286,'/uploads/bb4/7a3/345/b481a09cb83ffe7d81e1.jpg',180),(72785,135,20287,'/uploads/d02/31b/83d/24e8a04dfe685490182e.jpg',180),(72786,150,20281,'/uploads/392/6a3/f50/6585ec62df71aed3b442.jpg',200),(72787,210,20281,'/uploads/0b4/4d6/dee/820c5e6495d63804d49f.jpg',280),(72788,68,20282,'/uploads/101/b64/ba3/63a4371edad346c32b9e.jpg',90),(72789,68,20280,'/uploads/de3/abc/592/6d1c475a53bb95117c4b.jpg',90),(72790,68,20287,'/uploads/791/b14/839/ad29460d330cfab0eb6a.jpg',90),(72795,135,20288,'/uploads/bff/592/f3e/5e98e4ebcc7c065a63cd.jpg',180),(72796,135,20289,'/uploads/3bb/86b/91f/3940a08fffdbe593f54e.jpg',180),(72797,135,20290,'/uploads/ef9/293/63a/4294e3a52f3efa8b3986.jpg',180),(72798,135,20291,'/uploads/643/a31/b4e/628889db77c2efbbf29c.jpg',180),(72799,135,20292,'/uploads/124/c87/1d4/a6334efbcc2696cf2708.jpg',180),(72800,135,20293,'/uploads/d83/408/7bd/8f64d29e01711e0dd074.jpg',180),(72801,135,20294,'/uploads/a77/2f9/a4d/b62caf1bf218ce6757f9.jpg',180),(72802,135,20295,'/uploads/e06/978/09e/4136d368cb814ce83762.jpg',180),(72803,210,20288,'/uploads/3c2/1b3/c56/9682c6804736dad47289.jpg',280),(72804,68,20289,'/uploads/01c/d4b/387/057f923cecea7bf28fee.jpg',90),(72805,68,20290,'/uploads/0bd/457/806/1abb6e63ed26c25dde2a.jpg',90),(72806,68,20291,'/uploads/50b/978/f53/d51d82f9bdb1a942f2a2.jpg',90),(72807,150,20292,'/uploads/a8c/49f/2d8/7be9dd05dd6e725cffff.jpg',200),(72808,210,20292,'/uploads/cd3/2d0/d7a/9c18e8fb7404239ab433.jpg',280),(72809,68,20293,'/uploads/bf9/d22/a8c/8361b31280ac400dd71f.jpg',90),(72810,405,20292,'/uploads/0e6/8a0/192/747261e7cbeed44bb9bd.jpg',540),(72811,132,20293,'/uploads/6d9/c8c/bbc/a6c7abdac1e2df4800c0.jpg',176),(72812,132,20291,'/uploads/830/c7b/078/b454b7c32e4dfefbb53e.jpg',176),(72813,132,20290,'/uploads/997/a09/df0/febb3cffd2f5171c6651.jpg',176),(72814,132,20288,'/uploads/b74/842/b65/bbdf97a8090822a34759.jpg',176),(72815,132,20294,'/uploads/6a6/063/614/a345bf95a77ff870d159.jpg',176),(72816,132,20295,'/uploads/df7/74b/f64/4e3f41888032a3fbb008.jpg',176),(72817,132,20289,'/uploads/8cc/8fc/657/75d6a986173d2cb014eb.jpg',176),(72818,405,20293,'/uploads/359/d21/35a/5b5172874ebdef6c73f7.jpg',540),(72819,405,20291,'/uploads/7fe/ea5/183/624a6b03636b0d28dd27.jpg',540),(72820,405,20290,'/uploads/ebc/916/a20/a22219df8da7dc991082.jpg',540),(72821,405,20288,'/uploads/f0e/315/fcd/58f2088b0d8f759e5909.jpg',540),(72822,405,20294,'/uploads/4fa/63d/bb9/4ede746af275036a91c3.jpg',540),(72823,405,20295,'/uploads/208/470/094/ea1c89c00054ff39f42a.jpg',540),(72824,405,20289,'/uploads/d05/b31/814/912730215e88ee21348e.jpg',540),(72840,405,20281,'/uploads/3ab/592/89c/40bdb3e577080f51e37b.jpg',540),(72841,132,20282,'/uploads/a4d/115/aa4/76d02070c7cbbf426016.jpg',176),(72842,132,20280,'/uploads/bb4/3b6/dd8/e308ef1028e95614d0d2.jpg',176),(72843,132,20287,'/uploads/3c0/101/503/4cbf6d1886d47152f709.jpg',176),(72844,132,20285,'/uploads/c57/833/64f/c93a5204a6b1ff0e2329.jpg',176),(72845,132,20283,'/uploads/9ef/2dd/4df/9588710c23bed5188d16.jpg',176),(72846,132,20284,'/uploads/4ca/3e1/e09/a7b239da5da876ee2baa.jpg',176),(72847,132,20286,'/uploads/37b/a7e/abe/c0fb01fe8afa3834c13a.jpg',176),(72848,405,20282,'/uploads/354/b9a/b12/a998452fe9158efdedea.jpg',540),(72849,405,20280,'/uploads/1d2/eee/08b/6fe08b0654adbc5cf1cf.jpg',540),(72850,405,20287,'/uploads/ff5/df1/70f/6e8878770e1fc56a668e.jpg',540),(72851,405,20285,'/uploads/117/4c9/967/07de15e647a20c0170de.jpg',540),(72852,405,20283,'/uploads/74b/035/4da/03eb4c5927a4d21dcb60.jpg',540),(72853,405,20284,'/uploads/257/c67/f37/8676ecf090073d105282.jpg',540),(72854,405,20286,'/uploads/90a/1c7/630/303266368c04ce4ecc34.jpg',540),(72856,132,20253,'/uploads/552/caf/ee8/60d1188ff0026a705c97.jpg',176),(72857,132,20243,'/uploads/820/345/6d7/0828bfa3dff019d402ca.jpg',176),(72858,132,20281,'/uploads/cee/715/9cc/9b73d233d2b45bb116da.jpg',176),(72859,132,20292,'/uploads/7db/226/6c2/d5e9dd77e276b876db75.jpg',176),(72862,132,20268,'/uploads/1ab/df0/f6d/76a642c3b219db810419.jpg',176),(72863,150,20296,'/uploads/c21/f9e/2f5/4d626425d4ed39eaf24c.jpg',200),(72864,296,20296,'/uploads/f07/346/f7b/437c76105b18c5aff752.jpg',820),(72865,150,20297,'/uploads/201/890/af0/857647591ee2d2098eec.jpg',200),(72866,296,20297,'/uploads/7a7/b51/952/0b21d14c95ac8d8dbbc6.jpg',820),(72867,150,20298,'/uploads/c1b/4d5/bd3/15674f85131d96aec911.jpg',200),(72868,296,20298,'/uploads/2e7/f9c/72d/983feb471c86e70e7cfb.jpg',820),(72894,135,20315,'/uploads/254/960/3f8/8ee17b5444fda63c9d34.jpg',180),(72895,135,20316,'/uploads/365/1b3/2e7/b350e6273b4603d5c96d.jpg',180),(72896,135,20317,'/uploads/46b/913/ce5/5a16905dc82af4c5c4db.jpg',180),(72897,135,20318,'/uploads/929/75a/a03/0de059023a42c222c1d4.jpg',180),(72898,135,20319,'/uploads/ad8/17e/173/d85d49aaed6b37479b73.jpg',180),(72899,135,20320,'/uploads/8f6/1a0/2b8/104b96cdb4dbd008b202.jpg',180),(72900,135,20321,'/uploads/4bb/07c/329/09e950c7d1ec96c65b9c.jpg',180),(72901,135,20322,'/uploads/318/710/c5e/8ec58c04257b355a8eca.jpg',180),(72902,210,20315,'/uploads/2c1/d3c/5ff/a7ae9d09b932f74fa8c7.jpg',280),(72903,68,20316,'/uploads/367/856/aab/f6f0984ced1d5efb4f4c.jpg',90),(72904,68,20317,'/uploads/f93/cc4/6df/36bd4dbfed962632bc2a.jpg',90),(72905,68,20318,'/uploads/d7d/c87/f8d/23b5e6b8a98953046b09.jpg',90),(72906,150,20317,'/uploads/2e0/de6/d12/d185bec85f3444556fdc.jpg',200),(72907,210,20317,'/uploads/49f/e5d/19c/7f1fafd7c0e0e85bdad3.jpg',280),(72908,68,20315,'/uploads/5cd/4db/167/6666f5039bb945f5c91c.jpg',90),(72909,135,20323,'/uploads/8ca/c9f/3cd/efba31a63a7aa5edcc1d.jpg',180),(72910,135,20324,'/uploads/89d/5cc/8db/8d0c55572ef751771cf7.jpg',180),(72911,135,20325,'/uploads/a00/16e/088/cb2d4259b400c966e88b.jpg',180),(72912,135,20326,'/uploads/dc7/9f2/b05/5ae7dc9a9721aeb4fbd9.jpg',180),(72913,135,20327,'/uploads/f99/0ae/465/dc3a9e695bb1ce130bca.jpg',180),(72914,135,20328,'/uploads/31e/f20/a6c/739a7d5ee2e0016f9fcd.jpg',180),(72915,135,20329,'/uploads/9ca/1be/e5f/5d62a7c3c6c08cabb41e.jpg',180),(72916,135,20330,'/uploads/a36/ca7/50e/deca1b2b4dcb7a39e8fb.jpg',180),(72917,210,20325,'/uploads/e31/a18/cab/f2e147ef8a26be429bf9.jpg',280),(72918,68,20324,'/uploads/838/15b/26d/2246a9687bc1850b381f.jpg',90),(72919,68,20323,'/uploads/af0/ad5/2a3/1f8933d44dca5acdd957.jpg',90),(72920,68,20330,'/uploads/7cc/90b/cb6/ca492b4b195865a8498b.jpg',90),(72921,150,20325,'/uploads/a93/06d/15a/ee54dbd40798c8f8b438.jpg',200),(72922,405,20325,'/uploads/057/e83/3f7/fe80b427e93cb4a75099.jpg',540),(72923,132,20324,'/uploads/d75/eb2/8e3/0e8cd4fb8649ed187624.jpg',176),(72924,132,20323,'/uploads/a20/5f9/2c3/c391240d534c6344db3e.jpg',176),(72925,132,20330,'/uploads/37f/184/23c/55506d696bf431df44d4.jpg',176),(72926,132,20328,'/uploads/775/f9b/bf6/5fd8e54dde93bb10e874.jpg',176),(72927,132,20326,'/uploads/982/e3f/1c3/c9a17af6a3184df2a4ca.jpg',176),(72928,132,20327,'/uploads/eec/6ee/9ea/20dd62abf5389cfd868b.jpg',176),(72929,132,20329,'/uploads/71b/85f/515/a99962d3810a09de575c.jpg',176),(72930,405,20324,'/uploads/a23/a9d/43e/8816479cefda6fe0fadc.jpg',540),(72931,405,20323,'/uploads/bff/655/643/0723b11413c5cb6afca0.jpg',540),(72932,405,20330,'/uploads/ffc/369/9bb/5ccafdff65c9e9fe291f.jpg',540),(72933,405,20328,'/uploads/391/4bb/e4f/515b20ccb94c29161ab0.jpg',540),(72934,405,20326,'/uploads/018/8e4/8dd/706e60398a60fcc941dd.jpg',540),(72935,405,20327,'/uploads/19f/85b/d52/2c30f7c1a970fc3d4375.jpg',540),(72936,405,20329,'/uploads/0b9/2f5/144/a566049b346080914d16.jpg',540),(72937,405,20317,'/uploads/6b9/0c2/a13/3ef38915e845a660db94.jpg',540),(72938,132,20318,'/uploads/986/d66/51d/758a0e285c6bb670e807.jpg',176),(72939,132,20316,'/uploads/e6a/24b/124/9adc64672f031cc9af96.jpg',176),(72940,132,20315,'/uploads/100/b0c/2de/e37bd6ec2d05870f61e8.jpg',176),(72941,132,20321,'/uploads/50f/3f5/964/41102331075fa333d7e5.jpg',176),(72942,132,20319,'/uploads/25a/69f/636/eb8d808065a81812d719.jpg',176),(72943,132,20320,'/uploads/ef7/ad3/23a/88e66ff2bab1154dfc40.jpg',176),(72944,132,20322,'/uploads/b8a/8ba/c73/f772401572611ed65aa8.jpg',176),(72945,405,20318,'/uploads/3e0/379/fc5/2ebddf5a8e351b1ac532.jpg',540),(72946,405,20316,'/uploads/c20/8e4/0f5/93688441ad1732ebfd52.jpg',540),(72947,405,20315,'/uploads/fc1/8bd/ae3/9658fa1a450545023946.jpg',540),(72948,405,20321,'/uploads/b1c/8d0/f02/d4d4af39834b04fa8760.jpg',540),(72949,405,20319,'/uploads/603/74c/2ad/50f9c49b69813b35eca5.jpg',540),(72950,405,20320,'/uploads/369/1ad/40a/bab5b2610157ae6fa0b2.jpg',540),(72951,405,20322,'/uploads/6db/f92/5bf/0acb4f98a633822fc4a8.jpg',540),(72952,132,20317,'/uploads/fb1/c5d/64e/58048b3d53174c7e2380.jpg',176),(72953,135,20331,'/uploads/0a8/a38/776/ea7c760fd1003cad8353.jpg',180),(72954,135,20332,'/uploads/718/3c6/da3/0bfd5a7bc55f3f2915d2.jpg',180),(72955,135,20333,'/uploads/fc6/f5e/751/aca9d8b48546723a2639.jpg',180),(72956,135,20334,'/uploads/651/8d4/7f6/5d2d33cf542d76567cbd.jpg',180),(72957,135,20335,'/uploads/66b/fe0/f27/9e9e3c6055ffd9261965.jpg',180),(72958,135,20336,'/uploads/883/233/834/41f1eb3c4a78a74750ff.jpg',180),(72959,135,20337,'/uploads/507/394/2fa/aafcf9f5542de0fecebc.jpg',180),(72960,135,20338,'/uploads/693/214/8e9/062357c13db6c9347d41.jpg',180),(72961,150,20333,'/uploads/fc2/333/bb1/ac874dfd21fc93a3ea2a.jpg',200),(72975,210,20333,'/uploads/41a/b0b/417/43a29e1d25284885b300.jpg',280),(72976,68,20334,'/uploads/f4b/6ef/c41/3ef0458a63d89d51d36c.jpg',90),(72977,68,20332,'/uploads/124/8d8/474/a5a98ba1a641e2373cac.jpg',90),(72978,68,20331,'/uploads/8d1/b0a/d06/9cbacaa7763bd182c77d.jpg',90),(72993,135,20356,'/uploads/4ad/588/0db/c97d768b39ae8a4e7bdb.jpg',180),(72994,135,20357,'/uploads/b37/4f5/cd0/fdf567ad7c01865150ab.jpg',180),(72995,135,20358,'/uploads/626/b84/37d/43ee0f74450e3b0c824a.jpg',180),(72996,135,20359,'/uploads/19c/7cd/872/8fe9ccf46b66c785c18e.jpg',180),(72997,135,20360,'/uploads/19d/ede/331/2b5e872a11cca3f0ad80.jpg',180),(72998,135,20361,'/uploads/7e7/78c/830/ba6833beecd969177e85.jpg',180),(72999,135,20362,'/uploads/4e4/e9b/565/d553aa7f21a71944ebdc.jpg',180),(73000,135,20363,'/uploads/476/284/31b/e77e3d13fbf285e68066.jpg',180),(73001,210,20356,'/uploads/b82/621/3cb/c9795ff7f9deb1c5dc5a.jpg',280),(73002,68,20357,'/uploads/c24/a96/5c7/6b822454ccf43947ae0a.jpg',90),(73003,68,20358,'/uploads/8e5/5a6/7d8/6c1f129b76478ae07cae.jpg',90),(73004,68,20359,'/uploads/107/516/d5a/9f38693a67f26ffdcdf4.jpg',90),(73005,210,20360,'/uploads/9fb/548/702/d07fa4df5e39c7c6f090.jpg',280),(73006,68,20361,'/uploads/7f5/cfa/83b/5223350724d5454ada6d.jpg',90),(73007,150,20360,'/uploads/9f9/6de/5fb/ca1675e0471846be097e.jpg',200),(73008,405,20333,'/uploads/62a/ab8/455/c45ba551711eb201edd5.jpg',540),(73009,132,20334,'/uploads/eaf/0fa/6d1/870b44a486ab473b75ca.jpg',176),(73010,132,20332,'/uploads/7d1/702/63f/941299e369b8bfe8de20.jpg',176),(73011,132,20331,'/uploads/bff/836/90a/88f5564fe42e9ee14f71.jpg',176),(73012,132,20337,'/uploads/3a2/02b/ec7/894ea6b4fc6c37d376f4.jpg',176),(73013,132,20335,'/uploads/2db/7da/74c/90720d4a744e4fb2188f.jpg',176),(73014,132,20336,'/uploads/793/c75/bb1/ab448ce8cfdae33dd616.jpg',176),(73015,132,20338,'/uploads/cf5/416/a1b/08bf0ca8f863b56d85b4.jpg',176),(73016,405,20334,'/uploads/666/f5a/b99/f3b50c2353fa9930ef04.jpg',540),(73017,405,20332,'/uploads/e49/2aa/7c7/bfab9d0885a53be38f07.jpg',540),(73018,405,20331,'/uploads/74a/782/a49/73191abe43e576cce969.jpg',540),(73019,405,20337,'/uploads/695/fce/34f/231dacfb3738b5db4677.jpg',540),(73020,405,20335,'/uploads/008/a87/c7b/0a49a6248c3c931b0c3a.jpg',540),(73021,405,20336,'/uploads/6f7/6ea/aa0/ca3430a6cabf8cfb9c0f.jpg',540),(73022,405,20338,'/uploads/90c/af6/46a/473460b8766e82afff2d.jpg',540),(73023,135,20364,'/uploads/46f/ef7/998/e465800553e856fdf079.jpg',180),(73024,135,20365,'/uploads/68f/d12/335/0d35a5af351885accf6d.jpg',180),(73025,135,20366,'/uploads/296/b33/ed6/4c6392f2c9418307333e.jpg',180),(73026,135,20367,'/uploads/c5f/1bd/632/4b7ac7794d213950fdf3.jpg',180),(73027,135,20368,'/uploads/44f/db7/938/1f7a8953279f065ec1b8.jpg',180),(73028,135,20369,'/uploads/711/324/6e6/939e8eec14aa99f3d3ce.jpg',180),(73029,135,20370,'/uploads/474/61f/2bc/2733a3abeaf70de1a614.jpg',180),(73030,135,20371,'/uploads/fac/541/c32/6a5074363ef937c57f8a.jpg',180),(73031,210,20367,'/uploads/1c8/b08/624/1d5622e35e018859e56e.jpg',280),(73032,68,20368,'/uploads/fdd/93c/413/dc792c90d08ae349c757.jpg',90),(73033,68,20366,'/uploads/374/05a/cb8/c693e4cbeb70a75637c4.jpg',90),(73034,68,20365,'/uploads/ce2/dfe/e54/73d111cd6ae183170d13.jpg',90),(73035,150,20367,'/uploads/0ec/a05/45b/609db06fe5669fdd30a1.jpg',200),(73036,405,20360,'/uploads/339/29d/8f2/b3f1486811d87428c61e.jpg',540),(73037,132,20361,'/uploads/d7d/b2c/24a/09003a5664af27a6d17b.jpg',176),(73038,132,20359,'/uploads/3c9/56b/62b/e1c80a849c1177278c37.jpg',176),(73039,132,20358,'/uploads/3ad/ce5/174/d6e8642c92cdfabaffbf.jpg',176),(73040,132,20356,'/uploads/7b1/b09/a06/1ffcdbd07728114b2f2d.jpg',176),(73041,132,20362,'/uploads/4f2/203/aee/34c755e08b62ec4de834.jpg',176),(73042,132,20363,'/uploads/159/2ca/400/0f1f0684a46257e2275f.jpg',176),(73043,132,20357,'/uploads/070/2c2/cd2/36abe45c90ba0d71a14e.jpg',176),(73044,405,20361,'/uploads/423/12b/6f3/6b36416e5c64c41eaf5e.jpg',540),(73045,405,20359,'/uploads/7e8/219/bac/bb24504fa575129a51f1.jpg',540),(73046,405,20358,'/uploads/8ad/18b/e52/8d3baac9ef46bf626706.jpg',540),(73047,405,20356,'/uploads/45f/9a7/b77/1694110ee069d76b60bb.jpg',540),(73048,405,20362,'/uploads/261/8b9/a6c/c5f74ff122c5ba3610a5.jpg',540),(73049,405,20363,'/uploads/267/198/e7f/5a7ae65ad9f9d96856b2.jpg',540),(73050,405,20357,'/uploads/b40/e07/c6d/b64383a872d95abd3c1b.jpg',540),(73051,405,20367,'/uploads/e73/d7f/5c6/adc253f1e107bf25a004.jpg',540),(73052,132,20368,'/uploads/49c/cf7/420/a715e6f7bb733b08e116.jpg',176),(73053,132,20366,'/uploads/4a2/078/da7/45373e68deedd35cac7b.jpg',176),(73054,132,20365,'/uploads/b8f/ad9/2b4/16b476b0b9d25138426b.jpg',176),(73055,132,20371,'/uploads/11d/848/f57/7ac58e83053b32b12ee8.jpg',176),(73056,132,20369,'/uploads/5ea/ec4/484/9d395f7ebb539547240c.jpg',176),(73057,132,20370,'/uploads/f74/65e/29a/4f6391c6c1bed4c92b9c.jpg',176),(73058,132,20364,'/uploads/3b1/b75/e2c/af610aa56b8c8251dbd6.jpg',176),(73059,405,20368,'/uploads/fe6/57c/a9f/d381245475b99e9159e6.jpg',540),(73060,405,20366,'/uploads/e2b/f41/0df/0b4b9cc7ba2202dc8038.jpg',540),(73061,405,20365,'/uploads/03e/e94/c0e/d711e8c7591be10ece6e.jpg',540),(73062,405,20371,'/uploads/704/90c/d79/c5e1acd799820f8d3c7c.jpg',540),(73063,405,20369,'/uploads/bed/68c/bcc/dcb5346a5312b95ff516.jpg',540),(73064,405,20370,'/uploads/024/bad/2c9/8c28fbd36973e2c89cba.jpg',540),(73065,405,20364,'/uploads/3b8/89b/bf7/a8ddf15bb7420da628d2.jpg',540),(73097,132,20333,'/uploads/bff/200/df1/1c7248c43bfbfc7b99e8.jpg',176),(73098,132,20360,'/uploads/777/a31/1f4/6a543824c6aee90748af.jpg',176),(73099,132,20325,'/uploads/0b6/e8d/49c/9f70018e3b40ff7f7cac.jpg',176),(73101,132,20367,'/uploads/f19/df3/6f3/82cfab999edebb0efc39.jpg',176),(73103,68,20373,'/uploads/316/2bc/f3f/5f5035bee2455bd33233.jpg',90),(73104,68,20374,'/uploads/d8c/c11/6a8/1424927736b7a6652f6d.jpg',90),(73105,68,20375,'/uploads/fef/b73/986/50da4d290317b4105ea8.jpg',90),(73107,135,20373,'/uploads/7b9/79f/ab5/4743044891a21daf8cb0.jpg',180),(73108,135,20374,'/uploads/073/ca8/be3/a2778068da4227777c5a.jpg',180),(73109,135,20375,'/uploads/105/7fe/f67/3d6d37a032d59f0872a4.jpg',180),(73110,135,20376,'/uploads/f9a/70d/0b4/a60184eacf37c92b9139.jpg',180),(73111,135,20377,'/uploads/f1f/111/8ff/fdf4ae238d39c510b0a0.jpg',180),(73112,135,20378,'/uploads/9bf/062/760/a854c6cc3dd2807ad912.jpg',180),(73115,132,20373,'/uploads/89d/c97/ee9/c6044d01fb1e0ef2db5a.jpg',176),(73116,132,20374,'/uploads/e62/0e8/1cc/fae031042df57e8a8944.jpg',176),(73117,132,20375,'/uploads/6f8/13f/98d/772a6e7763eb6185ccab.jpg',176),(73118,132,20376,'/uploads/1c8/24d/596/8fd45c2b29ddccb3527f.jpg',176),(73119,132,20377,'/uploads/b4a/c64/5b9/579a6c5c137e20ea6535.jpg',176),(73120,132,20378,'/uploads/9b4/611/40a/7b01121f2ecd84c40d2d.jpg',176),(73121,405,20373,'/uploads/b6a/354/9c2/8507763e107bfef49963.jpg',540),(73122,405,20374,'/uploads/2a4/27b/0ba/fad5303da1de57789b8a.jpg',540),(73123,405,20375,'/uploads/9fb/c39/70f/d62cfbb5f1c6aae73c1f.jpg',540),(73124,405,20376,'/uploads/c54/16e/38f/0eec37ba49d90efc58e8.jpg',540),(73125,405,20377,'/uploads/860/e0c/8e9/facfeae2d65251f64b4c.jpg',540),(73126,405,20378,'/uploads/ef2/922/397/e7036a3071704c1b5638.jpg',540),(73127,135,20379,'/uploads/b69/189/851/f0d940b9c131a4d4c834.jpg',180),(73128,135,20380,'/uploads/2e2/b46/ca6/4f24d5c9f90c16399553.jpg',180),(73129,135,20381,'/uploads/fb2/813/45f/0f25eb44c9373ec0f34f.jpg',180),(73130,135,20382,'/uploads/e87/357/780/c71ed69789bf1757c43c.jpg',180),(73131,135,20383,'/uploads/e4d/544/c4a/cf7005cfe028cfbaef12.jpg',180),(73132,135,20384,'/uploads/b47/1cb/2fd/25903718c8b0c7ba3498.jpg',180),(73133,135,20385,'/uploads/515/c42/470/fb5cde7c3e466493acce.jpg',180),(73134,135,20386,'/uploads/107/946/d6e/49926ced707a4cc3c259.jpg',180),(73135,210,20379,'/uploads/598/c2b/2a0/e6cc196fdee7104ced92.jpg',280),(73136,68,20380,'/uploads/aee/d25/170/2f16f9160e8ce0c94d21.jpg',90),(73137,68,20381,'/uploads/8ad/06a/6ac/91d1a5cd56697107151d.jpg',90),(73138,68,20382,'/uploads/ec1/1c6/2d7/458f851cfbb9af42afe9.jpg',90),(73139,150,20379,'/uploads/462/d1b/14d/fa12f35a569344ea95fe.jpg',200),(73140,405,20379,'/uploads/553/751/426/19d62888ab2638c830b0.jpg',540),(73141,132,20380,'/uploads/ee7/fc9/d5d/91d4aa280bbb909cbc9d.jpg',176),(73142,132,20381,'/uploads/29a/d69/aab/fc1dfa264e99d4050f69.jpg',176),(73143,132,20382,'/uploads/831/b6c/690/76a2c1f2770125aa078f.jpg',176),(73144,132,20383,'/uploads/c90/7a1/0a3/e697c2a6cbfa3418c586.jpg',176),(73145,132,20384,'/uploads/dec/8b5/fad/eb5bcece4845330aa50b.jpg',176),(73146,132,20385,'/uploads/93f/cac/0e5/8006f34d5446239e15f4.jpg',176),(73147,132,20386,'/uploads/39d/f0d/fe0/09a534d7fc4c66e153dd.jpg',176),(73148,405,20380,'/uploads/77d/f8a/df8/214d61ea4e9de46b4363.jpg',540),(73149,405,20381,'/uploads/279/28e/8b5/0fa4c96228c46b9603cb.jpg',540),(73150,405,20382,'/uploads/97c/340/02a/ab38ae7bdc8fa006a4ae.jpg',540),(73151,405,20383,'/uploads/934/f59/4a8/f0077668868b9d92d3b1.jpg',540),(73152,405,20384,'/uploads/7b9/261/642/faa6ae2c068b4dff5bd9.jpg',540),(73153,405,20385,'/uploads/0a2/a61/f77/708a29e62ed719cdd6a3.jpg',540),(73154,405,20386,'/uploads/2b5/8dd/db5/6dc8a546815799b2854f.jpg',540),(73155,150,20373,'/uploads/f68/72d/7e7/e94fdc05b245702e0fa5.jpg',200),(73156,210,20373,'/uploads/aeb/50d/9ec/b9f830fc92f4f481abc1.jpg',280),(73157,68,20376,'/uploads/f53/040/075/de0eda4094d457f751f4.jpg',90),(73171,135,20395,'/uploads/e82/247/a11/53150d502272d5c08030.jpg',180),(73172,135,20396,'/uploads/f69/2ea/cae/48000a8aa8a9ded63dd9.jpg',180),(73173,135,20397,'/uploads/fd8/d79/a6e/020ede9539ba77564323.jpg',180),(73174,135,20398,'/uploads/9e0/8c7/58b/189eb12d4cb74e53f9b9.jpg',180),(73175,135,20399,'/uploads/5e3/744/24a/fa3816647555f4ef4f38.jpg',180),(73176,135,20400,'/uploads/580/f83/e99/faee255ee608a86f08a1.jpg',180),(73177,135,20401,'/uploads/d15/944/ae7/e83b728213951502c650.jpg',180),(73178,135,20402,'/uploads/bfd/05d/72b/85213472ed9df3374faa.jpg',180),(73179,150,20395,'/uploads/a32/801/209/d4340321b33ea527cb14.jpg',200),(73180,210,20395,'/uploads/df7/bc8/8d9/24bd0f64db33b8d54689.jpg',280),(73181,68,20396,'/uploads/75e/7d1/674/6f517fdae7009d1de805.jpg',90),(73182,68,20397,'/uploads/13b/33b/5f7/5bb00a09a2bdb565da24.jpg',90),(73183,68,20398,'/uploads/3c7/96f/191/69bebe6caea641f3d2d4.jpg',90),(73197,135,20411,'/uploads/39b/f76/97a/547b33300ff2d4963e97.jpg',180),(73198,135,20412,'/uploads/380/fbe/dcd/354da4022f48d14ea03c.jpg',180),(73199,135,20413,'/uploads/31a/812/7cb/5dd48180372b2286ffda.jpg',180),(73200,135,20414,'/uploads/4c1/2b7/6c1/22147a39b7f8620df3e5.jpg',180),(73201,135,20415,'/uploads/191/51f/643/76389b9215612defeadd.jpg',180),(73202,135,20416,'/uploads/48b/9af/1ca/450ac093d1020ae8b776.jpg',180),(73203,135,20417,'/uploads/d45/00b/2c2/066b02d21dd5b6e55473.jpg',180),(73204,150,20411,'/uploads/8da/55c/c40/d0568276b5b6345ae87d.jpg',200),(73205,210,20411,'/uploads/c00/802/8e6/e6a6a945af6dab61ec21.jpg',280),(73206,68,20412,'/uploads/e23/c7f/fe6/0fa19de1c0e01064d0ea.jpg',90),(73207,68,20413,'/uploads/2f6/a7c/2d2/61216f242113bee1e638.jpg',90),(73208,68,20414,'/uploads/423/eb5/71b/a91f6a891c7ce7d1b8da.jpg',90),(73209,135,20418,'/uploads/d2b/0df/8f6/a80d16405158f77bf6d2.jpg',180),(73210,135,20419,'/uploads/89d/4eb/2c1/3a8a78f8e6f93c4f89f3.jpg',180),(73211,135,20420,'/uploads/fdc/805/133/e0edcbc05299866a6d96.jpg',180),(73212,135,20421,'/uploads/67e/880/7e7/aac6bfe64bd46e9b8cd9.jpg',180),(73213,135,20422,'/uploads/ea3/47e/682/a2e79a0ceaf33b72b369.jpg',180),(73214,135,20423,'/uploads/6f3/d2b/01d/b6e2fe72e3285a588a8c.jpg',180),(73215,135,20424,'/uploads/af4/bbe/390/a35ac94378a613afa140.jpg',180),(73216,150,20418,'/uploads/721/114/f95/92039081a4571f865e55.jpg',200),(73217,135,20425,'/uploads/a81/900/8b4/22021ba9989989491a03.jpg',180),(73218,135,20426,'/uploads/8da/70e/a81/7770e6a28ac3fefeeb21.jpg',180),(73219,135,20427,'/uploads/5f2/a00/e4f/809f0992cdf961d70fab.jpg',180),(73220,135,20428,'/uploads/932/e48/3d5/647be652d78fb7991fc6.jpg',180),(73221,135,20429,'/uploads/6ae/70b/3db/b9867f110cef551b87e2.jpg',180),(73222,135,20430,'/uploads/5c8/1a5/d71/67694d90cad95d27d2c7.jpg',180),(73223,135,20431,'/uploads/980/2c5/6ae/22bc2582643736f79e34.jpg',180),(73224,135,20432,'/uploads/b0b/95c/84c/afb5ed7591c6856da2d4.jpg',180),(73225,150,20425,'/uploads/656/c82/963/4f2762550f34e10b2a10.jpg',200),(73226,210,20418,'/uploads/436/e12/d68/af5feb80a9c95a28af86.jpg',280),(73227,68,20419,'/uploads/baa/95d/aa0/845743fb15f803239739.jpg',90),(73228,68,20420,'/uploads/6cf/f19/eab/6f67c6cf29faf86f0e65.jpg',90),(73229,68,20421,'/uploads/8d6/98b/ce9/e70853e88589fb0a7dba.jpg',90),(73230,135,20433,'/uploads/e65/861/8eb/e49b42d15292ac224b40.jpg',180),(73231,135,20434,'/uploads/fe3/c7a/e7b/bbe7c4cc0e5e76b4d262.jpg',180),(73232,135,20435,'/uploads/f2e/2d1/c09/803d1ce3a0a16866326e.jpg',180),(73233,135,20436,'/uploads/b7c/565/6fb/b606e10e785b684f69b7.jpg',180),(73234,135,20437,'/uploads/e6c/c36/873/fa4674cd3ec9655b9ec7.jpg',180),(73235,135,20438,'/uploads/4fc/e0c/ee7/270b40c7537d1a52e3cd.jpg',180),(73236,135,20439,'/uploads/116/690/d55/a0c381b44cd6143ae343.jpg',180),(73237,135,20440,'/uploads/f41/e16/da9/3b8c0b12a25ea5690296.jpg',180),(73238,150,20433,'/uploads/ac0/10a/9f6/f4f2c8f30f70f36a8903.jpg',200),(73239,210,20425,'/uploads/075/017/8b5/21191603fad1a307221b.jpg',280),(73240,68,20426,'/uploads/181/2b6/990/7314a24912878f308558.jpg',90),(73241,68,20427,'/uploads/d5c/78c/443/7214b93f4390fd748630.jpg',90),(73242,68,20428,'/uploads/17a/8f4/f62/8cd6faac4d31b90f155a.jpg',90),(73243,210,20433,'/uploads/7a2/d62/3ab/3455e99f2222176a41fd.jpg',280),(73244,68,20434,'/uploads/18a/edd/c81/4429a8b11646bf41e1ba.jpg',90),(73245,68,20435,'/uploads/b2f/dca/4d4/f5fd48e912cd273ce7dc.jpg',90),(73246,68,20436,'/uploads/b65/2fd/70f/144c14f9f00d2119f59c.jpg',90),(73247,405,20411,'/uploads/34c/fc2/61d/dbb79ecec4f0568c8d3d.jpg',540),(73248,132,20412,'/uploads/638/7ed/36e/6217fdea76065ce44d5c.jpg',176),(73249,132,20413,'/uploads/a38/cf2/bc6/52f3fb40591bd0510257.jpg',176),(73250,132,20414,'/uploads/fe3/2b4/bf7/b9c253e79272d2f37a2f.jpg',176),(73251,132,20415,'/uploads/326/304/8f5/705df1642a73a29e8a39.jpg',176),(73252,132,20416,'/uploads/8c1/0bb/bd2/1b93b03f57ca1d232e97.jpg',176),(73253,132,20417,'/uploads/116/c58/c37/fac11be2245051c6ee71.jpg',176),(73254,405,20412,'/uploads/359/6ed/0bb/4832ae24a2ce91a63032.jpg',540),(73255,405,20413,'/uploads/25a/e11/03c/24c6cbf8604b4639350e.jpg',540),(73256,405,20414,'/uploads/7b5/721/c53/77329418f27540424b33.jpg',540),(73257,405,20415,'/uploads/5be/eb6/13a/cb72babb285cbb5fa450.jpg',540),(73258,405,20416,'/uploads/335/387/397/915467ab09773a9986f5.jpg',540),(73259,405,20417,'/uploads/76b/cbe/7cf/276fc62a8eed0f30c0f7.jpg',540),(73260,405,20418,'/uploads/43a/7be/fa7/0183c036b2933936286b.jpg',540),(73261,132,20419,'/uploads/af7/5ba/ea2/6be5fb21ddf93336f1ef.jpg',176),(73262,132,20420,'/uploads/c85/0f3/8ba/d33ac7a2fb04da85cd91.jpg',176),(73263,132,20421,'/uploads/de9/428/d30/2fc7b054b7c81a70bbe4.jpg',176),(73264,132,20422,'/uploads/595/2cd/e2a/cf369ceab69cb6a9c3d5.jpg',176),(73265,132,20423,'/uploads/8d9/399/ccc/de94a611abe926e01655.jpg',176),(73266,132,20424,'/uploads/767/713/350/cd8ec060e692c3c1a694.jpg',176),(73267,405,20419,'/uploads/217/68b/6f8/79c072e7dd535a589d39.jpg',540),(73268,405,20420,'/uploads/e5c/f0e/e33/7876c593d128afb0e407.jpg',540),(73269,405,20421,'/uploads/dbc/a06/bfe/a023dafe351f5f0fb55a.jpg',540),(73270,405,20422,'/uploads/637/7d0/a9d/9488e638da896990c2c6.jpg',540),(73271,405,20423,'/uploads/183/fb7/316/98688a8cee5df80486e0.jpg',540),(73272,405,20424,'/uploads/e6d/72e/7db/b290323fad775acda95a.jpg',540),(73273,135,20441,'/uploads/258/850/215/f362b0d16a2b6d502b37.jpg',180),(73274,135,20442,'/uploads/4ef/d89/14b/433c6e36e2a73f07c30b.jpg',180),(73275,135,20443,'/uploads/d33/1dc/c99/5096c1e0fe3511168cb8.jpg',180),(73276,135,20444,'/uploads/79b/8fc/a09/26fd33ba7be8e0fa3850.jpg',180),(73277,135,20445,'/uploads/064/bba/b8f/27eb6422dfa3f7372104.jpg',180),(73278,135,20446,'/uploads/402/806/1de/38e0efa731d054913436.jpg',180),(73279,135,20447,'/uploads/2a2/e25/061/cd8fc7355367fe184127.jpg',180),(73280,135,20448,'/uploads/105/e89/749/bae4503081f47b8ea7d3.jpg',180),(73281,150,20441,'/uploads/58b/151/6b6/ec22a653fb34d1925904.jpg',200),(73282,135,20449,'/uploads/2af/03c/2ac/7dda46ed91fc237f0565.jpg',180),(73283,135,20450,'/uploads/ee3/aad/e48/20de8f4a65ab84f2f115.jpg',180),(73284,135,20451,'/uploads/143/20b/bb1/c2e0299811266b2bd579.jpg',180),(73285,135,20452,'/uploads/bac/bd9/cdb/c3cd4b63fd545cc971a0.jpg',180),(73286,135,20453,'/uploads/e2c/65b/d64/bab41260c514153aeebf.jpg',180),(73287,135,20454,'/uploads/a2b/a03/e6b/ba9ceb9213a4e0823d3b.jpg',180),(73288,135,20455,'/uploads/bdd/5d9/500/c84dfb4d48ed7ea0e250.jpg',180),(73289,150,20449,'/uploads/6d3/f2e/0a8/5b77fb5c29f8deb5f4c9.jpg',200),(73290,210,20441,'/uploads/7c4/e2b/63f/4598422c0c2e236a090c.jpg',280),(73291,68,20442,'/uploads/905/e51/d07/40c3eecedcb7860ed1a9.jpg',90),(73292,68,20443,'/uploads/087/823/e7d/d610935620c635f7ae07.jpg',90),(73293,68,20444,'/uploads/962/fbb/7d7/e9894c06fdce359a00df.jpg',90),(73294,135,20456,'/uploads/504/f65/67f/8353e2973c851dc07d04.jpg',180),(73295,135,20457,'/uploads/0f7/d05/346/be142fd5fc7082b53101.jpg',180),(73296,135,20458,'/uploads/6a9/cc8/3e1/55c2d4f98b3342592f8d.jpg',180),(73297,135,20459,'/uploads/304/06a/b95/541435a0cd99ccc7ab5b.jpg',180),(73298,135,20460,'/uploads/3db/f1f/54f/414031bf93f2d4d865fe.jpg',180),(73299,135,20461,'/uploads/4de/fbf/5d5/4ba2cacd49caf055daf6.jpg',180),(73300,135,20462,'/uploads/ca2/2a7/6ab/b43455c8c8046d55391b.jpg',180),(73301,135,20463,'/uploads/b0a/ab1/e32/d74a92a955f1998c15f2.jpg',180),(73302,150,20456,'/uploads/b1c/1ba/27c/41edc98b10ff40edc9ae.jpg',200),(73303,210,20456,'/uploads/87e/c20/0dd/5f455e466591a0e46dba.jpg',280),(73304,68,20457,'/uploads/7c0/c7e/155/d439d3e969b02f3799bb.jpg',90),(73305,68,20458,'/uploads/0a9/a21/756/19a0929fd54b41862f99.jpg',90),(73306,68,20459,'/uploads/b86/973/a55/499df62ceadd1d7aa54f.jpg',90),(73307,210,20449,'/uploads/cfb/a41/dc9/83de6d0919e991333caf.jpg',280),(73308,68,20450,'/uploads/fe0/c3b/735/2a542c660b5367e4fe30.jpg',90),(73309,68,20451,'/uploads/7b0/46b/1d6/a56e9f5084d186ea196d.jpg',90),(73310,68,20452,'/uploads/af3/ff7/5c7/7b15a361903d749229bf.jpg',90),(73311,405,20441,'/uploads/5d4/83a/fcd/8ff642286b8f9a2ba4e2.jpg',540),(73312,132,20442,'/uploads/2eb/aa3/a76/6757203f4d2caab88716.jpg',176),(73313,132,20443,'/uploads/24e/92e/241/9065f115d4da8e3c1d37.jpg',176),(73314,132,20444,'/uploads/cda/2db/382/d4bc3e9a6dba60ed3ca7.jpg',176),(73315,132,20445,'/uploads/756/2c5/efc/89fae2c4000797060d11.jpg',176),(73316,132,20446,'/uploads/327/334/f29/d7d9152667c554ce0c2d.jpg',176),(73317,132,20447,'/uploads/aea/464/87f/f9b2ca252aa9bd6cd925.jpg',176),(73318,132,20448,'/uploads/d37/e2f/144/d69cd676d8ee1a7a2c1a.jpg',176),(73319,405,20442,'/uploads/e31/ab9/52a/e7daca1d57180bc19ca2.jpg',540),(73320,405,20443,'/uploads/057/1f6/483/45e2cc5333991f36d11f.jpg',540),(73321,405,20444,'/uploads/f28/c4b/51e/fafdac998baab3b13aa4.jpg',540),(73322,405,20445,'/uploads/e05/a46/003/3ca085dab4e67514b1ff.jpg',540),(73323,405,20446,'/uploads/3df/bba/24d/e77fcbd55971b12c947e.jpg',540),(73324,405,20447,'/uploads/c8e/050/f9a/08289a92d30d446e59af.jpg',540),(73325,405,20448,'/uploads/a78/958/b87/dcc46c460fa1d94f0c44.jpg',540),(73326,405,20449,'/uploads/680/1cf/906/b589ee82e7810510b613.jpg',540),(73327,132,20450,'/uploads/07a/197/725/947069f56200436ec8bc.jpg',176),(73328,132,20451,'/uploads/4f0/6e8/e18/46e0aa166978bc0e4335.jpg',176),(73329,132,20452,'/uploads/240/0e9/01f/53cd20623854b768f1a1.jpg',176),(73330,132,20453,'/uploads/2b6/721/067/40ad85195ff9c603121d.jpg',176),(73331,132,20454,'/uploads/4ed/41f/e0e/aa7b728e74e0c8e06da2.jpg',176),(73332,132,20455,'/uploads/a24/c45/aa9/8c53295ba30eeeba915e.jpg',176),(73333,405,20450,'/uploads/d10/943/05a/ea829b69f99e5bdf2e4b.jpg',540),(73334,405,20451,'/uploads/121/0b0/ab9/90d44b566b797e78f900.jpg',540),(73335,405,20452,'/uploads/0f8/566/54d/d8bd15b3e441189b342c.jpg',540),(73336,405,20453,'/uploads/4b2/534/a59/30c1e6744958845fb2f0.jpg',540),(73337,405,20454,'/uploads/48c/5e3/8c9/2cc94822e6d77739b375.jpg',540),(73338,405,20455,'/uploads/b4f/6d5/cd9/8813f8d652bd6560e8ba.jpg',540),(73339,405,20395,'/uploads/667/6e2/f95/949ed954bf7f489d72bd.jpg',540),(73340,132,20396,'/uploads/ff1/b1a/d33/e67a3299c607e1c8c89e.jpg',176),(73341,132,20397,'/uploads/c09/d6b/5c4/928d1fd553bc6f42d485.jpg',176),(73342,132,20398,'/uploads/cd1/8f8/e8a/a52a7f4ab6cf4c5b6218.jpg',176),(73343,132,20399,'/uploads/51a/503/0e4/95a972938a4620fac5ad.jpg',176),(73344,132,20400,'/uploads/2d3/9d0/36d/53dc895305221d846fa3.jpg',176),(73345,132,20401,'/uploads/7f0/f07/ee2/eec006976b4049eabafd.jpg',176),(73346,132,20402,'/uploads/fbf/211/c9e/3b2d3ae8f41af01bb2d2.jpg',176),(73347,405,20396,'/uploads/572/71a/2d9/f679dbeedaf2b7e8d159.jpg',540),(73348,405,20397,'/uploads/caa/c8a/d4c/08e3c4ea41d542fd0ec3.jpg',540),(73349,405,20398,'/uploads/503/1ae/e0c/620d2a3355a968b653df.jpg',540),(73350,405,20399,'/uploads/9ca/b99/6b6/c66d9cbe76d0a6c76fe5.jpg',540),(73351,405,20400,'/uploads/054/987/505/005ffd0291a481105ba0.jpg',540),(73352,405,20401,'/uploads/da5/0e5/572/d6019b61fcced42d8fb5.jpg',540),(73353,405,20402,'/uploads/511/e0f/6de/34cede5d73bf91e14240.jpg',540),(73384,405,20456,'/uploads/aea/2b2/2ee/dfe115c10eb07788e4e1.jpg',540),(73385,132,20457,'/uploads/99f/7c2/9b8/9a9d293f9dbcac89f6c2.jpg',176),(73386,132,20458,'/uploads/8bf/585/e6f/ee3d835eafdb3a7d3b0a.jpg',176),(73387,132,20459,'/uploads/73e/280/0d9/0eda2f9604cacdafe622.jpg',176),(73388,132,20460,'/uploads/cc7/905/0d0/4d5fb9deefc4d0f1df7a.jpg',176),(73389,132,20461,'/uploads/a2b/eab/1df/6e606ea0999e6f6f998c.jpg',176),(73390,132,20462,'/uploads/581/620/e5c/a4701e23798b40fefda6.jpg',176),(73391,132,20463,'/uploads/302/85f/a34/aad626b986e109e23b85.jpg',176),(73392,405,20457,'/uploads/751/b89/6d9/54fe7f4dfc5c956ca7e8.jpg',540),(73393,405,20458,'/uploads/c7d/36f/65a/8c3854ef8228cba966aa.jpg',540),(73394,405,20459,'/uploads/201/8e1/082/f4d0fe2dc061d1aea2a2.jpg',540),(73395,405,20460,'/uploads/394/e2e/619/88a89beb3246ff3aa1f7.jpg',540),(73396,405,20461,'/uploads/f55/09f/532/f81cd85dcff4c435373a.jpg',540),(73397,405,20462,'/uploads/b98/bfd/312/74aa5ccc11ba75945b4b.jpg',540),(73398,405,20463,'/uploads/d27/47f/d05/c89aa5db182afc272e62.jpg',540),(73399,405,20425,'/uploads/0e2/a9e/9de/9a26a79cfff37ffba8d1.jpg',540),(73400,132,20426,'/uploads/60b/ab7/a2e/00e7b8ee320d9b4ab1f7.jpg',176),(73401,132,20427,'/uploads/08c/3a6/27d/a7c004981dada2f32209.jpg',176),(73402,132,20428,'/uploads/ddc/929/3f8/d7726129f187f5b2190a.jpg',176),(73403,132,20429,'/uploads/cdf/28a/b68/cb8848f0ddcb4328d7be.jpg',176),(73404,132,20430,'/uploads/a9b/641/09c/6e0078020dbe9dc36ce0.jpg',176),(73405,132,20431,'/uploads/bf9/bb3/364/8f735f371797f726fb37.jpg',176),(73406,132,20432,'/uploads/17f/d95/134/b75206817402e276ea51.jpg',176),(73407,405,20426,'/uploads/dd8/964/8b2/0d574848e359e7fa332f.jpg',540),(73408,405,20427,'/uploads/6f0/44d/47c/0da1bd02595310fb9b66.jpg',540),(73409,405,20428,'/uploads/b02/108/44c/288bc70cb80989e96136.jpg',540),(73410,405,20429,'/uploads/c6a/c1b/e56/23b5f7397e80ee44e48b.jpg',540),(73411,405,20430,'/uploads/149/9c3/bb3/962598176bd0dcb9fae0.jpg',540),(73412,405,20431,'/uploads/5cc/1f6/5db/4690001f62acb53f70dd.jpg',540),(73413,405,20432,'/uploads/8f5/935/3e3/f0d984a0b5a9f0b115f1.jpg',540),(73414,405,20433,'/uploads/955/e91/5f2/c57e75393d7002f35575.jpg',540),(73415,132,20434,'/uploads/821/abd/3b4/05e178653086785a41cf.jpg',176),(73416,132,20435,'/uploads/e55/c16/bd8/8a03035699339f4d0c23.jpg',176),(73417,132,20436,'/uploads/719/03c/dc6/b3673fd8726b70a97beb.jpg',176),(73418,132,20437,'/uploads/9fa/95c/898/b7278b5e246c1ba5efa5.jpg',176),(73419,132,20438,'/uploads/adb/cd5/da8/e5748599eaebd17af964.jpg',176),(73420,132,20439,'/uploads/9c0/43a/4de/1bc41c144d9ae6af9e72.jpg',176),(73421,132,20440,'/uploads/ef3/d2a/29d/aa798c0a7622a486af39.jpg',176),(73422,405,20434,'/uploads/722/780/913/e3498c9da1507117c94d.jpg',540),(73423,405,20435,'/uploads/2cf/b3c/916/980373bcd260b06617d6.jpg',540),(73424,405,20436,'/uploads/eea/53b/7b8/68475ea74a2fb72ea6ae.jpg',540),(73425,405,20437,'/uploads/9c2/fe6/164/f920550558633861970c.jpg',540),(73426,405,20438,'/uploads/255/b20/a57/75ed97eff8c834b4be3b.jpg',540),(73427,405,20439,'/uploads/d30/a54/5a4/34a27808a02bc70e4228.jpg',540),(73428,405,20440,'/uploads/c75/a26/e16/faec7f6a18c2f4409eda.jpg',540),(73430,132,20425,'/uploads/c18/5b8/4b5/65b958b976b6e41fe5ae.jpg',176),(73431,132,20433,'/uploads/3e5/747/8cd/e4915d436410ffe6365d.jpg',176),(73432,132,20441,'/uploads/f23/faa/7e3/1e283a6d7ae4a2b605ef.jpg',176),(73433,132,20449,'/uploads/25f/d10/6fd/fbe091f9cc3a76b11f2f.jpg',176),(73434,132,20456,'/uploads/833/9d1/d40/208ddf328a8d06aff37e.jpg',176),(73435,132,20411,'/uploads/b78/817/8ac/4d4ab649e77deef86d02.jpg',176),(73436,211,19777,'/uploads/f9b/cb9/ab7/ad05b3a03ba84981ece8.jpg',375),(73437,211,19863,'/uploads/f11/e7d/0b8/a78a419e00341d851065.jpg',375),(73438,211,20030,'/uploads/2ca/c56/dcd/98155391188d8661becf.jpg',375),(73439,211,20067,'/uploads/054/07a/b55/6079cd24710a02e00162.jpg',375),(73440,211,20021,'/uploads/edb/889/e75/e10eeed05838032b8522.jpg',375),(73441,211,20015,'/uploads/928/b3a/16e/734e9cc27e52d26a07c5.jpg',375),(73442,211,20051,'/uploads/a88/d6d/b4f/2fe43058fbe8dd34c2ac.jpg',375),(73443,211,19967,'/uploads/c89/7ce/001/60f0b138b21bc7579e23.jpg',375),(73444,211,20110,'/uploads/de5/b85/084/c21b4c70890296b5e45d.jpg',375),(73445,211,20101,'/uploads/f12/063/15a/8962cd4e916e000a2589.jpg',375),(73446,132,20379,'/uploads/0e3/33c/dad/678faa30ee792c2d3686.jpg',235),(73447,211,20373,'/uploads/49c/b6b/d2e/cde3789b70ed04c8405c.jpg',375),(73448,211,20243,'/uploads/abd/631/38e/8b1584c7f6ff44e9f3bd.jpg',375),(73449,211,20202,'/uploads/e97/f99/be2/38b011103d7d83dc5976.jpg',375),(73450,211,20253,'/uploads/5a8/6ca/256/a41c354284cf2f4c9ad5.jpg',375),(73451,211,20317,'/uploads/9ad/8f0/e32/28ec4fe98fc6ed8398f6.jpg',375),(73452,211,20208,'/uploads/ea7/b2d/dd6/909db6921477789caf1e.jpg',375),(73453,211,20418,'/uploads/158/7e3/0af/3371dcce95ccc9e24c65.jpg',375),(73454,211,20260,'/uploads/31d/150/44d/67a20dc0a9171b67da19.jpg',375),(73455,211,20425,'/uploads/4d3/7b2/afc/a165294a1b2c8fc7a763.jpg',375),(73456,211,20433,'/uploads/867/db7/277/31fba4db5cd7e91b27fe.jpg',375),(73457,211,20325,'/uploads/f63/d7f/d6d/800735ddd6c1e24c00a2.jpg',375),(73458,211,20360,'/uploads/845/186/b7f/e9e3050d9590678588f5.jpg',375),(73459,211,20268,'/uploads/bba/60d/3f9/cb2cc8d0178bed437db8.jpg',375),(73460,211,20292,'/uploads/114/271/63d/d31c4c2c28b0522b4d54.jpg',375),(73461,211,20449,'/uploads/7b4/5ff/8ed/43b4959ca533f195d1f4.jpg',375),(73462,211,20367,'/uploads/589/7de/32a/46bde919c427a8d95d8a.jpg',375),(73463,132,20067,'/uploads/039/bca/354/5b26e09c629e73aeb353.jpg',235),(73464,132,20021,'/uploads/2d3/f80/34f/e682cab398ded3248cc1.jpg',235),(73465,132,19777,'/uploads/6db/fc6/009/89addab9d41751dc0090.jpg',235),(73466,132,20411,'/uploads/146/504/6d6/dd0f9b198969cd1aa96b.jpg',235),(73467,132,20051,'/uploads/a22/e33/137/6da965e6810737031722.jpg',235),(73468,132,20075,'/uploads/c7c/3dc/0f0/793a4fac273c140f3a3e.jpg',235),(73469,132,20243,'/uploads/268/3ad/707/0afdf222d79be0a5e12a.jpg',235),(73470,132,20030,'/uploads/d31/c63/d6b/ef78a9e1e302bb4552ea.jpg',235),(73471,132,20202,'/uploads/717/368/c14/a477a5dd069950ba025a.jpg',235),(73472,132,20281,'/uploads/a4d/87b/56c/26fac7faeab85439ed27.jpg',235),(73473,132,20253,'/uploads/733/c7e/edb/568c17889fc87d0c0e2c.jpg',235),(73474,132,20317,'/uploads/f5a/2a3/32d/6895e69faf6a2d78a966.jpg',235),(73475,132,20208,'/uploads/8fd/65a/090/6cd7fcafa031d60ba09e.jpg',235),(73476,132,20418,'/uploads/f35/2f9/fc0/298f248416cb412548d0.jpg',235),(73477,132,20043,'/uploads/745/739/0eb/d7b47627786907e3d419.jpg',235),(73478,132,20425,'/uploads/4f7/4e8/bc3/d02d84d6157c6b7822c9.jpg',235),(73479,132,20433,'/uploads/b8e/e0b/c1b/38c46b185d35daf724f1.jpg',235),(73480,132,19814,'/uploads/7ac/46f/692/5dc394ceb1c3b48ec180.jpg',235),(73481,132,20325,'/uploads/741/4f2/0d5/71fa035ddfcf95216413.jpg',235),(73482,132,20360,'/uploads/1f5/c4d/fee/ec81a38fbff4118a24e1.jpg',235),(73483,132,20441,'/uploads/914/a50/10b/b1a6cdee85fe155e2184.jpg',235),(73484,132,20268,'/uploads/a00/938/ba7/22270e9064437930d8f3.jpg',235),(73485,132,20292,'/uploads/124/3a7/e02/81819431886ded514073.jpg',235),(73486,132,20449,'/uploads/91a/c0e/616/a07de1eefcd6daa76ac5.jpg',235),(73487,132,20367,'/uploads/c85/cfb/97e/7d345a1e21f388663741.jpg',235),(73488,132,20110,'/uploads/78d/be7/567/ba68c3934b3acda2cf33.jpg',235),(73489,68,20110,'/uploads/8b5/fe1/5f3/2bfd807b92e01ddf0bbb.jpg',121),(73490,132,20101,'/uploads/556/c8c/97b/fc83bce73527b21f7416.jpg',235),(73491,68,19777,'/uploads/c86/236/3c8/70b0e9cda1b5c996b368.jpg',121),(73492,68,20101,'/uploads/aa4/8ca/6ee/cf5b4665beb0b0d3f1fa.jpg',121),(73493,68,20367,'/uploads/921/e91/373/441a8cc69c9a6fa2da4d.jpg',121),(73494,132,20333,'/uploads/1ad/a13/a0b/ba7ce447234a3024d183.jpg',235),(73495,68,19967,'/uploads/252/5f6/152/193785dc86d640831077.jpg',121),(73496,68,20253,'/uploads/586/227/218/bda5d62af5e55a96ae91.jpg',121),(73497,68,20325,'/uploads/8b1/abe/3ec/5fa0cf45ca342f78e49e.jpg',121),(73498,68,20425,'/uploads/796/f0a/85c/6e390bb6be4773ce8283.jpg',121),(73499,68,20208,'/uploads/c84/772/a39/c389e465849cea529323.jpg',121),(73500,68,20449,'/uploads/d18/eed/13e/cc022fdcc28adedfa32b.jpg',121),(73501,68,20373,'/uploads/f7e/a58/718/f2883d589ec4e7a3ef29.jpg',121),(73502,68,20202,'/uploads/e36/9b0/1b3/f9a4e05ea2b18794f58b.jpg',121),(73503,68,20030,'/uploads/96e/b85/0fc/0a0339eb99c9d21b110d.jpg',121),(73504,68,20021,'/uploads/abf/4fe/648/3fdfce11106bb7e0ccf1.jpg',121),(73505,68,19863,'/uploads/efd/451/0e3/edf1439d03d5bc42c094.jpg',121),(73506,68,20433,'/uploads/b76/a69/17a/aaf9ce9e63fcccc54578.jpg',121),(73507,68,20418,'/uploads/c9f/9ca/c47/be20f116a4e440181d2f.jpg',121),(73508,68,20051,'/uploads/1c7/916/dd6/a1160dc1b08131ddb94b.jpg',121),(73509,68,20243,'/uploads/802/ec5/3c0/b08373193aa67faecfaf.jpg',121),(73510,68,20067,'/uploads/1d1/72e/e3b/4dddff8116a9bc1bf8fc.jpg',121),(73511,68,20379,'/uploads/7d3/632/e7b/bdfdf9cb0cba07350c54.jpg',121),(73512,132,20418,'/uploads/35e/472/ea1/428f09041e81369428e3.jpg',176),(73513,132,20379,'/uploads/3d0/2fd/75e/9c11b43741c6c7f227f5.jpg',176),(73514,135,20466,'/uploads/c4a/433/072/b516427dd6eb70efa86f.jpg',180),(73515,135,20465,'/uploads/725/d9b/f3d/d80ae696d8fca10c0a84.jpg',180),(73516,135,20464,'/uploads/e45/3ee/0b2/d826f153857aec94a795.jpg',180),(73517,135,20471,'/uploads/5ed/f64/3f4/38efd8ff7717ef30ee2d.jpg',180),(73518,135,20469,'/uploads/29a/1f3/e44/3485cb7fc9f37ac1dc59.jpg',180),(73519,135,20467,'/uploads/80e/4c5/03a/8deb26948e899bc75151.jpg',180),(73520,135,20468,'/uploads/d6f/8e0/c06/615a369c408e5fd2e8b5.jpg',180),(73521,135,20470,'/uploads/2d0/09f/2a9/767ec445e84d7ff3ed37.jpg',180),(73522,150,20466,'/uploads/4a8/327/473/09b620d63299cb11159a.jpg',200),(73523,211,20466,'/uploads/874/6ee/3c9/6222c476f90ae862b6bb.jpg',375);
/*!40000 ALTER TABLE `resize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `cookie_id` int(11) DEFAULT NULL,
  `car_id` int(11) NOT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `cookie_id` (`cookie_id`,`car_id`),
  KEY `rev_fk_2` (`car_id`),
  CONSTRAINT `rev_fk_1` FOREIGN KEY (`cookie_id`) REFERENCES `cookie` (`cookie_id`) ON DELETE CASCADE,
  CONSTRAINT `rev_fk_2` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,2750,'2018-12-14 07:05:13'),(1,2836,'2018-12-14 07:06:40'),(1,2780,'2018-12-14 07:07:14'),(2,2851,'2018-12-14 07:14:57'),(3,2796,'2018-12-14 07:19:03'),(3,2851,'2018-12-14 07:19:48'),(5,2851,'2018-12-14 07:26:45'),(4,2851,'2018-12-14 07:27:24'),(7,2851,'2018-12-14 07:35:39'),(8,2746,'2018-12-14 07:43:18'),(8,2829,'2018-12-14 07:49:32'),(10,2836,'2018-12-14 09:42:59'),(6,2851,'2018-12-14 08:25:37'),(11,2851,'2018-12-14 07:56:17'),(11,2709,'2018-12-14 07:56:42'),(11,2836,'2018-12-14 07:57:22'),(14,2836,'2018-12-14 08:16:34'),(14,2851,'2018-12-14 08:17:03'),(15,2851,'2018-12-14 08:26:23'),(15,2836,'2018-12-14 08:32:05'),(20,2851,'2018-12-14 08:51:52'),(20,2718,'2018-12-14 08:54:18'),(6,2718,'2018-12-14 08:55:04'),(21,2809,'2018-12-14 08:56:37'),(24,2836,'2018-12-14 09:05:58'),(24,2730,'2018-12-14 09:05:53'),(19,2829,'2018-12-14 09:24:38'),(29,2851,'2018-12-14 10:26:28'),(30,2750,'2018-12-14 10:00:37'),(31,2851,'2018-12-14 09:39:05'),(32,2729,'2018-12-14 09:39:27'),(33,2848,'2018-12-14 09:42:12'),(33,2750,'2018-12-14 09:42:51'),(18,2836,'2018-12-14 09:54:49'),(18,2851,'2018-12-14 09:55:16'),(12,2851,'2018-12-14 09:57:30'),(35,2851,'2018-12-14 10:13:38'),(36,2836,'2018-12-14 10:18:31'),(36,2851,'2018-12-14 10:16:12'),(36,2843,'2018-12-14 10:18:08'),(36,2829,'2018-12-14 10:17:54'),(36,2811,'2018-12-14 10:17:59'),(29,2836,'2018-12-14 10:26:56'),(39,2851,'2018-12-14 10:46:46'),(38,2750,'2018-12-14 10:36:43');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rules`
--

DROP TABLE IF EXISTS `rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rules` (
  `topic` varchar(32) NOT NULL,
  `rules` text,
  PRIMARY KEY (`topic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rules`
--

LOCK TABLES `rules` WRITE;
/*!40000 ALTER TABLE `rules` DISABLE KEYS */;
INSERT INTO `rules` VALUES ('cookies','<p><strong>Політика використання файлів &laquo;cookie&raquo;</strong></p>\r\n\r\n<p>Використовуючи цей веб-сайт ви погоджуєтеся на використання файлів &laquo;cookie&raquo; та інших засобів відстеження з метою пропонування Вам адаптованих до Ваших інтересів послуг та пропозицій та ведення статистики відвідувань. Додаткова інформація та параметри налаштування файлів &laquo;cookie&raquo; вказані нижче.</p>\r\n\r\n<p><strong>1 &mdash; ЩО ТАКЕ файли &laquo;cookie&raquo;?</strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong> </strong></p>\r\n\r\n<p>Файл &laquo;cookie&raquo; &mdash; це невеликий фрагмент даних, що генерується та зберігається веб-браузером на комп&rsquo;ютері користувача під час перегляду цього веб-сайту користувачем. Кожного разу, коли користувач відкриває веб-сайт, веб-браузер надсилає файли &laquo;cookie&raquo; назад на сервер, щоб повідомити веб-сайт про попередню активність користувача. Файли &laquo;cookie&raquo; були розроблені як механізм, який дозволяє веб-сайтам запам&rsquo;ятовувати інформацію стосовно стану (наприклад, елементи в кошику для покупок) або зберігати історію переглядів користувача, зокрема натискання певних кнопок, входів у систему та сторінок, які користувач відвідав декілька місяців або навіть років тому.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Для оцінки ефективності роботи нашого веб-сайту ми використовуємо три типи файлів &laquo;cookie&raquo;:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>1.1 Технічні файли &laquo;cookie&raquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Вони необхідні для перегляду нашого веб-сайту, а також доступу до різних продуктів та інших послуг. Зокрема, технічні файли &laquo;cookie&raquo; дозволяють впізнавати Вас, повідомляти про відвідування Вами різних сторінок, а потім покращувати Ваш досвід використання нашого веб-сайту: наприклад, адаптувати веб-сайт таким чином, щоб він швидко реагував залежно від типу пристрою, який використовує веб-користувач (мови, які використовуються, роздільна здатність дисплея), паролів та іншої інформації зазначеної користувачем. Ці файли &laquo;cookie&raquo; дозволяють нам впроваджувати заходи безпеки, наприклад, коли вас знову просять ввести пароль, щоб підключитись до останньої сесії через певний час. Файли такого типу не можна вимкнути або налаштувати.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>1.2 Функціональні файли &laquo;cookie&raquo;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ми використовуємо функціональні файли &laquo;cookie&raquo; з метою оцінки продуктивності роботи різних сторінок та частин веб-сайту для їх оптимізації. Ці файли &laquo;cookie&raquo; дають нам можливість виявляти будь-які проблеми з переглядом веб-сайту та покращувати зручність його використання. За допомогою функціональних файлів &laquo;cookie&raquo; ведеться анонімна статистика та збір даних про кількість відвідувань. Строк зберігання для використання функціональних файлів &laquo;cookie&raquo; не перевищує 13 місяців.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>1.3 Аналітичні файли &laquo;cookie&raquo; </strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ці файли &laquo;cookie&raquo; дозволяють нам отримувати інформацію про використання та ефективність роботи нашого веб-сайту, формувати статистику, отримувати інформацію про обсяг трафіку та використання різних елементів нашого веб-сайту (контент, переходи за посиланнями), що, у свою чергу, дає нам можливість підвищити привабливість наших сервісів та покращити їх зручність.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;<strong>2 &mdash; СТОРОННІ ФАЙЛИ &laquo;COOKIE&raquo; </strong></p>\r\n\r\n<p>Використання сторонніх файлів &laquo;cookie&raquo; врегульовано політикою третіх осіб щодо використання таких файлів &laquo;cookie&raquo;. Ми завжди будемо інформувати Вас про такі файли &laquo;cookie&raquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;<strong>3 &mdash; ВІДМОВА / БЛОКУВАННЯ ФАЙЛІВ &laquo;COOKIE&raquo;</strong></p>\r\n\r\n<p>Погодження на використання файлів &laquo;cookie&raquo; здійснюється на Ваш власний розсуд, як кінцевого користувача. Ви можете у будь-який час вільно змінити налаштування файлів &laquo;cookie&raquo;. Якщо Ви погодились на їх використання у своєму браузері/пристрої, файли &laquo;cookie&raquo;, інтегровані у сторінки та контенти, які Ви переглядали, тимчасово зберігатимуться у певному місці:</p>\r\n\r\n<p><a href=\"http://windows.microsoft.com/en-au/windows-vista/Block-or-allow-cookieshttp:/windows.microsoft.com/en-au/windows-vista/Block-or-allow-cookies\"><strong>Якщо Ви використовуєте </strong><strong>Internet</strong><strong> </strong><strong>Explorer&nbsp;</strong></a></p>\r\n\r\n<p><a href=\"https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences\"><strong>Якщо Ви використовуєте </strong><strong>Mozilla</strong><strong> </strong><strong>Firefox&nbsp;</strong></a></p>\r\n\r\n<p><a href=\"https://support.apple.com/en-us/HT201265\"><strong>Якщо Ви використовуєте </strong><strong>Safari&nbsp;</strong></a></p>\r\n\r\n<p><a href=\"https://support.google.com/chrome/answer/95647?hl=en-GB&amp;hlrm=en&amp;safe=on\"><strong>Якщо Ви використовуєте </strong><strong>Google</strong><strong> </strong><strong>Chrome&nbsp;</strong></a></p>\r\n\r\n<p><a href=\"http://help.opera.com/Windows/10.20/en/cookies.htmll\"><strong>Якщо Ви використовуєте </strong><strong>Opera</strong></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Файли &laquo;cookie&raquo; програвача Adobe Flash Player</strong></p>\r\n\r\n<p>Adobe Flash Player &mdash; це безкоштовне програмне забезпечення для користування контентом, створеним на платформі Adobe Flash, зокрема переглядів мультимедійних даних, запуску повнофункціональних інтернет-додатків та відтворення потокового відео та аудіо. Flash Player може працювати у веб-браузері в якості плагіну браузера або на мобільних пристроях, які підтримують це програмне забезпечення<a href=\"https://en.wikipedia.org/wiki/Adobe_Flash_Player#cite_note-6\">[6]</a>. Flash Player був створений компанією Macromedia і був розроблений та розповсюджений компанією Adobe Systems після того, як компанія Adobe придбала компанію Macromedia.</p>\r\n\r\n<p>Flash Player має широку базу користувачів і є загальним форматом для ігор, анімації та графічних інтерфейсів користувача (GUI), вбудованих у веб-сторінки. Компанія Adobe заявляє, що понад 400 мільйонів з більш ніж 1 мільярда підключених персональних комп&rsquo;ютерів оновлюються до нової версії програвача Flash Player протягом шести тижнів після випуску.</p>\r\n\r\n<p>Отримати доступ до управління файлами &laquo;cookie&raquo; можна тут: <a href=\"http://www.adobe.com/\">http://www.adobe.com/</a></p>\r\n\r\n<p><a href=\"https://ico.org.uk/\">Натисніть тут для отримання докладнішої інформаціЇ про файли cookie</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>4 &mdash; ПОЛІТИКА</strong> <strong>КОНФІДЕНЦІЙНОСТІ</strong></p>\r\n\r\n<p>Для отримання додаткової інформації про те, як ми використовуємо інформацію, що зібрана за допомогою файлів &laquo;Cookies&raquo;, зверніться до нашої політики конфіденційності <a href=\"https://www.aldautomotive.com/data-privacy-policy\">https://www.aldautomotive.com/data-privacy-policy</a>, що опублікована на сайті. Ця Політика використання файлів &laquo;cookie&raquo; є частиною і включена в нашу політику конфіденційності. Користуючись сайтом, ви погоджуєтеся з цією політикою &laquo;cookie&raquo; та політикою конфіденційності.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>5 &mdash; ЗВ&#39;ЯЖІТЬСЯ З НАМИ</strong></p>\r\n\r\n<p>Якщо у вас є питання або зауваження з приводу цієї Політики використання файлів &laquo;cookie&raquo;, будь ласка, зв&#39;яжіться з нами за адресою: <a href=\"mailto:ua.compliance@aldautomotive.com\">ua.compliance@aldautomotive.com</a></p>\r\n'),('privacy','<p><strong>ЗАХИСТ ДАНИХ</strong></p>\r\n\r\n<p><strong>1.1 Збір</strong> <strong>даних</strong></p>\r\n\r\n<p><a name=\"_GoBack\"></a> В результаті користування даним веб-сайтом, ALD може прямо або опосередковано збирати персональні дані користувача, іноді шляхом використання <em>online</em> або <em>offline</em> форм. Ця інформація буде зберігатися і оброблятися ALD та / або його постачальниками чи консультантами інформаційних систем. Надання деяких персональних даних може бути обов&#39;язковим для того, щоб обробити запити користувача. В такому випадку, обов&#39;язкові поля будуть відзначені зірочкою або будь-яким іншим еквівалентом.</p>\r\n\r\n<p>Користувач має усі права щодо захисту його персональних даних, які передбачено чинним законодавством України, зокрема, Законом України &laquo;Про захист персональних даних&raquo;.</p>\r\n\r\n<p>Погоджуючись з даним розділом , користувач надає свою згоду на збирання та обробку ALD персональних даних, які надаються користувачем під час підписки на оновлення.</p>\r\n\r\n<p><strong>1.2 Використання</strong></p>\r\n\r\n<p>Зібрані дані будуть використовуватися для того, щоб повідомляти користувача про оновлення на ALD сайті.</p>\r\n\r\n<p>ALD здійснює обробку персональних даних користувача будь-якими способами з метою: надіслання користувачу оновлень сайту ALD.</p>\r\n\r\n<p>ALD включає дані користувача до бази персональних даних користувачів сайту з моменту, коли Користувач вносить електронну поштову адресу з метою підписки на оновлення від ALD, а також постійно, увесь період, протягом якого Користувач користується підпискою. Строк зберігання даних становить період, протягом якого Користувач користується підпискою, а також після відміни від підписки та закінчення використання сайту Користувачем.</p>\r\n\r\n<p><strong>1.3 Зв&#39;язок з третіми сторонами</strong></p>\r\n\r\n<p>Зібрані персональні дані, як було погоджено вище, можуть бути доступними дочірнім компаніям, партнерам, субпідрядникам ALD в обсязі необхідному для забезпечення мети обробки даних.</p>\r\n\r\n<p>ALD має право передати персональні дані, базу персональних даних, до якої включені персональні дані користувача, повністю або частково третім особам без повідомлення про це користувача, та виключно для досягнення мети для якої ці персональні дані було зібрано, у наступних випадках:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>особам, у ведення, володіння або власність яких передано Сайт; юридичній особі-правонаступнику ALD у разі реорганізації Товариства; особам, що є пов&rsquo;язаними/афілійованими з ALD; новому власнику та/або володільцю Сайту для оброблення з вказаною метою.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>1.4 Забезпечення безпеки обробки персональних даних.</strong></p>\r\n\r\n<p>ALD приймає відповідні фізичні, технічні та організаційні заходи, необхідні для забезпечення безпеки та конфіденційності персональних даних, зокрема, з метою захисту від втрати, руйнування, змін, і несанкціонованого доступу.</p>\r\n\r\n<p>Також ALD зобов&rsquo;язується у разі передачі даних третім особам здійснювати таку передачу тільки тим третім особам, які забезпечують захист даних аналогічний рівню захисту даних встановленого ALD.</p>\r\n\r\n<p><strong>1.5 Застосування європейських правил про передачу персональних даних за межами Європейського Союзу.</strong></p>\r\n\r\n<p>Беручи до уваги міжнародну діяльність групи Сосьете Женераль (до якої відноситься і ALD Automotive Україна) і для того, щоб оптимізувати якість обслуговування, передача інформації, що згадана вище, може включати передачу персональних даних як для країн в межах Європейської економічної зони так і для країн за межами Європейської економічної зони, чиє законодавство про захист персональних даних відрізняється від законодавства, що діє в межах Європейського Союзу, але рівень захисту даних не нижче рівня встановленого в межах Європейського Союзу, тобто країни забезпечують належний рівень захисту даних. Впродовж всього періоду нинішніх відносин і надання послуг, ALD може доручити певні послуги з оперативними функціями постачальникам послуг, обраних для їх експертизи та надійності як в межах так і за межами Європейського Союзу, але рівень захисту даних не нижче рівня встановленого в межах Європейського Союзу, тобто країни забезпечують належний рівень захисту даних, щоб забезпечити проведення цільових і обмежених ІТ-послуг (тести, розробки, технічне обслуговування).</p>\r\n\r\n<p><strong>1.6 Права доступу, модифікації та видалення</strong></p>\r\n\r\n<p>Користувач має право відписатись від оновлень будь коли та відкликати свою згоду на обробку персональних даних. Користувач може також на законних підставах, виступати проти обробки персональних даних користувача. Кожен, без пояснення свого рішення, може відмовитися від надання інформації, що його стосується, для використання третій стороні в комерційних цілях.</p>\r\n\r\n<p>Усі права користувача пов&rsquo;язані із захистом його персональних даних викладено у ст.8 ЗУ &laquo;Про захист персональних даних&raquo;.</p>\r\n');
/*!40000 ALTER TABLE `rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salon`
--

DROP TABLE IF EXISTS `salon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salon` (
  `salon_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_car_id` int(11) DEFAULT '0',
  `salon_date` varchar(50) DEFAULT NULL,
  `salon_phone` varchar(50) DEFAULT NULL,
  `salon_read` tinyint(1) DEFAULT '0',
  `salon_time` varchar(50) DEFAULT NULL,
  `salon_name` text,
  PRIMARY KEY (`salon_id`),
  KEY `salon_car_id` (`salon_car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salon`
--

LOCK TABLES `salon` WRITE;
/*!40000 ALTER TABLE `salon` DISABLE KEYS */;
/*!40000 ALTER TABLE `salon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo`
--

DROP TABLE IF EXISTS `seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_description` text,
  `seo_keywords` text,
  `seo_text` text,
  `seo_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo`
--

LOCK TABLES `seo` WRITE;
/*!40000 ALTER TABLE `seo` DISABLE KEYS */;
INSERT INTO `seo` VALUES (1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slide`
--

DROP TABLE IF EXISTS `slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slide` (
  `slide_id` int(11) NOT NULL AUTO_INCREMENT,
  `slide_image_id` int(11) DEFAULT '0',
  `slide_order` tinyint(1) DEFAULT '1',
  `slide_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`slide_id`),
  KEY `slide_image_id` (`slide_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slide`
--

LOCK TABLES `slide` WRITE;
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;
/*!40000 ALTER TABLE `slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `slider_name` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  PRIMARY KEY (`slider_id`),
  KEY `img_id` (`img_id`),
  KEY `idx_slider_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (13,3569,1,'',''),(14,20199,1,'Спеціальні умови для дилерів!','http://www.aldcarmarket.com.ua/dileram'),(15,5251,0,'встреча с дилерами','https://www.facebook.com/ald.carmarket.ukraine/posts/542113119316572'),(18,20298,0,'BLACK FRIDAY',''),(19,15855,0,'NY2018',''),(22,18471,0,'Vacation','');
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `smart`
--

DROP TABLE IF EXISTS `smart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smart` (
  `smart_id` int(11) NOT NULL AUTO_INCREMENT,
  `smart_name` varchar(255) DEFAULT NULL,
  `smart_order` int(11) DEFAULT '0',
  `smart_status` int(11) DEFAULT '1',
  PRIMARY KEY (`smart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smart`
--

LOCK TABLES `smart` WRITE;
/*!40000 ALTER TABLE `smart` DISABLE KEYS */;
INSERT INTO `smart` VALUES (1,'Економічне авто',1,0),(2,'Авто середнього класу',2,1),(3,'Бюджетне авто',3,1),(4,'Статусне авто',4,1),(5,'Позашляховик',5,1);
/*!40000 ALTER TABLE `smart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_code` varchar(64) NOT NULL,
  `subscription_email` varchar(255) NOT NULL,
  `subscription_last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `subscription_code` (`subscription_code`),
  UNIQUE KEY `subscription_email` (`subscription_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (1,'d8dae3c5b0632d582cc8e88ef1d97ca8','o.chechyk@greenlight-cs.com','2018-12-20 13:31:22',1),(2,'4410a23f5de56fa2cdc068321c30cf7a','nadiia.zelentsova@aldautomotive.com','2018-12-20 13:31:22',1);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmission`
--

DROP TABLE IF EXISTS `transmission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmission` (
  `transmission_id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `transmission_name` varchar(255) DEFAULT NULL,
  `transmission_status` tinyint(1) DEFAULT '1',
  `transmission_import_key` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`transmission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmission`
--

LOCK TABLES `transmission` WRITE;
/*!40000 ALTER TABLE `transmission` DISABLE KEYS */;
INSERT INTO `transmission` VALUES (1,'Автомат',1,'A/T'),(2,'Механіка',1,'M/T');
/*!40000 ALTER TABLE `transmission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_created` int(11) DEFAULT '0',
  `user_login` varchar(50) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `user_role` int(11) DEFAULT '0',
  `user_status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1454662970,'admin','be147e9eaa6057702254fcf29ac4d67d',3,1),(2,1454662970,'Denys Fedash','ff094413d9f29b5ac8bfdb5192e347fe',2,1),(3,1540903958,'Anton Chukur','fff2bc6e5abde803c8f5bd61fe0229af',1,1),(4,1540909383,'joliot','8d9d2d1e527ea347a01d39fa5e48fce0',1,1),(5,1543316327,'aleks-ks','c4ca4238a0b923820dcc509a6f75849b',3,1),(6,0,'Roman Mazur','0ceb3a835838a94194c82399f28fb40b',2,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-13 18:40:25
