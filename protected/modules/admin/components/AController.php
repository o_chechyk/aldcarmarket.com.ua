<?php

class AController extends Controller
{
    public $layout          = '//layouts/admin';
    public $rus             = array('А','Б','В','Г','Д','Е','Є','Ё','Ж','З','И','І','Ї','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','а','б','в','г','д','е','є','ё','ж','з','и','і','ї','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',' ','(',')',',','.',':',';','"',"'",'!','@','#','$','%','^','&','*','-','=','+','<','>','\\','|','№','/','`','~','«','»');
    public $lat             = array('a','b','v','g','d','e','e','e','gh','z','i','i','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya','a','b','v','g','d','e','e','e','gh','z','i','i','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya','_','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
    public $success_message = 'Зміни успішно збережені';
    public $user_role;

    public function beforeAction($action)
    {
        if (Yii::app()->user->isGuest)
        {
            $this->redirect('/admin/auth');
        }
        else
        {
            $c_user = new UserModel();
            $o_user = $c_user->findByPk(Yii::app()->user->id);

	    if( !isset($o_user) or
		$o_user['user_status'] == 0) { // disabled user
		Yii::app()->user->logout();
		$this->redirect('/admin/auth');
	    }

            if (($o_user['user_role']&1) or ($o_user['user_role']&2))
            {
		Yii::app()->db->createCommand("SET @user_id=".Yii::app()->user->id)->execute();
                return true;
            }
            else
            {
                Yii::app()->user->logout();
                $this->redirect('/admin/auth');
            }
        }
    }
}