<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php if($rules['topic']) echo "Редагування Правил ".$rules['topic']; else echo "Нові Правила"; ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/rules" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">


<?php if (Yii::app()->user->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable text-center">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php print Yii::app()->user->getFlash('error'); ?>
    </div>
<?php } ?>

        <form method="POST">

            <div class="tab-content">

                <div class="tab-pane fade in active" id="main">
		    <div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover">
	                    <tr>
				<td class="col-lg-3">
				    <?php if($rules->topic) $str=" readonly"; else $str=""; ?>
				    <input type="text" name="topic" value="<?php echo $rules->topic; ?>" required<?=$str?>>
				</td>
	                        <td>
	                            <textarea
					 class="ckeditor"
	                                name="rules"
	                                rows="3"
	                            required><?php echo $rules->rules; ?></textarea>
	                        </td>
	                    </tr>
	                </table>
		    </div> <!-- data wrapper -->
		</div> <!-- main tab -->

	    </div> <!-- tab-content -->

            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти">
            </p>
        </form>
    </div>
</div>
<!-- /.row -->





