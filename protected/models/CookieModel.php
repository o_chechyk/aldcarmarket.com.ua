<?php

class CookieModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cookie';
    }

    public function primaryKey()
    {
        return 'cookie_id';
    }
}
