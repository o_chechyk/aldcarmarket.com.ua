<?php
    if( ! $user ) { echo "<script>document.location = '/admin/users'</script>"; exit; }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редагувати користувача: <?=$user['user_login']?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/users" class="link-img link-plus">
                <button type="button" class="btn btn-info">
                    <i class="fa fa-list"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php if (Yii::app()->user->hasFlash('success')) { ?>
    <div class="message-success">
	<?php print Yii::app()->user->getFlash('success'); ?>
    </div>
<?php } ?>
<?php if (Yii::app()->user->hasFlash('error')) { ?>
    <div class="message-error">
	<?php print Yii::app()->user->getFlash('error'); ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <form method="POST">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Ім'я</td>
                        <td>
                            <input
                                class="form-control"
                                name="login"
                                type="text"
                                value="<?=$user['user_login']?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Роль</td>
                        <td>
			    <input type="hidden" id="user_role" name="user_role" value="<?=$user['user_role']?>">
                            <?php
				$roles = array();
				if($user['user_role'] & 1) {
				    $roles[] = "Administrator";
				}
				if($user['user_role'] & 2) {
				    $roles[] = "Super user";
				}
				echo "<input type=\"text\" id=\"roles_text\" value=\"".implode(',', $roles)."\" disabled>";
			    ?>
			    <button type="button" class="btn btn-info" onclick="editRole()">
				<i class="fa fa-pencil"></i>
			    </button>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Пароль</td>
                        <td>
                            <input
                                class="form-control"
                                name="pass1"
                                type="password"
                                value="No password here"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Пароль ще раз</td>
                        <td>
                            <input
                                class="form-control"
                                name="pass2"
                                type="password"
                                value="No password here"
                            />
                        </td>
                    </tr>
		    <tr>
			<td class="col-lg-3">Статус</td>
			<td>
			    <?php
				if( $user['user_status'] ) {
				    $status1 = " selected";
				    $status0 = "";
				} else {
				    $status0 = " selected";
				    $status1 = "";
				}
			    ?>
			    <select class="form-control" name="user_status">
				<option value="0"<?=$status1?>>Неактивний</option>
				<option value="1"<?=$status1?>>Активний</option>
			    </select>
			</td>
		    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div id="role_dialog" title="Редагування ролей користувача" hidden>
    <input type="checkbox" id="role_administrator">
    <label for="role_administrator">Administrator</label>
    <br>
    <input type="checkbox" id="role_superuser">
    <label for="role_superuser">Super user</label>
</div>
