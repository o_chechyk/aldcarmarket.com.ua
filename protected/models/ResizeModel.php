<?php

class ResizeModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'resize';
    }

    public function primaryKey()
    {
        return 'resize_id';
    }
}