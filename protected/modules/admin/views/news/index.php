<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="text-center">
            <a href="/admin/news/create" class="link-img link-plus">
                <button type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </button>
            </a>
        </p>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover" id="data-table">
                <thead>
                    <tr>
                        <th>Назва</th>
                        <th class="col-lg-1">Статус</th>
                        <th class="col-lg-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($a_news as $item) { ?>
                        <tr>
                            <td>
                                <span style="display:none;">
                                    <?php print $item['news_date']; ?>
                                </span>
                                <?php print $item['news_name']; ?>
                            </td>
                            <td>
                                <a href="/admin/news/status/<?php print $item['news_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['news_status']) { ?>danger<?php } else { ?>success<?php } ?> btn-circle">
                                        <i class="fa fa-power-off"></i>
                                    </button>
                                </a>
                            </td>
                            <td>
                                <a href="/admin/news/edit/<?php print $item['news_id']; ?>">
                                    <button type="button" class="btn btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="/admin/news/delete/<?php print $item['news_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->