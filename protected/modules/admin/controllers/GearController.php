<?php

class GearController extends AController
{
    public function actionIndex()
    {
        $a_gear     = GearModel::model()->findAll();
        $h1         = 'Привід';

        $this->render('index', array('a_gear' => $a_gear, 'h1' => $h1));
    }

    public function actionCreate()
    {
        if (isset($_POST['data']))
        {
            $c_gear = new GearModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_gear->$key = $value;
            }

            $c_gear->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/gear');
            exit;
        }

        $h1 = 'Редагування приводу';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_gear     = GearModel::model()->findByPk($id);

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_gear->$key = $value;
            }

            $o_gear->save();

            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/gear');
            exit;
        }

        $h1 = 'Редагування приводу';

        $this->render('form', array('o_gear' => $o_gear, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        GearModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/gear');
    }
}