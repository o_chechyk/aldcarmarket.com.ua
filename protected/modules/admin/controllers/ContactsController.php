<?php

class ContactsController extends AController
{
    public function actionIndex()
    {
        $o_contacts = ContactsModel::model()->findByPk(1);
        $h1         = 'Контакти';

        $this->render('index', array('o_contacts' => $o_contacts, 'h1' => $h1));
    }

    public function actionEdit()
    {
        if (isset($_POST['data']))
        {
            $o_contacts = ContactsModel::model()->findByPk(1);

            foreach ($_POST['data'] as $key => $value)
            {
                $o_contacts->$key = $value;
            }

            $o_contacts->save();

            Yii::app()->user->setFlash('success', $this->success_message);
        }

        $this->redirect('/admin/contacts');
    }
}