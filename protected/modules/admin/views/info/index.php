<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="POST" action="/admin/info/edit">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <td class="col-lg-3">Посилання facebook</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[info_facebook]"
                                type="text"
                                value="<?php if (isset ($o_info)) { print $o_info['info_facebook']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Посилання instaram</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[info_instagram]"
                                type="text"
                                value="<?php if (isset ($o_info)) { print $o_info['info_instagram']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">Графік роботи, через кому</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[info_time]"
                                type="text"
                                value="<?php if (isset ($o_info)) { print $o_info['info_time']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">SEO title</td>
                        <td>
                            <input
                                class="form-control"
                                name="data[seo_title]"
                                type="text"
                                value="<?php if (isset ($o_info)) { print $o_info['seo_title']; } ?>"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>SEO description</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_description]"
                            ><?php if (isset ($o_info)) { print $o_info['seo_description']; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>SEO keywords</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_keywords]"
                            ><?php if (isset ($o_info)) { print $o_info['seo_keywords']; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>SEO text</td>
                        <td>
                            <textarea
                                class="form-control"
                                name="data[seo_text]"
                            ><?php if (isset ($o_info)) { print $o_info['seo_text']; } ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <p class="text-center">
                <input type="submit" class="btn btn-default text-center" value="Зберегти" />
            </p>
        </form>
    </div>
</div>