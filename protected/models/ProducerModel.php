<?php

class ProducerModel extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'producer';
    }

    public function primaryKey()
    {
        return 'producer_id';
    }
}