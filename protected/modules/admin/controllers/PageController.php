<?php

class PageController extends AController
{
    public function actionIndex()
    {
        $a_page  = PageModel::model()->findAll();
        $h1     = 'Сторінки';

        $this->render('index', array('a_page' => $a_page, 'h1' => $h1));
    }

    public function actionCreate()
    {
        $redirect = 0;

        if (isset($_POST['data']))
        {
            $c_page = new PageModel();

            foreach ($_POST['data'] as $key => $value)
            {
                $c_page->$key = $value;
            }

            $c_page->page_url = str_replace($this->rus, $this->lat, $c_page->page_name);
            $c_page->save();

            $page_id    = $c_page->page_id;
            $id         = $c_page->page_id;

            $redirect = 1;
        }

        for ($i=1; $i<=3; $i++)
        {
            if (isset($_FILES['image_' . $i]['name']) &&
                !empty($_FILES['image_' . $i]['name']))
            {
                $image = $_FILES['image_' . $i];

                $ext  = $image['name'];
                $ext  = explode('.', $ext);
                $ext  = end($ext);
                $file = $image['tmp_name'];

                $image_url = ImageIgosja::put_file($file, $ext);

                $o_image = new ImageModel();
                $o_image->image_url = $image_url;
                $o_image->save();

                $image_id = $o_image->image_id;

                $page_image = 'page_image_' . $i;
                $o_page = PageModel::model()->findByPk($page_id);
                $o_page->$page_image = $image_id;
                $o_page->save();

                $redirect = 1;
            }
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/page');
            exit;
        }

        $h1 = 'Редагування сторінки';

        $this->render('form', array('h1' => $h1));
    }

    public function actionEdit($id)
    {
        $id         = (int) $id;
        $o_page     = PageModel::model()->findByPk($id);
        $redirect   = 0;

        for ($i=1; $i<=3; $i++)
        {
            if (isset($_GET['image_' . $i]))
            {
                $image_id = (int) $_GET['image_' . $i];

                $o_image = ImageModel::model()->findByPk($image_id);

                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
                }

                ImageModel::model()->deleteByPk($image_id);

                $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

                foreach ($a_resize as $item)
                {
                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
                    {
                        unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
                    }
                }

                ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));

                $redirect = 1;
            }
        }

        if (isset($_POST['data']))
        {
            foreach ($_POST['data'] as $key => $value)
            {
                $o_page->$key = $value;
            }

            $o_page->save();

            $redirect = 1;
        }

        for ($i=1; $i<=3; $i++)
        {
            if (isset($_FILES['image_' . $i]['name']) &&
                !empty($_FILES['image_' . $i]['name']))
            {
                $image = $_FILES['image_' . $i];

                $ext  = $image['name'];
                $ext  = explode('.', $ext);
                $ext  = end($ext);
                $file = $image['tmp_name'];

                $image_url = ImageIgosja::put_file($file, $ext);

                $o_image = new ImageModel();
                $o_image->image_url = $image_url;
                $o_image->save();

                $image_id = $o_image->image_id;

                $page_image = 'page_image_' . $i;
                $o_page = PageModel::model()->findByPk($id);
                $o_page->$page_image  = $image_id;
                $o_page->save();

                $redirect = 1;
            }
        }

        if (1 == $redirect)
        {
            Yii::app()->user->setFlash('success', $this->success_message);

            $this->redirect('/admin/page');
            exit;
        }

        $h1 = 'Редагування сторінки';

        $this->render('form', array('o_page' => $o_page, 'h1' => $h1));
    }

    public function actionDelete($id)
    {
        $id       = (int) $id;
        $o_page   = PageModel::model()->findByPk($id);

        for ($i=1; $i<=3; $i++)
        {
            $page_image = 'page_image_' . $i;
            $image_id   = $o_page->$page_image;
            $o_image    = ImageModel::model()->findByPk($image_id);

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']))
            {
                unlink($_SERVER['DOCUMENT_ROOT'] . $o_image['image_url']);
            }

            ImageModel::model()->deleteByPk($image_id);

            $a_resize = ResizeModel::model()->findAllByAttributes(array('resize_image_id' => $image_id));

            foreach ($a_resize as $item)
            {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $item['resize_url']);
                }
            }

            ResizeModel::model()->deleteAllByAttributes(array('resize_image_id' => $image_id));
        }

        PageModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/page');
    }
}