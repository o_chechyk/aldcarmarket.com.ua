<?php

class QuestionController extends AController
{
    public function actionIndex()
    {
        $a_question  = QuestionModel::model()->with('q_manager')->findAll(array('order' => 'question_read'));
        $h1     = 'Зворотній зв`язок';

        $this->render('index', array('a_question' => $a_question, 'h1' => $h1));
    }

    public function actionAnswer($id)
    {
        $id         = (int) $id;
        $o_question = QuestionModel::model()->findByPk($id);
//        QuestionModel::model()->updateByPk($id, array('question_read' => 1));

        if (isset($_POST['data']))
        {
            $answer     = $_POST['data']['answer'];
            $mail_to    = $o_question['question_email'];
            $subject    = 'Відповідь на Ваше питання на сайті ' . $_SERVER['HTTP_HOST'];
            $message    = $_POST['data']['answer'];;
            $header     = "From: <noreply@" . $_SERVER['HTTP_HOST'] . ">\r\n";
            $header    .= "Content-type:text/html; charset=utf-8\r\n";

            mail($mail_to, $subject, $message, $header);

//            Yii::app()->user->setFlash('success', $this->success_message);

	    // Save the reply text
	    $o_question['question_reply'] = mb_ereg_replace("\&nbsp\;", " ", $answer);
	    $o_question->save();

            //$this->redirect('/admin/question');
	    $this->actionRead( $id );
        }

        $h1 = 'Перегляд питання';
        $this->render('answer', array('o_question' => $o_question, 'h1' => $h1));
    }

    public function actionRead($id)
    {
        $id = (int) $id;
        QuestionModel::model()->updateByPk($id, array('question_read' => time(), 'question_manager' => Yii::app()->user->id));

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/question');
    }

    public function actionDelete($id)
    {
        $id = (int) $id;
        QuestionModel::model()->deleteByPk($id);

        Yii::app()->user->setFlash('success', $this->success_message);

        $this->redirect('/admin/question');
    }
   
}