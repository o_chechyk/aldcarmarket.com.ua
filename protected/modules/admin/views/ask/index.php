<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php print $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Авто</th>
                        <th>Email</th>
                        <th>Надійшло</th>
                        <th>Відреагували</th>
                        <th>Відповідь</th>
                        <th>Відповів</th>
                        <th class="col-lg-3"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($a_ask as $item) { ?>
                        <tr>
                            <td><?php if($item['car']) {print "<a href=\"/admin/car/edit/".$item['car']['car_id']."\" target=\"_blank\">".$item['car']['car_name']."</a>";} else {print "-";}?></td>
                            <td><?php print $item['ask_email']; ?></td>
                            <td><?php print date('H:i d.m.Y', $item['ask_date']); ?></td>
                            <td><?php if($item['ask_read']) print date('H:i d.m.Y', $item['ask_read']); ?></td>
			    <td><?php
                                        if($item['ask_reply']) {
                                                if( $pos=mb_strpos($item['ask_reply'], "повідомляємо:") ) {
                                                        echo mb_substr($item['ask_reply'], ($pos+13));
                                                } else {
                                                        echo $item['ask_reply'];
                                                }
                                        }
                                ?></td>
			    <td><?php if( $item->a_manager ) echo $item->a_manager->user_login; ?></td>
                            <td>
                                <a href="/admin/ask/read/<?php print $item['ask_id']; ?>">
                                    <button type="button" class="btn btn-<?php if (0 == $item['ask_read']) { ?>default<?php } else { ?>success<?php } ?>">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </a>
                                <a href="/admin/ask/answer/<?php print $item['ask_id']; ?>">
                                    <button type="button" class="btn btn-info">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="/admin/ask/delete/<?php print $item['ask_id']; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->