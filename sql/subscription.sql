drop table if exists subscription;
create table subscription (
    subscription_id int(11) primary key auto_increment,
    subscription_code varchar(64) unique not null,
    subscription_email varchar(255) unique not null,
    subscription_last_seen timestamp default NOW(),
    subscription_status tinyint(1) default 0
);